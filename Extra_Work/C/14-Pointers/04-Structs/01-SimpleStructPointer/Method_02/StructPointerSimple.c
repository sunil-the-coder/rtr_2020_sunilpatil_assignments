#include <stdio.h>

struct MyData
{
	int i;
	char ch;
};

int main(void)
{
	struct MyData *pMyData;

	pMyData = (struct MyData *)malloc(sizeof(struct MyData));

	if(pMyData == NULL)
	{
		printf("Memory Allocation failed. Exiting now.");
		exit(0);
	}

	pMyData->i = 10;
	pMyData->ch = 'A';

	printf("Sizeof(struct MyData) = %d\n", sizeof(struct MyData));
	printf("Sizeof(struct MyData *) = %d\n", sizeof(struct MyData *));
	printf("Sizeof(pMyData->i) = %d\n", sizeof(pMyData->i));
	printf("Sizeof(pMyData->ch) = %d\n", sizeof(pMyData->ch));

	if(pMyData)
	{
		free(pMyData);
		pMyData = NULL;
	}

	return (0);
}

/*

Sizeof(struct MyData) = 8
Sizeof(struct MyData *) = 4
Sizeof(pMyData->i) = 4
Sizeof(pMyData->ch) = 1

*/