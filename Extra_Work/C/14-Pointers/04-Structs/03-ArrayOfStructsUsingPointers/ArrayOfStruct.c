#include <stdio.h>

#define NAME_MAX_LENGTH 30
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_MAX_LENGTH];
	int age;
	float salary;
	char sex;
};

int main(void)
{
	struct Employee *pEmployee;
	int number;
	int i;

	printf("Enter How Many Employee to Store:");
	scanf("%d",&number);

	pEmployee = (struct Employee *)malloc(sizeof(struct Employee) * number);

	if(pEmployee == NULL)
	{
		printf("Memory Allocation failed for employees. Exiting now.\n");
		exit(0);
	}

	for(i = 0; i < number; i++)
	{
		printf("Enter details of employee:%d\n",(i+1));
		
		printf("Enter name:");
		scanf("%s",pEmployee[i].name);

		printf("Enter Age:");
		scanf("%d",&pEmployee[i].age);

		printf("Enter Sex:");
		pEmployee[i].sex = getch();
		printf("%c\n",pEmployee[i].sex);

		printf("Enter Salary:");
		scanf("%f",&pEmployee[i].salary);
	}

	printf("\n\n");
	printf("************** Details of all employees *****************\n\n");

	for(i = 0; i < number; i++)
	{
		printf("*******************************************\n\n");

		printf("Name: %s \n",pEmployee[i].name);
		printf("Age: %d \n",pEmployee[i].age);
		printf("Sex: %c \n",pEmployee[i].sex);
		printf("Salary: %f \n",pEmployee[i].salary);

		printf("*******************************************\n\n");
	}

	if(pEmployee)
	{
		free(pEmployee);
		pEmployee = NULL;
		printf("Memory freed.\n");
	}


	printf("\n\n");

	return (0);
}

/*

Enter How Many Employee to Store:2
Enter details of employee:1
Enter name:sunil
Enter Age:2
Enter Sex:M
Enter Salary:25000
Enter details of employee:2
Enter name:Ganesh
Enter Age:30
Enter Sex:M
Enter Salary:56000


************** Details of all employees *****************

*******************************************

Name: sunil
Age: 2
Sex: M
Salary: 25000.000000
*******************************************

*******************************************

Name: Ganesh
Age: 30
Sex: M
Salary: 56000.000000
*******************************************

Memory freed.



*/