#include <stdio.h>

struct MyData
{
	int i;
	int *pInt;

	float f;
	float *pFloat;

	double d;
	double *pDouble;
};

int main(void)
{
	struct MyData *pMyData;

	pMyData = (struct MyData *)malloc(sizeof(struct MyData));

	if(pMyData == NULL)
	{
		printf("Memory Allocation failed. Exiting now.");
		exit(0);
	}


	(*pMyData).i = 10;
	(*pMyData).pInt = &(*pMyData).i;

	(*pMyData).f = 10.5f;
	(*pMyData).pFloat = &(*pMyData).f;

	(*pMyData).d = 105252.5252;
	(*pMyData).pDouble = &(*pMyData).d;

	
	printf("Data=%d at address=%p\n",(*pMyData).i, (*pMyData).pInt);
	printf("Data=%f at address=%p\n",(*pMyData).f, (*pMyData).pFloat);
	printf("Data=%lf at address=%p\n",(*pMyData).d, (*pMyData).pDouble);

	printf("\n\n");

	return (0);
}

/*

Data=10 at address=006A4280
Data=10.500000 at address=006A4288
Data=105252.525200 at address=006A4290

*/