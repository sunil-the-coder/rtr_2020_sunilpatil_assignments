#include <stdio.h>

struct MyData
{
	int i;
	int *pInt;

	float f;
	float *pFloat;

	double d;
	double *pDouble;
};

int main(void)
{
	struct MyData myData;

	myData.i = 10;
	myData.pInt = &myData.i;

	myData.f = 10.5f;
	myData.pFloat = &myData.f;

	myData.d = 105252.5252;
	myData.pDouble = &myData.d;

	
	printf("Data=%d at address=%p\n",myData.i, myData.pInt);
	printf("Data=%f at address=%p\n",myData.f, myData.pFloat);
	printf("Data=%lf at address=%p\n",myData.d, myData.pDouble);

	printf("\n\n");

	return (0);
}

/*

Data=10 at address=00CFFC58
Data=10.500000 at address=00CFFC60
Data=105252.525200 at address=00CFFC68

*/