#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
};

//You can define any type with typedef
typedef struct MyData* MyDataPtr;

int main(void)
{
	//FUnction declaration
	void ChangeValues(MyDataPtr);

	MyDataPtr pMyData;

	pMyData = (MyDataPtr)malloc(sizeof(struct MyData));

	if(pMyData == NULL)
	{
		printf("Memory Allocation failed. Exiting now.");
		exit(0);
	}


	pMyData->i = 10;
	pMyData->f = 10.5f;
	pMyData->d = 105252.5252;
	
	printf("\n\n-------- Before Update -------------\n\n");
	printf("i=%d \n",pMyData->i);
	printf("f=%f \n",pMyData->f);
	printf("d=%lf \n",pMyData->d);

	ChangeValues(pMyData);

	printf("\n\n-------- After Update -------------\n\n");
	printf("i=%d \n",pMyData->i);
	printf("f=%f \n",pMyData->f);
	printf("d=%lf \n",pMyData->d);
	
	if(pMyData)
	{
		free(pMyData);
		pMyData = NULL;
		printf("\nMemory Freed.\n\n");
	}

	return (0);
}

void ChangeValues(MyDataPtr pMyData)
{
	(*pMyData).i = 20;
	(*pMyData).f = 20.4f;
	(*pMyData).d = 9045.324;
}

/*

-------- Before Update -------------

i=10
f=10.500000
d=105252.525200


-------- After Update -------------

i=20
f=20.400000
d=9045.324000

Memory Freed.


*/