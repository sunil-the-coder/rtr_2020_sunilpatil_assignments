#include <stdio.h>

struct Data
{
	int i;
	float f;
	double d;
	char ch;
};

int main(void)
{
	
	printf("\n\nData Type Sizes + Pointer Sizes: \n\n");
	
	//Size of pointer mostly depends on underlying CPU architecture + Compiler.
	//cl gives 4 bytes on 64 bit, gcc might give 8 bytes.
	
	printf("int=%d & int*=%d\n\n",sizeof(int), sizeof(int *));
	printf("char=%d & char*=%d\n\n",sizeof(char), sizeof(char *));
	printf("float=%d & float*=%d\n\n",sizeof(float), sizeof(float *));
	printf("double=%d & double*=%d\n\n",sizeof(double), sizeof(double *));
	printf("struct Data=%d & struct Data*=%d\n\n",sizeof(struct Data), sizeof(struct Data *));
	
	printf("\n\n");
		
	return (0);
}

/*


Data Type Sizes + Pointer Sizes:

int=4 & int*=4

char=1 & char*=4

float=4 & float*=4

double=8 & double*=4

struct Data=24 & struct Data*=4

*/