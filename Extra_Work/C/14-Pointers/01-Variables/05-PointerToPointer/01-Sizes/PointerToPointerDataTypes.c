#include <stdio.h>

struct Employee
{
	int i;
	float f;
	double d;
	char name[20];
	
};

int main(void)
{
	printf("\n\nInt:%d , %d , %d\n\n",sizeof(int), sizeof(int *), sizeof(int **));
	
	printf("\n\nFloat: %d , %d , %d\n\n",sizeof(float), sizeof(float *), sizeof(float **));
	
	printf("\n\nDouble: %d , %d , %d\n\n",sizeof(double), sizeof(double *), sizeof(double **));
	
	
	printf("\n\nStruct: %d , %d , %d\n\n",sizeof(struct Employee), sizeof(struct Employee *), sizeof(struct Employee **));
	
	return (0);
}


/*

So basically all pointer sizes are same irrespective of data types.

Int:4 , 4 , 4



Float: 4 , 4 , 4



Double: 8 , 4 , 4



Struct: 40 , 4 , 4
*/