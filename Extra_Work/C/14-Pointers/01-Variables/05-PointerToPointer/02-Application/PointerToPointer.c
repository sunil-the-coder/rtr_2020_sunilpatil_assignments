#include <stdio.h>


int main(void)
{
	int num;
	int *ptr;
	int **pptr;
	
	num = 10;
	ptr = &num;
	
	printf("Num = %d\n",num);
	printf("&Num = %p\n",&num);
	printf("*(&Num) = %d\n",*(&num));
	printf("ptr = %p\n",ptr);
	printf("*ptr = %d\n", *ptr);	
	
	pptr = &ptr;
	
	printf("pptr = %p\n",pptr);
	printf("*pptr = %p\n", *pptr);	
	printf("**pptr = %d\n", **pptr);	
	
	printf("Hello World programming");
	
	return (0);
}


/*

Num = 10
&Num = 004FFA7C
*(&Num) = 10
ptr = 004FFA7C
*ptr = 10
pptr = 004FFA78
*pptr = 004FFA7C
**pptr = 10

*/