#include <stdio.h>

int main(void)
{
	char number_SMP;
	
	char *ptr_SMP = NULL;  // ptr is pointer of type char
	
	number_SMP = 'S'; 
	
	printf("\n\n");
	
	printf("---------Before ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %c\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %c\n", *(&number_SMP));
	
	ptr_SMP = &number_SMP;
	
	printf("\n\n");
	
	printf("---------After ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %c\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %c\n", *(&number_SMP));
	
	printf("\n\n");
	
	return (0);
}

/*

---------Before ptr_SMP=&number_SMP --------

value of 'number_SMP' = S
Address of 'number_SMP' = 0113FD13
value at Address of 'number_SMP' = S


---------After ptr_SMP=&number_SMP --------

value of 'number_SMP' = S
Address of 'number_SMP' = 0113FD13
value at Address of 'number_SMP' = S

*/