#include <stdio.h>

int main(void)
{
	int number_SMP;
	
	int *ptr_SMP = NULL; // ptr is pointer of type int
	
	number_SMP = 10;
	
	printf("\n\n");
	
	printf("---------Before ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %d\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %d\n", *(&number_SMP));
	
	ptr_SMP = &number_SMP;
	
	printf("\n\n");
	
	printf("---------After ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %d\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %d\n", *(&number_SMP));
	
	printf("\n\n");
	
	return (0);
}

/*

---------Before ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10
Address of 'number_SMP' = 0053FE90
value at Address of 'number_SMP' = 10


---------After ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10
Address of 'number_SMP' = 0053FE90
value at Address of 'number_SMP' = 10

*/