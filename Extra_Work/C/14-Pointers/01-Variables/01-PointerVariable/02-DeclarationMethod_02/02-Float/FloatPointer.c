#include <stdio.h>

int main(void)
{
	float number_SMP;
	
	float* ptr_SMP = NULL; // ptr is variable of 'float*'
	
	number_SMP = 10.5f;
	
	printf("\n\n");
	
	printf("---------Before ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %f\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %f\n", *(&number_SMP));
	
	ptr_SMP = &number_SMP;
	
	printf("\n\n");
	
	printf("---------After ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %f\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %f\n", *(&number_SMP));
	
	printf("\n\n");
	
	return (0);
}

/*

---------Before ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10.500000
Address of 'number_SMP' = 00F3F8B8
value at Address of 'number_SMP' = 10.500000


---------After ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10.500000
Address of 'number_SMP' = 00F3F8B8
value at Address of 'number_SMP' = 10.500000

*/