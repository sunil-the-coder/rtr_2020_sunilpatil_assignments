#include <stdio.h>

int main(void)
{
	double number_SMP;
	
	double* ptr_SMP = NULL;  // ptr is variable of type 'double*'
	
	number_SMP = 10535.53737;
	
	printf("\n\n");
	
	printf("---------Before ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %lf\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %lf\n", *(&number_SMP));
	
	ptr_SMP = &number_SMP;
	
	printf("\n\n");
	
	printf("---------After ptr_SMP=&number_SMP --------\n\n");
	printf("value of 'number_SMP' = %lf\n", number_SMP);
	printf("Address of 'number_SMP' = %p\n", &number_SMP);
	printf("value at Address of 'number_SMP' = %lf\n", *(&number_SMP));
	
	printf("\n\n");
	
	return (0);
}

/*

---------Before ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10535.537370
Address of 'number_SMP' = 00F7F984
value at Address of 'number_SMP' = 10535.537370


---------After ptr_SMP=&number_SMP --------

value of 'number_SMP' = 10535.537370
Address of 'number_SMP' = 00F7F984
value at Address of 'number_SMP' = 10535.537370

*/