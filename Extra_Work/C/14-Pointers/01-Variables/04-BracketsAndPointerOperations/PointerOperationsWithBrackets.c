#include <stdio.h>


int main(void)
{
	int number_SMP;
	int *ptr = NULL;
	int result;
	
	number_SMP = 5;
	ptr = &number_SMP;
		
	printf("\n\n");
	
	printf("number_SMP = %d\n", number_SMP);
	printf("&number_SMP = %p\n", &number_SMP);	
	printf("*(&number_SMP) = %d\n", *(&number_SMP));
	printf("ptr = %p\n", ptr);
	printf("*(ptr) = %d\n", *ptr);
		
	printf("\n\n");

	printf("(ptr+10) = %p\n",(ptr+10)); // move pointer by next 10 positions ( ptr + 10 * sizeof(datatype) )
	
	printf("*(ptr+10) = %d\n",*(ptr + 10)); // Might print garbage as we don't any data at 10th index location.
	
	printf("(*ptr+10) = %d\n",(*ptr + 10)); // Add value to actual value.
	
	
	//Associatitvity ( right to left)
	++*ptr; // Increment actual by 1. *ptr = *ptr + 1;	
	printf("(++*ptr) = %d\n",*ptr); // Update value print. Here 6.
	
	*ptr++; // Incorrect way to incrementing value. Here ptr will incrementn first and they it will try to get the value at from new address.
	printf("(*ptr++) = %d\n",*ptr); 
	
	ptr = &number_SMP; //Re initialize same pointer with address of any variable.
	
	(*ptr)++; //Correct way to increment actual value using postfix++ operator.
	
	printf("(*ptr)++ = %d\n",*ptr); //Value will be 7 here as incremented by ++*ptr
	
	printf("\n\n");
	
	return (0);
}

/*

number_SMP = 5
&number_SMP = 008FF97C
*(&number_SMP) = 5
ptr = 008FF97C
*(ptr) = 5


(ptr+10) = 008FF9A4
*(ptr+10) = 8110080
(*ptr+10) = 15
(++*ptr) = 6
(*ptr++) = 9435520
(*ptr)++ = 7

*/