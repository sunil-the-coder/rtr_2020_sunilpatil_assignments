#include <stdio.h>


int main(void)
{
	int number_SMP;
	int *ptr = NULL;
	int *copy_ptr = NULL;
	
	number_SMP = 10;

	ptr = &number_SMP;
		
	printf("\n\n");
	
	printf("---------Before copy_ptr=ptr --------\n\n");
	printf("number_SMP = %d\n", number_SMP);
	printf("&number_SMP = %p\n", &number_SMP);	
	printf("*(&number_SMP) = %d\n", *(&number_SMP));
	printf("ptr = %p\n", ptr);
	printf("*(ptr) = %d\n", *ptr);
	
	
	copy_ptr = ptr; //Both ptr will start pointing to same variable number.
	
	
		
	printf("\n---------After copy_ptr=ptr --------\n\n");
	printf("number_SMP = %d\n", number_SMP);
	printf("&number_SMP = %p\n", &number_SMP);	
	printf("*(&number_SMP) = %d\n", *(&number_SMP));
	printf("ptr = %p\n", ptr);
	printf("*(ptr) = %d\n", *ptr);
	
	
		
	return (0);
}

/*

---------Before copy_ptr=ptr --------

number_SMP = 10
&number_SMP = 00AFFD38
*(&number_SMP) = 10
ptr = 00AFFD38
*(ptr) = 10

---------After copy_ptr=ptr --------

number_SMP = 10
&number_SMP = 00AFFD38
*(&number_SMP) = 10
ptr = 00AFFD38
*(ptr) = 10

*/