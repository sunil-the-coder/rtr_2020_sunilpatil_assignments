#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	float fArray[NUMBER_OF_ELEMENTS];// = {10.5f, 5.5f, 6.1f, 7.89f, 8.45f};	
	int i;
	
	printf("\n\n");
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		fArray[i] = (float) (i + 1) * 1.5f;
	}
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("fArray[%d] = %f\n",i, fArray[i]);		
	}
	
	printf("\n\n");
	
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("fArray[%d] = %f \t at Address:%p\n",i, fArray[i],&fArray[i]);		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


Elements of an array:

fArray[0] = 1.500000
fArray[1] = 3.000000
fArray[2] = 4.500000
fArray[3] = 6.000000
fArray[4] = 7.500000


Elements of an array with address:

fArray[0] = 1.500000     at Address:00ABF998
fArray[1] = 3.000000     at Address:00ABF99C
fArray[2] = 4.500000     at Address:00ABF9A0
fArray[3] = 6.000000     at Address:00ABF9A4
fArray[4] = 7.500000     at Address:00ABF9A8

*/