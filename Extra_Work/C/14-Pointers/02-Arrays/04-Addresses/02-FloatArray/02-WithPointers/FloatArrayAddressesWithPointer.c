#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	float fArray[] = {10.45f, 5.5f, 6.6f, 7.4f, 8.67f};	
	float *ptr = NULL;
	int i;
	
	printf("\n\n");
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("fArray[%d] = %f\n",i, fArray[i]);		
	}
	
	printf("\n\n");
	
	ptr = fArray;
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("fArray[%d] = %f \t at Address = %p\n", i, *(ptr + i), (ptr + i));		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


Elements of an array:

fArray[0] = 10.450000
fArray[1] = 5.500000
fArray[2] = 6.600000
fArray[3] = 7.400000
fArray[4] = 8.670000


Elements of an array with address:

fArray[0] = 10.450000    at Address = 00FDFCF8
fArray[1] = 5.500000     at Address = 00FDFCFC
fArray[2] = 6.600000     at Address = 00FDFD00
fArray[3] = 7.400000     at Address = 00FDFD04
fArray[4] = 8.670000     at Address = 00FDFD08

*/