#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	int iArray[] = {10, 5, 6, 7, 8};	
	int i;
	
	printf("\n\n");
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("iArray[%d] = %d\n",i, iArray[i]);		
	}
	
	printf("\n\n");
	
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("iArray[%d] = %d \t at Address:%p\n",i, iArray[i],&iArray[i]);		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


Elements of an array:

iArray[0] = 10
iArray[1] = 5
iArray[2] = 6
iArray[3] = 7
iArray[4] = 8


Elements of an array with address:

iArray[0] = 10   at Address:0118F898
iArray[1] = 5    at Address:0118F89C
iArray[2] = 6    at Address:0118F8A0
iArray[3] = 7    at Address:0118F8A4
iArray[4] = 8    at Address:0118F8A8

*/