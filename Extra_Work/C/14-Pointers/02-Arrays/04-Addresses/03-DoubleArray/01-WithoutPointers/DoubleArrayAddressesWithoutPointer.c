#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	double dArray[NUMBER_OF_ELEMENTS];// = {10.5f, 5.5f, 6.1f, 7.89f, 8.45f};	
	int i;
	
	printf("\n\n");
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		dArray[i] = (double) (i + 1) * 1.5f;
	}
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("dArray[%d] = %lf\n",i, dArray[i]);		
	}
	
	printf("\n\n");
	
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("dArray[%d] = %lf \t at Address=%p\n",i, dArray[i],&dArray[i]);		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


Elements of an array:

dArray[0] = 1.500000
dArray[1] = 3.000000
dArray[2] = 4.500000
dArray[3] = 6.000000
dArray[4] = 7.500000


Elements of an array with address:

dArray[0] = 1.500000     at Address=0056FC94
dArray[1] = 3.000000     at Address=0056FC9C
dArray[2] = 4.500000     at Address=0056FCA4
dArray[3] = 6.000000     at Address=0056FCAC
dArray[4] = 7.500000     at Address=0056FCB4

*/