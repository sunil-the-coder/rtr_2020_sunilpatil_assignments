#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	double dArray[] = {10.4522, 51.5, 614.65, 742.45, 8124.671};	
	double *ptr = NULL;
	int i;
	
	printf("\n\n");
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("dArray[%d] = %lf\n",i, dArray[i]);		
	}
	
	printf("\n\n");
	
	ptr = dArray;
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("dArray[%d] = %lf \t at Address = %p\n", i, *(ptr + i), (ptr + i));		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


Elements of an array:

dArray[0] = 10.452200
dArray[1] = 51.500000
dArray[2] = 614.650000
dArray[3] = 742.450000
dArray[4] = 8124.671000


Elements of an array with address:

dArray[0] = 10.452200    at Address = 008FFBD0
dArray[1] = 51.500000    at Address = 008FFBD8
dArray[2] = 614.650000   at Address = 008FFBE0
dArray[3] = 742.450000   at Address = 008FFBE8
dArray[4] = 8124.671000          at Address = 008FFBF0

*/