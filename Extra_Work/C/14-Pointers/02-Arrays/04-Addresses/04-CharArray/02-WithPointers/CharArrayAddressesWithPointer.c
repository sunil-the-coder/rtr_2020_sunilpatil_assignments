#include <stdio.h>

#define NUMBER_OF_ELEMENTS 10

int main(void)
{
	char cArray[NUMBER_OF_ELEMENTS];
	char *ptr = NULL;
	
	int i;
	
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		cArray[i] = (char) (i + 65);
	}
	
	printf("\n\n");
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("cArray[%d] = %d\n",i, cArray[i]);		
	}
	
	printf("\n\n");
	
	ptr = cArray;
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("cArray[%d] = %c \t at Address = %p\n", i, *(ptr + i), (ptr + i));		
	}
	
	printf("\n\n");
		
	return (0);
}

/*

Elements of an array:

cArray[0] = 65
cArray[1] = 66
cArray[2] = 67
cArray[3] = 68
cArray[4] = 69
cArray[5] = 70
cArray[6] = 71
cArray[7] = 72
cArray[8] = 73
cArray[9] = 74


Elements of an array with address:

cArray[0] = A    at Address = 005CFB78
cArray[1] = B    at Address = 005CFB79
cArray[2] = C    at Address = 005CFB7A
cArray[3] = D    at Address = 005CFB7B
cArray[4] = E    at Address = 005CFB7C
cArray[5] = F    at Address = 005CFB7D
cArray[6] = G    at Address = 005CFB7E
cArray[7] = H    at Address = 005CFB7F
cArray[8] = I    at Address = 005CFB80
cArray[9] = J    at Address = 005CFB81

*/