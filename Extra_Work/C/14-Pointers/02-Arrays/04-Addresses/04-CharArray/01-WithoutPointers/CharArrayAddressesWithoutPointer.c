#include <stdio.h>

#define NUMBER_OF_ELEMENTS 10

int main(void)
{
	char cArray[NUMBER_OF_ELEMENTS];
	int i;
	
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		cArray[i] = (char) (i + 65);
	}
	
	printf("\n\n");
	
	printf("Elements of an array:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("cArray[%d] = %c\n",i, cArray[i]);		
	}
	
	printf("\n\n");
	
	printf("Elements of an array with address:\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("cArray[%d] = %c \t at Address = %p\n",i, cArray[i],&cArray[i]);		
	}
	
	printf("\n\n");
		
	return (0);
}

/*

Elements of an array:

cArray[0] = A
cArray[1] = B
cArray[2] = C
cArray[3] = D
cArray[4] = E
cArray[5] = F
cArray[6] = G
cArray[7] = H
cArray[8] = I
cArray[9] = J


Elements of an array with address:

cArray[0] = A    at Address = 00EFFC3C
cArray[1] = B    at Address = 00EFFC3D
cArray[2] = C    at Address = 00EFFC3E
cArray[3] = D    at Address = 00EFFC3F
cArray[4] = E    at Address = 00EFFC40
cArray[5] = F    at Address = 00EFFC41
cArray[6] = G    at Address = 00EFFC42
cArray[7] = H    at Address = 00EFFC43
cArray[8] = I    at Address = 00EFFC44
cArray[9] = J    at Address = 00EFFC45


*/