#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	int iArray[] = {10, 5, 6, 7, 8};
	float fArray[] = {10.5f, 5.6f, 34.6f, 8.5f, 9.2f};
	double dArray[] = {101.55, 55.61, 354.667, 128.56, 56439.23456};
	
	char chArray[] = {'A', 'B', 'C', 'D', 'E'};
	int i;
	
	printf("\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("iArray - Value=[%d] and address=[%p]\n",*(iArray+i),(iArray+i));		
	}
	
	printf("\n\n");
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("fArray - Value=[%f] and address=[%p]\n",*(fArray+i),(fArray+i));		
	}
	
	printf("\n\n");
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("dArray - Value=[%lf] and address=[%p]\n",*(dArray+i),(dArray+i));		
	}
	
	printf("\n\n");
	
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("chArray - Value=[%d] and address=[%p]\n",*(chArray+i),(chArray+i));		
	}
		

	printf("\n\n");
	
	return (0);
}

/*

iArray - Value=[10] and address=[012FFD40]
iArray - Value=[5] and address=[012FFD44]
iArray - Value=[6] and address=[012FFD48]
iArray - Value=[7] and address=[012FFD4C]
iArray - Value=[8] and address=[012FFD50]


fArray - Value=[10.500000] and address=[012FFD2C]
fArray - Value=[5.600000] and address=[012FFD30]
fArray - Value=[34.599998] and address=[012FFD34]
fArray - Value=[8.500000] and address=[012FFD38]
fArray - Value=[9.200000] and address=[012FFD3C]


dArray - Value=[101.550000] and address=[012FFD04]
dArray - Value=[55.610000] and address=[012FFD0C]
dArray - Value=[354.667000] and address=[012FFD14]
dArray - Value=[128.560000] and address=[012FFD1C]
dArray - Value=[56439.234560] and address=[012FFD24]


chArray - Value=[65] and address=[012FFD54]
chArray - Value=[66] and address=[012FFD55]
chArray - Value=[67] and address=[012FFD56]
chArray - Value=[68] and address=[012FFD57]

*/