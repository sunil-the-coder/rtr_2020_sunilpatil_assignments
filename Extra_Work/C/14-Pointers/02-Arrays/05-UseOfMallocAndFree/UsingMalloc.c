#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int *ptr = NULL;
	unsigned int iLength;
	int i;
	
	printf("\n\n");
	printf("Enter number of elements to store in an array:");
	scanf("%d",&iLength);
	
	//Allocate dynamic memory.
	
	ptr = (int *)malloc(sizeof(int) * iLength);
	
	if(ptr == NULL)
	{
		printf("Could not allocate the memory for integer array. Exiting..\n");
		exit(0);
	}else
	{
		printf("Allocated memory: %d bytes from %p to %p",
		(sizeof(int) * iLength ), ptr, (ptr + (iLength - 1)));
	}
	
	printf("\n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("Enter data:");
		scanf("%d",ptr + i); //Nice one.		
	}
	
	printf("\n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("ptr[%d] = %d \t at address &ptr[%d] : %p\n", 
		i, ptr[i], i, &ptr[i]);		
	}
	
	printf("\n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("*(ptr + %d) = %d \t at address (ptr + %d) : %p\n", 
		i, *(ptr + i), i, ptr + i);		
	}


		//cleanup the moery if its still callocated.
		
	if( ptr )
	{
		free(ptr);
		ptr = NULL;
		
		printf("\n Memory is freed.\n\n");
	}		
	return (0);
}

/*


Enter number of elements to store in an array:5
Allocated memory: 20 bytes from 01445558 to 01445568

Enter data:10
Enter data:20
Enter data:30
Enter data:40
Enter data:50


ptr[0] = 10      at address &ptr[0] : 01445558
ptr[1] = 20      at address &ptr[1] : 0144555C
ptr[2] = 30      at address &ptr[2] : 01445560
ptr[3] = 40      at address &ptr[3] : 01445564
ptr[4] = 50      at address &ptr[4] : 01445568


*(ptr + 0) = 10          at address (ptr + 0) : 01445558
*(ptr + 1) = 20          at address (ptr + 1) : 0144555C
*(ptr + 2) = 30          at address (ptr + 2) : 01445560
*(ptr + 3) = 40          at address (ptr + 3) : 01445564
*(ptr + 4) = 50          at address (ptr + 4) : 01445568

Memory is freed.

*/