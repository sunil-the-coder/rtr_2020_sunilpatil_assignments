#include <stdio.h>


#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	int iArray[NUM_ROWS][NUM_COLUMNS];
	int i,j;
	int *ptr = NULL;
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		ptr = iArray[i]; // Every 1D array in 2D array is itself array of integers with its columns.
		
		//So above points to each of those base address of 1D array.
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			*(ptr + j) = (i * 1 ) + (j * 1);
		}
	}
	
	// Here iArray[i] can be treated as 1D array using pointers.
	
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		ptr = iArray[i];
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			printf("*(ptr + %d) = %d \t at address: %p\n",
				j,*(ptr + j),(ptr + j));			
		}
		
		printf("\n\n");
	}
	
	return (0);
}


/*

 

 Array Values with addresss :

iArray[0][0] = 0         at address: 00EFF9B4
iArray[0][1] = 1         at address: 00EFF9B8
iArray[0][2] = 2         at address: 00EFF9BC


iArray[1][0] = 1         at address: 00EFF9C0
iArray[1][1] = 2         at address: 00EFF9C4
iArray[1][2] = 3         at address: 00EFF9C8


iArray[2][0] = 2         at address: 00EFF9CC
iArray[2][1] = 3         at address: 00EFF9D0
iArray[2][2] = 4         at address: 00EFF9D4


iArray[3][0] = 3         at address: 00EFF9D8
iArray[3][1] = 4         at address: 00EFF9DC
iArray[3][2] = 5         at address: 00EFF9E0


iArray[4][0] = 4         at address: 00EFF9E4
iArray[4][1] = 5         at address: 00EFF9E8
iArray[4][2] = 6         at address: 00EFF9EC

*/