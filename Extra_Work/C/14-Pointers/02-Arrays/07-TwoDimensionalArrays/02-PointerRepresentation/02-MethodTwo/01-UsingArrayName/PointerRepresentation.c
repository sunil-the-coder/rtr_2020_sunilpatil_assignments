#include <stdio.h>


#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	int iArray[NUM_ROWS][NUM_COLUMNS];
	int i,j;	
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		//So above points to each of those base address of 1D array.
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			//a[i] => *(a + i)
			//a[i][j] = *( *(a + i) + j)
			 *( *(iArray + i) + j) = (i * 1 ) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{		
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			printf("*( *(iArray + %d) + %d) = %d \t at address: %p\n",
				i,j,*(*(iArray + i) + j),(*(iArray + i) + j));			
		}
		
		printf("\n\n");
	}
	
	return (0);
}


/*

 
 Array Values with addresss :

*( *(iArray + 0) + 0) = 0        at address: 009CF918
*( *(iArray + 0) + 1) = 1        at address: 009CF91C
*( *(iArray + 0) + 2) = 2        at address: 009CF920


*( *(iArray + 1) + 0) = 1        at address: 009CF924
*( *(iArray + 1) + 1) = 2        at address: 009CF928
*( *(iArray + 1) + 2) = 3        at address: 009CF92C


*( *(iArray + 2) + 0) = 2        at address: 009CF930
*( *(iArray + 2) + 1) = 3        at address: 009CF934
*( *(iArray + 2) + 2) = 4        at address: 009CF938


*( *(iArray + 3) + 0) = 3        at address: 009CF93C
*( *(iArray + 3) + 1) = 4        at address: 009CF940
*( *(iArray + 3) + 2) = 5        at address: 009CF944


*( *(iArray + 4) + 0) = 4        at address: 009CF948
*( *(iArray + 4) + 1) = 5        at address: 009CF94C
*( *(iArray + 4) + 2) = 6        at address: 009CF950


*/