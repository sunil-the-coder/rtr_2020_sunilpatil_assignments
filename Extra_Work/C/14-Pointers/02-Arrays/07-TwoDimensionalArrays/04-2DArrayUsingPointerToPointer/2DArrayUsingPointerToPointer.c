#include <stdio.h>
#include <stdlib.h>


int main(void)
{
	int **ptr = NULL;
	int i, j;	
	int num_rows, num_columns;


	printf("\n\nEnter Number of Rows:");
	scanf("%d",&num_rows);
	
	
	printf("\n\nEnter Number of Columns:");
	scanf("%d",&num_columns);
	
	
	//Allocate memory for 2D array first which has pointers to 1D arrays of type int *.
	ptr = (int **) malloc( num_rows * sizeof(int *));
	
	
	if(ptr == NULL)
	{
		printf("Memory Allocation failed for 2D array. Exiting now.\n");
		exit(0);
	}else
	{
		printf("Memory allocation for 1D array of base addresses of %d rows has succeeded.\n",num_rows);
	}
	

	//Now allocate memory for each individual 1D in 2D array.
	
	for( i = 0; i < num_rows; i++)
	{
		ptr[i] = (int *) malloc( num_columns * sizeof(int));
		
		if(ptr[i] == NULL)
		{
			printf("Memory allocation for the columns of row %d has failed. Exiting now. !!!",i);
			exit(0);
		}else
		{
			printf("Memory allocation to the columns of row %d has Succeeded.\n\n",i);
		}
	}
	
	
	// Now you can assign values & print it.
	for(i = 0; i < num_rows; i++)
	{
		//So above points to each of those base address of 1D array.
		for( j = 0; j < num_columns; j++)
		{			
			 *( *(ptr + i) + j) = (i * 1 ) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < num_rows; i++)
	{		
		for( j = 0; j < num_columns; j++)
		{			
			printf("*( *(ptr + %d) + %d) = %d \t at address: %p\n",
				i,j,*(*(ptr + i) + j),(*(ptr + i) + j)); //ptr[i][j], &ptr[i][j]
		}
		
		printf("\n\n");
	}
	
	
	
	// Free the Allocated Memory Row by Row.
	//Always try to clean up in reverse order to avoid any runtime issues.
	
	for(i = (num_rows - 1); i >= 0; i--)
	{
		if( ptr[i] ) //if( *(ptr + i) )
		{
			free(*(ptr + i)); // free(ptr[i])
			*(ptr + i) = NULL; // ptr[i] = NULL; //clean up the any garbage value.
			
			printf("Memory allocated for row %d has been freed. !!\n",i);
		}
	}
	
	//Clean up the main array which has the addresses of 1D array.
	if( ptr )
	{
		free(ptr);
		ptr = NULL;
		
		printf("Memory allocated for array has been successfully freed.\n\n");
	}
	
	return (0);
}


/*

Enter Number of Rows:6

Enter Number of Columns:4

Memory allocation for 1D array of base addresses of 6 rows has succeeded.
Memory allocation to the columns of row 0 has Succeeded.

Memory allocation to the columns of row 1 has Succeeded.

Memory allocation to the columns of row 2 has Succeeded.

Memory allocation to the columns of row 3 has Succeeded.

Memory allocation to the columns of row 4 has Succeeded.

Memory allocation to the columns of row 5 has Succeeded.



 Array Values with addresss :

*( *(ptr + 0) + 0) = 0   at address: 006B5078
*( *(ptr + 0) + 1) = 1   at address: 006B507C
*( *(ptr + 0) + 2) = 2   at address: 006B5080
*( *(ptr + 0) + 3) = 3   at address: 006B5084


*( *(ptr + 1) + 0) = 1   at address: 006B5090
*( *(ptr + 1) + 1) = 2   at address: 006B5094
*( *(ptr + 1) + 2) = 3   at address: 006B5098
*( *(ptr + 1) + 3) = 4   at address: 006B509C


*( *(ptr + 2) + 0) = 2   at address: 006B50A8
*( *(ptr + 2) + 1) = 3   at address: 006B50AC
*( *(ptr + 2) + 2) = 4   at address: 006B50B0
*( *(ptr + 2) + 3) = 5   at address: 006B50B4


*( *(ptr + 3) + 0) = 3   at address: 006B4EC8
*( *(ptr + 3) + 1) = 4   at address: 006B4ECC
*( *(ptr + 3) + 2) = 5   at address: 006B4ED0
*( *(ptr + 3) + 3) = 6   at address: 006B4ED4


*( *(ptr + 4) + 0) = 4   at address: 006B50C0
*( *(ptr + 4) + 1) = 5   at address: 006B50C4
*( *(ptr + 4) + 2) = 6   at address: 006B50C8
*( *(ptr + 4) + 3) = 7   at address: 006B50CC


*( *(ptr + 5) + 0) = 5   at address: 006B5150
*( *(ptr + 5) + 1) = 6   at address: 006B5154
*( *(ptr + 5) + 2) = 7   at address: 006B5158
*( *(ptr + 5) + 3) = 8   at address: 006B515C


Memory allocated for row 5 has been freed. !!
Memory allocated for row 4 has been freed. !!
Memory allocated for row 3 has been freed. !!
Memory allocated for row 2 has been freed. !!
Memory allocated for row 1 has been freed. !!
Memory allocated for row 0 has been freed. !!
Memory allocated for array has been successfully freed.


*/