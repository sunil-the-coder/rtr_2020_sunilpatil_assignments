#include <stdio.h>
#include <stdlib.h>


#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	int *iArray[NUM_ROWS]; // Columns will be decided later.
	int i,j;	
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		//Dynamic columns based on user requirements.
		iArray[i] = (int *) malloc ( NUM_COLUMNS * sizeof(int));
		
		if(iArray[i] == NULL)
		{
			printf("Memory allocation failed for row *d of an 2D int array. Exiting now. !!!",i);
			exit(0);
		}else
		{
				printf("Memory allocation is succedded for row %d\n",i);
		}
	}
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			 iArray[i][j] = (i * 1 ) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{		
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			printf("iArray[%d][%d]= %d \t at address: %p\n",
				i,j,iArray[i][j],(*(iArray + i) + j));			
		}
		
		printf("\n\n");
	}
	
	
	// Free the Allocated Memory Row by Row.
	//Always try to clean up in reverse order to avoid any runtime issues.
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		if(iArray[i]) //if(ptr[i])
		{
			free(iArray[i]);
			iArray[i] = NULL;
			
			printf("Memory allocated for row %d has been freed. !!\n",i);
		}
	}
	
	return (0);
}


/*
Memory allocation is succedded for row 0
Memory allocation is succedded for row 1
Memory allocation is succedded for row 2
Memory allocation is succedded for row 3
Memory allocation is succedded for row 4


 Array Values with addresss :

iArray[0][0]= 0          at address: 010B5328
iArray[0][1]= 1          at address: 010B532C
iArray[0][2]= 2          at address: 010B5330


iArray[1][0]= 1          at address: 010B52C8
iArray[1][1]= 2          at address: 010B52CC
iArray[1][2]= 3          at address: 010B52D0


iArray[2][0]= 2          at address: 010B5208
iArray[2][1]= 3          at address: 010B520C
iArray[2][2]= 4          at address: 010B5210


iArray[3][0]= 3          at address: 010B5250
iArray[3][1]= 4          at address: 010B5254
iArray[3][2]= 5          at address: 010B5258


iArray[4][0]= 4          at address: 010B5220
iArray[4][1]= 5          at address: 010B5224
iArray[4][2]= 6          at address: 010B5228


Memory allocated for row 4 has been freed. !!
Memory allocated for row 3 has been freed. !!
Memory allocated for row 2 has been freed. !!
Memory allocated for row 1 has been freed. !!
Memory allocated for row 0 has been freed. !!


*/