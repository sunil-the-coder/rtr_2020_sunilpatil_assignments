#include <stdio.h>
#include <stdlib.h>


#define NUM_ROWS 5
#define NUM_COLUMNS_ONE 3
#define NUM_COLUMNS_TWO 8

int main(void)
{
	int *iArray[NUM_ROWS]; // Columns will be decided later.
	int i,j;	
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		//Dynamic columns based on user requirements.
		iArray[i] = (int *) malloc ( NUM_COLUMNS_ONE * sizeof(int));
		
		if(iArray[i] == NULL)
		{
			printf("Memory allocation failed for row *d of an 2D int array. Exiting now. !!!",i);
			exit(0);
		}else
		{
				printf("Memory allocation is succedded for row %d\n",i);
		}
	}
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < NUM_COLUMNS_ONE; j++)
		{			
			 iArray[i][j] = (i * 1 ) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{		
		for( j = 0; j < NUM_COLUMNS_ONE; j++)
		{			
			printf("iArray[%d][%d]= %d \t at address: %p\n",
				i,j,iArray[i][j],(*(iArray + i) + j));			
		}
		
		printf("\n\n");
	}
	
	
	// Free the Allocated Memory Row by Row.
	//Always try to clean up in reverse order to avoid any runtime issues.
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		if(iArray[i]) //if(ptr[i])
		{
			free(iArray[i]);
			iArray[i] = NULL;
			
			printf("Memory allocated for row %d has been freed. !!\n",i);
		}
	}
	
	//Allocating memory for an array of 8 integers.
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		//Dynamic columns based on user requirements.
		iArray[i] = (int *) malloc ( NUM_COLUMNS_TWO * sizeof(int));
		
		if(iArray[i] == NULL)
		{
			printf("Memory allocation failed for row %d of an 2D int array. Exiting now. !!!",i);
			exit(0);
		}else
		{
				printf("Memory allocation is succedded for row %d\n",i);
		}
	}
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < NUM_COLUMNS_TWO; j++)
		{			
			 iArray[i][j] = (i * 1 ) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{		
		for( j = 0; j < NUM_COLUMNS_TWO; j++)
		{			
			printf("iArray[%d][%d]= %d \t at address: %p\n",
				i,j,iArray[i][j],(*(iArray + i) + j));			
		}
		
		printf("\n\n");
	}
	
	
	// Free the Allocated Memory Row by Row.
	//Always try to clean up in reverse order to avoid any runtime issues.
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		if(iArray[i]) //if(ptr[i])
		{
			free(iArray[i]);
			iArray[i] = NULL;
			
			printf("Memory allocated for row %d has been freed. !!\n",i);
		}
	}
	
	return (0);
}


/*
Memory allocation is succedded for row 0
Memory allocation is succedded for row 1
Memory allocation is succedded for row 2
Memory allocation is succedded for row 3
Memory allocation is succedded for row 4


 Array Values with addresss :

iArray[0][0]= 0          at address: 00745278
iArray[0][1]= 1          at address: 0074527C
iArray[0][2]= 2          at address: 00745280


iArray[1][0]= 1          at address: 00745398
iArray[1][1]= 2          at address: 0074539C
iArray[1][2]= 3          at address: 007453A0


iArray[2][0]= 2          at address: 007452A8
iArray[2][1]= 3          at address: 007452AC
iArray[2][2]= 4          at address: 007452B0


iArray[3][0]= 3          at address: 007454A0
iArray[3][1]= 4          at address: 007454A4
iArray[3][2]= 5          at address: 007454A8


iArray[4][0]= 4          at address: 007452C0
iArray[4][1]= 5          at address: 007452C4
iArray[4][2]= 6          at address: 007452C8


Memory allocated for row 4 has been freed. !!
Memory allocated for row 3 has been freed. !!
Memory allocated for row 2 has been freed. !!
Memory allocated for row 1 has been freed. !!
Memory allocated for row 0 has been freed. !!

Memory allocation is succedded for row 0
Memory allocation is succedded for row 1
Memory allocation is succedded for row 2
Memory allocation is succedded for row 3
Memory allocation is succedded for row 4


 Array Values with addresss :

iArray[0][0]= 0          at address: 00753660
iArray[0][1]= 1          at address: 00753664
iArray[0][2]= 2          at address: 00753668
iArray[0][3]= 3          at address: 0075366C
iArray[0][4]= 4          at address: 00753670
iArray[0][5]= 5          at address: 00753674
iArray[0][6]= 6          at address: 00753678
iArray[0][7]= 7          at address: 0075367C


iArray[1][0]= 1          at address: 00753570
iArray[1][1]= 2          at address: 00753574
iArray[1][2]= 3          at address: 00753578
iArray[1][3]= 4          at address: 0075357C
iArray[1][4]= 5          at address: 00753580
iArray[1][5]= 6          at address: 00753584
iArray[1][6]= 7          at address: 00753588
iArray[1][7]= 8          at address: 0075358C


iArray[2][0]= 2          at address: 00753520
iArray[2][1]= 3          at address: 00753524
iArray[2][2]= 4          at address: 00753528
iArray[2][3]= 5          at address: 0075352C
iArray[2][4]= 6          at address: 00753530
iArray[2][5]= 7          at address: 00753534
iArray[2][6]= 8          at address: 00753538
iArray[2][7]= 9          at address: 0075353C


iArray[3][0]= 3          at address: 007536B0
iArray[3][1]= 4          at address: 007536B4
iArray[3][2]= 5          at address: 007536B8
iArray[3][3]= 6          at address: 007536BC
iArray[3][4]= 7          at address: 007536C0
iArray[3][5]= 8          at address: 007536C4
iArray[3][6]= 9          at address: 007536C8
iArray[3][7]= 10         at address: 007536CC


iArray[4][0]= 4          at address: 007535C0
iArray[4][1]= 5          at address: 007535C4
iArray[4][2]= 6          at address: 007535C8
iArray[4][3]= 7          at address: 007535CC
iArray[4][4]= 8          at address: 007535D0
iArray[4][5]= 9          at address: 007535D4
iArray[4][6]= 10         at address: 007535D8
iArray[4][7]= 11         at address: 007535DC


Memory allocated for row 4 has been freed. !!
Memory allocated for row 3 has been freed. !!
Memory allocated for row 2 has been freed. !!
Memory allocated for row 1 has been freed. !!
Memory allocated for row 0 has been freed. !!

*/