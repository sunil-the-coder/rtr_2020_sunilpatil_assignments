#include <stdio.h>
#include <stdlib.h>


#define NUM_ROWS 5
#define NUM_COLUMNS 5

int main(void)
{
	int *iArray[NUM_ROWS]; // Columns will be decided later.
	int i,j;	
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		//Dynamic columns based on user requirements.
		// Row 0 - Will have 5 columns
		//Row 1 will have 4 columns
		//Row 2 will have 3 columns
		//Row 3 will have 2 columns
		//Row 4 will have 1 column.
		
		// Because of this there won't be contigious memory allocation. Hense we may use it as 2D array but
		// in its not really a 2D array in memory.
		
		iArray[i] = (int *) malloc( (NUM_COLUMNS - i) * sizeof(int));
		
		if(iArray[i] == NULL)
		{
			printf("Memory allocation failed for row %d of an 2D int array. Exiting now. !!!",i);
			exit(0);
		}else
		{
				printf("Memory allocation is succedded for row %d with columns %d\n",i,(NUM_COLUMNS - i));
		}
	}
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < (NUM_COLUMNS - i); j++)
		{			
			 iArray[i][j] = (i * 1) + (j * 1);
		}
	}
		
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{		
		for( j = 0; j < (NUM_COLUMNS - i); j++)
		{			
			printf("iArray[%d][%d]= %d \t at address: %p\n",
				i,j,iArray[i][j],(*(iArray + i) + j));			
		}
		
		printf("\n\n");
	}
	
	
	// Free the Allocated Memory Row by Row.
	//Always try to clean up in reverse order to avoid any runtime issues.
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		if(iArray[i]) //if(ptr[i])
		{
			free(iArray[i]);
			iArray[i] = NULL;
			
			printf("Memory allocated for row %d has been freed. !!\n",i);
		}
	}
	
	
	
	return (0);
}


/*

Memory allocation is succedded for row 0 with columns 5
Memory allocation is succedded for row 1 with columns 4
Memory allocation is succedded for row 2 with columns 3
Memory allocation is succedded for row 3 with columns 2
Memory allocation is succedded for row 4 with columns 1


 Array Values with addresss :

iArray[0][0]= 0          at address: 0083BD88
iArray[0][1]= 1          at address: 0083BD8C
iArray[0][2]= 2          at address: 0083BD90
iArray[0][3]= 3          at address: 0083BD94
iArray[0][4]= 4          at address: 0083BD98


iArray[1][0]= 1          at address: 00845340
iArray[1][1]= 2          at address: 00845344
iArray[1][2]= 3          at address: 00845348
iArray[1][3]= 4          at address: 0084534C


iArray[2][0]= 2          at address: 00845298
iArray[2][1]= 3          at address: 0084529C
iArray[2][2]= 4          at address: 008452A0


iArray[3][0]= 3          at address: 00856688
iArray[3][1]= 4          at address: 0085668C


iArray[4][0]= 4          at address: 008566F8


Memory allocated for row 4 has been freed. !!
Memory allocated for row 3 has been freed. !!
Memory allocated for row 2 has been freed. !!
Memory allocated for row 1 has been freed. !!
Memory allocated for row 0 has been freed. !!


*/