#include <stdio.h>


#define NUM_ROWS 3
#define NUM_COLUMNS 2

int main(void)
{
	int iArray[NUM_ROWS][NUM_COLUMNS];
	int i,j;
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			iArray[i][j] = (i * 1 ) + (j * 1);
		}
	}
	
	printf("\n\n Array Values with addresss :\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		for( j = 0; j < NUM_COLUMNS; j++)
		{			
			printf("iArray[%d][%d] = %d \t at address: %p\n",
				i,j,iArray[i][j],&iArray[i][j]);			
		}
		
		printf("\n\n");
	}
	
	return (0);
}


/*

 Array Values with addresss :

iArray[0][0] = 0         at address: 00AFFABC
iArray[0][1] = 1         at address: 00AFFAC0


iArray[1][0] = 1         at address: 00AFFAC4
iArray[1][1] = 2         at address: 00AFFAC8


iArray[2][0] = 2         at address: 00AFFACC
iArray[2][1] = 3         at address: 00AFFAD0

*/