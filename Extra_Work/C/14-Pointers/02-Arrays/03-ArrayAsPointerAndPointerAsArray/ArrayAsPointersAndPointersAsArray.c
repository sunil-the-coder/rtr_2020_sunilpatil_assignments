#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	int iArray[] = {10, 5, 6, 7, 8};
	int *ptr = NULL;
	
	int i;
	
	printf("\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("iArray[%d] - Value=[%d] and address=[%p]\n",i, *(iArray + i),(iArray + i));		
	}
	
	ptr = iArray; // As good as &iArray[0]; 
	
	printf("\n\n");
	
	printf("Accessing array using pointer like array.\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
	printf("ptr[%d] - Value=[%d] and &ptr[%d]=[%p]\n",i, ptr[i], i, &ptr[i]);		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


iArray[0] - Value=[10] and address=[00CFFA54]
iArray[1] - Value=[5] and address=[00CFFA58]
iArray[2] - Value=[6] and address=[00CFFA5C]
iArray[3] - Value=[7] and address=[00CFFA60]
iArray[4] - Value=[8] and address=[00CFFA64]


Accessing array using pointer like array.

ptr[0] - Value=[10] and &ptr[0]=[00CFFA54]
ptr[1] - Value=[5] and &ptr[1]=[00CFFA58]
ptr[2] - Value=[6] and &ptr[2]=[00CFFA5C]
ptr[3] - Value=[7] and &ptr[3]=[00CFFA60]
ptr[4] - Value=[8] and &ptr[4]=[00CFFA64]

*/