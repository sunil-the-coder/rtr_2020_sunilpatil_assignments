#include <stdio.h>
#include <stdlib.h>

#define INT_SIZE sizeof(int)
#define FLOAT_SIZE sizeof(float)
#define DOUBLE_SIZE sizeof(double)
#define CHAR_SIZE sizeof(char)

int main(void)
{
	int *iptr = NULL;	
	float *fptr = NULL;
	char *cptr = NULL;
	double *dptr = NULL;
	
	unsigned int iLength;
	unsigned int fLength;
	unsigned int dLength;
	unsigned int cLength;
	
	int i;
	
	printf("\n\n");
	printf("Enter number of elements to store in an int array:");
	scanf("%u",&iLength);
	
	//Allocate dynamic memory.	
	iptr = (int *)malloc(INT_SIZE * iLength);
	
	if(iptr == NULL)
	{
		printf("Could not allocate the memory for integer array. Exiting..\n");
		exit(0);
	}else
	{
		printf("Allocated memory for int array: %d bytes from %p to %p",
		(sizeof(int) * iLength ), iptr, (iptr + (iLength - 1)));
	}
	
	printf("\n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("Enter int data:");
		scanf("%d",iptr + i); 		
	}
	
	
	//Allocate & take input for float numbers.	
	printf("\n\n");
	printf("Enter number of elements to store in an float array:");
	scanf("%u",&fLength);
	
	//Allocate dynamic memory.	
	fptr = (float *)malloc(FLOAT_SIZE * fLength);
	
	if(fptr == NULL)
	{
		printf("Could not allocate the memory for float array. Exiting..\n");
		exit(0);
	}else
	{
		printf("Allocated memory for float array: %d bytes from %p to %p",
		(FLOAT_SIZE * fLength ), fptr, (fptr + (fLength - 1)));
	}
	
	printf("\n\n");
	for( i = 0; i < fLength; i++)
	{
		printf("Enter float data:");
		scanf("%f",fptr + i); 
	}
		
	
	//Allocate & take input for double numbers.	
	printf("\n\n");
	printf("Enter number of elements to store in an double array:");
	scanf("%u",&dLength);
	
	//Allocate dynamic memory.	
	dptr = (double *)malloc(DOUBLE_SIZE * dLength);
	
	if(dptr == NULL)
	{
		printf("Could not allocate the memory for double array. Exiting..\n");
		exit(0);
	}else
	{
		printf("Allocated memory for double array: %d bytes from %p to %p",
		(DOUBLE_SIZE * dLength ), dptr, (dptr + (dLength - 1)));
	}
	
	printf("\n\n");
	for( i = 0; i < dLength; i++)
	{
		printf("Enter double data:");
		scanf("%lf",dptr + i); 
	}
	
	
	
	//Allocate memory & take input for characters
	printf("\n\n");
	printf("Enter number of characters to store in an character array:");
	scanf("%u",&cLength);
	
	cptr = (char *)malloc(CHAR_SIZE * cLength);
	
	if(cptr == NULL)
	{
		printf("Could not allocate the memory for char array. Exiting..\n");
		exit(0);
	}else
	{
		printf("Allocated memory for char array: %d bytes from %p to %p",
		(CHAR_SIZE * cLength ), cptr, (cptr + (cLength - 1)));
	}
	
	printf("\n\n");
	printf("Enter the %d character elements for charArray: \n\n",cLength);
	for( i = 0; i < cLength; i++)
	{
		*(cptr + i) = getch();
		printf("%c\n", *(cptr + i));		
	}
	
	
	

	//Display all arrays data.
	
	printf("\n\n Integer Array: \n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("iptr[%d] = %d \t at address &iptr[%d] : %p\n", 
		i, iptr[i], i, &iptr[i]);		
	}
	
	printf("\n\n");
	for( i = 0; i < iLength; i++)
	{
		printf("*(iptr + %d) = %d \t at address (iptr + %d) : %p\n", 
		i, *(iptr + i), i, iptr + i);		
	}


	printf("\n\n Float Array: \n\n");
	for( i = 0; i < fLength; i++)
	{
		printf("fptr[%d] = %f \t at address &fptr[%d] : %p\n", 
		i, fptr[i], i, &fptr[i]);		
	}
	
	printf("\n\n");
	for( i = 0; i < fLength; i++)
	{
		printf("*(fptr + %d) = %f \t at address (fptr + %d) : %p\n", 
		i, *(fptr + i), i, fptr + i);		
	}
	
	printf("\n\n Double Array: \n\n");
	for( i = 0; i < dLength; i++)
	{
		printf("dptr[%d] = %lf \t at address &dptr[%d] : %p\n", 
		i, dptr[i], i, &dptr[i]);		
	}
	
	printf("\n\n");
	for( i = 0; i < dLength; i++)
	{
		printf("*(dptr + %d) = %lf \t at address (dptr + %d) : %p\n", 
		i, *(dptr + i), i, dptr + i);		
	}
	
	
	printf("\n\n Character Array: \n\n");
	for( i = 0; i < cLength; i++)
	{
		printf("cptr[%d] = %c \t at address &cptr[%d] : %p\n", 
		i, cptr[i], i, &cptr[i]);		
	}
	
	printf("\n\n");
	for( i = 0; i < cLength; i++)
	{
		printf("*(cptr + %d) = %c \t at address (cptr + %d) : %p\n", 
		i, *(cptr + i), i, cptr + i);		
	}


	//cleanup the memory if it's still allocated all types of pointers.
	
	if( cptr )
	{
		free(cptr);
		cptr = NULL;
		
		printf("\n Character Memory is freed.\n\n");
	}	
	
	if( dptr )
	{
		free(dptr);
		dptr = NULL;
		
		printf("\n Double Memory is freed.\n\n");
	}	
	
	if( fptr )
	{
		free(fptr);
		fptr = NULL;
		
		printf("\n Float Memory is freed.\n\n");
	}	
	
	if( iptr )
	{
		free(iptr);
		iptr = NULL;
		
		printf("\n Integer Memory is freed.\n\n");
	}		
	
	return (0);
}

/*

Enter number of elements to store in an int array:5
Allocated memory for int array: 20 bytes from 0078F5C8 to 0078F5D8

Enter int data:10
Enter int data:20
Enter int data:30
Enter int data:40
Enter int data:50


Enter number of elements to store in an float array:3
Allocated memory for float array: 12 bytes from 00774DC0 to 00774DC8

Enter float data:5.4
Enter float data:5.3
Enter float data:7.8


Enter number of elements to store in an double array:6
Allocated memory for double array: 24 bytes from 00775650 to 00775678

Enter double data:5678.32
Enter double data:644.45
Enter double data:90.4355
Enter double data:123.45
Enter double data:567.32
Enter double data:8956.43


Enter number of characters to store in an character array:10
Allocated memory for char array: 10 bytes from 00774CE8 to 00774CF1

Enter the 10 character elements for charArray:

s
u
n
i
l
p
a
t
i
l


 Integer Array:

iptr[0] = 10     at address &iptr[0] : 0078F5C8
iptr[1] = 20     at address &iptr[1] : 0078F5CC
iptr[2] = 30     at address &iptr[2] : 0078F5D0
iptr[3] = 40     at address &iptr[3] : 0078F5D4
iptr[4] = 50     at address &iptr[4] : 0078F5D8


*(iptr + 0) = 10         at address (iptr + 0) : 0078F5C8
*(iptr + 1) = 20         at address (iptr + 1) : 0078F5CC
*(iptr + 2) = 30         at address (iptr + 2) : 0078F5D0
*(iptr + 3) = 40         at address (iptr + 3) : 0078F5D4
*(iptr + 4) = 50         at address (iptr + 4) : 0078F5D8


 Float Array:

fptr[0] = 5.400000       at address &fptr[0] : 00774DC0
fptr[1] = 5.300000       at address &fptr[1] : 00774DC4
fptr[2] = 7.800000       at address &fptr[2] : 00774DC8


*(fptr + 0) = 5.400000   at address (fptr + 0) : 00774DC0
*(fptr + 1) = 5.300000   at address (fptr + 1) : 00774DC4
*(fptr + 2) = 7.800000   at address (fptr + 2) : 00774DC8


 Double Array:

dptr[0] = 5678.320000    at address &dptr[0] : 00775650
dptr[1] = 644.450000     at address &dptr[1] : 00775658
dptr[2] = 90.435500      at address &dptr[2] : 00775660
dptr[3] = 123.450000     at address &dptr[3] : 00775668
dptr[4] = 567.320000     at address &dptr[4] : 00775670
dptr[5] = 8956.430000    at address &dptr[5] : 00775678


*(dptr + 0) = 5678.320000        at address (dptr + 0) : 00775650
*(dptr + 1) = 644.450000         at address (dptr + 1) : 00775658
*(dptr + 2) = 90.435500          at address (dptr + 2) : 00775660
*(dptr + 3) = 123.450000         at address (dptr + 3) : 00775668
*(dptr + 4) = 567.320000         at address (dptr + 4) : 00775670
*(dptr + 5) = 8956.430000        at address (dptr + 5) : 00775678


 Character Array:

cptr[0] = s      at address &cptr[0] : 00774CE8
cptr[1] = u      at address &cptr[1] : 00774CE9
cptr[2] = n      at address &cptr[2] : 00774CEA
cptr[3] = i      at address &cptr[3] : 00774CEB
cptr[4] = l      at address &cptr[4] : 00774CEC
cptr[5] = p      at address &cptr[5] : 00774CED
cptr[6] = a      at address &cptr[6] : 00774CEE
cptr[7] = t      at address &cptr[7] : 00774CEF
cptr[8] = i      at address &cptr[8] : 00774CF0
cptr[9] = l      at address &cptr[9] : 00774CF1


*(cptr + 0) = s          at address (cptr + 0) : 00774CE8
*(cptr + 1) = u          at address (cptr + 1) : 00774CE9
*(cptr + 2) = n          at address (cptr + 2) : 00774CEA
*(cptr + 3) = i          at address (cptr + 3) : 00774CEB
*(cptr + 4) = l          at address (cptr + 4) : 00774CEC
*(cptr + 5) = p          at address (cptr + 5) : 00774CED
*(cptr + 6) = a          at address (cptr + 6) : 00774CEE
*(cptr + 7) = t          at address (cptr + 7) : 00774CEF
*(cptr + 8) = i          at address (cptr + 8) : 00774CF0
*(cptr + 9) = l          at address (cptr + 9) : 00774CF1

 Character Memory is freed.


 Double Memory is freed.


 Float Memory is freed.


 Integer Memory is freed



*/