#include <stdio.h>

#define NUMBER_OF_ELEMENTS 5

int main(void)
{
	int iArray[] = {10, 5, 6, 7, 8};
	int *ptr = NULL;
	
	int i;
	
	printf("\n\n");
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("iArray - Value=[%d] and address=[%p]\n",*(iArray + i),(iArray + i));		
	}
	
	ptr = iArray; // As good as &iArray[0]; 
	
	printf("\n\n");
	
	//You can manipulate using pointer as well. 
	for(i = 0; i < NUMBER_OF_ELEMENTS; i++)
	{
		printf("ptr - Value=[%d] and address=[%p]\n",*(ptr + i),(ptr + i));		
	}
	
	printf("\n\n");
		
	return (0);
}

/*


iArray - Value=[10] and address=[00EFFD10]
iArray - Value=[5] and address=[00EFFD14]
iArray - Value=[6] and address=[00EFFD18]
iArray - Value=[7] and address=[00EFFD1C]
iArray - Value=[8] and address=[00EFFD20]


ptr - Value=[10] and address=[00EFFD10]
ptr - Value=[5] and address=[00EFFD14]
ptr - Value=[6] and address=[00EFFD18]
ptr - Value=[7] and address=[00EFFD1C]
ptr - Value=[8] and address=[00EFFD20]

*/