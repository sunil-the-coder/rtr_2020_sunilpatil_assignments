#include <stdio.h>

int main(void)
{
	void MyAlloc(int **ptr, unsigned int numberOfElements);
	
	int *ptr = NULL;
	int i;
	int numberOfElements;

	printf("Enter N:");
	scanf("%u",&numberOfElements);

	MyAlloc(&ptr, numberOfElements);

	printf("Enter %d elements in array:\n",numberOfElements);
	for(i = 0; i < numberOfElements; i++)
		scanf("%d",&ptr[i]);

	printf("Your Array:\n");	
	for(i = 0; i < numberOfElements; i++)
		printf("%d\t",ptr[i]);

	printf("\n\n");

	if(ptr)
	{
		free(ptr);
		ptr = NULL;
		printf("Memory Freed.");
	}


	return (0);
}

void MyAlloc(int **ptr, unsigned int numberOfElements)
{

	*ptr = (int *)malloc(sizeof(int) * numberOfElements);

	if(*ptr == NULL)
	{
		printf("Memory Allocation Failed.");
	}
}

/*

D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\02-Arrays\08-PointerToPointerFor1DArray>PointerToPointer1DArray.exe
Enter N:3
Enter 3 elements in array:
1
2
3
Your Array:
1       2       3

*/
