#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Take the input as string & return new string with replaced vowels.
	char* ReplaceVowelWithHash(char*);
	
	char chSource[MAX_STRING_LENGTH];
	char *chDestination;
	

	printf("Enter string:");
	gets_s(chSource,MAX_STRING_LENGTH);

	chDestination = ReplaceVowelWithHash(chSource);
	if(chDestination == NULL)
	{
		printf("Vowel replacement failed. Exiting now !!!");
		exit(0);
	}
	
	
	printf("Relaced String:%s\n",chDestination);
	
	
	if(chDestination)
	{
		free(chDestination);
		chDestination = NULL;
	}
	
	return (0);
}

char *ReplaceVowelWithHash(char *source)
{
	int MyStrLength(char *);
	
	char *returned_string;
	
	int i;
	int length = MyStrLength(source);
	
	returned_string = (char *) malloc( sizeof(char) * length);
	if(returned_string == NULL)
	{
		printf("Memory allocation failed for new string. Exiting Now. !!!");
		exit(0);
	}
	
	for(i = 0; i < length; i++)
	{
		switch(source[i])
		{
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				returned_string[i] = '#';
				break;
			default:
				returned_string[i] = source[i];
				break;
		}
	}
	
	return returned_string;
	
}


int MyStrLength(char *str)
{
	int length,i;
	
	i = 0, length = 0;
	
	while( i < MAX_STRING_LENGTH)
	{
		if(*(str + i) == '\0')
			break;
		else
			length++;
		i++;
	}
	
	return (length);
}

/*

Enter string:Sunil Patil
Relaced String:S#n#l P#t#l

*/