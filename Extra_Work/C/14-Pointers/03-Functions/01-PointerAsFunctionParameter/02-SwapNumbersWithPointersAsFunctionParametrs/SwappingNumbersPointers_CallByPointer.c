#include <stdio.h>


int main(void)
{
	//Call by Value
	void SwapMyNumbes(int *, int *);
	
	int a;
	int b;
	
	a = 10;
	b = 20;
	
	printf("\n\n");
	
	printf("Before Swapping in Main: A=%d, B=%d\n\n", a, b);
	
	SwapMyNumbes(&a, &b);
	
	printf("After Swapping in Main: A=%d, B=%d\n\n", a, b);
	
	printf("\n\n");
		
	return (0);
}

void SwapMyNumbes(int *a, int *b)
{
	int temp;

	printf("Before Swapping in Swap(): A=%d, B=%d\n\n", *a, *b);

	temp = *a;
	*a = *b;
	*b = temp;
	
	printf("After Swapping in Swap(): A=%d, B=%d\n\n", *a, *b);
}

/*

Before Swapping in Main: A=10, B=20

Before Swapping in Swap(): A=10, B=20

After Swapping in Swap(): A=20, B=10

After Swapping in Main: A=20, B=10

*/