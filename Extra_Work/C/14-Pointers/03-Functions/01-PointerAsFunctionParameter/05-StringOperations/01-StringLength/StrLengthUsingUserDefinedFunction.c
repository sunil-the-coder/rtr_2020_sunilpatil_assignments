#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLength(char *);
	
	char *str = NULL;
	int length;
	
	str = (char *) malloc( sizeof(char) * MAX_STRING_LENGTH);
	
	if(str == NULL)
	{
		printf("Memory Allocation failed. Exiting Now.\n");
		exit(0);
	}
	
	printf("Enter string:");
	gets_s(str, MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",str);
	
	length = MyStrLength(str);
	printf("Length:%d\n",length);
	
	if(str)
	{
		free(str);
		str = NULL;
	}
	
	return (0);
}

int MyStrLength(char *str)
{
	int length,i;
	
	i = 0, length = 0;
	
	while( i < MAX_STRING_LENGTH)
	{
		if(*(str + i) == '\0')
			break;
		else
			length++;
		i++;
	}
	
	return (length);
}

/*

Enter string:Sunil
Entered string:Sunil
Length:5

*/