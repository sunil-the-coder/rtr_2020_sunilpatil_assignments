#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrCopy(char*, char*);
	
	char *chSource;
	char *chDestination;
	
	chSource = (char *) malloc( sizeof(char) * MAX_STRING_LENGTH);
	if(chSource == NULL)
	{
		printf("Memory Allocation failed. Exiting Now.\n");
		exit(0);
	}
	
	chDestination = (char *) malloc( sizeof(char) * MAX_STRING_LENGTH);
	if(chDestination == NULL)
	{
		printf("Memory Allocation failed. Exiting Now.\n");
		exit(0);
	}
	
	printf("Enter string:");
	gets_s(chSource,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chSource);
	
	MyStrCopy(chDestination,chSource);
	
	printf("Copied String:%s\n",chDestination);
	
	
	if(chDestination)
	{
		free(chDestination);
		chDestination = NULL;
	}
	
	if(chSource)
	{
		free(chSource);
		chSource = NULL;
	}
	
	
	return (0);
}

void MyStrCopy(char *str_destination, char *str_source)
{
	int MyStrLength(char *);
	
	int length = 0;
	int i;
	
	//code
	length = MyStrLength(str_source);
	
	for (i = 0; i < length; i++)
		*(str_destination + i) = *(str_source + i);
	
	*(str_destination + i) = '\0';
}
 

int MyStrLength(char *str)
{
	int length,i;
	
	i = 0, length = 0;
	
	while( i < MAX_STRING_LENGTH)
	{
		if(*(str + i) == '\0')
			break;
		else
			length++;
		i++;
	}
	
	return (length);
}

/*

Enter string:Shree Ganesh
Entered string:Shree Ganesh
Copied String:Shree Ganesh

*/