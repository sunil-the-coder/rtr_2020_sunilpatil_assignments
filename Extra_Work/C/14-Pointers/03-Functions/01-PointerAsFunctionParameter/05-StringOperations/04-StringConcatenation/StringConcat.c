#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrConcat(char *, char *);
	
	char *chSource = NULL;
	char *chDestination = NULL;
	
	chSource = (char *) malloc( sizeof(char) * MAX_STRING_LENGTH);
	if(chSource == NULL)
	{
		printf("Memory Allocation failed. Exiting Now.\n");
		exit(0);
	}
	
	chDestination = (char *) malloc( sizeof(char) * MAX_STRING_LENGTH);
	if(chDestination == NULL)
	{
		printf("Memory Allocation failed. Exiting Now.\n");
		exit(0);
	}
	
	printf("Enter string:");
	gets_s(chSource,MAX_STRING_LENGTH);
	
	printf("Enter string:");
	gets_s(chDestination,MAX_STRING_LENGTH);
	
	
	printf("Entered string:%s\n",chSource);
	printf("Entered string:%s\n",chDestination);
	
	MyStrConcat(chSource,chDestination);
	
	printf("\n\n After Concatination:\n");
	printf("First String:%s\n",chSource);
	printf("Second String:%s\n",chDestination);
	
	
	if(chDestination)
	{
		free(chDestination);
		chDestination = NULL;
	}
	
	if(chSource)
	{
		free(chSource);
		chSource = NULL;
	}
	
	
	return (0);
}
void MyStrConcat(char *str_destination, char *str_source)
{
	int MyStrLength(char *);
	
	int source_length = 0, destination_length;
	int i, j;
	
	//code
	source_length = MyStrLength(str_source);
	destination_length = MyStrLength(str_destination);
	
	
	//COpy from source ( from 0 to its legth) into destination ( length )
	for (i = destination_length, j = 0; j < source_length; i++, j++)
		*(str_destination + i) = *(str_source + j);
	
	*(str_destination + i) = '\0';
}


void MyStrCopy(char *str_destination, char *str_source)
{
	int MyStrLength(char *);
	
	int length = 0;
	int i;
	
	//code
	length = MyStrLength(str_source);
	
	for (i = 0; i < length; i++)
		*(str_destination + i) = *(str_source + i);
	
	*(str_destination + i) = '\0';
}
 

int MyStrLength(char *str)
{
	int length,i;
	
	i = 0, length = 0;
	
	while( i < MAX_STRING_LENGTH)
	{
		if(*(str + i) == '\0')
			break;
		else
			length++;
		i++;
	}
	
	return (length);
}

/*

Enter string:Sunil
Enter string:Patil
Entered string:Sunil
Entered string:Patil


 After Concatination:
First String:SunilPatil
Second String:Patil

*/