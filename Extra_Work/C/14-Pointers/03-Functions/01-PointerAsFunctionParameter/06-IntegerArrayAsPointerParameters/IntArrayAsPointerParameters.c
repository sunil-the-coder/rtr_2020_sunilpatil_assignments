#include <stdio.h>


int main(void)
{
	void MultiplyArrayByNumber(int *, int, int);
	
	int *ptr = NULL;
	int iNumberOfElements;
	int i;
	
	printf("\nEnter number of elements for array to store:");
	scanf("%d",&iNumberOfElements);
	
	ptr = (int *) malloc(sizeof(int) * iNumberOfElements);
	if(ptr == NULL)
	{
		printf("Memory allocation failed. Exiting now. !!!");
		exit(0);
		
	}
	
	for(i = 0; i < iNumberOfElements; i++)
		ptr[i] = (i + 1);
	
	printf("\n\nArray Elements before multiplication:\n");
	for(i = 0; i < iNumberOfElements; i++)
		printf("%d, ",ptr[i]);
	
	
	MultiplyArrayByNumber(ptr, iNumberOfElements, 5);
	
	printf("\n\nArray Elements after multiplication:\n");
	for(i = 0; i < iNumberOfElements; i++)
		printf("%d, ",ptr[i]);
	
	
	
	return (0);
}

void MultiplyArrayByNumber(int *ptr, int iNumberOfElements, int number)
{
	int i;
	
	for(i = 0; i < iNumberOfElements; i++)
		ptr[i] = (ptr[i] * number);
	
}


/*

Enter number of elements for array to store:5


Array Elements before multiplication:
1, 2, 3, 4, 5,

Array Elements after multiplication:
5, 10, 15, 20, 25,

*/