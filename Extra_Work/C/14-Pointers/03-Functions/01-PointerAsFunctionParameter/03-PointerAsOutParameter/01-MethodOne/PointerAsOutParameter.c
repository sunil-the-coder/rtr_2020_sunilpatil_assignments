#include <stdio.h>


int main(void)
{
	// parameterized return values OR OUT parameters.
	//Function can return multiple values using OUT parameters.
	//We just pass address, the function will take care of filling values	
	void DivideNumbers(int, int, int *, int *);
	
	int a;
	int b;
	int quotient;
	int remainder;
	
	a = 100;
	b = 23;
	
	printf("\n\n");
		
	DivideNumbers(a, b, &quotient, &remainder);
	
	printf("After division - Quotient = %d and Remainder = %d\n\n", quotient, remainder);
	
	printf("\n\n");
		
	return (0);
}

//
void DivideNumbers(int a, int b, int *quotient, int *remainder)
{
		*quotient = a / b;
		*remainder = a % b;
}

/*

After division - Quotient = 4 and Remainder = 8

*/