#include <stdio.h>


int main(void)
{
	// parameterized return values OR OUT parameters.
	//Function can return multiple values using OUT parameters.
	//We just pass address, the function will take care of filling values	
	void DivideNumbers(int, int, int *, int *);
	
	int a;
	int b;
	int *quotient = NULL;
	int *remainder = NULL;
	
	a = 100;
	b = 23;
	
	printf("\n\n");
	
	//Dynamically allocaiton memory for OUT parameters.
	quotient = (int *) malloc( 1 * sizeof(int));
	if(quotient == NULL)
	{
		printf("Memory allocation failed. Exiting now..!!!\n");
		exit(0);
	}
	
	remainder = (int *) malloc( 1 * sizeof(int));
	if(remainder == NULL)
	{
		printf("Memory allocation failed. Exiting now..!!!\n");
		exit(0);
	}
			
	DivideNumbers(a, b, quotient, remainder);
	
	printf("After division - Quotient = %d and Remainder = %d\n\n", *quotient, *remainder);
	
	printf("\n\n");
		
	return (0);
}

//
void DivideNumbers(int a, int b, int *quotient, int *remainder)
{
		*quotient = a / b;
		*remainder = a % b;
}

/*

After division - Quotient = 4 and Remainder = 8

*/