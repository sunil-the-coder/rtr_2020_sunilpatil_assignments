#include <stdio.h>

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

int main(void)
{
	int DifferenceOfTwoNumbers(int, int, int *);
		
	int a_SMP;
	int b_SMP;
	int answer_SMP;
	int retValue_SMP;
	
	a_SMP = 5;
	b_SMP = 5;
	
	printf("\n\n");
		
	retValue_SMP = DifferenceOfTwoNumbers(a_SMP, b_SMP, &answer_SMP);

	printf("Diff of %d and %d = %d \n\n", a_SMP, b_SMP, answer_SMP);
	
	if(retValue_SMP == POSITIVE)
		printf("Difference is Positive.\n");
	else if(retValue_SMP == NEGATIVE)
		printf("Difference is Negative.\n");
	else
		printf("Difference is Zero.\n");
	
	
	printf("\n\n");
		
	return (0);
}

//Returning multiple values using OUT parameter as well as actual function return value.

int DifferenceOfTwoNumbers(int a_SMP, int b_SMP, int *answer_SMP)
{
	*answer_SMP =  a_SMP - b_SMP;
	
	if(*answer_SMP < 0)
		return  NEGATIVE;
	else if(*answer_SMP > 0)
		return POSITIVE;
	else
		return ZERO;
}

/*

Diff of 100 and 23 = 77

Difference is Positive.



D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\03-Functions\01-PointerAsFunctionParameter\04-SignificanceOfParameterizedReturnValue>cl SignificanceOfUsingPointerAsOutParameter.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.26.28806 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

SignificanceOfUsingPointerAsOutParameter.c
Microsoft (R) Incremental Linker Version 14.26.28806.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:SignificanceOfUsingPointerAsOutParameter.exe
SignificanceOfUsingPointerAsOutParameter.obj

D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\03-Functions\01-PointerAsFunctionParameter\04-SignificanceOfParameterizedReturnValue>SignificanceOfUsingPointerAsOutParameter.exe


Diff of 5 and 23 = -18

Difference is Negative.



D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\03-Functions\01-PointerAsFunctionParameter\04-SignificanceOfParameterizedReturnValue>cl SignificanceOfUsingPointerAsOutParameter.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.26.28806 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

SignificanceOfUsingPointerAsOutParameter.c
Microsoft (R) Incremental Linker Version 14.26.28806.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:SignificanceOfUsingPointerAsOutParameter.exe
SignificanceOfUsingPointerAsOutParameter.obj

D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\03-Functions\01-PointerAsFunctionParameter\04-SignificanceOfParameterizedReturnValue>SignificanceOfUsingPointerAsOutParameter.exe


Diff of 5 and 5 = 0

Difference is Zero.

*/