#include <stdio.h>

typedef int (*AddIntFunPtr)(int,int);
typedef float (*AddFloatFunPtr)(float,float);

int main(void)
{
	//Function declarations
	int AddIntegers(int, int);
	float AddFloats(float, float);
	int SubstractInt(int, int);

	int iResult;
	float fResult;

	//function pointer variables
	AddIntFunPtr pfAddInt = NULL;
	AddIntFunPtr pfSubInt = NULL;
	AddFloatFunPtr pfAddFloat = NULL;


	pfAddInt = AddIntegers;
	iResult = pfAddInt(10,20);
	printf("Int Addition = %d\n",iResult);

	pfSubInt = SubstractInt;
	iResult = pfSubInt(50,25);
	printf("Int Substraction = %d\n",iResult);
		
	pfAddFloat = AddFloats;
	fResult = pfAddFloat(10.5f, 50.3f);
	printf("Float Addition = %f\n",fResult);

	return (0);
}

int AddIntegers(int num_one, int num_two)
{
	int result = 0;

	result = num_one + num_two;

	return (result);
}


int SubstractInt(int num_one, int num_two)
{
	int result = 0;

	result = num_one - num_two;

	return (result);
}


float AddFloats(float num_one, float num_two)
{
	float result = 0;

	result = num_one + num_two;

	return (result);
}


/*

D:\RTR2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\14-Pointers\03-Functions\03-FunctionPointers>FunctionPointers.exe
Int Addition = 30
Int Substraction = 25
Float Addition = 60.799999

*/
