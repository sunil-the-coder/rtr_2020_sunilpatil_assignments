#include<stdio.h>

#define MAX_ELEMENTS 10

//Prime Number -> The number which is divisible by 1 and by itself number.
//1 is not prime number.

int main(void)
{
		int iArray_SMP[MAX_ELEMENTS] = {1, 5, 7, 17, 30, 24, 56, 12, 25, 51};
		
		int i_SMP, j_SMP;
		
		printf("\n\n");
		
		int count = 0;
		
		for(i_SMP = 0; i_SMP < MAX_ELEMENTS; i_SMP++) 
		{
			//If more than 2 numbers are able to divide means number is not prime except 1.
			
			//int is_prime = 1;
			
			for(j_SMP = 1; j_SMP <= iArray_SMP[i_SMP] ; j_SMP++)
			{
				if((iArray_SMP[i_SMP] % j_SMP) == 0) {
					count++;
				//	is_prime = 0;
				//	break;
				}
									
			}
			
			//if(is_prime)
				if(count == 2)
					printf("Prime:%d\n",iArray_SMP[i_SMP]);
				
				count = 0; //Reset for next number check
		}
				
		printf("\n\n");
	
	return(0);
	
}

/*

Prime:5
Prime:7
Prime:17

*/