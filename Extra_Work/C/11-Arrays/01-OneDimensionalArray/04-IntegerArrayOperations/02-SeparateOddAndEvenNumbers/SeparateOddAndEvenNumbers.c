#include<stdio.h>

#define MAX_ELEMENTS 8

int main(void)
{
		int iArray_SMP[MAX_ELEMENTS] = {1, 5, 7, 30, 24, 56, 12, 51};
		int i_SMP;
		
		printf("\n\n");
		
		for(i_SMP = 0; i_SMP < MAX_ELEMENTS; i_SMP++) 
		{
			if((iArray_SMP[i_SMP] % 2) == 0)				
				printf("Even:%d\n",iArray_SMP[i_SMP]);
			else				
				printf("Odd:%d\n",iArray_SMP[i_SMP]);
		}
				
		printf("\n\n");
	
	return(0);
	
}

/*

Odd:1
Odd:5
Odd:7
Even:30
Even:24
Even:56
Even:12
Odd:51

*/