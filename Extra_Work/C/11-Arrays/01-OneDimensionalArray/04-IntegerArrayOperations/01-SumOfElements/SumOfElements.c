#include<stdio.h>

#define MAX_ELEMENTS 5

int main(void)
{
		int iArray_SMP[MAX_ELEMENTS];
		int i_SMP, sum_SMP = 0;
		
		for(i_SMP = 0; i_SMP < MAX_ELEMENTS; i_SMP++) 
		{
			iArray_SMP[i_SMP] = (i_SMP+1) * 10;
		}
		
		for(i_SMP = 0; i_SMP < MAX_ELEMENTS; i_SMP++) 
		{
			sum_SMP = sum_SMP + iArray_SMP[i_SMP];
		}
		
		printf("Sum=%d\n\n",sum_SMP);
	
	return(0);
	
}

/*

Sum=150

*/