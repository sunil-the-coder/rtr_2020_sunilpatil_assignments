#include<stdio.h>

#define INT_ARRAY_NUM_ELEMENTS_SMP 3
#define FLOAT_ARRAY_NUM_ELEMENTS_SMP 2
#define CHAR_ARRAY_NUM_ELEMENTS_SMP 2

int main(void)
{

	int iArray_SMP[INT_ARRAY_NUM_ELEMENTS_SMP];
	float fArray_SMP[FLOAT_ARRAY_NUM_ELEMENTS_SMP];
	char cArray_SMP[CHAR_ARRAY_NUM_ELEMENTS_SMP];
	
	int i_SMP;
	
	for(i_SMP = 0; i_SMP < INT_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{
		printf("Enter data for int array (%d):",i_SMP+1);
		scanf("%d",&iArray_SMP[i_SMP]);
	}
	
	for(i_SMP = 0; i_SMP < FLOAT_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{
		printf("Enter data for float array (%d):",i_SMP+1);
		scanf("%f",&fArray_SMP[i_SMP]);
	}
	
	for(i_SMP = 0; i_SMP < CHAR_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{
		printf("Enter data for char array (%d):",i_SMP+1);
		cArray_SMP[i_SMP] = getch();
		printf("%c\n",cArray_SMP[i_SMP]);
	}
	
	//Display All values
	printf("\n\n");
	
	printf("Integer Array Elements: ");
	for(i_SMP = 0; i_SMP < INT_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{		
		printf("%d ",iArray_SMP[i_SMP]);
	}
	
	printf("\n\nFloat Array Elements: ");
	for(i_SMP = 0; i_SMP < FLOAT_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{		
		printf("%f ",fArray_SMP[i_SMP]);
	}
	
	printf("\n\nCharacter Array Elements: ");
	for(i_SMP = 0; i_SMP < CHAR_ARRAY_NUM_ELEMENTS_SMP; i_SMP++)
	{		
		printf("%c ",cArray_SMP[i_SMP]);
	}
	
	printf("\n\n");
	
	return (0);

}

/*


>UserInputArray.exe

Enter data for int array (1):10
Enter data for int array (2):20
Enter data for int array (3):30
Enter data for float array (1):10.5
Enter data for float array (2):4.5
Enter data for char array (1):a
Enter data for char array (2):b


Integer Array Elements: 10 20 30

Float Array Elements: 10.500000 4.500000

Character Array Elements: a b

*/