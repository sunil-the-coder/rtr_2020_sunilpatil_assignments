#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	
	
	char chArrayOne_SMP[MAX_STRING_LENGTH];
	char chArrayTwo_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArrayOne_SMP,MAX_STRING_LENGTH);
	
	printf("Enter string:");
	gets_s(chArrayTwo_SMP,MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("\n\n Before Contactination:\n");
	printf("Entered string:%s\n",chArrayOne_SMP);
	printf("Entered string:%s\n",chArrayTwo_SMP);
	
	strcat(chArrayOne_SMP,chArrayTwo_SMP);
	
	printf("\n\n After Contactination:\n");
	printf("1st String:%s\n",chArrayOne_SMP);
	printf("2nd String:%s\n",chArrayTwo_SMP);
	
	return (0);
}

/*

Enter string:sunil
Enter string:patil




 Before Contactination:
Entered string:sunil
Entered string:patil


 After Contactination:
1st String:sunilpatil
2nd String:patil

*/