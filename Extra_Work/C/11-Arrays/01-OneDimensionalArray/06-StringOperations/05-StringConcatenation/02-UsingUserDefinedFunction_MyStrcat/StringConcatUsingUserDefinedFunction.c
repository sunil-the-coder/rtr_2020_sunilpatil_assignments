#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrConcat_SMP(char[], char[]);
	
	char chArrayOne_SMP[MAX_STRING_LENGTH];
	char chArrayTwo_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArrayOne_SMP,MAX_STRING_LENGTH);
	
	printf("Enter string:");
	gets_s(chArrayTwo_SMP,MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("\n\n Before Contactination:\n");
	printf("Entered string:%s\n",chArrayOne_SMP);
	printf("Entered string:%s\n",chArrayTwo_SMP);
	
	MyStrConcat_SMP(chArrayOne_SMP,chArrayTwo_SMP);
	
	printf("\n\n After Contactination:\n");
	printf("1st String:%s\n",chArrayOne_SMP);
	printf("2nd String:%s\n",chArrayTwo_SMP);
	
	return (0);
}

void MyStrConcat_SMP(char str_destination_SMP[], char str_source_SMP[])
{
	int MyStrLength(char[]);
	
	int source_length_SMP = 0, destination_length_SMP;
	int i_SMP, j_SMP;
	
	//code
	source_length_SMP = MyStrLength(str_source_SMP);
	destination_length_SMP = MyStrLength(str_destination_SMP);
	
	j_SMP = length_SMP - 1;
	
	//COpy from source ( from 0 to its legth) into destination ( length )
	for (i_SMP = destination_length_SMP, j_SMP = 0; j_SMP < source_length_SMP; i_SMP++, j_SMP--)
		str_destination_SMP[i_SMP] = str_source_SMP[j_SMP];
	
	str_destination_SMP[i_SMP] = '\0';
}
 

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:sunil
Enter string:patil




 Before Contactination:
Entered string:sunil
Entered string:sunil


 After Contactination:
1st String:sunilpatil
2nd String:patil

*/