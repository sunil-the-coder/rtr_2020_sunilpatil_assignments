#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLength(char[]);
	
	int i_SMP, iLength_SMP, word_count_SMP, space_count_SMP;
	
	char chArraySource_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
	
	iLength_SMP = MyStrLength(chArraySource_SMP);
	
	space_count_SMP = 0;
	for(i_SMP = 0; i_SMP < iLength_SMP; i_SMP++)
	{
		switch(chArraySource_SMP[i_SMP])
		{
			case 32:
				space_count_SMP++;
				break;
			default: 
				break;
		}		
	}
	word_count_SMP = 0;
	word_count_SMP = space_count_SMP + 1;
	
	printf("Total Spaces:%d\n",space_count_SMP);
	printf("Total Words:%d\n",word_count_SMP);
		
	return (0);
}

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:sunil maruti patil shevate
Entered string:sunil maruti patil shevate
Total Spaces:3
Total Words:4


*/