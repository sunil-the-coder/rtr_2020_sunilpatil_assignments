#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	char chArraySource_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
	
	strrev(chArraySource_SMP);
	
	printf("Reverssed String:%s\n",chArraySource_SMP);
	
	return (0);
}



/*

Enter string:sunil
Entered string:sunil
Reverssed String:linus

*/