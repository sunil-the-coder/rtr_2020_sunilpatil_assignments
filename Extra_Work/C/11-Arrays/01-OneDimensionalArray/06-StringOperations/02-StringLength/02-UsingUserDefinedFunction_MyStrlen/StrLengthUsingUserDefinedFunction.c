#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLength_SMP(char[]);
	
	char chArray_SMP[MAX_STRING_LENGTH];
	int length_SMP;
	
	printf("Enter string:");
	gets_s(chArray_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArray_SMP);
	
	length_SMP = MyStrLength_SMP(chArray_SMP);
	printf("Length:%d\n",length_SMP);
	
	return (0);
}

int MyStrLength_SMP(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:pandharpur
Entered string:pandharpur
Length:10

*/