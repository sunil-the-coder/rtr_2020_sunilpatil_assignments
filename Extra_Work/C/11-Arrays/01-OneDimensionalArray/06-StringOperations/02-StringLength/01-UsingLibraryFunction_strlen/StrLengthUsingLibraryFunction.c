#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	char chArray[MAX_STRING_LENGTH];
	int length_SMP;
	
	printf("Enter string:");
	gets(chArray,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArray);
	
	length_SMP = strlen(chArray);
	printf("Length:%d\n",length_SMP);
	
	return (0);
}

/*

Enter string:sunil
Entered string:sunil
Length:5

*/