#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLength(char[]);
	
	int i_SMP,j_SMP, iLength_SMP;
	
	char chArraySource_SMP[MAX_STRING_LENGTH];
	char chArrayDestination_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
		
	iLength_SMP = MyStrLength(chArraySource_SMP);
	
	for(i_SMP = 0, j_SMP = 0; i_SMP < iLength_SMP; i_SMP++)
	{
		if(chArraySource_SMP[i_SMP] == ' ')
			continue;			
		else
			chArrayDestination_SMP[j_SMP++] = chArraySource_SMP[i_SMP];
	}
	
	chArrayDestination_SMP[j_SMP] = '\0';
	
	printf("Final String: %s\n",chArrayDestination_SMP);
	return (0);
}


int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:My name is sunil
Entered string:My name is sunil
Final String: Mynameissunil


*/