#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	
	char chArraySource_SMP[MAX_STRING_LENGTH];
	char chArrayDestination_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
	
	strcpy(chArrayDestination_SMP,chArraySource_SMP);
	
	printf("Copied String:%s\n",chArrayDestination_SMP);
	
	return (0);
}

/*

Enter string:RTR2020
Entered string:RTR2020
Copied String:RTR2020


*/