#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrCopy_SMP(char[], char[]);
	
	char chArraySource_SMP[MAX_STRING_LENGTH];
	char chArrayDestination_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
	
	MyStrCopy_SMP(chArrayDestination_SMP,chArraySource_SMP);
	
	printf("Copied String:%s\n",chArrayDestination_SMP);
	
	return (0);
}

void MyStrCopy_SMP(char str_destination_SMP[], char str_source_SMP[])
{
	int MyStrLength(char[]);
	
	int length_SMP = 0;
	int i_SMP;
	
	//code
	length_SMP = MyStrLength(str_source_SMP);
	
	for (i_SMP = 0; i_SMP < length_SMP; i_SMP++)
		str_destination_SMP[i_SMP] = str_source_SMP[i_SMP];
	
	str_destination_SMP[i_SMP] = '\0';
}
 

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:Shree Ganesh
Entered string:Shree Ganesh
Copied String:Shree Ganesh

*/