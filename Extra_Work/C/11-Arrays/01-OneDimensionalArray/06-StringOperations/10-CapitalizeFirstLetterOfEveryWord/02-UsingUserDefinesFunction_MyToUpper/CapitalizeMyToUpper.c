#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLength(char[]);
	
	int i_SMP,j_SMP, iLength_SMP;
	
	char chArraySource_SMP[MAX_STRING_LENGTH];
	char chArrayDestination_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArraySource_SMP,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArraySource_SMP);
		
	iLength_SMP = MyStrLength(chArraySource_SMP);
	
	j_SMP = 0;
	for(i_SMP = 0; i_SMP < iLength_SMP; i_SMP++)
	{
		if(i_SMP == 0)
		{
			chArrayDestination_SMP[j_SMP] = toupper(chArraySource_SMP[i_SMP]);
		}else if(chArraySource_SMP[i_SMP] == ' ')
		{
			chArrayDestination_SMP[j_SMP] = toupper(chArraySource_SMP[i_SMP]);
			
			chArrayDestination_SMP[j_SMP+1] = toupper(chArraySource_SMP[i_SMP+1]);
			
			j_SMP++;
			i_SMP++;
		}else if((chArraySource_SMP[i_SMP] == '.' || chArraySource_SMP[i_SMP] == ',' || chArraySource_SMP[i_SMP] == '!' || chArraySource_SMP[i_SMP] == '?') && (chArraySource_SMP[i_SMP] != ' '))
		{
			
			chArrayDestination_SMP[j_SMP] = chArraySource_SMP[i_SMP];
			chArrayDestination_SMP[j_SMP+1] = ' ';
			chArrayDestination_SMP[j_SMP+2] = MyToUpper(chArraySource_SMP[i_SMP+1]);
			
			j_SMP = j_SMP + 2;
			i_SMP++;
		}
		
		else
			chArrayDestination_SMP[j_SMP] = chArraySource_SMP[i_SMP];
		
		j_SMP++;
		
	}
	
	chArrayDestination_SMP[j_SMP] = '\0';
	
	printf("Final String: %s\n",chArrayDestination_SMP);
	return (0);
}


int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

char MyToUpper(char ch_SMP)
{
	int number_SMP;
	int temp_SMP;
	
	//Based on ascii code you can convert lower to upper and upper to lower.
	//Just substract diff from character.
	
	number_SMP = 'a' - 'A';
	
	if((int)ch_SMP >= 97 && (int)ch_SMP <= 122)
	{
		temp_SMP = (int)ch_SMP - num;
		return ((char)temp_SMP)		
	}else
		return (ch_SMP);
	
}

/*

Enter string:sunil maruti patil
Entered string:sunil maruti patil
Final String: Sunil Maruti Patil

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\11-Arrays\01-OneDimensionalArray\06-StringOperations\10-CapitalizeFirstLetterOfEveryWord\01-UsingLibraryFunction_toupper>Capitalize.exe

Enter string:sunil! maruti patil.
Entered string:sunil! maruti patil.
Final String: Sunil! Maruti Patil.



*/