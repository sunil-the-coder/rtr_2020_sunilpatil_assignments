#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	char chArray[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets(chArray,MAX_STRING_LENGTH);
	
	printf("Entered string:%s\n",chArray);
	
	return (0);
}

/*

Enter string:sunil patil rtr2020
Entered string:sunil patil rtr2020

*/