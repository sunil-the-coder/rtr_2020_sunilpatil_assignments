#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	
	int MyStrLength(char[]);
	
	int iLength_SMP;
	char aVowels_SMP[] = {'A','E','I','O','U'};
	char aVowelsCount_SMP[5] = {0}; 
	int i_SMP;
	
	char chArrayOne_SMP[MAX_STRING_LENGTH];
	
	printf("Enter string:");
	gets_s(chArrayOne_SMP,MAX_STRING_LENGTH);

	iLength_SMP = MyStrLength(chArrayOne_SMP);
	
	for(i_SMP = 0; i_SMP < iLength_SMP; i_SMP++)
	{
		switch(chArrayOne_SMP[i_SMP])
		{
			case 'A':
			case 'a':
				aVowelsCount_SMP[0] = aVowelsCount_SMP[0] + 1;
				break;
			case 'E':
			case 'e':
				aVowelsCount_SMP[1] = aVowelsCount_SMP[1] + 1;
				break;	
			case 'I':
			case 'i':
				aVowelsCount_SMP[2] = aVowelsCount_SMP[2] + 1;
				break;		
			case 'O':
			case 'o':
				aVowelsCount_SMP[3] = aVowelsCount_SMP[3] + 1;
				break;			
			case 'U':
			case 'u':
				aVowelsCount_SMP[4] = aVowelsCount_SMP[4] + 1;
				break;
			default: 
				break;
		}		
	}
	
	for(i_SMP = 0; i_SMP < 5; i_SMP++)
	{
		printf("%c occured %d times\n",aVowels_SMP[i_SMP],aVowelsCount_SMP[i_SMP]);
	}

	
	return (0);
}

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

Enter string:SunilMarutiPATIL
A occured 2 times
E occured 0 times
I occured 3 times
O occured 0 times
U occured 2 times

*/