#include<stdio.h>

int main(void)
{
	char cArray_01[] = {'S','U','N','I','L','\0'};
	char cArray_02[11] = {'P','A','N','D','H','A','R','P','U','R','\0'};
	char cArray_03[] = "SHEVATE"; // \0  internally ends.
	
	char cArray_04[] = {'R','T','R'}; //Without \0 ( ends in garbage when prints)
	
	printf("Size of 01=%lu\n",sizeof(cArray_01)); //6
	printf("Size of 02=%lu\n",sizeof(cArray_02)); //11
	printf("Size of 03=%lu\n",sizeof(cArray_03)); //8
	printf("Size of 04=%lu\n",sizeof(cArray_04)); //3
	
	printf("\n\n");
	
	//PRint Strings.
	
	printf("%s\n",cArray_01);
	printf("%s\n",cArray_02);
	printf("%s\n",cArray_03);
	printf("%s\n",cArray_04);  //Might display garbage at end ( RTR )
	
	
	return(0);
	
}

/*

Size of 01=6
Size of 02=11
Size of 03=8
Size of 04=3


SUNIL
PANDHARPUR
SHEVATE
RTR

*/