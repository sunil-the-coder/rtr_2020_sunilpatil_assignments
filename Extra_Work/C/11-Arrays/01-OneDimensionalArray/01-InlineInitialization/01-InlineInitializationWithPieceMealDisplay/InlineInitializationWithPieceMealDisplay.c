#include<stdio.h>

int main(void)
{

	int iArray[] = {44, 56, 2, 45, 89};
	int iSize;
	int iArray_Size;
	int iTotal_Elements;
	
	
	float fArray[] = {1.3f, 5.6f, 2.5f, 4.5f, 8.9f};
	int fSize;
	int fArray_Size;
	int fTotal_Elements;
	
	
	char cArray[] = {'s','u','n','i','l'};
	int cSize;
	int cArray_Size;
	int cTotal_Elements;
	
	
	//Access integer array using piecemeal & display its valus.	
	printf("iArray[0] - 1st element = %d\n",iArray[0]);
	printf("iArray[1] - 2nd element = %d\n",iArray[1]);
	printf("iArray[2] - 3rd element = %d\n",iArray[2]);
	
	iSize =  sizeof(int);
	iArray_Size = sizeof(iArray);
	iTotal_Elements = iArray_Size / iSize;
	
	printf("int size = %d bytes\n",iSize);
	printf("iArray size = %d bytes\n",iArray_Size);
	printf("iTotal_Elements = %d \n",iTotal_Elements);
	
	printf("\n\n");
	
	//Access float array using piecemeal & display its valus.	
	printf("fArray[0] - 1st element = %f\n",fArray[0]);
	printf("fArray[1] - 2nd element = %f\n",fArray[1]);
	printf("fArray[2] - 3rd element = %f\n",fArray[2]);
		
	fSize =  sizeof(float);
	fArray_Size = sizeof(fArray);
	fTotal_Elements = fArray_Size / fSize;
	
	printf("float size = %d bytes\n",fSize);
	printf("fArray size = %d bytes\n",fArray_Size);
	printf("fTotal_Elements = %d \n",fTotal_Elements);
	
	printf("\n\n");
	
	//Access char array using piecemeal & display its valus.	
	printf("cArray[0] - 1st element = %c\n",cArray[0]);
	printf("cArray[1] - 2nd element = %c\n",cArray[1]);
	printf("cArray[2] - 3rd element = %c\n",cArray[2]);
		
	cSize =  sizeof(char);
	cArray_Size = sizeof(cArray);
	cTotal_Elements = cArray_Size / cSize;
	
	printf("char size = %d bytes\n",cSize);
	printf("cArray size = %d bytes\n",cArray_Size);
	printf("cTotal_Elements = %d \n",cTotal_Elements);
	
	printf("\n\n");
	
	return (0);

}

/*


iArray[0] - 1st element = 44
iArray[1] - 2nd element = 56
iArray[2] - 3rd element = 2
int size = 4 bytes
iArray size = 20 bytes
iTotal_Elements = 5


fArray[0] - 1st element = 1.300000
fArray[1] - 2nd element = 5.600000
fArray[2] - 3rd element = 2.500000
float size = 4 bytes
fArray size = 20 bytes
fTotal_Elements = 5


cArray[0] - 1st element = s
cArray[1] - 2nd element = u
cArray[2] - 3rd element = n
char size = 1 bytes
cArray size = 5 bytes
cTotal_Elements = 5

*/