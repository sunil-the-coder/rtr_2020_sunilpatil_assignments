#include<stdio.h>

int main(void)
{

	int iArray_SMP[] = {44, 56, 2, 45, 89};
	int iSize_SMP;
	int iArray_Size_SMP;
	int iTotal_Elements_SMP;
	
	
	float fArray_SMP[] = {1.3f, 5.6f, 2.5f, 4.5f, 8.9f};
	int fSize_SMP;
	int fArray_Size_SMP;
	int fTotal_Elements_SMP;
	
	
	char cArray_SMP[] = {'s','u','n','i_SMP','l'};
	int cSize_SMP;
	int cArray_Size_SMP;
	int cTotal_Elements_SMP;
	
	int i_SMP;
	
	
	//Access integer array using piecemeal & display its valus.		
	iSize_SMP =  sizeof(int);
	iArray_Size_SMP = sizeof(iArray_SMP);
	iTotal_Elements_SMP = iArray_Size_SMP / iSize_SMP;
	
	for(i_SMP= 0; i_SMP < iTotal_Elements_SMP; i_SMP++)
	{
		printf("iArray_SMP[%d] - Element(%d) = %d\n",i_SMP,i_SMP+1,iArray_SMP[i_SMP]);
	}
	
	printf("int size = %d bytes\n",iSize_SMP);
	printf("iArray_SMP size = %d bytes\n",iArray_Size_SMP);
	printf("iTotal_Elements_SMP = %d \n",iTotal_Elements_SMP);
	
	printf("\n\n");
	
	//Access float array using piecemeal & display its valus.	
	
	fSize_SMP =  sizeof(float);
	fArray_Size_SMP = sizeof(fArray_SMP);
	fTotal_Elements_SMP = fArray_Size_SMP / fSize_SMP;
	
	for(i_SMP= 0; i_SMP < fTotal_Elements_SMP; i_SMP++)
	{
		printf("fArray_SMP[%d] - Element(%d) = %f\n",i_SMP,i_SMP+1,fArray_SMP[i_SMP]);
	}
	
	printf("float size = %d bytes\n",fSize_SMP);
	printf("fArray_SMP size = %d bytes\n",fArray_Size_SMP);
	printf("fTotal_Elements_SMP = %d \n",fTotal_Elements_SMP);
	
	printf("\n\n");
	
	//Access char array using piecemeal & display its valus.	
		
	cSize_SMP =  sizeof(char);
	cArray_Size_SMP = sizeof(cArray_SMP);
	cTotal_Elements_SMP = cArray_Size_SMP / cSize_SMP;
	
	for(i_SMP= 0; i_SMP < cTotal_Elements_SMP; i_SMP++)
	{
		printf("cArray_SMP[%d] - Element(%d) = %c\n",i_SMP,i_SMP+1,cArray_SMP[i_SMP]);
	}
	
	printf("char size = %d bytes\n",cSize_SMP);
	printf("cArray_SMP size = %d bytes\n",cArray_Size_SMP);
	printf("cTotal_Elements_SMP = %d \n",cTotal_Elements_SMP);
	
	printf("\n\n");
	
	return (0);

}

/*


iArray_SMP[0] - Element(1) = 44
iArray_SMP[1] - Element(2) = 56
iArray_SMP[2] - Element(3) = 2
iArray_SMP[3] - Element(4) = 45
iArray_SMP[4] - Element(5) = 89
int size = 4 bytes
iArray_SMP size = 20 bytes
iTotal_Elements_SMP = 5


fArray_SMP[0] - Element(1) = 1.300000
fArray_SMP[1] - Element(2) = 5.600000
fArray_SMP[2] - Element(3) = 2.500000
fArray_SMP[3] - Element(4) = 4.500000
fArray_SMP[4] - Element(5) = 8.900000
float size = 4 bytes
fArray_SMP size = 20 bytes
fTotal_Elements_SMP = 5


cArray_SMP[0] - Element(1) = s
cArray_SMP[1] - Element(2) = u
cArray_SMP[2] - Element(3) = n
cArray_SMP[3] - Element(4) = i_SMP
cArray_SMP[4] - Element(5) = l
char size = 1 bytes
cArray_SMP size = 5 bytes
cTotal_Elements_SMP = 5

*/