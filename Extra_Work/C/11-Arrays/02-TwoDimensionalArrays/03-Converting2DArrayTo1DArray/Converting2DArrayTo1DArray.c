#include <stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	
	int iArray_2D[NUM_ROWS][NUM_COLUMNS];
	int iArray_1D[NUM_ROWS * NUM_COLUMNS];
	
	int i,j;
	
	printf("\n\n");
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			iArray_2D[i][j] = i+1 * j+1;
		}
	}
	
	//Store 2D array data into 1D array using formula
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			iArray_1D[(i * NUM_COLUMNS) + j] = iArray_2D[i][j];
		}
	}
	
	for(i = 0; i < NUM_ROWS * NUM_COLUMNS; i++)
	{			
		printf("%d ",iArray_1D[i]);
	}
	
	printf("\n\n");
	
	return (0);
}

/*

1 2 3 2 3 4 3 4 5 4 5 6 5 6 7

*/