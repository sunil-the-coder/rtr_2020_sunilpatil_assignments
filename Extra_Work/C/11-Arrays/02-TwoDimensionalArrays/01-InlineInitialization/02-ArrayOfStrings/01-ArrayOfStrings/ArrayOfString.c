#include<stdio.h>

#define MAX_STRING_LENGTH 255

int main(void)
{

	char strArray_SMP[10][15] = { "Hello","Welcome","To","RTR2020-21","Batch"};
	
	int strSize_SMP;
	int strArray_Size_SMP;
	int strTotal_Elements_SMP, strRows_SMP, strColumns_SMP;
	int i_SMP, j_SMP;
	int i_total_characters_SMP;

	strSize_SMP =  sizeof(char);
	strArray_Size_SMP = sizeof(strArray_SMP);
	strRows_SMP = strArray_Size_SMP / sizeof(strArray_SMP[0]);
	strColumns_SMP = sizeof(strArray_SMP[0]) / strSize_SMP;	
	strTotal_Elements_SMP = strRows_SMP * strColumns_SMP;
	
	printf("int size = %d bytes\n",strSize_SMP);
	printf("strArray_SMP size = %d bytes\n",strArray_Size_SMP);
	printf("strRows_SMP = %d \n",strRows_SMP);
	printf("strColumns_SMP = %d \n",strColumns_SMP);
	printf("strTotal_Elements_SMP = %d \n",strTotal_Elements_SMP);
	printf("\n\n");
	
	
	i_total_characters_SMP = 0;
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		i_total_characters_SMP = i_total_characters_SMP + MyStrLength(strArray_SMP[i_SMP]);			
	}
	printf("Total characters = %d \n",i_total_characters_SMP);
	
	
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		printf("%s\n",strArray_SMP[i_SMP]);			
	}
	
	
	printf("\n\n");
	
	return (0);

}

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

int size = 1 bytes
strArray_SMP size = 150 bytes
strRows_SMP = 10
strColumns_SMP = 15
strTotal_Elements_SMP = 150


Total characters = 29
Hello
Welcome
To
RTR2020-21
Batch

*/