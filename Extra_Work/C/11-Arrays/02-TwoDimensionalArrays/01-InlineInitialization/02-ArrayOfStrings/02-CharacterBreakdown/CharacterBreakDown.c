#include<stdio.h>

#define MAX_STRING_LENGTH 255

int main(void)
{

	char strArray_SMP[5][15] = { "Hello","Welcome","To","RTR2020-21","Batch"};
	
	int iStrLengths_SMP[5]; //To store length of each string in 2d array
	
	int strSize_SMP;
	int strArray_Size_SMP;
	int strTotal_Elements_SMP, strRows_SMP, strColumns_SMP;
	int i_SMP, j_SMP;
	int i_total_characters_SMP;

	strSize_SMP =  sizeof(char);
	strArray_Size_SMP = sizeof(strArray_SMP);
	strRows_SMP = strArray_Size_SMP / sizeof(strArray_SMP[0]);
	strColumns_SMP = sizeof(strArray_SMP[0]) / strSize_SMP;	
	strTotal_Elements_SMP = strRows_SMP * strColumns_SMP;
	
	printf("int size = %d bytes\n",strSize_SMP);
	printf("strArray_SMP size = %d bytes\n",strArray_Size_SMP);
	printf("strRows_SMP = %d \n",strRows_SMP);
	printf("strColumns_SMP = %d \n",strColumns_SMP);
	printf("strTotal_Elements_SMP = %d \n",strTotal_Elements_SMP);
	printf("\n\n");
	
	
	i_total_characters_SMP = 0;
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		i_total_characters_SMP = i_total_characters_SMP + MyStrLength(strArray_SMP[i_SMP]);			
	}
	printf("Total characters = %d \n",i_total_characters_SMP);
	
	//Populate actual length of each string & store in array.
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		iStrLengths_SMP[i_SMP] = MyStrLength(strArray_SMP[i_SMP]);			
	}
	
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		printf("String %d => %s\n\n", (i_SMP+1), strArray_SMP[i_SMP]);
		
		for(j_SMP = 0; j_SMP < iStrLengths_SMP[i_SMP]; j_SMP++)
		{
			printf("Character %d = %c\n",(j_SMP+1),strArray_SMP[i_SMP][j_SMP]);
		}	

		printf("\n\n");		
	}
	
	
	printf("\n\n");
	
	return (0);

}

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

int size = 1 bytes
strArray_SMP size = 75 bytes
strRows_SMP = 5
strColumns_SMP = 15
strTotal_Elements_SMP = 75


Total characters = 29
String 1 => Hello

Character 1 = H
Character 2 = e
Character 3 = l
Character 4 = l
Character 5 = o


String 2 => Welcome

Character 1 = W
Character 2 = e
Character 3 = l
Character 4 = c
Character 5 = o
Character 6 = m
Character 7 = e


String 3 => To

Character 1 = T
Character 2 = o


String 4 => RTR2020-21

Character 1 = R
Character 2 = T
Character 3 = R
Character 4 = 2
Character 5 = 0
Character 6 = 2
Character 7 = 0
Character 8 = -
Character 9 = 2
Character 10 = 1


String 5 => Batch

Character 1 = B
Character 2 = a
Character 3 = t
Character 4 = c
Character 5 = h

*/