#include<stdio.h>

int main(void)
{

	int iArray_SMP[5][3] = {
			{10,20,30},
			{40,50,60},
			{70,80,90},
			{100,110,120},
			{130,140,150}};
	
	int iSize_SMP;
	int iArray_Size_SMP;
	int iTotal_Elements_SMP, iRows_SMP, iColumns_SMP;
	
		
	//Access integer array using piecemeal & display its values.	
	printf("Row 1 => \n");
	printf("iArray_SMP[0][0] - 1st element = %d\n",iArray_SMP[0][0]);
	printf("iArray_SMP[0][1] - 2nd element = %d\n",iArray_SMP[0][1]);
	printf("iArray_SMP[0][2] - 3rd element = %d\n",iArray_SMP[0][2]);
	
	printf("\n\n");
		
	printf("Row 2 => \n");
	printf("iArray_SMP[1][0] - 1st element = %d\n",iArray_SMP[1][0]);
	printf("iArray_SMP[1][1] - 2nd element = %d\n",iArray_SMP[1][1]);
	printf("iArray_SMP[1][2] - 3rd element = %d\n",iArray_SMP[1][2]);
	
	iSize_SMP =  sizeof(int);
	iArray_Size_SMP = sizeof(iArray_SMP);
	iRows_SMP = iArray_Size_SMP / sizeof(iArray_SMP[0]);
	iColumns_SMP = sizeof(iArray_SMP[0]) / iSize_SMP;	
	iTotal_Elements_SMP = iArray_Size_SMP / iSize_SMP;
	
	printf("int size = %d bytes\n",iSize_SMP);
	printf("iArray_SMP size = %d bytes\n",iArray_Size_SMP);
	printf("iRows_SMP = %d \n",iRows_SMP);
	printf("iColumns_SMP = %d \n",iColumns_SMP);
	printf("iTotal_Elements_SMP = %d \n",iTotal_Elements_SMP);
	printf("\n\n");
	
	
	printf("\n\n");
	
	return (0);

}

/*

Row 1 =>
iArray_SMP[0][0] - 1st element = 10
iArray_SMP[0][1] - 2nd element = 20
iArray_SMP[0][2] - 3rd element = 30


Row 2 =>
iArray_SMP[1][0] - 1st element = 40
iArray_SMP[1][1] - 2nd element = 50
iArray_SMP[1][2] - 3rd element = 60
int size = 4 bytes
iArray_SMP size = 60 bytes
iRows_SMP = 5
iColumns_SMP = 3
iTotal_Elements_SMP = 15

*/