#include<stdio.h>

int main(void)
{

	int iArray_SMP[3][3];
	
	int iSize_SMP;
	int iArray_Size_SMP;
	int iTotal_Elements_SMP, iRows_SMP, iColumns_SMP;
	int i_SMP, j_SMP;
		
	iSize_SMP =  sizeof(int);
	iArray_Size_SMP = sizeof(iArray_SMP);
	iRows_SMP = iArray_Size_SMP / sizeof(iArray_SMP[0]);
	iColumns_SMP = sizeof(iArray_SMP[0]) / iSize_SMP;	
	iTotal_Elements_SMP = iArray_Size_SMP / iSize_SMP;
	
	printf("int size = %d bytes\n",iSize_SMP);
	printf("iArray_SMP size = %d bytes\n",iArray_Size_SMP);
	printf("iRows_SMP = %d \n",iRows_SMP);
	printf("iColumns_SMP = %d \n",iColumns_SMP);
	printf("iTotal_Elements_SMP = %d \n",iTotal_Elements_SMP);
	printf("\n\n");
	
	iArray_SMP[0][0] = 10;
	iArray_SMP[0][1] = 20;
	iArray_SMP[0][2] = 30;
	
	
	iArray_SMP[1][0] = 40;
	iArray_SMP[1][1] = 50;
	iArray_SMP[1][2] = 60;
	
	iArray_SMP[2][0] = 70;
	iArray_SMP[2][1] = 80;
	iArray_SMP[2][2] = 90;
	
	for(i_SMP = 0; i_SMP < iRows_SMP; i_SMP++)
	{
		printf("======== Row %d ==========\n",(i_SMP+1));
		for(j_SMP = 0; j_SMP < iColumns_SMP; j_SMP++)
		{
			printf("iArray_SMP[%d][%d] = %d\n",i_SMP,j_SMP,iArray_SMP[i_SMP][j_SMP]);
		}
		
		printf("\n\n");
	}
	
	
	
	
	printf("\n\n");
	
	return (0);

}

/*

int size = 4 bytes
iArray_SMP size = 36 bytes
iRows_SMP = 3
iColumns_SMP = 3
iTotal_Elements_SMP = 9


======== Row 1 ==========
iArray_SMP[0][0] = 10
iArray_SMP[0][1] = 20
iArray_SMP[0][2] = 30


======== Row 2 ==========
iArray_SMP[1][0] = 40
iArray_SMP[1][1] = 50
iArray_SMP[1][2] = 60


======== Row 3 ==========
iArray_SMP[2][0] = 70
iArray_SMP[2][1] = 80
iArray_SMP[2][2] = 90

*/