#include<stdio.h>

#define MAX_STRING_LENGTH 255

int main(void)
{

	void MyStrcpy(char[], char[]);
	
	char strArray_SMP[5][15];
	
	int strSize_SMP;
	int strArray_Size_SMP;
	int strTotal_Elements_SMP, strRows_SMP, strColumns_SMP;
	int i_SMP;
	int i_total_characters_SMP;

	strSize_SMP =  sizeof(char);
	strArray_Size_SMP = sizeof(strArray_SMP);
	strRows_SMP = strArray_Size_SMP / sizeof(strArray_SMP[0]);
	strColumns_SMP = sizeof(strArray_SMP[0]) / strSize_SMP;	
	strTotal_Elements_SMP = strRows_SMP * strColumns_SMP;
	
	printf("char size = %d bytes\n",strSize_SMP);
	printf("strArray_SMP size = %d bytes\n",strArray_Size_SMP);
	printf("strRows_SMP = %d \n",strRows_SMP);
	printf("strColumns_SMP = %d \n",strColumns_SMP);
	printf("strTotal_Elements_SMP = %d \n",strTotal_Elements_SMP);
	printf("\n\n");
	
	
	MyStrcpy(strArray_SMP[0],"Hello");
	MyStrcpy(strArray_SMP[1],"My");
	MyStrcpy(strArray_SMP[2],"Name");
	MyStrcpy(strArray_SMP[3],"Is");
	MyStrcpy(strArray_SMP[4],"Sunil Patil");
	
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		printf("%s\n",strArray_SMP[i_SMP]);			
	}
	
	
	printf("\n\n");
	
	return (0);

}

void MyStrcpy(char str_destination_SMP[], char str_source_SMP[])
{
	int MyStrLength(char[]);
	
	int length_SMP = 0;
	int i_SMP;
	
	//code
	length_SMP = MyStrLength(str_source_SMP);
	
	for (i_SMP = 0; i_SMP < length_SMP; i_SMP++)
		str_destination_SMP[i_SMP] = str_source_SMP[i_SMP];
	
	str_destination_SMP[i_SMP] = '\0';
}
 

int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

char size = 1 bytes
strArray_SMP size = 75 bytes
strRows_SMP = 5
strColumns_SMP = 15
strTotal_Elements_SMP = 75


Hello
My
Name
Is
Sunil Patil

*/