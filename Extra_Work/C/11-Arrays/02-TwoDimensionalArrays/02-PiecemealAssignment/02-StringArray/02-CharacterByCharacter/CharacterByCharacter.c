#include<stdio.h>

#define MAX_STRING_LENGTH 255

int main(void)
{

	void MyStrcpy(char[], char[]);
	
	char strArray_SMP[2][10];
	
	int strSize_SMP;
	int strArray_Size_SMP;
	int strTotal_Elements_SMP, strRows_SMP, strColumns_SMP;
	int i_SMP;
	int i_total_characters_SMP;

	strSize_SMP =  sizeof(char);
	strArray_Size_SMP = sizeof(strArray_SMP);
	strRows_SMP = strArray_Size_SMP / sizeof(strArray_SMP[0]);
	strColumns_SMP = sizeof(strArray_SMP[0]) / strSize_SMP;	
	strTotal_Elements_SMP = strRows_SMP * strColumns_SMP;
	
	printf("char size = %d bytes\n",strSize_SMP);
	printf("strArray_SMP size = %d bytes\n",strArray_Size_SMP);
	printf("strRows_SMP = %d \n",strRows_SMP);
	printf("strColumns_SMP = %d \n",strColumns_SMP);
	printf("strTotal_Elements_SMP = %d \n",strTotal_Elements_SMP);
	printf("\n\n");
	
	strArray_SMP[0][0] = 'H';
	strArray_SMP[0][1] = 'e';
	strArray_SMP[0][2] = 'l';
	strArray_SMP[0][3] = 'l';
	strArray_SMP[0][4] = 'o';
	strArray_SMP[0][5] = '\0';
	
	strArray_SMP[1][0] = 'R';
	strArray_SMP[1][1] = 'T';
	strArray_SMP[1][2] = 'R';
	strArray_SMP[1][3] = '\0';
	
	for(i_SMP = 0; i_SMP < strRows_SMP; i_SMP++)
	{
		printf("%s\n",strArray_SMP[i_SMP]);			
	}	
	
	printf("\n\n");
	
	return (0);

}


int MyStrLength(char str[])
{
	int length_SMP,i_SMP;
	
	i_SMP = 0, length_SMP = 0;
	
	while( i_SMP < MAX_STRING_LENGTH)
	{
		if(str[i_SMP] == '\0')
			break;
		else
			length_SMP++;
		i_SMP++;
	}
	
	return (length_SMP);
}

/*

char size = 1 bytes
strArray_SMP size = 20 bytes
strRows_SMP = 2
strColumns_SMP = 10
strTotal_Elements_SMP = 20


Hello
RTR

*/
