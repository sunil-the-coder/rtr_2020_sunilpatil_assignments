#include<stdio.h>

int main(void)
{

	int iArray_SMP[5][3][2] = {
			{{10,20},{30,40},{50,60}},
			{{70,80},{90,100},{530,610}},
			{{1,2},{3,4},{5,6}},
			{{101,202},{303,404},{505,606}},
			{{107,207},{308,409},{501,601}}
			};
	
	int iSize_SMP;
	int iArray_Size_SMP;
	int iTotal_Elements_SMP, iRows_SMP, iColumns_SMP, iDepth_SMP;
	
	int i_SMP,j_SMP,k_SMP;
	
	printf("\n\n");
	
	
	iSize_SMP =  sizeof(int);
	iArray_Size_SMP = sizeof(iArray_SMP);
	iRows_SMP = iArray_Size_SMP / sizeof(iArray_SMP[0]);
	iColumns_SMP = sizeof(iArray_SMP[0]) / sizeof(iArray_SMP[0][0]);
	iDepth_SMP = sizeof(iArray_SMP[0][0]) / iSize_SMP;
		
	iTotal_Elements_SMP = iRows_SMP * iColumns_SMP * iDepth_SMP;
			
	printf("int size = %d bytes\n",iSize_SMP);
	printf("iArray_SMP size = %d bytes\n",iArray_Size_SMP);
	printf("iRows_SMP = %d \n",iRows_SMP);
	printf("iColumns_SMP = %d \n",iColumns_SMP);
	printf("iDepth_SMP = %d \n",iDepth_SMP);
	printf("iTotal_Elements_SMP = %d \n",iTotal_Elements_SMP);
	
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP < iRows_SMP; i_SMP++)
	{
		printf("-- Row %d ---- \n",(i_SMP+1));
		for(j_SMP = 0; j_SMP < iColumns_SMP; j_SMP++)
		{			
			printf("-- Column %d ---- \n",(j_SMP+1));
			for(k_SMP = 0; k_SMP < iDepth_SMP; k_SMP++)
			{
				printf("iArray[%d][%d][%d] = %d\n",i_SMP,j_SMP,k_SMP,iArray_SMP[i_SMP][j_SMP][k_SMP]);				
			}
			printf("\n");
		}
	}
	
	printf("\n\n");
	
	return (0);

}

/*

int size = 4 bytes
iArray_SMP size = 120 bytes
iRows_SMP = 5
iColumns_SMP = 3
iDepth_SMP = 2
iTotal_Elements_SMP = 30


-- Row 1 ----
-- Column 1 ----
iArray[0][0][0] = 10
iArray[0][0][1] = 20

-- Column 2 ----
iArray[0][1][0] = 30
iArray[0][1][1] = 40

-- Column 3 ----
iArray[0][2][0] = 50
iArray[0][2][1] = 60

-- Row 2 ----
-- Column 1 ----
iArray[1][0][0] = 70
iArray[1][0][1] = 80

-- Column 2 ----
iArray[1][1][0] = 90
iArray[1][1][1] = 100

-- Column 3 ----
iArray[1][2][0] = 530
iArray[1][2][1] = 610

-- Row 3 ----
-- Column 1 ----
iArray[2][0][0] = 1
iArray[2][0][1] = 2

-- Column 2 ----
iArray[2][1][0] = 3
iArray[2][1][1] = 4

-- Column 3 ----
iArray[2][2][0] = 5
iArray[2][2][1] = 6

-- Row 4 ----
-- Column 1 ----
iArray[3][0][0] = 101
iArray[3][0][1] = 202

-- Column 2 ----
iArray[3][1][0] = 303
iArray[3][1][1] = 404

-- Column 3 ----
iArray[3][2][0] = 505
iArray[3][2][1] = 606

-- Row 5 ----
-- Column 1 ----
iArray[4][0][0] = 107
iArray[4][0][1] = 207

-- Column 2 ----
iArray[4][1][0] = 308
iArray[4][1][1] = 409

-- Column 3 ----
iArray[4][2][0] = 501
iArray[4][2][1] = 601


*/