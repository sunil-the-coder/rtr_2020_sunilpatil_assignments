#include<stdio.h>

int main(void)
{

	int iArray_SMP[5][3][2] = {
			{{10,20},{30,40},{50,60}},
			{{70,80},{90,100},{530,610}},
			{{1,2},{3,4},{5,6}},
			{{101,202},{303,404},{505,606}},
			{{107,207},{308,409},{501,601}}
			};
	
	int iSize_SMP;
	int iArray_Size_SMP;
	int iTotal_Elements_SMP, iRows_SMP, iColumns_SMP, iDepth_SMP;
	
	printf("\n\n");
	
	//Access integer array using piecemeal & display its values.	
	printf("Row 1 = Column 1 => \n");
	printf("iArray_SMP[0][0][0] - 1st element = %d\n",iArray_SMP[0][0][0]);
	printf("iArray_SMP[0][0][1] - 2nd element = %d\n",iArray_SMP[0][0][1]);
	
	printf("\n\n");
	
	printf("Row 1 => Column 2 \n");
	printf("iArray_SMP[0][1][0] - 1st element = %d\n",iArray_SMP[0][1][0]);
	printf("iArray_SMP[0][1][1] - 2nd element = %d\n",iArray_SMP[0][1][1]);	
	
	printf("\n\n");
	
	printf("Row 1 => Column 3 \n");
	printf("iArray_SMP[0][2][0] - 1st element = %d\n",iArray_SMP[0][2][0]);
	printf("iArray_SMP[0][2][1] - 2nd element = %d\n",iArray_SMP[0][2][1]);	
	
	printf("\n\n");
	
	printf("Row 2 = Column 1 => \n");
	printf("iArray_SMP[1][0][0] - 1st element = %d\n",iArray_SMP[1][0][0]);
	printf("iArray_SMP[1][0][1] - 2nd element = %d\n",iArray_SMP[1][0][1]);
	
	printf("\n\n");	
	
	iSize_SMP =  sizeof(int);
	iArray_Size_SMP = sizeof(iArray_SMP);
	iRows_SMP = iArray_Size_SMP / sizeof(iArray_SMP[0]);
	iColumns_SMP = sizeof(iArray_SMP[0]) / sizeof(iArray_SMP[0][0]);
	iDepth_SMP = sizeof(iArray_SMP[0][0]) / iSize_SMP;
		
	iTotal_Elements_SMP = iRows_SMP * iColumns_SMP * iDepth_SMP;
			
	printf("int size = %d bytes\n",iSize_SMP);
	printf("iArray_SMP size = %d bytes\n",iArray_Size_SMP);
	printf("iRows_SMP = %d \n",iRows_SMP);
	printf("iColumns_SMP = %d \n",iColumns_SMP);
	printf("iDepth_SMP = %d \n",iDepth_SMP);
	printf("iTotal_Elements_SMP = %d \n",iTotal_Elements_SMP);
	printf("\n\n");
	
	
	printf("\n\n");
	
	return (0);

}

/*

Row 1 = Column 1 =>
iArray_SMP[0][0][0] - 1st element = 10
iArray_SMP[0][0][1] - 2nd element = 20

Row 1 => Column 2
iArray_SMP[0][1][0] - 1st element = 30
iArray_SMP[0][1][1] - 2nd element = 40

Row 1 => Column 3
iArray_SMP[0][2][0] - 1st element = 50
iArray_SMP[0][2][1] - 2nd element = 60

Row 2 = Column 1 =>
iArray_SMP[1][0][0] - 1st element = 70
iArray_SMP[1][0][1] - 2nd element = 80

int size = 4 bytes
iArray_SMP size = 120 bytes
iRows_SMP = 5
iColumns_SMP = 3
iDepth_SMP = 2
iTotal_Elements_SMP = 30


*/