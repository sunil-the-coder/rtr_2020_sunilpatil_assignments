#include <stdio.h>

#define NUM_ROWS_SMP 5
#define NUM_COLUMNS_SMP 3
#define DEPTH_SMP 2

int main(void)
{
		
	int iArray_SMP[NUM_ROWS_SMP][NUM_COLUMNS_SMP][DEPTH_SMP] = {
			{{10,20},{30,40},{50,60}},
			{{70,80},{90,100},{530,610}},
			{{1,2},{3,4},{5,6}},
			{{101,202},{303,404},{505,606}},
			{{107,207},{308,409},{501,601}}
			};
			
	int iArray_2D[NUM_ROWS_SMP][NUM_COLUMNS_SMP * DEPTH_SMP];
	
	int i_SMP,j_SMP,k_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP; i_SMP++)
	{
		for(j_SMP = 0; j_SMP < NUM_COLUMNS_SMP; j_SMP++)
		{
			for(k_SMP = 0; k_SMP < DEPTH_SMP; k_SMP++)
			{
				printf("iArray[%d][%d][%d] = %d\n", i_SMP, j_SMP, k_SMP, iArray_SMP[i_SMP][j_SMP][k_SMP]);
			}
			
			printf("\n");			
		}
		printf("\n");
	}
	
	printf("\n\n");
	
	//Store 3D to 2D 
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP; i_SMP++)
	{
		for(j_SMP = 0; j_SMP < NUM_COLUMNS_SMP; j_SMP++)
		{
			for(k_SMP = 0; k_SMP < DEPTH_SMP; k_SMP++) {
				iArray_2D[i_SMP][(j_SMP * DEPTH_SMP) + k_SMP] = iArray_SMP[i_SMP][j_SMP][k_SMP];
			}
		}
	}
	
	//Display 2D array
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP; i_SMP++)
	{
		for(j_SMP = 0; j_SMP < NUM_ROWS_SMP * NUM_COLUMNS_SMP; j_SMP++) {			
			printf("iArray_2d[%d][%d] = %d\n",i_SMP,j_SMP,iArray_2D[i_SMP][j_SMP]);
		}
	}
	
	printf("\n\n");
	
	return (0);
}

/*

iArray[0][0][0] = 10
iArray[0][0][1] = 20

iArray[0][1][0] = 30
iArray[0][1][1] = 40

iArray[0][2][0] = 50
iArray[0][2][1] = 60


iArray[1][0][0] = 70
iArray[1][0][1] = 80

iArray[1][1][0] = 90
iArray[1][1][1] = 100

iArray[1][2][0] = 530
iArray[1][2][1] = 610


iArray[2][0][0] = 1
iArray[2][0][1] = 2

iArray[2][1][0] = 3
iArray[2][1][1] = 4

iArray[2][2][0] = 5
iArray[2][2][1] = 6


iArray[3][0][0] = 101
iArray[3][0][1] = 202

iArray[3][1][0] = 303
iArray[3][1][1] = 404

iArray[3][2][0] = 505
iArray[3][2][1] = 606


iArray[4][0][0] = 107
iArray[4][0][1] = 207

iArray[4][1][0] = 308
iArray[4][1][1] = 409

iArray[4][2][0] = 501
iArray[4][2][1] = 601






iArray_2d[0][0] = 10
iArray_2d[0][1] = 20
iArray_2d[0][2] = 30
iArray_2d[0][3] = 40
iArray_2d[0][4] = 50
iArray_2d[0][5] = 60
iArray_2d[0][6] = 70
iArray_2d[0][7] = 80
iArray_2d[0][8] = 90
iArray_2d[0][9] = 100
iArray_2d[0][10] = 530
iArray_2d[0][11] = 610
iArray_2d[0][12] = 1
iArray_2d[0][13] = 2
iArray_2d[0][14] = 3
iArray_2d[1][0] = 70
iArray_2d[1][1] = 80
iArray_2d[1][2] = 90
iArray_2d[1][3] = 100
iArray_2d[1][4] = 530
iArray_2d[1][5] = 610
iArray_2d[1][6] = 1
iArray_2d[1][7] = 2
iArray_2d[1][8] = 3
iArray_2d[1][9] = 4
iArray_2d[1][10] = 5
iArray_2d[1][11] = 6
iArray_2d[1][12] = 101
iArray_2d[1][13] = 202
iArray_2d[1][14] = 303
iArray_2d[2][0] = 1
iArray_2d[2][1] = 2
iArray_2d[2][2] = 3
iArray_2d[2][3] = 4
iArray_2d[2][4] = 5
iArray_2d[2][5] = 6
iArray_2d[2][6] = 101
iArray_2d[2][7] = 202
iArray_2d[2][8] = 303
iArray_2d[2][9] = 404
iArray_2d[2][10] = 505
iArray_2d[2][11] = 606
iArray_2d[2][12] = 107
iArray_2d[2][13] = 207
iArray_2d[2][14] = 308
iArray_2d[3][0] = 101
iArray_2d[3][1] = 202
iArray_2d[3][2] = 303
iArray_2d[3][3] = 404
iArray_2d[3][4] = 505
iArray_2d[3][5] = 606
iArray_2d[3][6] = 107
iArray_2d[3][7] = 207
iArray_2d[3][8] = 308
iArray_2d[3][9] = 409
iArray_2d[3][10] = 501
iArray_2d[3][11] = 601
iArray_2d[3][12] = 10
iArray_2d[3][13] = 20
iArray_2d[3][14] = 30
iArray_2d[4][0] = 107
iArray_2d[4][1] = 207
iArray_2d[4][2] = 308
iArray_2d[4][3] = 409
iArray_2d[4][4] = 501
iArray_2d[4][5] = 601
iArray_2d[4][6] = 10
iArray_2d[4][7] = 20
iArray_2d[4][8] = 30
iArray_2d[4][9] = 40
iArray_2d[4][10] = 50
iArray_2d[4][11] = 60
iArray_2d[4][12] = 70
iArray_2d[4][13] = 80
iArray_2d[4][14] = 90

*/