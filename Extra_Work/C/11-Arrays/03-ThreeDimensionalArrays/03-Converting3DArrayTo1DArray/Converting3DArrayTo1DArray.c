
#include <stdio.h>

#define NUM_ROWS_SMP 5
#define NUM_COLUMNS_SMP 3
#define DEPTH_SMP 2

int main(void)
{
		
	int iArray_SMP[NUM_ROWS_SMP][NUM_COLUMNS_SMP][DEPTH_SMP] = {
			{{10,20},{30,40},{50,60}},
			{{70,80},{90,100},{530,610}},
			{{1,2},{3,4},{5,6}},
			{{101,202},{303,404},{505,606}},
			{{107,207},{308,409},{501,601}}
			};
			
	int iArray_1D[NUM_ROWS_SMP * NUM_COLUMNS_SMP * DEPTH_SMP];
	
	int i_SMP,j_SMP,k_SMP;
	int index_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP; i_SMP++)
	{
		for(j_SMP = 0; j_SMP < NUM_COLUMNS_SMP; j_SMP++)
		{
			for(k_SMP = 0; k_SMP < DEPTH_SMP; k_SMP++)
			{
				printf("iArray[%d][%d][%d] = %d\n", i_SMP, j_SMP, k_SMP, iArray_SMP[i_SMP][j_SMP][k_SMP]);
			}
			
			printf("\n");			
		}
		printf("\n");
	}
	
	printf("\n\n");
	
	//Store 3D to 1D 
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP; i_SMP++)
	{
		for(j_SMP = 0; j_SMP < NUM_COLUMNS_SMP; j_SMP++)
		{
			for(k_SMP = 0; k_SMP < DEPTH_SMP; k_SMP++) {
				
				index_SMP = (i_SMP * NUM_COLUMNS_SMP * DEPTH_SMP) + (j_SMP * DEPTH_SMP) + k_SMP;
				
				iArray_1D[index_SMP] = iArray_SMP[i_SMP][j_SMP][k_SMP];
			}
		}
	}
	
	//Display 1D array
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP < NUM_ROWS_SMP * NUM_COLUMNS_SMP * DEPTH_SMP; i_SMP++)
	{
			printf("iArray_1d[%d] = %d\n",i_SMP, iArray_1D[i_SMP]);
		
	}
	
	printf("\n\n");
	
	return (0);
}

/*

iArray[0][0][0] = 10
iArray[0][0][1] = 20

iArray[0][1][0] = 30
iArray[0][1][1] = 40

iArray[0][2][0] = 50
iArray[0][2][1] = 60


iArray[1][0][0] = 70
iArray[1][0][1] = 80

iArray[1][1][0] = 90
iArray[1][1][1] = 100

iArray[1][2][0] = 530
iArray[1][2][1] = 610


iArray[2][0][0] = 1
iArray[2][0][1] = 2

iArray[2][1][0] = 3
iArray[2][1][1] = 4

iArray[2][2][0] = 5
iArray[2][2][1] = 6


iArray[3][0][0] = 101
iArray[3][0][1] = 202

iArray[3][1][0] = 303
iArray[3][1][1] = 404

iArray[3][2][0] = 505
iArray[3][2][1] = 606


iArray[4][0][0] = 107
iArray[4][0][1] = 207

iArray[4][1][0] = 308
iArray[4][1][1] = 409

iArray[4][2][0] = 501
iArray[4][2][1] = 601






iArray_1d[0] = 10
iArray_1d[1] = 20
iArray_1d[2] = 30
iArray_1d[3] = 40
iArray_1d[4] = 50
iArray_1d[5] = 60
iArray_1d[6] = 70
iArray_1d[7] = 80
iArray_1d[8] = 90
iArray_1d[9] = 100
iArray_1d[10] = 530
iArray_1d[11] = 610
iArray_1d[12] = 1
iArray_1d[13] = 2
iArray_1d[14] = 3
iArray_1d[15] = 4
iArray_1d[16] = 5
iArray_1d[17] = 6
iArray_1d[18] = 101
iArray_1d[19] = 202
iArray_1d[20] = 303
iArray_1d[21] = 404
iArray_1d[22] = 505
iArray_1d[23] = 606
iArray_1d[24] = 107
iArray_1d[25] = 207
iArray_1d[26] = 308
iArray_1d[27] = 409
iArray_1d[28] = 501
iArray_1d[29] = 601
*/