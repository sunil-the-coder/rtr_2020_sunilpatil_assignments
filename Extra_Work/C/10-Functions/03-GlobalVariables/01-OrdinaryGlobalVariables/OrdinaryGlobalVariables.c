#include<stdio.h>

//Global scope of code.

//Good Practice to initialize the global variables with some values otherwise there will be default based on type of data ( 0, 0.0,'\0')

int globalVariable = 0;

int main(void)
{
	void UpdateCountOne(void);
	void UpdateCountTwo(void);
	void UpdateCountThree(void);
	
	UpdateCountOne();
	UpdateCountTwo();
	UpdateCountThree();
	
	return (0);
}

void UpdateCountOne(void)
{
	globalVariable = 100;
	printf("Global variable value (count_one):%d\n\n",globalVariable);
}

void UpdateCountTwo(void)
{
	globalVariable += 200;
	printf("Global variable value (count_one):%d\n\n",globalVariable);
}

void UpdateCountThree(void)
{
	globalVariable = 1;
	printf("Global variable value (count_one):%d\n\n",globalVariable);
}

/*


Global variable value (count_one):100

Global variable value (count_one):300

Global variable value (count_one):1


*/

