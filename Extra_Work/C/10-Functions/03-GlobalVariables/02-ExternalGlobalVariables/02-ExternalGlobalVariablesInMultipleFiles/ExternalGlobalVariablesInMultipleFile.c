#include<stdio.h>

//Global scope of code.
int globalCount_SMP = 0;

int main(void)
{
	void UpdateCountOne(void);
	void UpdateCountTwo(void);
	void UpdateCountThree(void);
	
	printf("Global variable value before (count_one):%d\n\n",globalCount_SMP);
	
	UpdateCountOne();
	UpdateCountTwo(); // defined in File_01.c
	UpdateCountThree(); // defined in File_02.c
	
	printf("Global variable value after (3):%d\n\n",globalCount_SMP);
		
	return (0);
}

void UpdateCountOne(void)
{
	globalCount_SMP = globalCount_SMP + 1;
	printf("Global variable value (count_one):%d\n\n",globalCount_SMP);
}

/*

>cl ExternalGlobalVariablesInMultipleFile.c File_01.c File_02.c


Global variable value before (count_one):0

Global variable value (count_one):1

Global variable value (count_two):11 in File_01.c

Global variable value (count_three):31 in File_02.c

Global variable value after (3):31


*/



