#include<stdio.h>

//Global scope of code.

int main(void)
{
	void UpdateCountOne(void);
	
	//global variable declaration
	extern int globalVariable;
	printf("Global variable value before (count_one):%d\n\n",globalVariable);
	
	UpdateCountOne();
	
	printf("Global variable value after (count_one):%d\n\n",globalVariable);
	
	return (0);
}

//globalVariable declared before UpdateCountOne, so it can access directly & use as ordinary variable but main can't as it after declared.  So it must be redeclared in main() as an external global variable by means of the 'extern' keyword and type of the variable.
//Now you can access as ordinary variable in main as well.

int globalVariable = 0;
void UpdateCountOne(void)
{
	globalVariable = 100;
	printf("Global variable value (count_one):%d\n\n",globalVariable);
}





/*

Global variable value before (count_one):0

Global variable value (count_one):100

Global variable value after (count_one):100


*/

