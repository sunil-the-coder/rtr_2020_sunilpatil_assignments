#include <stdio.h>

//1. Calling some functions from main method + rest trace their call indirect to main

int main(int argc, char * argv[], char *envp[])
{

	//function prototype
	void DisplayInformation(void); 
	void DisplayMessage(void); 
	
	//function calls
	DisplayInformation();
	DisplayMessage();
	
	return (0);
}

void DisplayInformation(void)
{
	void DisplayFirstName(void);
	void DisplayLastName(void);
	
	DisplayFirstName();
	DisplayLastName();
} 

void DisplayFirstName(void)
{
	printf("Sunil \n\n");
}

void DisplayLastName(void)
{
	printf("Patil \n\n");
}

void DisplayMessage(void)
{
	printf("I Live in most beautiful country INDIA.\n\n");
}


/*

Sunil

Patil

I Live in most beautiful country INDIA.

*/