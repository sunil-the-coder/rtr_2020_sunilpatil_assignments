#include <stdio.h>

//1. Calling some functions from main method + rest trace their call indirect to main

//Interesting One. As you go deep and deep you understand the flow.

int main(int argc, char * argv[], char *envp[])
{

	//function prototype
	void DisplayMessage(void); 
	
	//function calls
	DisplayMessage();
	
	printf("Back in Main !!!\n\n");
	
	return (0);
}


void DisplayMessage(void)
{
	void DisplayLastName();
	
	DisplayLastName();
	
	printf("I Live in most beautiful country INDIA.\n\n");
}

void DisplayLastName(void)
{
	void DisplayFirstName();
	
	DisplayFirstName();
	
	printf("Patil \n\n");
}

void DisplayFirstName(void)
{
	printf("Sunil \n\n");
}


/*

Sunil

Patil

I Live in most beautiful country INDIA.

Back in Main !!!

*/