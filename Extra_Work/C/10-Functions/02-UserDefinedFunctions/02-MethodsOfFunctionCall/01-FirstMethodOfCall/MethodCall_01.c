#include <stdio.h>

//1. Calling all functions from main method.

int main(int argc, char * argv[], char *envp[])
{

	//function prototype / declaration / signature
	void MyAddition(void); 
	int MySubstraction(void); 
	void MyMultiplication(int,int); 
	int MyDivision(int,int); 
	
	int a_SMP, b_SMP;	
	int result_substraction_SMP,result_division_SMP;
		
	//Function call
	MyAddition();
		
	result_substraction_SMP = MySubstraction();
	printf("Substracton Result = %d\n\n",result_substraction_SMP);
		
	MyMultiplication(10,20);

	result_division_SMP = MyDivision(50,2);
	printf("Division Result = %d\n\n",result_division_SMP);
	
	return (0);
}

void MyAddition(void)
{
	int a_SMP, b_SMP, result_SMP;
	
	printf("\n\n");
	printf("Enter integer value for a_SMP:");	
	scanf("%d",&a_SMP);
	
	printf("Enter integer value for b_SMP:");	
	scanf("%d",&b_SMP);
		
	result_SMP = a_SMP + b_SMP;
	printf("Addition Result = %d\n\n",result_SMP);
	
} 

int MySubstraction(void)
{
	int a_SMP, b_SMP, result_SMP;
	
	printf("\n\n");
	printf("Enter integer value for a_SMP:");	
	scanf("%d",&a_SMP);
	
	printf("Enter integer value for b_SMP:");	
	scanf("%d",&b_SMP);
		
	result_SMP = a_SMP - b_SMP;
	
	return (result_SMP);
	
}

void MyMultiplication(int a_SMP, int b_SMP)
{
	int result_SMP;
		
	result_SMP = a_SMP * b_SMP;
	
	printf("Muliplication Result = %d\n\n",result_SMP);
	
}

int MyDivision(int a_SMP, int b_SMP)
{
	int result_SMP;
		
	if(a_SMP > b_SMP)
		result_SMP = a_SMP / b_SMP;
	else
		result_SMP = b_SMP / a_SMP;
	
	return (result_SMP);
	
}


/*

Enter integer value for a_SMP:10
Enter integer value for b_SMP:20
Addition Result = 30



Enter integer value for a_SMP:30
Enter integer value for b_SMP:40
Substracton Result = -10

Muliplication Result = 200

Division Result = 25

*/