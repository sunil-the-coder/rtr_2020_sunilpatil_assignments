#include <stdio.h>


int main(void)
{
	
	int AddTwoNumbers(int, int);
	
	int num1, num2, ans;
	
	num1 = 100;
	num2  = 20;
	
	ans = AddTwoNumbers(num1, num2); //execute and store return value using 3rd variable.
	
	printf("Answer is:%d\n",ans);
	
	
	printf("Answer is:%d\n",AddTwoNumbers(100, 25)); // Execute direct function and print return value without storing into third variable.
	
	AddTwoNumbers(20,25); //Ignore return value.
	
	return (0);	
}

int AddTwoNumbers(int num1, int num2)
{
	return (num1 + num2);
}

/*

Answer is:120
Answer is:125

*/

