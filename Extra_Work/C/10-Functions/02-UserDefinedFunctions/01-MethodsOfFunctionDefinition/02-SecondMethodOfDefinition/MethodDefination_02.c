#include <stdio.h>


int main(int argc, char * argv[], char *envp[])
{
	
	//function prototype / declaration / signature
	int MyAddition(void); 

	int result_SMP;
		
	//Function call
	result_SMP = MyAddition();
	
	printf("Result = %d\n\n",result_SMP);
	
	return (0);
}

//Function returning value
int MyAddition(void)
{
	int a_SMP, b_SMP, sum_SMP;
	
	printf("\n\n");
	printf("Enter integer value for a_SMP:");	
	scanf("%d",&a_SMP);
	
	printf("Enter integer value for b_SMP:");	
	scanf("%d",&b_SMP);
		
	sum_SMP = a_SMP + b_SMP;
	
	return (sum_SMP);
	
} 


/*

Enter integer value for a_SMP:10
Enter integer value for b_SMP:4
Result = 14

*/