#include <stdio.h>


int main(int argc, char * argv[], char *envp[])
{

	//function prototype / declaration / signature
	void MyAddition(void); 
	
	//Function call
	MyAddition();
		
	return (0);
}

void MyAddition(void)
{
	int a_SMP, b_SMP, sum_SMP;
	
	printf("\n\n");
	printf("Enter integer value for a_SMP:");	
	scanf("%d",&a_SMP);
	
	printf("Enter integer value for b_SMP:");	
	scanf("%d",&b_SMP);
	
	
	sum_SMP = a_SMP + b_SMP;
	printf("Sum = %d\n\n",sum_SMP);
	
} 


/*

Enter integer value for a_SMP:10
Enter integer value for b_SMP:20
Sum = 30

*/