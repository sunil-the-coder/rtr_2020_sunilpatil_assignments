#include <stdio.h>


int main(int argc, char * argv[], char *envp[])
{
	
	//function prototype / declaration / signature
	void MyAddition(int,int); 
		
	int a_SMP, b_SMP;
	
	printf("\n\n");
	printf("Enter integer value for a_SMP:");	
	scanf("%d",&a_SMP);
	
	printf("Enter integer value for b_SMP:");	
	scanf("%d",&b_SMP);
		
	//Function call
	MyAddition(a_SMP,b_SMP);
		
	return (0);
}

//Function accept parameters but don't return the value
void MyAddition(int a_SMP, int b_SMP)
{
	int result_SMP;
	
	result_SMP = a_SMP + b_SMP;
	
	printf("Result = %d\n\n",result_SMP);	

} 


/*

Enter integer value for a_SMP:14
Enter integer value for b_SMP:12
Result = 26

*/