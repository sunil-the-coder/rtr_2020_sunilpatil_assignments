#include<stdio.h>

#define MAX(a, b)  ((a > b) ? a : b)

int main(void)
{
	int iResult_SMP;
	float fResult_SMP;
	
	printf("\n\n");
	
	iResult_SMP = MAX(10,5);
	fResult_SMP = MAX(56.4f,89.345f);
	
	printf("Int Max Result:%d\n",iResult_SMP);
	printf("Float Max Result:%f\n",fResult_SMP);
	
	printf("\n\n");
	
	return (0);
}


/*

Int Max Result:10
Float Max Result:89.345001

*/