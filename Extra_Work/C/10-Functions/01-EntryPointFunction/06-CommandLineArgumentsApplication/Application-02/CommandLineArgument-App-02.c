#include<stdio.h> //printf()
#include<stdlib.h> //exit()

//Print full names ( names are passed from CLA )

int main(int argc, char *argv[], char *envp[])
{
	int i_SMP;
	
	printf("\n\n");
	
	if(argc != 4) 
	{		
		printf("Invalid Usage !!! Exiting now....\n\n");
		printf("Usage: CommandLineArgument-App-02 <first name> <middle name> <last name> \n\n");
	}
		
	for(i_SMP = 1; i_SMP < argc; i_SMP++)
	{
		printf("%s ",argv[i_SMP]);
	}
	
	printf("\n\n");
	
	return (0);
}

/*

> CommandLineArgument-App-02.exe Sunil Maruti Patil


Sunil Maruti Patil

*/