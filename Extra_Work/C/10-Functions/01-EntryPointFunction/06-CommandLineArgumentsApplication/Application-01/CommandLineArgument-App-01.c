#include<stdio.h> //printf()
#include<ctype.h> //atoi()
#include<stdlib.h> //exit()

//Add all numbers given on CLA.

int main(int argc, char *argv[], char *envp[])
{
	int i_SMP;
	int num_SMP;
	int sum_SMP = 0;
	
	printf("\n\n");
	
	if(argc == 1) 
	{		
		printf("No numbers given for addition. Exiting now....\n\n");
		printf("Usage: CommandLineArgument-App-01 <first number> <second number> .....\n\n");
		exit(0);
	}
		
	for(i_SMP = 1; i_SMP < argc; i_SMP++)
	{
		num_SMP = atoi(argv[i_SMP]); //convert string to integer ( "5" => 5 )
		sum_SMP = sum_SMP + num_SMP;		
	}
	
	printf("Sum=%d\n\n",sum_SMP);
	
	return (0);
}

/*

> CommandLineArgument-App-01.exe 10 20 30


Sum=60


> CommandLineArgument-App-01.exe


No numbers given for addition. Exiting now....

Usage: CommandLineArgument-App-01 <first number> <second number> .....


*/