#include <stdio.h>

//CLA with count + actual values.

int main(int argc, char *argv[])
{
	int i;
	
	printf("\n\n");
	
	printf("Hello World\n"); //Library function
	
	printf("No of arguments passed to main:%d\n",argc);
	
	for(i = 0; i < argc; i++)
	{
		printf("Argument:(%d) = %s\n",i+1,argv[i]);
	}
	
	printf("\n\n");
	
	return (0);
}

/*

> EntryPointFunction_Type_04.exe 10.5 5 a sunil


Hello World
No of arguments passed to main:5
Argument:(1) = EntryPointFunction_Type_04.exe
Argument:(2) = 10.5
Argument:(3) = 5
Argument:(4) = a
Argument:(5) = sunil

*/