#include <stdio.h>

//CLA with count + actual values + environment variable.

int main(int argc, char *argv[], char *envp[])
{
	int i_SMP;
	
	printf("\n\n");
	
	printf("Hello World\n"); //Library function
	
	printf("No of arguments passed to main:%d\n",argc);
	
	for(i_SMP = 0; i_SMP < argc; i_SMP++)
	{
		printf("Argument:(%d) = %s\n",i_SMP+1,argv[i_SMP]);
	}
	
	printf("\n\n");
	
	printf("Printing 20 environment variable passed to this program by OS:\n\n");
	
	for(i_SMP = 0; i_SMP < 30; i_SMP++)
	{
		printf("Environment Variable:(%d) = %s\n",i_SMP+1,envp[i_SMP]);
	}
	
	printf("\n\n");
	
	return (0);
}

/*

Hello World
No of arguments passed to main:3
Argument:(1) = EntryPointFunctionAllCLA.exe
Argument:(2) = sunil
Argument:(3) = 28


Printing 20 environment variable passed to this program by OS:

Environment Variable:(1) = ALLUSERSPROFILE=C:\ProgramData
Environment Variable:(2) = ANDROID_SDK_HOME=C:\Android
Environment Variable:(3) = APPDATA=C:\Users\admin\AppData\Roaming
Environment Variable:(4) = CommandPromptType=Native
Environment Variable:(5) = CommonProgramFiles=C:\Program Files (x86)\Common Files
Environment Variable:(6) = CommonProgramFiles(x86)=C:\Program Files (x86)\Common Files
Environment Variable:(7) = CommonProgramW6432=C:\Program Files\Common Files
Environment Variable:(8) = COMPUTERNAME=DESKTOP-QC7QTEH
Environment Variable:(9) = ComSpec=C:\WINDOWS\system32\cmd.exe
Environment Variable:(10) = DevEnvDir=E:\Softwares\Visual Studio Code 2019\Common7\IDE\
Environment Variable:(11) = DriverData=C:\Windows\System32\Drivers\DriverData
Environment Variable:(12) = ExtensionSdkDir=C:\Program Files (x86)\Microsoft SDKs\Windows Kits\10\ExtensionSDKs
Environment Variable:(13) = FPS_BROWSER_APP_PROFILE_STRING=Internet Explorer
Environment Variable:(14) = FPS_BROWSER_USER_PROFILE_STRING=Default
Environment Variable:(15) = Framework40Version=v4.0
Environment Variable:(16) = FrameworkDir=C:\Windows\Microsoft.NET\Framework\
Environment Variable:(17) = FrameworkDir32=C:\Windows\Microsoft.NET\Framework\
Environment Variable:(18) = FrameworkVersion=v4.0.30319
Environment Variable:(19) = FrameworkVersion32=v4.0.30319
Environment Variable:(20) = FSHARPINSTALLDIR=E:\Softwares\Visual Studio Code 2019\Common7\IDE\CommonExtensions\Microsoft\FSharp\
Environment Variable:(21) = HOMEDRIVE=C:
Environment Variable:(22) = HOMEPATH=\Users\admin
Environment Variable:(23) = INCLUDE=E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\ATLMFC\include;E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\include;C:\Program Files (x86)\Windows Kits\NETFXSDK\4.8\include\um;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\ucrt;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\shared;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\um;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\winrt;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\cppwinrt
Environment Variable:(24) = JAVA_HOME=C:\Program Files\Java\jdk1.8.0_151
Environment Variable:(25) = JRE_HOME=C:\Program Files\Java\jdk1.8.0_151\jre
Environment Variable:(26) = LIB=E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\ATLMFC\lib\x86;E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\lib\x86;C:\Program Files (x86)\Windows Kits\NETFXSDK\4.8\lib\um\x86;C:\Program Files (x86)\Windows Kits\10\lib\10.0.18362.0\ucrt\x86;C:\Program Files (x86)\Windows Kits\10\lib\10.0.18362.0\um\x86;
Environment Variable:(27) = LIBPATH=E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\ATLMFC\lib\x86;E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\lib\x86;E:\Softwares\Visual Studio Code 2019\VC\Tools\MSVC\14.25.28610\lib\x86\store\references;C:\Program Files (x86)\Windows Kits\10\UnionMetadata\10.0.18362.0;C:\Program Files (x86)\Windows Kits\10\References\10.0.18362.0;C:\Windows\Microsoft.NET\Framework\v4.0.30319;
Environment Variable:(28) = LOCALAPPDATA=C:\Users\admin\AppData\Local
Environment Variable:(29) = LOGONSERVER=\\DESKTOP-QC7QTEH
Environment Variable:(30) = MAVEN_HOME=E:\Softwares\apache-maven-3.5.2
*/