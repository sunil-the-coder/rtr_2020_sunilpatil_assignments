#include <stdio.h>

//with return type of main + CLA argc parameter
int main(int argc)
{
	printf("Hello World\n"); //Library function
	
	printf("No of arguments passed to main:%d\n",argc);
	
	return (0);
}

/*

$ EntryPointFunction_Type_03.exe 1 sunil patil

Hello World
No of arguments passed to main:4

*/