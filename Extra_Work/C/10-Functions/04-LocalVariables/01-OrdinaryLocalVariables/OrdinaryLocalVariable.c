#include<stdio.h>

int main(void)
{
	//local to main function.
	int a_SMP = 10;
	
	void UpdateCount();
	
	printf("\n\n a=%d\n",a_SMP);
	
	UpdateCount();
	
	UpdateCount();
	
	return (0);
}

void UpdateCount(void)
{
	//local to function only. reinitialize everytime call the function.
	int local_count_SMP = 5;
	
	local_count_SMP++;
	
	printf("Local count:%d\n",local_count_SMP);
	
}

/*

 a=10
Local count:6
Local count:6

*/