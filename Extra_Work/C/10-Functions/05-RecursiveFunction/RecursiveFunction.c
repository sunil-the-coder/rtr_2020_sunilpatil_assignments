#include<stdio.h>

int main(void)
{
	unsigned int number_SMP = 10;
	
	void recursive(unsigned int);
	
	printf("\n\n");
	
	recursive(number_SMP);
	
	printf("\n\n");
	
	return (0);
}

void recursive(unsigned int number_SMP)
{
	printf("Number:%d\n",number_SMP);
	
	if(number_SMP > 0)
	{
		recursive(number_SMP - 1);
	}
	
}


/*


Number:10
Number:9
Number:8
Number:7
Number:6
Number:5
Number:4
Number:3
Number:2
Number:1
Number:0

*/