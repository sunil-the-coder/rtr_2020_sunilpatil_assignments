#include <stdio.h>

//Pointer
//1. What is pointer ? 

Variable which stores the address of another variable.



int main(void) {
	
	int a = 10;	
	printf("a=%d\n",a); //10

	int arr[] = {10,20,30,40,50}; 
				
	printf("arr=%d\n",arr); //10
	printf("arr=%d\n",arr+1); //100 + //1*sizeof(type)
	printf("arr=%d\n",arr+2); //arr[2]

	printf("arr=%d\n",arr[2]); //arr[2]
	
	//arr[2] => *(arr + 2 * sizeof(type))
	
	//*(100 + 2 * 4) => *(108) => 30
	
	
	//float[] arr = {10.5f,20.2f,30.1f,40.6f,50.5f};
	
	return (0);
}