#include <stdio.h>
int main(void)
{
	//code
    printf("\n\n");
    printf("**************************************************************************************");
    printf("\n\n");

    printf("Sunil Hello World !!!\n\n");

	int a_SMP = 13;
	printf("Integer Decimal Value Of 'a_SMP' = %d\n", a_SMP);
	printf("Integer Octal Value Of 'a_SMP' = %o\n", a_SMP);
	printf("Integer Hexadecimal Value Of 'a_SMP' (Hexadecimal Letters In Lower Case) = %x\n", a_SMP);
	printf("Integer Hexadecimal Value Of 'a_SMP' (Hexadecimal Letters In Lower Case) = %X\n\n", a_SMP);

	char ch_SMP = 'A';
	printf("Character ch_SMP = %c\n", ch_SMP);
	char str_SMP[] = "AstroMediComp's Real Time Rendering Batch 3.0 (2020-2021)";
	printf("String str_SMP = %s\n\n", str_SMP);

	long num_SMP = 30121995L;
	printf("Long Integer = %ld\n\n", num_SMP);

	unsigned int b_SMP = 7;
	printf("Unsigned Integer 'b_SMP' = %u\n\n", b_SMP);
	
	signed int c_SMP = -7;
	printf("Signed Integer 'c' with negative value = %d\n\n", c_SMP);
	
	signed int s_SMP = 4294967295;
	printf("s_SMP = %d\n\n", s_SMP);
	printf("s_SMP = %x\n\n", s_SMP);

	float f_num_SMP = 3012.1995f;
	printf("Floating Point Number With Just %%f 'f_num_SMP' = %f\n", f_num_SMP);
	printf("Floating Point Number With %%4.2f 'f_num_SMP' = %4.2f\n", f_num_SMP);
	printf("Floating Point Number With %%6.5f 'f_num_SMP' = %6.5f\n\n", f_num_SMP);

	double d_pi_SMP = 3.14159265358979323846;
	printf("Double Precision Floating Point Number Without Exponential = %g\n", d_pi_SMP);
	printf("Double Precision Floating Point Number With Exponential (Lower Case) = %e\n", d_pi_SMP);
	printf("Double Precision Floating Point Number With Exponential (Upper Case) = %E\n\n", d_pi_SMP);
	printf("Double Hexadecimal Value Of 'd_pi_SMP' (Hexadecimal Letters In Lower Case) = %a\n", d_pi_SMP);
	printf("Double Hexadecimal Value Of 'd_pi_SMP' (Hexadecimal Letters In Upper Case) = %A\n\n", d_pi_SMP);
    
    printf("**************************************************************************************\n");
    printf("\n\n");
	return(0);
}


/* Output

**************************************************************************************

Sunil Hello World !!!

Integer Decimal Value Of 'a_SMP' = 13
Integer Octal Value Of 'a_SMP' = 15
Integer Hexadecimal Value Of 'a_SMP' (Hexadecimal Letters In Lower Case) = d
Integer Hexadecimal Value Of 'a_SMP' (Hexadecimal Letters In Lower Case) = D

Character ch_SMP = A
String str_SMP = AstroMediComp's Real Time Rendering Batch 3.0 (2020-2021)

Long Integer = 30121995

Unsigned Integer 'b_SMP' = 7

Unsigned Integer 'c' with negative value = 4294967289

Floating Point Number With Just %f 'f_num_SMP' = 3012.199463
Floating Point Number With %4.2f 'f_num_SMP' = 3012.20
Floating Point Number With %6.5f 'f_num_SMP' = 3012.19946

Double Precision Floating Point Number Without Exponential = 3.14159
Double Precision Floating Point Number With Exponential (Lower Case) = 3.141593e+00
Double Precision Floating Point Number With Exponential (Upper Case) = 3.141593E+00

Double Hexadecimal Value Of 'd_pi_SMP' (Hexadecimal Letters In Lower Case) = 0x1.921fb54442d18p+1
Double Hexadecimal Value Of 'd_pi_SMP' (Hexadecimal Letters In Upper Case) = 0X1.921FB54442D18P+1

**************************************************************************************


*/