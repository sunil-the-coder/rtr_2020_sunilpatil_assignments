#include <stdio.h>
int main(void)
{

	signed int a = -1;
	printf("a= %d\n\n", a); //-1
	
	unsigned int b = -1;
	printf("b= %d\n\n", b); //-1

	unsigned int c = -4294967294;
	printf("c= %u, c=%x\n\n",c,c ); //c= 4294967295, c=ffffffff
	
	unsigned int d = -2;
	printf("d= %u, d=%x\n\n",d,d ); //d= 4294967294, d=fffffffe
	
	unsigned int e = -5;
	printf("e= %u, e=%x\n\n",e,e ); //e= 4294967291, e=fffffffb
	
	unsigned char g = -2;
	printf("g=%d\n",g);
	
	char h = 256;
	printf("h=%d\n",h);
	
    printf("\n\n");
	return(0);
}


