#include <stdio.h>

int main(void)
{
	
	//variable declaration
	int i_SMP,j_SMP;
	int ch_01_SMP, ch_02_SMP;
	
	int a_SMP, i_result_SMP;
	float f_SMP, f_result_SMP;
	
	int i_explicit_SMP;
	float f_explicit_SMP;
	
	
	//code
	printf("\n\n");
	
	//Inverconversation and implicit type casting between 'char' AND 'int' Types.
	
	i_SMP = 70;
	ch_01_SMP = i_SMP;
	
	printf("i_SMP=%d, ch_01_SMP=%c\n\n",i_SMP, ch_01_SMP);
	
	ch_02_SMP = 'Q';
	j_SMP = ch_02_SMP;
	printf("ch_02_SMP=%c, j_SMP=%d\n\n", ch_02_SMP, j_SMP);
	
	
	//Implicit conversion between int and float.
	a_SMP = 6;
	f_SMP = 8.5f;
	
	f_result_SMP = a_SMP + f_SMP;
	printf("Int a_SMP=%d and Float f_SMP=%f gives floating point sum= %f\n\n",a_SMP,f_SMP,f_result_SMP);
	
	i_result_SMP = a_SMP + f_SMP;
	printf("Int a_SMP=%d and Float f_SMP=%f gives int sum= %d\n\n",a_SMP,f_SMP,i_result_SMP);
	
	//Explicit type casting using cast operator.
	f_explicit_SMP = 50.567382;
	i_explicit_SMP = (int)f_explicit_SMP; //convert float to int by removing any numbers after .
	
	
	printf("float=%f type casted to int=%d\n\n",f_explicit_SMP, i_explicit_SMP);
	
	
	return (0);
}

/*

i_SMP=70, ch_01_SMP=F

ch_02_SMP=Q, j_SMP=81

Int a_SMP=6 and Float f_SMP=8.500000 gives floating point sum= 14.500000

Int a_SMP=6 and Float f_SMP=8.500000 gives int sum= 14

float=50.567383 type casted to int=50

*/