#include<stdio.h>

struct Point
{
	int x;
	int y;	
};


int main(void)
{	
	typedef struct Point MY_POINT;
	
	MY_POINT point_A_SMP;
	MY_POINT point_B_SMP;
	
	point_A_SMP.x = 4;
	point_A_SMP.y = 8;
	
	point_B_SMP.x = 3;
	point_B_SMP.y = 6;
	
	printf("\n\n");
	
	printf("Point_A: (%d,%d)\n",point_A_SMP.x,point_A_SMP.y);
	printf("Point_B: (%d,%d)\n",point_B_SMP.x,point_B_SMP.y);
		
	printf("\n\n");
	
	return (0);
}

/*

	Point_A: (4,8)
	Point_B: (3,6)

*/