#include <stdio.h>

int main(void)
{

    // variable declarations
    int i_SMP = 5;
    float f_SMP = 3.9f;
    double d_SMP = 8.041997;
    char c_SMP = 'A';
    
	//code
    printf("\n\n");
    
    printf("i_SMP = %d\n", i_SMP);
    printf("f_SMP = %f\n", f_SMP);
    printf("d_SMP = %lf\n", d_SMP);
    printf("c_SMP = %c\n", c_SMP);

    printf("\n\n");

    i_SMP = 43;
    f_SMP = 6.54f;
    d_SMP = 26.1294;
    c_SMP = 'P';
    
    printf("i_SMP = %d\n", i_SMP);
    printf("f_SMP = %f\n", f_SMP);
    printf("d_SMP = %lf\n", d_SMP);
    printf("c_SMP = %c\n", c_SMP);

    printf("\n\n");

    return(0);
}

/* Output 

i_SMP = 5
f_SMP = 3.900000
d_SMP = 8.041997
c_SMP = A


i_SMP = 43
f_SMP = 6.540000
d_SMP = 26.129400
c_SMP = P


*/


