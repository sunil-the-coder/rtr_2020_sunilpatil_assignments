#include <stdio.h>
#include <stdarg.h>

#define NUMBER_TO_FIND 3
#define TOTAL_NUMBERS 7


int main(void)
{
	//local function declaration
	void FindNumber(int, int, ...);

	FindNumber(NUMBER_TO_FIND, TOTAL_NUMBERS, 5,4,3,2,3,9,10);

	return (0);
}

//variadic function
void FindNumber(int number_to_find, int total_numbers, ...)
{
	//variable declarations
	int n;
	int count = 0;
	va_list number_list;

	va_start(number_list, total_numbers);

	while(total_numbers)
	{
		n = va_arg(number_list, int);
		if(n == number_to_find)
			count++;
		total_numbers--;
	}

	if(count == 0)
		printf("Number %d not found.\n",number_to_find);
	else
		printf("Number %d found %d times in list\n",number_to_find, count);

}

/*

Number 3 found 2 times in list

*/

