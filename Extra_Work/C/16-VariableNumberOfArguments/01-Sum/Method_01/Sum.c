#include <stdio.h>
#include <stdarg.h>

int main(void)
{
	//function declarations
	int CalculateSum(int, ...);

	//variable declarations
	int answer;

	answer = CalculateSum(3,10,20,30);
	printf("Answer of 3 numbers=%d\n",answer);

	answer = CalculateSum(5,10,20,30,40,50);
	printf("Answer of 5 number =%d\n",answer);


	answer = CalculateSum(0);
	printf("Answer of 0 number =%d\n",answer);

	return (0);
}

//variadic function
int CalculateSum(int num, ...)
{
	//varible declarations
	int total_sum = 0;
	int n;
	va_list number_list;

	//code
	va_start(number_list, num);

	while(num)
	{
		n = va_arg(number_list, int);
		total_sum += n;
		num--;
	}

	va_end(number_list);

	return (total_sum);
}

/*

Answer of 3 numbers=60
Answer of 5 number =150
Answer of 0 number =0

*/