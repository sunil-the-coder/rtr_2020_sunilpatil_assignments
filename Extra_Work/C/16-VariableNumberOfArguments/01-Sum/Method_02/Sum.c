#include <stdio.h>
#include <stdarg.h>

int main(void)
{
	//function declarations
	int CalculateSum(int, ...);

	//variable declarations
	int answer;

	answer = CalculateSum(3,10,20,30);
	printf("Answer of 3 numbers=%d\n",answer);

	answer = CalculateSum(5,10,20,30,40,50);
	printf("Answer of 5 number =%d\n",answer);


	answer = CalculateSum(0);
	printf("Answer of 0 number =%d\n",answer);

	return (0);
}

//variadic function
int CalculateSum(int num, ...)
{
	//local function declarations
	int va_CalculateSum(int, va_list);

	//local varible declarations
	int total_sum = 0;
	va_list number_list;

	//code
	va_start(number_list, num);

	total_sum = va_CalculateSum(num, number_list);

	va_end(number_list);

	return (total_sum);
}

int va_CalculateSum(int num, va_list numbers)
{
	//variable declaration
	int n;
	int total_sum = 0;

	while(num)
	{
		n = va_arg(number_list, int);
		total_sum += n;
		num--;
	}

	return (total_sum);
}

/*

Answer of 3 numbers=60
Answer of 5 number =150
Answer of 0 number =0

*/