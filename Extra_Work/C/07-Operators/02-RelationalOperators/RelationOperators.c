#include <stdio.h>

int main(void) { 

//variable declarations 
	int a_SMP; int b_SMP; int result;

 //code printf("\n\n"); 
 printf("Enter One Integer : "); 
 scanf("%d", &a_SMP);

 printf("\n\n"); 
 printf("Enter Another Integer : "); 
 scanf("%d", &b_SMP);
 
 printf("\n\n"); 
 printf("If Answer = 0, It Is 'FALSE'.\n"); 
 printf("If Answer = 1, It Is 'TRUE'.\n\n");
 
 result = (a_SMP < b_SMP); 
 printf("(a_SMP < b_SMP)  A = %d Is Less Than B = %d                \t Answer = %d\n", a_SMP, b_SMP, result);
 
 result = (a_SMP > b_SMP); 
 printf("(a_SMP > b_SMP)  A = %d Is Greater Than B = %d             \t Answer = %d\n", a_SMP, b_SMP, result);
 
 result = (a_SMP <= b_SMP); 
 printf("(a_SMP <= b_SMP) A = %d Is Less Than Or Equal To B = %d    \t Answer = %d\n", a_SMP, b_SMP, result);
 
 result = (a_SMP >= b_SMP); 
 printf("(a_SMP >= b_SMP) A = %d Is Greater Than Or Equal To B = %d \t Answer = %d\n", a_SMP, b_SMP, result);
 
 result = (a_SMP == b_SMP); 
 printf("(a_SMP == b_SMP) A = %d Is Equal To B = %d                 \t Answer = %d\n", a_SMP, b_SMP, result);
 
 result = (a_SMP != b_SMP); 
 printf("(a_SMP != b_SMP) A = %d Is NOT Equal To B = %d             \t Answer = %d\n", a_SMP, b_SMP, result);
 
 return(0); 
}

/*

Enter One Integer : 10


Enter Another Integer : 2


If Answer = 0, It Is 'FALSE'.
If Answer = 1, It Is 'TRUE'.

(a_SMP < b_SMP)  A = 10 Is Less Than B = 2                       Answer = 0
(a_SMP > b_SMP)  A = 10 Is Greater Than B = 2                    Answer = 1
(a_SMP <= b_SMP) A = 10 Is Less Than Or Equal To B = 2           Answer = 0
(a_SMP >= b_SMP) A = 10 Is Greater Than Or Equal To B = 2        Answer = 1
(a_SMP == b_SMP) A = 10 Is Equal To B = 2                        Answer = 0
(a_SMP != b_SMP) A = 10 Is NOT Equal To B = 2                    Answer = 1

*/