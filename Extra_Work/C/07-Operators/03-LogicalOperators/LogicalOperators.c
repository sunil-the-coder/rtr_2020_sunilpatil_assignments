#include <stdio.h>
int main(void) { 

//variable declarations
 int a_SMP; int b_SMP; int c; int result_SMP;
 //code printf("\n\n"); 
 printf("Enter First Integer : "); 
 scanf("%d", &a_SMP);
 
 printf("\n\n"); printf("Enter Second Integer : "); 
 scanf("%d", &b_SMP);
 
 printf("\n\n");

 printf("Enter Third Integer : ");
 scanf("%d", &c);
 
 printf("\n\n"); 
 printf("If Answer = 0, It Is 'FALSE'.\n"); 
 printf("If Answer = 1, It Is 'TRUE'.\n\n");
 result_SMP = (a_SMP <= b_SMP) && (b_SMP != c); 
 printf("LOGICAL AND (&&) : Answer is TRUE (1) If And Only If BOTH Conditions Are True. The Answer is FALSE (0), If Any One Or Both Conditions Are False. \n\n"); 
 
 printf("A = %d Is Less Than Or Equal To B = %d AND B = %d Is NOT Equal To C = %d      \t Answer = %d\n\n", a_SMP, b_SMP, b_SMP, c, result_SMP);
 
 result_SMP = (b_SMP >= a_SMP) || (a_SMP == c); 
 printf("LOGICAL OR (||) : Answer is FALSE (0) If And Only If BOTH Conditions Are False. The Answer is TRUE (1), If Any One Or Both Conditions Are True.\n \n"); 
 printf("Either B = %d Is Greater Than Or Equal To A = %d OR A = %d Is Equal To C = %d \t Answer = %d\n\n", b_SMP, a_SMP, a_SMP, c, result_SMP);
 
 result_SMP = !a_SMP;
 printf("A = %d And Using Logical NOT (!) Operator on A Gives Result = %d\n\n", a_SMP, result_SMP);
 
 result_SMP = !b_SMP;
 printf("B = %d And Using Logical NOT (!) Operator on B Gives Result = %d\n\n", b_SMP, result_SMP);
 
 result_SMP = !c; 
 printf("C = %d And Using Logical NOT (!) Operator on C Gives Result = %d\n\n", c, result_SMP);
 

 result_SMP = (!(a_SMP <= b_SMP) && !(b_SMP != c)); 
 printf("Using Logical NOT (!) On (a_SMP <= b_SMP) And Also On (b_SMP != c) And then ANDing Them Afterwards Gives Result = %d\n", result_SMP);
   
   printf("\n\n");
    
	result_SMP = !((b_SMP >= a_SMP) || (a_SMP == c));
	printf("Using Logical NOT (!) On Entire Logical Expression (b_SMP >= a_SMP) || (a_SMP == c) Gives Result = %d\n", result_SMP);
    
	printf("\n\n");      

	return(0); 
}

/*

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\07-Operators\02-RelationalOperators>LogicalOperators.exe
Enter First Integer : 4


Enter Second Integer : 3


Enter Third Integer : 6


If Answer = 0, It Is 'FALSE'.
If Answer = 1, It Is 'TRUE'.

LOGICAL AND (&&) : Answer is TRUE (1) If And Only If BOTH Conditions Are True. The Answer is FALSE (0), If Any One Or Both Conditions Are False.

A = 4 Is Less Than Or Equal To B = 3 AND B = 3 Is NOT Equal To C = 6             Answer = 0

LOGICAL OR (||) : Answer is FALSE (0) If And Only If BOTH Conditions Are False. The Answer is TRUE (1), If Any One Or Both Conditions Are True.

Either B = 3 Is Greater Than Or Equal To A = 4 OR A = 4 Is Equal To C = 6        Answer = 0

A = 4 And Using Logical NOT (!) Operator on A Gives Result = 0

B = 3 And Using Logical NOT (!) Operator on B Gives Result = 0

C = 6 And Using Logical NOT (!) Operator on C Gives Result = 0

Using Logical NOT (!) On (a_SMP <= b_SMP) And Also On (b_SMP != c) And then ANDing Them Afterwards Gives Result = 0


Using Logical NOT (!) On Entire Logical Expression (b_SMP >= a_SMP) || (a_SMP == c) Gives Result = 1



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\07-Operators\02-RelationalOperators>LogicalOperators.exe

*/