#include<stdio.h>
#define BINARY_DIGITS 8

int main(void)  
{
	
	//function protoytpe
	void PrintBinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a_SMP,num_bits_SMP;
	unsigned int result_SMP;
	
	printf("\n\n"); 
	printf("Enter An Integer = "); 
	scanf("%u", &a_SMP);
	
	printf("\n\n"); 
	printf("By How Many Bits Do You Want To Shift A = %d To The Right ? ", a_SMP); 
	scanf("%u", &num_bits_SMP);
	
 printf("\n\n\n\n"); 
 
result_SMP = a_SMP >> num_bits_SMP; 
printf("Bitwise RIGHT-SHIFTing A_SMP = %d By %d Bits \nGives The Result = %d (Decimal).\n\n", a_SMP, num_bits_SMP, result_SMP); 

	 PrintBinaryFormOfNumber(a_SMP); 	 
	 PrintBinaryFormOfNumber(result_SMP); 
	
	return (0);
}

void PrintBinaryFormOfNumber(unsigned int decimal_number_SMP) 
{
	unsigned int quotient_SMP, remainder_SMP;
	unsigned int binary_digits[BINARY_DIGITS];
	unsigned int i_SMP, tempNumber_SMP;
	
	//Initialize array with 0 - Erase all garbage values.
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		binary_digits[i_SMP] = 0;
	}
	
	tempNumber_SMP = decimal_number_SMP;
	i_SMP = BINARY_DIGITS - 1;

	while(tempNumber_SMP != 0) {
		
		quotient_SMP = tempNumber_SMP / 2;
		remainder_SMP = tempNumber_SMP % 2;
		tempNumber_SMP = quotient_SMP;
		binary_digits[i_SMP] = remainder_SMP;
		i_SMP--;
	}
	
	printf("Binary of %u is\t=\t",decimal_number_SMP);
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		printf("%u", binary_digits[i_SMP]);
	}
	
	printf("\n\n"); 
	
}


/*

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\07-Operators\04-BitwiseOperators\05-BitwiseRightShift>BitwiseRightShift.exe


Enter An Integer = 14


By How Many Bits Do You Want To Shift A = 14 To The Right ? 2




Bitwise RIGHT-SHIFTing A_SMP = 14 By 2 Bits
Gives The Result = 3 (Decimal).

Binary of 14 is =       00001110

Binary of 3 is  =       00000011

*/