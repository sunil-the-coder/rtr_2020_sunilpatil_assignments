#include<stdio.h>
#define BINARY_DIGITS 8

int main(void)  
{
	
	//function protoytpe
	void PrintBinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a_SMP;
	unsigned int b_SMP;
	unsigned int result_SMP;
	
	printf("\n\n"); 
	printf("Enter An Integer = "); 
	scanf("%u", &a_SMP);

	 printf("\n\n"); 
	 
	 printf("Enter Another Integer = "); 
	 scanf("%u", &b_SMP);
	 
	 printf("\n\n\n\n"); 
	 result_SMP = a_SMP & b_SMP; 
	 
	 printf("Bitwise AND-ing Of \nA = %d (Decimal) and B = %d (Decimal) gives result %d (Decimal).\n\n", a_SMP, b_SMP, result_SMP);     
	 
	 PrintBinaryFormOfNumber(a_SMP); 
	 PrintBinaryFormOfNumber(b_SMP); 
	 PrintBinaryFormOfNumber(result_SMP); 
	
	return (0);
}

void PrintBinaryFormOfNumber(unsigned int decimal_number_SMP) 
{
	unsigned int quotient_SMP, remainder_SMP;
	unsigned int binary_digits[BINARY_DIGITS];
	unsigned int i_SMP, tempNumber_SMP;
	
	//Initialize array with 0 - Erase all garbage values.
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		binary_digits[i_SMP] = 0;
	}
	
	tempNumber_SMP = decimal_number_SMP;
	i_SMP = BINARY_DIGITS - 1;

	while(tempNumber_SMP != 0) {
		
		quotient_SMP = tempNumber_SMP / 2;
		remainder_SMP = tempNumber_SMP % 2;
		tempNumber_SMP = quotient_SMP;
		binary_digits[i_SMP] = remainder_SMP;
		i_SMP--;
	}
	
	printf("Binary of %u is\t=\t",decimal_number_SMP);
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		printf("%u", binary_digits[i_SMP]);
	}
	
	printf("\n\n"); 
	
	
}

/*

Enter An Integer = 34


Enter Another Integer = 2




Bitwise AND-ing Of
A = 34 (Decimal) and B = 2 (Decimal) gives result 2 (Decimal).

Binary of 34 is =       00100010

Binary of 2 is  =       00000010

Binary of 2 is  =       00000010

*/