#include<stdio.h>
#define BINARY_DIGITS 8

int main(void)  
{
	
	//function protoytpe
	void PrintBinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a_SMP;
	unsigned int result_SMP;
	
	printf("\n\n"); 
	printf("Enter An Integer = "); 
	scanf("%u", &a_SMP);

	 result_SMP = ~a_SMP;    
	 printf("Bitwise COMPLEMENTING Of \nA_SMP = %d (Decimal) gives result %d (Decimal). \n\n", a_SMP, result_SMP); 
	 
	 PrintBinaryFormOfNumber(a_SMP); 	 
	 PrintBinaryFormOfNumber(result_SMP); 
	
	return (0);
}

void PrintBinaryFormOfNumber(unsigned int decimal_number_SMP) 
{
	unsigned int quotient_SMP, remainder_SMP;
	unsigned int binary_digits[BINARY_DIGITS];
	unsigned int i_SMP, tempNumber_SMP;
	
	//Initialize array with 0 - Erase all garbage values.
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		binary_digits[i_SMP] = 0;
	}
	
	tempNumber_SMP = decimal_number_SMP;
	i_SMP = BINARY_DIGITS - 1;

	while(tempNumber_SMP != 0) {
		
		quotient_SMP = tempNumber_SMP / 2;
		remainder_SMP = tempNumber_SMP % 2;
		tempNumber_SMP = quotient_SMP;
		binary_digits[i_SMP] = remainder_SMP;
		i_SMP--;
	}
	
	printf("Binary of %u is\t=\t",decimal_number_SMP);
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		printf("%u", binary_digits[i_SMP]);
	}
	
	printf("\n\n"); 
	
}


/*

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\07-Operators\04-BitwiseOperators\04-BitwiseOnesComplement>BitwiseOnesComplement.exe


Enter An Integer = 10
Bitwise COMPLEMENTING Of
A_SMP = 10 (Decimal) gives result -11 (Decimal).

Binary of 10 is =       00001010

Binary of 4294967285 is =       11110101


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\07-Operators\04-BitwiseOperators\04-BitwiseOnesComplement>BitwiseOnesComplement.exe


Enter An Integer = 4
Bitwise COMPLEMENTING Of
A_SMP = 4 (Decimal) gives result -5 (Decimal).

Binary of 4 is  =       00000100

Binary of 4294967291 is =       11111011

*/


/*
For finding the one’s complement of a number, first find its binary equivalent. Here, decimal number 2 is represented as 0000 0010 in binary form. Now taking its one’s complement by inverting (flipping all 1’s into 0’s and all 0’s into 1’s) all the digits of its binary representation, which will result in:

0000 0010 → 1111 1101

This is the one’s complement of the decimal number 2. And since the first bit, i.e., the sign bit is 1 in the binary number, it means that the sign is negative for the number it stored. (here, the number referred to is not 2 but the one’s complement of 2).

Now, since the numbers are stored as 2’s complement (taking the one’s complement of a number plus one), so to display this binary number, 1111 1101, into decimal, first we need to find its 2’s complement, which will be:

1111 1101 → 0000 0010 + 1 → 0000 0011

This is the 2’s complement. The decimal representation of the binary number, 0000 0011, is 3. And, since the sign bit was one as mentioned above, so the resulting answer is -3.

Hint: If you read this procedure carefully, then you would have observed that the result for the one’s complement operator is actually, the number (operand - on which this operator is applied) plus one with a negative sign. You can try this with other numbers too.*/