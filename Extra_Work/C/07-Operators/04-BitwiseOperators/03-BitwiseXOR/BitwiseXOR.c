#include<stdio.h>
#define BINARY_DIGITS 8

int main(void)  
{
	
	//function protoytpe
	void PrintBinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a_SMP;
	unsigned int b_SMP;
	unsigned int result_SMP;
	
	printf("\n\n"); 
	printf("Enter An Integer = "); 
	scanf("%u", &a_SMP);

	 printf("\n\n"); 
	 
	 printf("Enter Another Integer = "); 
	 scanf("%u", &b_SMP);
	 
	 printf("\n\n\n\n"); 
	 result_SMP = a_SMP ^ b_SMP; 
	 
	 printf("Bitwise XOR-ing Of \nA = %d (Decimal) and B = %d (Decimal) gives result %d (Decimal).\n\n", a_SMP, b_SMP, result_SMP);     
	 
	 PrintBinaryFormOfNumber(a_SMP); 
	 PrintBinaryFormOfNumber(b_SMP); 
	 PrintBinaryFormOfNumber(result_SMP); 
	
	return (0);
}

void PrintBinaryFormOfNumber(unsigned int decimal_number_SMP) 
{
	unsigned int quotient_SMP, remainder_SMP;
	unsigned int binary_digits[BINARY_DIGITS];
	unsigned int i_SMP, tempNumber_SMP;
	
	//Initialize array with 0 - Erase all garbage values.
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		binary_digits[i_SMP] = 0;
	}
	
	tempNumber_SMP = decimal_number_SMP;
	i_SMP = BINARY_DIGITS - 1;

	while(tempNumber_SMP != 0) {
		
		quotient_SMP = tempNumber_SMP / 2;
		remainder_SMP = tempNumber_SMP % 2;
		tempNumber_SMP = quotient_SMP;
		binary_digits[i_SMP] = remainder_SMP;
		i_SMP--;
	}
	
	printf("Binary of %u is\t=\t",decimal_number_SMP);
	for(i_SMP = 0; i_SMP < BINARY_DIGITS; i_SMP++){
		printf("%u", binary_digits[i_SMP]);
	}
	
	printf("\n\n"); 
	
	
}


/*

Enter An Integer = 45


Enter Another Integer = 23




Bitwise XOR-ing Of
A = 45 (Decimal) and B = 23 (Decimal) gives result 58 (Decimal).

Binary of 45 is =       00101101

Binary of 23 is =       00010111

Binary of 58 is =       00111010

*/