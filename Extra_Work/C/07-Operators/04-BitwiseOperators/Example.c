#include <stdio.h>

int main(void)
{
	int IsOdd(unsigned int);
	
	printf("%d\n",IsOdd(15));
	
	printf("%d\n",IsOdd(7));
	
	printf("%d\n",IsOdd(4));	

	return (0);
}

//Faster as compared to number % 2.
int IsOdd(unsigned int number) {
	return number & 1; // If last bit  is 1 means number is going to odd only.
}


