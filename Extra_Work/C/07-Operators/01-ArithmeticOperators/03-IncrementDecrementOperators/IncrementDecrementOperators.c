#include <stdio.h>

int main(void)
{
	//variable declarations
	int a_SMP = 5;
	int b_SMP = 10;

	//code
	printf("\n\n");
	printf("A_SMP = %d\n", a_SMP);
	printf("A_SMP = %d\n", a_SMP++);
	printf("A_SMP = %d\n", a_SMP);
	printf("A_SMP = %d\n\n", ++a_SMP);

	printf("B_SMP = %d\n", b_SMP);
	printf("B_SMP = %d\n", b_SMP--);
	printf("B_SMP = %d\n", b_SMP);
	printf("B_SMP = %d\n\n", --b_SMP);

	return(0);
}

/* Output

A_SMP = 5
A_SMP = 5
A_SMP = 6
A_SMP = 7

B_SMP = 10
B_SMP = 10
B_SMP = 9
B_SMP = 8

*/
