#include <stdio.h>

int main(void)
{
	//variable declarations
	int a_SMP;
	int b_SMP;
	int x_SMP;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &a_SMP);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &b_SMP);

	printf("\n\n");

	//Since, In All The Following 5 Cases, The Operand on The Left 'a_SMP' Is Getting Repeated Immeiately On The Right (e.g : a_SMP = a_SMP + b_SMP or a_SMP = a_SMP -b_SMP),
	//We Are Using Compound Assignment Operators +=, -=, *=, /= and %=

	//Since, 'a_SMP' Will Be Assigned The Value Of (a_SMP + b_SMP) At The Expression (a_SMP += b_SMP), We Must Save The Original Value Of 'a_SMP' To Another Variable (x_SMP)
	x_SMP = a_SMP;
	a_SMP += b_SMP; // a_SMP = a_SMP + b_SMP
	printf("Addition Of A = %d And B = %d Gives %d.\n", x_SMP, b_SMP, a_SMP);

	//Value Of 'a_SMP' Altered In The Above Expression Is Used Here...
	//Since, 'a_SMP' Will Be Assigned The Value Of (a_SMP - b_SMP) At The Expression (a_SMP -= b_SMP), We Must Save The Original Value Of 'a_SMP' To Another Variable (x_SMP)
	x_SMP = a_SMP;
	a_SMP -= b_SMP; // a_SMP = a_SMP - b_SMP
	printf("Subtraction Of A = %d And B = %d Gives %d.\n", x_SMP, b_SMP, a_SMP);

	//Value Of 'a_SMP' Altered In The Above Expression Is Used Here...
	//Since, 'a_SMP' Will Be Assigned The Value Of (a_SMP * b_SMP) At The Expression (a_SMP *= b_SMP), We Must Save The Original Value Of 'a_SMP' To Another Variable (x_SMP)
	x_SMP = a_SMP;
	a_SMP *= b_SMP; // a_SMP = a_SMP * b_SMP
	printf("Multiplication Of A = %d And B = %d Gives %d.\n", x_SMP, b_SMP, a_SMP);

	//Value Of 'a_SMP' Altered In The Above Expression Is Used Here...
	//Since, 'a_SMP' Will Be Assigned The Value Of (a_SMP / b_SMP) At The Expression (a_SMP /= b_SMP), We Must Save The Original Value Of 'a_SMP' To Another Variable (x_SMP)
	x_SMP = a_SMP;
	a_SMP /= b_SMP; // a_SMP = a_SMP / b_SMP 
	printf("Division Of A = %d And B = %d Gives Quotient %d.\n", x_SMP, b_SMP, a_SMP);

	//Value Of 'a_SMP' Altered In The Above Expression Is Used Here...
	//Since, 'a_SMP' Will Be Assigned The Value Of (a_SMP % b_SMP) At The Expression (a_SMP %= b_SMP), We Must Save The Original Value Of 'a_SMP' To Another Variable (x_SMP)
	x_SMP = a_SMP;
	a_SMP %= b_SMP; // a_SMP = a_SMP % b_SMP
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", x_SMP, b_SMP, a_SMP);

	printf("\n\n");

	return(0);
}


/* Output 

Enter A Number : 10


Enter Another Number : 2


Addition Of A = 10 And B = 2 Gives 12.
Subtraction Of A = 12 And B = 2 Gives 10.
Multiplication Of A = 10 And B = 2 Gives 20.
Division Of A = 20 And B = 2 Gives Quotient 10.
Division Of A = 10 And B = 2 Gives Remainder 0.

*/