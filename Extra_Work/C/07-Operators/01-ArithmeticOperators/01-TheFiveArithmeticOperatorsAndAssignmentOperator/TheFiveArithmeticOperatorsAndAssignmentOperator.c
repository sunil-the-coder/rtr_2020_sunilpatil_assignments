#include <stdio.h>

int main(void)
{
	//variable declarations
	int a_SMP;
	int b_SMP;
	int result_SMP;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &a_SMP);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &b_SMP);

	printf("\n\n");

	// *** The Following Are The 5 Arithmetic Operators +, -, *, / and % ***
	// *** Also, The Resultants Of The Arithmetic Operations In All The Below Five Cases Have Been Assigned To The Variable 'result_SMP' Using the Assignment Operator (=) ***
	result_SMP = a_SMP + b_SMP;
	printf("Addition Of A = %d And B = %d Gives %d.\n", a_SMP, b_SMP, result_SMP);

	result_SMP = a_SMP - b_SMP;
	printf("Subtraction Of A = %d And B = %d Gives %d.\n", a_SMP, b_SMP, result_SMP);

	result_SMP = a_SMP * b_SMP;
	printf("Multiplication Of A = %d And B = %d Gives %d.\n", a_SMP, b_SMP, result_SMP);

	result_SMP = a_SMP / b_SMP;
	printf("Division Of A = %d And B = %d Gives Quotient %d.\n", a_SMP, b_SMP, result_SMP);

	result_SMP = a_SMP % b_SMP;
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", a_SMP, b_SMP, result_SMP);

	printf("\n\n");

	return(0);
}

/* Output

Enter A Number : 20


Enter Another Number : 3


Addition Of A = 20 And B = 3 Gives 23.
Subtraction Of A = 20 And B = 3 Gives 17.
Multiplication Of A = 20 And B = 3 Gives 60.
Division Of A = 20 And B = 3 Gives Quotient 6.
Division Of A = 20 And B = 3 Gives Remainder 2.

*/
