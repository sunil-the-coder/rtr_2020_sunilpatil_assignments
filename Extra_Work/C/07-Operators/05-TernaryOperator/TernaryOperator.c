#include <stdio.h>

int main(void)
{
	//variable declaration
	
	int a_SMP, b_SMP;
	int p_SMP,q_SMP;
	
	char ch_result_01_SMP, ch_result_02_SMP;
	int i_result_01_SMP, i_result_02_SMP;
	
	printf("\n\n");
	
	a_SMP = 7;
	b_SMP = 5;
	
	ch_result_01_SMP = (a_SMP > b_SMP) ? 'A' : 'B';
	i_result_01_SMP = (a_SMP > b_SMP) ? a_SMP : b_SMP;
	
	printf("Ternary Operator Answer 1: %c and %d.\n\n", ch_result_01_SMP,
	i_result_01_SMP);


	p_SMP = 30;
	q_SMP = 30;
	
	ch_result_02_SMP = (p_SMP != q_SMP) ? 'P' : 'Q';
	i_result_02_SMP = (p_SMP != q_SMP) ? p_SMP : q_SMP;

	printf("Ternary Operator Answer 2: %c and %d.\n\n", ch_result_02_SMP,
	i_result_02_SMP);

	printf("\n\n");

	return (0);
}


/*

Ternary Operator Answer 1: A and 7.

Ternary Operator Answer 2: Q and 30.

*/