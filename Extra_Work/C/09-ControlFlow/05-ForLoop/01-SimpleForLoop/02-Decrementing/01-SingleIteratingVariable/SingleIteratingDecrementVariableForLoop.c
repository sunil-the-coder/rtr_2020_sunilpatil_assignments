#include <stdio.h>

int main(void)
{
	int i_SMP;

	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");
	
	for (i_SMP = 10; i_SMP >= 1; i_SMP--)
	{
		printf("\t%d\n", i_SMP);
	}
	printf("\n\n");
	
	return (0);
}

/*

Printing Digits 10 to 1 :

        10
        9
        8
        7
        6
        5
        4
        3
        2
        1
		
*/
