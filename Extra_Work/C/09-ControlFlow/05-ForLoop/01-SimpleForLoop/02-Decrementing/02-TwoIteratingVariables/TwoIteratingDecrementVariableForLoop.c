#include <stdio.h>

int main(void)
{
	int i_SMP, j_SMP;

	printf("Printing Digits 10 to 1 and 100 to 10 : \n\n");

	for (i_SMP = 10, j_SMP = 100; i_SMP >= 1, j_SMP >= 10; i_SMP--, j_SMP -= 10)
	{
		printf("\t %d \t %d\n", i_SMP, j_SMP);
	}

	printf("\n\n");

	return (0);
}

/*

Printing Digits 10 to 1 and 100 to 10 :

         10      100
         9       90
         8       80
         7       70
         6       60
         5       50
         4       40
         3       30
         2       20
         1       10
		
*/
