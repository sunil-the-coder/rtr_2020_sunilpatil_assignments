#include <stdio.h>

int main(void)
{
	int i_SMP;

	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	for (i_SMP = 1; i_SMP <= 10; i_SMP++)
	{
		printf("\t%d\n", i_SMP);
	}
	
	printf("\n\n");

	return (0);
}

/*

Printing Digits 1 to 10 :

        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
		
		*/
