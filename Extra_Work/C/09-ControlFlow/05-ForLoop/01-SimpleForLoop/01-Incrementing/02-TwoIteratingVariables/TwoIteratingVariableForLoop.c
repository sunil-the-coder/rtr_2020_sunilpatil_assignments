#include <stdio.h>

int main(void)
{
	int i_SMP;
	int j_SMP;

	printf("\n\n");

	printf("Printing Digits 1 to 10 and 10 to 100 : \n\n");

	for (i_SMP = 1, j_SMP = 10; i_SMP <= 10, j_SMP <= 100; i_SMP++, j_SMP = j_SMP + 10)
	{
		printf("\t %d \t %d\n", i_SMP, j_SMP);
	}
	
	printf("\n\n");

	return (0);
}

/*

Printing Digits 1 to 10 and 10 to 100 :

         1       10
         2       20
         3       30
         4       40
         5       50
         6       60
         7       70
         8       80
         9       90
         10      100
		
*/
