#include <stdio.h>

int main(void)
{
	char option_SMP, ch_SMP = '\0';
		
	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	
	printf("Enter 'Y' oy 'y' To Initiate User Controlled Infinite Loop : ");
	
	printf("\n\n");
	
	option_SMP = getch();
	
	if (option_SMP == 'Y' || option_SMP == 'y')
	{
		for (;;) //Infinite Loop
		{
			printf("In Loop...\n");
			ch_SMP = getch();

			if (ch_SMP == 'Q' || ch_SMP == 'q')
				break; // Exit from loop
		}
	}
	
	printf("\n\n");
	
	printf("Exit from user controlled loop.");
	
	printf("\n\n");


	return (0);
}

/*

Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop :

Enter 'Y' oy 'y' To Initiate User Controlled Infinite Loop :

In Loop...
In Loop...
In Loop...
In Loop...
In Loop...


Exit from user controlled loop.
		
*/
