#include <stdio.h>

int main(void)
{			
	int i_SMP, j_SMP;
	
	printf("\n\n");

	for (i_SMP = 1; i_SMP <= 5; i_SMP++) {

		printf("i_SMP = %d\n", i_SMP);
		printf("--------\n\n");

		for (j_SMP = 1; j_SMP <= 3; j_SMP++) {
			printf("\tj_SMP = %d\n", j_SMP);
		}
		
		printf("\n\n");
	}


	return (0);
}

/*

i_SMP = 1
--------

        j_SMP = 1
        j_SMP = 2
        j_SMP = 3


i_SMP = 2
--------

        j_SMP = 1
        j_SMP = 2
        j_SMP = 3


i_SMP = 3
--------

        j_SMP = 1
        j_SMP = 2
        j_SMP = 3


i_SMP = 4
--------

        j_SMP = 1
        j_SMP = 2
        j_SMP = 3


i_SMP = 5
--------

        j_SMP = 1
        j_SMP = 2
        j_SMP = 3
		
*/
