#include <stdio.h>

int main(void)
{			
	int i_SMP, j_SMP,k_SMP;
	
	printf("\n\n");

	for (i_SMP = 1; i_SMP <= 5; i_SMP++) {

		printf("i_SMP = %d\n", i_SMP);
		printf("--------\n\n");

		for (j_SMP = 1; j_SMP <= 3; j_SMP++) {
			printf("\tj_SMP = %d\n", j_SMP);
			
			printf("\t--------\n\n");
			
			for (k_SMP = 1; k_SMP <= 2; k_SMP++) {
				printf("\t\tk_SMP = %d\n", k_SMP);
			}
			
			printf("\n\n");
		}
		
		printf("\n\n");
	}


	return (0);
}

/*

i_SMP = 1
--------

        j_SMP = 1
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 2
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 3
        --------

                k_SMP = 1
                k_SMP = 2




i_SMP = 2
--------

        j_SMP = 1
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 2
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 3
        --------

                k_SMP = 1
                k_SMP = 2




i_SMP = 3
--------

        j_SMP = 1
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 2
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 3
        --------

                k_SMP = 1
                k_SMP = 2




i_SMP = 4
--------

        j_SMP = 1
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 2
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 3
        --------

                k_SMP = 1
                k_SMP = 2




i_SMP = 5
--------

        j_SMP = 1
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 2
        --------

                k_SMP = 1
                k_SMP = 2


        j_SMP = 3
        --------

                k_SMP = 1
                k_SMP = 2

		
*/
