#include <stdio.h>

int main(void)
{			
	int i_SMP, j_SMP, k_SMP, p_SMP, result_SMP;
	
	printf("\n\n");

	for (i_SMP = 0; i_SMP < 64; i_SMP++) {
		
		for (j_SMP = 0; j_SMP < 64; j_SMP++) {
				
				k_SMP = ((i_SMP & 0x8) == 0);
				p_SMP = ((j_SMP & 0x8) == 0);
			
			//	printf("i_SMP=%d,", (i_SMP & 0x8));
			//	printf("R:%d,%d ",k_SMP,p_SMP);
				
				result_SMP = k_SMP ^ p_SMP;
				
				if(result_SMP == 0)
					printf(" ");
				
				if(result_SMP == 1)
					printf("* ");				
		}
		
		printf("\n\n");
				
	}

	return (0);
}

/*

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

        * * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *

* * * * * * * *         * * * * * * * *         * * * * * * * *         * * * * * * * *
		
*/
