#include <stdio.h>

int main(void)
{
	int number_SMP;
	
	printf("\n\n");
	printf("Enter Value For 'number_SMP' : ");
	scanf("%d", &number_SMP);
	
	if (number_SMP < 0)
		printf("Num = %d Is Less Than 0 (NEGATIVE) !!!\n\n", number_SMP);

	else if ((number_SMP > 0) && (number_SMP <= 100))
		printf("Num = %d Is Between 0 And 100 !!!\n\n", number_SMP);

	else if ((number_SMP > 100) && (number_SMP <= 200))
		printf("Num = %d Is Between 100 And 200 !!!\n\n", number_SMP);

	else if ((number_SMP > 200) && (number_SMP <= 300))
		printf("Num = %d Is Between 200 And 300 !!!\n\n", number_SMP);

	else if ((number_SMP > 300) && (number_SMP <= 400))
		printf("Num = %d Is Between 300 And 400 !!!\n\n", number_SMP);

	else if ((number_SMP > 400) && (number_SMP <= 500))
		printf("Num = %d Is Between 400 And 500 !!!\n\n", number_SMP);

	else if (number_SMP > 500)
		printf("Num = %d Is Greater Than 500 !!!\n\n", number_SMP);
	
	return (0);
}

/*
Enter Value For 'number_SMP' : 89
Num = 89 Is Between 0 And 100 !!!


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\03-IfElseIfLadder\01-IfElseIfLadder_WithoutOptionalEndingElse>IfElseIfLadder_WithoutOptionalEndingElse.exe


Enter Value For 'number_SMP' : 410
Num = 410 Is Between 400 And 500 !!!

*/