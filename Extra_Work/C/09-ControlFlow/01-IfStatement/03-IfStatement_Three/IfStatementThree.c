#include <stdio.h>

int main(void)
{
		
	int number_SMP;
	//code
	printf("\n\n");
	printf("Enter Value For number:");
	scanf("%d", &number_SMP);
	

	if (number_SMP < 0)
	{
		printf("Number_SMP = %d Is Less Than 0 (NEGATIVE).\n\n", number_SMP);
	}
	
	if ((number_SMP > 0) && (number_SMP <= 100))
	{
		printf("Number_SMP = %d Is Between 0 And 100.\n\n", number_SMP);
	}
	
	if ((number_SMP > 100) && (number_SMP <= 200))
	{
		printf("Number_SMP = %d Is Between 100 And 200.\n\n", number_SMP);
	}
	
	if ((number_SMP > 200) && (number_SMP <= 300))
	{
		printf("Number_SMP = %d Is Between 200 And 300.\n\n", number_SMP);
	}
	
	if ((number_SMP > 300) && (number_SMP <= 400))
	{
		printf("Number_SMP = %d Is Between 300 And 400.\n\n", number_SMP);
	}
	
	if ((number_SMP > 400) && (number_SMP <= 500))
	{
		printf("Number_SMP = %d Is Between 400 And 500.\n\n", number_SMP);
	}
	
	if (number_SMP > 500)
	{
		printf("Number_SMP = %d Is Greater Than 500.\n\n", number_SMP);
	}
	
	return (0);
}

/*
Enter Value For number:400
Num = 400 Is Between 300 And 400.


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\01-IfStatement\03-IfStatement_Three>IfStatementThree.exe


Enter Value For number:254
Num = 254 Is Between 200 And 300.


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\01-IfStatement\03-IfStatement_Three>IfStatementThree.exe


Enter Value For number:800
Num = 800 Is Greater Than 500.

*/