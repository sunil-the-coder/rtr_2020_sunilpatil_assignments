#include <stdio.h>

int main(void)
{
		
	int a_SMP;
	
	printf("\n\n");
	
	a_SMP = 5;
	if (a_SMP) // Non-zero Positive Value
	{
		printf("if-block 1 : 'A' Exists And Has Value = %d !!!\n\n", a_SMP);
	}
	
	a_SMP = -5;
	if (a_SMP) // Non-zero Negative Value
	{
		printf("if-block 2 : 'A' Exists And Has Value = %d !!!\n\n", a_SMP);
	}
	
	a_SMP = 0;
	if (a_SMP) // Zero Value
	{
		printf("if-block 3 : 'A' Exists And Has Value = %d !!!\n\n", a_SMP);
	}
	
	printf("All Three if-statements Are Done !!!\n\n");
	
	return (0);	
}

/*
if-block 1 : 'A' Exists And Has Value = 5 !!!

if-block 2 : 'A' Exists And Has Value = -5 !!!

All Three if-statements Are Done !!!

*/