#include <stdio.h>

int main(void)
{
	//variable declaration
	
	int a_SMP, b_SMP, p_SMP;
	
	a_SMP = 9, b_SMP = 30, p_SMP = 30;
	
	printf("\n");
	
	if(a_SMP < b_SMP)
	{
		printf("A_SMP is less than B_SMP\n\n");
	}
	
	if(b_SMP != p_SMP)
	{
		printf("b_SMP is not equal to p_SMP");
	}
	
	printf("Both comparison have been done.\n\n");
	
	return (0);
}

/*
A_SMP is less than B_SMP

Both comparison have been done.

*/