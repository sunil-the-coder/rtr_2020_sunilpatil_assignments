#include <stdio.h>

//Print numbers until user quits the application using break ( conditional on 'q')

int main(void)
{
	int i_SMP,j_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 1; i_SMP <= 20; i_SMP++)
	{
		for(j_SMP = 1; j_SMP <=20; j_SMP++) 
		{
			if(j_SMP > i_SMP)
			{
				break;
				
			}else
			{
				printf("* ");
			}				
		}
		
		printf("\n");
		
	}
	
	printf("\n\n");
	
}

/*

*
* *
* * *
* * * *
* * * * *
* * * * * *
* * * * * * *
* * * * * * * *
* * * * * * * * *
* * * * * * * * * *
* * * * * * * * * * *
* * * * * * * * * * * *
* * * * * * * * * * * * *
* * * * * * * * * * * * * *
* * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *
		
		
*/