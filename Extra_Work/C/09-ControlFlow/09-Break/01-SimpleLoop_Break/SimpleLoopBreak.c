#include <stdio.h>

//Print numbers until user quits the application using break ( conditional on 'q')

int main(void)
{
	int i_SMP;
	char ch_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 1; i_SMP <= 40; i_SMP++)
	{
		printf("\t%d\n",i_SMP);
		
		ch_SMP = getch();
		
		if(ch_SMP == 'Q' || ch_SMP =='q')
		{
			break;
		}
		
	}
	
	printf("\n\n");
	
}

/*
		 
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12
        13
        14
		
		*/