#include <stdio.h>

int main(void)
{
	int age_SMP;

	printf("\n\n");

	printf("Enter Age : ");
	scanf("%d", &age_SMP);

	printf("\n\n");

	if (age_SMP >= 18) 
	{
		printf("Entering if-block...\n\n");
		printf("You Are Eligible For Voting !!!\n\n");
	}
	else 
	{
		printf("Entering else-block...\n\n");
		printf("You Are NOT Eligible For Voting !!!\n\n");
	}

	printf("Bye !!!\n\n");

	
	return (0);
}

/*
Enter Age : 4


Entering else-block...

You Are NOT Eligible For Voting !!!

Bye !!!


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\02-IfElseStatement\02-IfElseStatement_Two>IfElseStatementTwo.exe


Enter Age : 45


Entering if-block...

You Are Eligible For Voting !!!

Bye !!!

*/