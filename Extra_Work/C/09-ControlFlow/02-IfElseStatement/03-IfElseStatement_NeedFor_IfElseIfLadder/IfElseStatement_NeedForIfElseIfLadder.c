#include <stdio.h>

int main(void)
{
	
	int number_SMP;
	
	//code
	printf("\n\n");
	printf("Enter Value For 'number_SMP' : ");
	scanf("%d", &number_SMP);
	
	if (number_SMP < 0) // 'if' - 01
	{
		printf("Num = %d Is Less Than 0 (NEGATIVE) !!!\n\n", number_SMP);
	}
	else // 'else' - 01
	{
		if ((number_SMP > 0) && (number_SMP <= 100)) // 'if' - 02
		{`
			printf("Num = %d Is Between 0 And 100 !!!\n\n", number_SMP);
		}
		else // 'else' - 02
		{
			if ((number_SMP > 100) && (number_SMP <= 200)) // 'if' - 03
			{
				printf("Num = %d Is Between 100 And 200 !!!\n\n", number_SMP);
			}
			else // 'else' - 03
			{
				if ((number_SMP > 200) && (number_SMP <= 300)) // 'if' - 04
				{
					printf("Num = %d Is Between 200 And 300 !!!\n\n", number_SMP);
				}
				else // 'else' - 04
				{
					if ((number_SMP > 300) && (number_SMP <= 400)) // 'if' - 05
					{
						printf("Num = %d Is Between 300 And 400 !!!\n\n", number_SMP);
					}
					else // 'else' - 05
					{
						if ((number_SMP > 400) && (number_SMP <= 500)) // 'if' - 06
						{
							printf("Num = %d Is Between 400 And 500 !!!\n\n",
							number_SMP);
						}
						else // 'else' - 06
						{
							printf("Num = %d Is Greater Than 500 !!!\n\n", number_SMP);
						} // closing brace of 'else' - 06
					} // closing brace of 'else' - 05
				} // closing brace of 'else' - 04
			} // closing brace of 'else' - 03
		} // closing brace of 'else' - 02
	} // closing brace of 'else' - 01

	
	return (0);
}

/*
E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\02-IfElseStatement\03-IfElseStatement_NeedFor_IfElseIfLadder>IfElseStatement_NeedForIfElseIfLadder.exe


Enter Value For 'number_SMP' : 45
Num = 45 Is Between 0 And 100 !!!


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\02-IfElseStatement\03-IfElseStatement_NeedFor_IfElseIfLadder>IfElseStatement_NeedForIfElseIfLadder.exe


Enter Value For 'number_SMP' : 278
Num = 278 Is Between 200 And 300 !!!


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\02-IfElseStatement\03-IfElseStatement_NeedFor_IfElseIfLadder>IfElseStatement_NeedForIfElseIfLadder.exe


Enter Value For 'number_SMP' : 122
Num = 122 Is Between 100 And 200 !!!


E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\02-IfElseStatement\03-IfElseStatement_NeedFor_IfElseIfLadder>IfElseStatement_NeedForIfElseIfLadder.exe


Enter Value For 'number_SMP' : 658
Num = 658 Is Greater Than 500 !!!

*/