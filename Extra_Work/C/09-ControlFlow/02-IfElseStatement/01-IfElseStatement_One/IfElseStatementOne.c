#include <stdio.h>

int main(void)
{
	int a_SMP, b_SMP, p_SMP;

	a_SMP = 9;
	b_SMP = 30;
	p_SMP = 30;


	// *** FIRST if-else PAIR ***
	printf("\n\n");
	if (a_SMP < b_SMP)
	{
		printf("Entering First if-block...\n\n");
		printf("A Is Less Than B !!!\n\n");
	}
	else
	{
		printf("Entering First else-block...\n\n");
		printf("A Is NOT Less Than B !!!\n\n");
	}
	
	printf("First if-else Pair Done !!!\n\n");
	
	// *** SECOND if-else PAIR ***
	printf("\n\n");
	
	if (b_SMP != p_SMP)
	{
		printf("Entering Second if-block...\n\n");
		printf("B Is NOT Equal To P !!!\n\n");
	}
	else
	{
		printf("Entering Second else-block...\n\n");
		printf("B Is Equal To P !!!\n\n");
	}

	printf("Second if-else Pair Done !!!\n\n");
	
	return (0);
}

/*
Entering First if-block...

A Is Less Than B !!!

First if-else Pair Done !!!



Entering Second else-block...

B Is Equal To P !!!

Second if-else Pair Done !!!

*/