#include<stdio.h>

int main(void)
{
	int i_num_SMP, num_SMP, i_SMP;


	printf("\n\n");
	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &i_num_SMP);

	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", i_num_SMP);
	scanf("%d", &num_SMP);

	printf("Printing Digits %d to %d : \n\n", i_num_SMP, (i_num_SMP + num_SMP));

	i_SMP = i_num_SMP;
	
	do
	{
		printf("\t%d\n", i_SMP);
		i_SMP++;
		
	}
	while (i_SMP <= (i_num_SMP + num_SMP));
	
	printf("\n\n");

	return (0);
}

/*

Enter An Integer Value From Which Iteration Must Begin : 3
How Many Digits Do You Want To Print From 3 Onwards ? : 4
Printing Digits 3 to 7 :

        3
        4
        5
        6
        7

*/