#include<stdio.h>

int main(void)
{
	int i, j;
	
	i = 1;
	
	do
	{
		printf("i=%d\n",i);
		printf("--------\n\n",i);
		
		j = 1;
	
		do
		{
			printf("\tj=%d\n",j);	
			j = j + 1;
			
		}while( j <= 2);
		
		i = i + 1;
		
		printf("\n\n");
	
	}
	while( i <= 3);
	
	return (0);
}

/*

i=1
--------

        j=1
        j=2


i=2
--------

        j=1
        j=2


i=3
--------

        j=1
        j=2

*/