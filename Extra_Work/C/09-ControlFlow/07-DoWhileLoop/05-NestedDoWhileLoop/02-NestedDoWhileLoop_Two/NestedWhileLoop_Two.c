#include<stdio.h>

int main(void)
{
	int i_SMP, j_SMP, k_SMP;
	
	i_SMP = 1;
	do
	{
		printf("i_SMP=%d\n",i_SMP);
		printf("--------\n\n");
		
		j_SMP = 1;
		
		do
		{
			printf("\tj=%d\n",j_SMP);
			printf("\t--------\n\n");

			k_SMP = 1;
						
			do
			{
				printf("\t\tk=%d\n",k_SMP);
				k_SMP++;
				
			}while ( k_SMP <= 2);
			
			printf("\n\n");
			
			j_SMP = j_SMP + 1;
			
		}while ( j_SMP <= 3);
		
		i_SMP = i_SMP + 1;
		
		printf("\n\n");
		
	}while ( i_SMP <= 5);
		
	return (0);
}

/*

i_SMP=1
--------

        j_SMP=1
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=2
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=3
        --------

                k_SMP=1
                k_SMP=2




i_SMP=2
--------

        j_SMP=1
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=2
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=3
        --------

                k_SMP=1
                k_SMP=2




i_SMP=3
--------

        j_SMP=1
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=2
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=3
        --------

                k_SMP=1
                k_SMP=2




i_SMP=4
--------

        j_SMP=1
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=2
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=3
        --------

                k_SMP=1
                k_SMP=2




i_SMP=5
--------

        j_SMP=1
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=2
        --------

                k_SMP=1
                k_SMP=2


        j_SMP=3
        --------

                k_SMP=1
                k_SMP=2

*/