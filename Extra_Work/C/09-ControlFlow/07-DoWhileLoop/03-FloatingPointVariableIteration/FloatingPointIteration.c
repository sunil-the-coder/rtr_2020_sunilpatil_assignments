#include <stdio.h>

int main(void)
{	
	
	float f_SMP;
	float f_num_SMP = 1.3f;  // we can change for diff output
	
	//code
	printf("\n\n");
	
	printf("Printing Numbers %f to %f : \n\n", f_num_SMP, (f_num_SMP * 10.0f));
	
	f_SMP = f_num_SMP;
	
	do 
	{
		printf("\t%f\n", f_SMP);
		
		f_SMP = f_SMP + f_num_SMP;
		
	}while( f_SMP <= (f_num_SMP * 10.0f) );
	
	printf("\n\n");


	return (0);
}
/*
Printing Numbers 1.300000_SMP to 13.000000_SMP :

        1.300000
        2.600000
        3.900000
        5.200000
        6.500000
        7.800000
        9.100000
        10.400001
        11.700001
*/