#include <stdio.h>

int main(void)
{	
	int a_SMP;
	
	printf("\n\n");
	
	a_SMP = 10;
	
	do 
	{
		printf("a=%d\n",a_SMP);
		a_SMP = a_SMP - 1;
		
	}while( a_SMP >= 1);
	
	printf("\n\n");
	
	return (0);
}
/*
	a=10
	a=9
	a=8
	a=7
	a=6
	a=5
	a=4
	a=3
	a=2
	a=1
*/