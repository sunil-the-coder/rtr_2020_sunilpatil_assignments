#include<stdio.h>

int main(void)
{
	int i, j;
	
	i = 1;
	while( i <= 10)
	{
		printf("i=%d\n",i);
		printf("--------\n\n",i);
		
		j = 1;
		while( j <= 5)
		{
			printf("\tj=%d\n",j);	
			j = j + 1;
		}
		
		i = i + 1;
		
		printf("\n\n");
		
	}
	return (0);
}

/*

i=1
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=2
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=3
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=4
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=5
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=6
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=7
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=8
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=9
--------

        j=1
        j=2
        j=3
        j=4
        j=5


i=10
--------

        j=1
        j=2
        j=3
        j=4
        j=5

*/