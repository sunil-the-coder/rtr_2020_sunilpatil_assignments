#include <stdio.h>
#include <conio.h>  //for getch()

// ASCII Values For 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING_SMP 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING_SMP 90

// ASCII Values For 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING_SMP 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING_SMP 122

// ASCII Values For '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING_SMP 48
#define CHAR_DIGIT_ENDING_SMP 57


int main(void)
{
		
	char ch_SMP;
	int ch_value_SMP;
	
	printf("\n\n");
	
	printf("Enter Character : ");
	ch_SMP = getch();
	
	printf("\n\n");
	
	switch (ch_SMP)
	{
	// FALL THROUGH CONDITION...
		case 'A':
		case 'a':
		
		case 'E':
		case 'e':
		
		case 'I':
		case 'i':
		
		case 'O':
		case 'o':
		
		case 'U':
		case 'u':
			printf("Character \'%c\' Entered By You, Is A VOWEL CHARACTER From TheEnglish Alphabet !!!\n\n", ch_SMP);
			break;
			
		default:
			ch_value_SMP = (int)ch_SMP;
			//If The Character Has ASCII Value Between 65 AND 90 OR Between 97 AND
		//122, It Is Still A Letter Of The Alphabet, But It Is A 'CONSONANT', and
		//NOT a 'VOWEL'...
			if ((ch_value_SMP >= CHAR_ALPHABET_UPPER_CASE_BEGINNING_SMP && ch_value_SMP <=
			CHAR_ALPHABET_UPPER_CASE_ENDING_SMP) || (ch_value_SMP >=
			CHAR_ALPHABET_LOWER_CASE_BEGINNING_SMP && ch_value_SMP <=
			CHAR_ALPHABET_LOWER_CASE_ENDING_SMP))
			{
				printf("Character \'%c\' Entered By You, Is A CONSONANT CHARACTER From The English Alphabet !!!\n\n", ch_SMP);
			}
			else if (ch_value_SMP >= CHAR_DIGIT_BEGINNING_SMP && ch_value_SMP <=
				CHAR_DIGIT_ENDING_SMP)
				{
				printf("Character \'%c\' Entered By You, Is A DIGIT CHARACTER !!!\n\n", ch_SMP);
			}
			else
			{
				printf("Character \'%c\' Entered By You, Is A SPECIAL CHARACTER !!!\n\n", ch_SMP);
			}
			break;
		}
		
	printf("Switch Case Block Complete !!!\n");
		
	return (0);
}

/*
Enter Character :

Character 'A' Entered By You, Is A VOWEL CHARACTER From TheEnglish Alphabet !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\02-CharacterRecognition\01-UsingSwitchCase>SwitchCase_CharacterRecongnition.exe


Enter Character :

Character 'p' Entered By You, Is A CONSONANT CHARACTER From The English Alphabet !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\02-CharacterRecognition\01-UsingSwitchCase>SwitchCase_CharacterRecongnition.exe


Enter Character :

Character '^' Entered By You, Is A SPECIAL CHARACTER !!!

Switch Case Block Complete !!!

*/