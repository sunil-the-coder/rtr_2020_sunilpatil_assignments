#include <stdio.h> 
#include <conio.h> 

int main(void)
{
	
	int a_SMP, b_SMP;
	int result_SMP;
	
	char option_SMP, option_division_SMP;
	
	printf("\n\n");
	
	printf("Enter Value For 'A' : ");
	scanf("%d", &a_SMP);
	
	printf("Enter Value For 'B' : ");
	scanf("%d", &b_SMP);
	
	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");
	
	printf("Enter Option : ");
	option_SMP = getch();
	
	printf("\n\n");
	
	switch (option_SMP)
	{
	// FALL THROUGH CONSITION FOR 'A' and 'a_SMP'
	case 'A':
	case 'a':
		result_SMP = a_SMP + b_SMP;
		printf("Addition Of A = %d And B = %d Gives Result %d !!!\n\n", a_SMP, b_SMP,
		result_SMP);
		break;
	
	// FALL THROUGH CONSITION FOR 'S' and 's'
	case 'S':
	case 's':
		if (a_SMP >= b_SMP)
		{
			result_SMP = a_SMP - b_SMP;
			printf("Subtraction Of B = %d From A = %d Gives Result %d !!!\n\n",
			b_SMP, a_SMP, result_SMP);
		}
		else
		{
			result_SMP = b_SMP - a_SMP;
			printf("Subtraction Of A = %d From B = %d Gives Result %d !!!\n\n",
			a_SMP, b_SMP, result_SMP);
		}
		break;

	// FALL THROUGH CONSITION FOR 'M' and 'm'
	case 'M':
	case 'm':
			result_SMP = a_SMP * b_SMP;
			printf("Multiplication Of A = %d And B = %d Gives Result %d !!!\n\n", a_SMP,b_SMP, result_SMP);
		break;

	// FALL THROUGH CONSITION FOR 'D' and 'd'
	case 'D':
	case 'd':
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter Option : ");
		option_division_SMP = getch();

		printf("\n\n");

		switch (option_division_SMP)
		{
			// FALL THROUGH CONSITION FOR 'Q' and 'q' and '/'
			case 'Q':
			case 'q':
			case '/':
				if (a_SMP >= b_SMP)
				{
					result_SMP = a_SMP / b_SMP;
					printf("Division Of A = %d By B = %d Gives Quotient = %d !!!\n\n", a_SMP, b_SMP, result_SMP);
				}
				else
				{
					result_SMP = b_SMP / a_SMP;
					printf("Division Of B = %d By A = %d Gives Quotient = %d !!!\n\n", b_SMP, a_SMP, result_SMP);
				}
				break; // 'break' of case 'Q' or case 'q' or case '/'

		 // FALL THROUGH CONSITION FOR 'R' and 'r' and '%'
			case 'R':
			case 'r':
			case '%':
				if (a_SMP >= b_SMP)
				{
						result_SMP = a_SMP % b_SMP;
					printf("Division Of A = %d By B = %d Gives Remainder = %d !!!\n\n", a_SMP, b_SMP, result_SMP);
				}
				else
				{
					result_SMP = b_SMP % a_SMP;
					printf("Division Of B = %d By A = %d Gives Remainder = %d !!!\n\n", b_SMP, a_SMP, result_SMP);
				}
				break; // 'break' of case 'R' or case 'r' or case '%'

		default: // 'default' case for switch(option_division_SMP)
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", option_division_SMP);
			break; // 'break' of 'default' of switch(option_division_SMP)
		} // ending curly brace of switch(option_division_SMP)

		break; // 'break' of case 'D' or case 'd'
	
	default: // 'default' case for switch (option_SMP)
		printf("Invalid Character %c Entered !!! Please Try Again...\n\n",
		option_SMP);
		break;
	} // ending curly brace of switch(option_SMP)

	printf("Switch Case Block Complete !!!\n");

	return(0);
}

/*

Enter Value For 'A' : 20
Enter Value For 'B' : 5
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Addition Of A = 20 And B = 5 Gives Result 25 !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>MathOperation_SwitchCase.exe


Enter Value For 'A' : 10
Enter Value For 'B' : 20
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Subtraction Of A = 10 From B = 20 Gives Result 10 !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>MathOperation_SwitchCase.exe


Enter Value For 'A' : 10
Enter Value For 'B' : 5
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Multiplication Of A = 10 And B = 5 Gives Result 50 !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>
E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>MathOperation_SwitchCase.exe


Enter Value For 'A' : 10
Enter Value For 'B' : 2
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Enter Option In Character :

'Q' or 'q' or '/' For Quotient Upon Division :
'R' or 'r' or '%' For Remainder Upon Division :
Enter Option :

Division Of A = 10 By B = 2 Gives Quotient = 5 !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>MathOperation_SwitchCase.exe


Enter Value For 'A' : 10
Enter Value For 'B' : 3
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Enter Option In Character :

'Q' or 'q' or '/' For Quotient Upon Division :
'R' or 'r' or '%' For Remainder Upon Division :
Enter Option :

Division Of A = 10 By B = 3 Gives Remainder = 1 !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\01-UsingSwitchCase>
                                      
*/