#include <stdio.h> 
#include <conio.h> 

int main(void)
{
	
	int a_SMP, b_SMP;
	int result_SMP;
	
	char option_SMP, option_division_SMP;
	
	printf("\n\n");
	
	printf("Enter Value For a_SMP : ");
	scanf("%d", &a_SMP);
	
	printf("Enter Value For b_SMP : ");
	scanf("%d", &b_SMP);
	
	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");
	
	printf("Enter Option : ");
	option_SMP = getch();
	
	printf("\n\n");
	
	if (option_SMP == 'A' || option_SMP == 'a')
	{
		result_SMP = a_SMP + b_SMP;
		printf("Addition Of a_SMP = %d And b_SMP = %d Gives Result %d !!!\n\n", a_SMP, b_SMP,
		result_SMP);
	}
	else if (option_SMP == 'S' || option_SMP == 's')
	{
		if (a_SMP >= b_SMP)
		{
			result_SMP = a_SMP - b_SMP;
			printf("Subtraction Of b_SMP = %d From a_SMP = %d Gives Result %d !!!\n\n",
			b_SMP, a_SMP, result_SMP);
		}
		else
		{
			result_SMP = b_SMP - a_SMP;
			printf("Subtraction Of a_SMP = %d From b_SMP = %d Gives Result %d !!!\n\n",
			a_SMP, b_SMP, result_SMP);
		}
	}
	else if(option_SMP == 'M' || option_SMP == 'm')
	{
		result_SMP = a_SMP * b_SMP;
		printf("Multiplication Of a_SMP = %d And b_SMP = %d Gives Result %d !!!\n\n", a_SMP,
		b_SMP, result_SMP);
	}
	else if (option_SMP == 'D' || option_SMP == 'd')
	{
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");
		printf("Enter Option : ");

		option_division_SMP = getch();

		printf("\n\n");

		if (option_division_SMP == 'Q' || option_division_SMP == 'q' || option_division_SMP
		== '/')
		{
			if (a_SMP >= b_SMP)
			{
				result_SMP = a_SMP / b_SMP;
				printf("Division Of a_SMP = %d By b_SMP = %d Gives Quotient = %d !!!\n\n", a_SMP, b_SMP, result_SMP);
			}
			else
			{
				result_SMP = b_SMP / a_SMP;
				printf("Division Of b_SMP = %d By a_SMP = %d Gives Quotient = %d !!!\n\n", b_SMP, a_SMP, result_SMP);
			}
		}
		else if (option_division_SMP == 'R' || option_division_SMP == 'r' || option_division_SMP == '%')
		{
			if (a_SMP >= b_SMP)
			{
				result_SMP = a_SMP % b_SMP;
				printf("Division Of a_SMP = %d By b_SMP = %d Gives Remainder = %d !!!\n\n", a_SMP, b_SMP, result_SMP);
			}
			else
			{
				result_SMP = b_SMP % a_SMP;
				printf("Division Of b_SMP = %d By a_SMP = %d Gives Remainder = %d !!!\n\n", b_SMP, a_SMP, result_SMP);
			}
		}
		else
		printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", option_division_SMP);
	}
	else
		printf("Invalid Character %c Entered !!! Please Try Again...\n\n",
		option_SMP);

	printf("If - Else If - Else Ladder Complete !!!\n");

	return(0);
}

/*

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 10
Enter Value For b_SMP : 4
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Addition Of a_SMP = 10 And b_SMP = 4 Gives Result 14 !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 20
Enter Value For b_SMP : 4
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Subtraction Of b_SMP = 4 From a_SMP = 20 Gives Result 16 !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>
E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 10
Enter Value For b_SMP : 3
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Multiplication Of a_SMP = 10 And b_SMP = 3 Gives Result 30 !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>
E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 56
Enter Value For b_SMP : 3
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Enter Option In Character :

'Q' or 'q' or '/' For Quotient Upon Division :
'R' or 'r' or '%' For Remainder Upon Division :
Enter Option :

Division Of a_SMP = 56 By b_SMP = 3 Gives Quotient = 18 !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 54
Enter Value For b_SMP : 3
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Invalid Character f Entered !!! Please Try Again...

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>MathOperation_IfElseIfLadder.exe


Enter Value For a_SMP : 56
Enter Value For b_SMP : 3
Enter Option In Character :

'A' or 'a' For Addition :
'S' or 's' For Subtraction :
'M' or 'm' For Multiplication :
'D' or 'd' For Division :

Enter Option :

Enter Option In Character :

'Q' or 'q' or '/' For Quotient Upon Division :
'R' or 'r' or '%' For Remainder Upon Division :
Enter Option :

Division Of a_SMP = 56 By b_SMP = 3 Gives Remainder = 2 !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\03-MathematicalOperations\02-UsingIfElseIfLadder>

                                      
*/