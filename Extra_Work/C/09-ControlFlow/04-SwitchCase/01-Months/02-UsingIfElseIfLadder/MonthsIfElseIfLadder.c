#include <stdio.h>

int main(void)
{
		
	int number_month_SMP;
	
	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) : ");
	scanf("%d", &number_month_SMP);
	printf("\n\n");

	// IF - ELSE - IF LADDER BEGINS FROM HERE...
	if (number_month_SMP == 1) //like 'case 1'
		printf("Month Number %d Is JANUARY !!!\n\n", number_month_SMP);

	else if (number_month_SMP == 2) //like 'case 2'
		printf("Month Number %d Is FEBRUARY !!!\n\n", number_month_SMP);

	else if (number_month_SMP == 3) //like 'case 3'
		printf("Month Number %d Is MARCH !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 4) //like 'case 4'
		printf("Month Number %d Is APRIL !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 5) //like 'case 5'
		printf("Month Number %d Is MAY !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 6) //like 'case 6'
		printf("Month Number %d Is JUNE !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 7) //like 'case 7'
		printf("Month Number %d Is JULY !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 8) //like 'case 8'
		printf("Month Number %d Is AUGUST !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 9) //like 'case 9'
		printf("Month Number %d Is SEPTEMBER !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 10) //like 'case 10'
		printf("Month Number %d Is OCTOBER !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 11) //like 'case 11'
		printf("Month Number %d Is NOVEMBER !!!\n\n", number_month_SMP);
	
	else if (number_month_SMP == 12) //like 'case 12'
		printf("Month Number %d Is DECEMBER !!!\n\n", number_month_SMP);
	
	else //like 'default'...just like 'default' is optional in switch-case, so is 
	//'else' in the if-else if-else ladder...
	printf("Invalid Month Number %d Entered !!! Please Try Again...\n\n",
	number_month_SMP);
	printf("If - Else If - Else Ladder Complete !!!\n");
		
	return (0);
}

/*
Enter Number Of Month (1 to 12) : 4


Month Number 4 Is APRIL !!!

If - Else If - Else Ladder Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\01-Months\02-UsingIfElseIfLadder>MonthsIfElseIfLadder.exe


Enter Number Of Month (1 to 12) : 13


Invalid Month Number 13 Entered !!! Please Try Again...

If - Else If - Else Ladder Complete !!!

*/