#include <stdio.h>

int main(void)
{
		
	int number_month_SMP;
	
	//code
	printf("\n\n");
	printf("Enter Number Of Month (1 to 12) : ");
	scanf("%d", &number_month_SMP);
	
	printf("\n\n");
	
	switch (number_month_SMP)
	{
	case 1: //like 'if'
		printf("Month Number %d Is JANUARY !!!\n\n", number_month_SMP);
		break;
		
	case 2: //like 'else if'
		printf("Month Number %d Is FEBRUARY !!!\n\n", number_month_SMP);
		break;
		
	case 3: //like 'else if'
		printf("Month Number %d Is MARCH !!!\n\n", number_month_SMP);
		break;
		
	case 4: //like 'else if'
		printf("Month Number %d Is APRIL !!!\n\n", number_month_SMP);
		break;
		
	case 5: //like 'else if'
		printf("Month Number %d Is MAY !!!\n\n", number_month_SMP);
		break;
		
	case 6: //like 'else if'
		printf("Month Number %d Is JUNE !!!\n\n", number_month_SMP);
		break;
		
	case 7: //like 'else if'
		printf("Month Number %d Is JULY !!!\n\n", number_month_SMP);
		break;
		
	case 8: //like 'else if'
		printf("Month Number %d Is AUGUST !!!\n\n", number_month_SMP);
		break;
		
	case 9: //like 'else if'
		printf("Month Number %d Is SEPTEMBER !!!\n\n", number_month_SMP);
		break;
	case 10: //like 'else if'
		printf("Month Number %d Is OCTOBER !!!\n\n", number_month_SMP);
		break;
		
	case 11: //like 'else if'
		printf("Month Number %d Is NOVEMBER !!!\n\n", number_month_SMP);
		break;
		
	case 12: //like 'else if'
		printf("Month Number %d Is DECEMBER !!!\n\n", number_month_SMP);
		break;
		
	default: //like ending OPTIONAL 'else'...just like terminating 'else' is
	//optional in if-else if-else ladder, so is the 'default' case optional in
	//switch-case
		printf("Invalid Month Number %d Entered !!! Please Try Again...\n\n",
		number_month_SMP);
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	
	return (0);
}

/*
Enter Number Of Month (1 to 12) : 6


Month Number 6 Is JUNE !!!

Switch Case Block Complete !!!

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\09-ControlFlow\04-SwitchCase\01-Months\01-UsingSwitchCase>MonthsSwitchCase.exe


Enter Number Of Month (1 to 12) : 13


Invalid Month Number 13 Entered !!! Please Try Again...

Switch Case Block Complete !!!

*/