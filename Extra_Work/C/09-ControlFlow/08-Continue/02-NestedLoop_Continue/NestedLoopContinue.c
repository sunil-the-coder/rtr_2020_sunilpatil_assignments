#include <stdio.h>

//Print odd and even numbers.

int main(void)
{
	int i_SMP, j_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP <= 5; i_SMP++)
	{
		if( i_SMP % 2 != 0)
		{
				printf("i_SMP = %d\n",i_SMP);
				printf("--------\n");
				
				//For each odd number netween(1-5) print even numbers between (1-5)
				for(j_SMP = 1; j_SMP <= 5; j_SMP++)
				{
					if(j_SMP % 2 == 0 )
					{
						printf("\tj=%d\n",j_SMP);
						
					}else
					{
						continue; // go to next loop iteration.
					}
				}
				printf("\n\n");
		}else 
		{
			continue;
		}
		
	}
	
	printf("\n\n");
	
}

/*

i_SMP = 1
--------
        j=2
        j=4


i_SMP = 3
--------
        j=2
        j=4


i_SMP = 5
--------
        j=2
        j=4
		
*/