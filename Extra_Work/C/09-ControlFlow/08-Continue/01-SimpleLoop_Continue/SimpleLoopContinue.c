#include <stdio.h>

//Print even numbers.

int main(void)
{
	int i_SMP;
	
	printf("\n\n");
	
	for(i_SMP = 0; i_SMP <= 50; i_SMP++)
	{
		if( i_SMP % 2 != 0)
			continue; // continue with next iteration of loop
		else
			printf("\tEven:%d\n",i_SMP);
		
	}
	
	printf("\n\n");
	
}

/*

		Even:0
        Even:2
        Even:4
        Even:6
        Even:8
        Even:10
        Even:12
        Even:14
        Even:16
        Even:18
        Even:20
        Even:22
        Even:24
        Even:26
        Even:28
        Even:30
        Even:32
        Even:34
        Even:36
        Even:38
        Even:40
        Even:42
        Even:44
        Even:46
        Even:48
        Even:50
		
		*/