#include<stdio.h>

struct Point
{
	int x;
	int y;	
} point_A_smp,point_B_SMP;


int main(void)
{	
	point_A_smp.x = 4;
	point_A_smp.y = 8;
	
	point_B_SMP.x = 3;
	point_B_SMP.y = 6;
	
	printf("\n\n");
	
	printf("Point_A: (%d,%d)\n",point_A_smp.x,point_A_smp.y);
	printf("Point_B: (%d,%d)\n",point_B_SMP.x,point_B_SMP.y);
		
	printf("\n\n");
	
	return (0);
}

/*

	Point_A: (4,8)
	Point_B: (3,6)

*/