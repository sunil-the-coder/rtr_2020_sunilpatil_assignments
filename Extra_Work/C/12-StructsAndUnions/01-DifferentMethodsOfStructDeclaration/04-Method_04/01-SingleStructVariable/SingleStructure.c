#include<stdio.h>

struct Student
{
	int iAge_SMP;
	float f;
	double dHeight_SMP;
	char cGender_SMP;
};



int main(void)
{
	struct Student stud; //local declaration
	int sTotalSize;
	
	stud.iAge_SMP = 10;
	stud.f = 5.4f;
	stud.dHeight_SMP = 5.6;
	stud.cGender_SMP = 'M';
	
	printf("\n\n");
	
	printf("Age:%d\n",stud.iAge_SMP);
	printf("f:%f\n",stud.f);
	printf("Height:%lf\n",stud.dHeight_SMP);
	printf("Gender:%c\n",stud.cGender_SMP);
	
	printf("\n\n");
	
	printf("Size of Age:%d bytes \n",sizeof(stud.iAge_SMP));
	printf("Size of f:%d bytes\n",sizeof(stud.f));
	printf("Size of dHeight_SMP:%d bytes\n",sizeof(stud.dHeight_SMP));
	printf("Size of cGender_SMP:%d bytes\n",sizeof(stud.cGender_SMP));
	
	printf("\n\n");
	sTotalSize = sizeof(struct Student);
	//sTotalSize = sizeof(stud);
	
	printf("Total Size :%d bytes \n",sTotalSize);
	
	return (0);
}

/*
Age:10
f:5.400000
Height:5.600000
Gender:M


Size of Age:4 bytes
Size of f:4 bytes
Size of dHeight_SMP:8 bytes
Size of cGender_SMP:1 bytes


Total Size :24 bytes

*/