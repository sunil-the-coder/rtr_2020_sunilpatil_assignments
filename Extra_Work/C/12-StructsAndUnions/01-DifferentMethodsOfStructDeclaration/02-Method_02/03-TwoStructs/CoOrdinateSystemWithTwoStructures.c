#include<stdio.h>

struct Point
{
	int x;
	int y;	
	
} ;

struct Point point;

struct PointProperties
{	
	int quadrant; //0,1,2,3,4 => 0 -> Point lies on line
	
	char axis_location[10]; //+ve X, -ve X, +ve Y, -ve Y
	
};

struct PointProperties point_props;


int main(void)
{
	printf("Enter x-Coordinate:");
	scanf("%d",&point.x);
	
	printf("Enter y-Coordinate:");
	scanf("%d",&point.y);
	
	printf("Coordinates: (x,y) = (%d,%d)\n",point.x,point.y);
		
	//Logic to locate quadrant and its axis_location.
	
	if(point.x == point.y)
	{
		printf("Point is the Center/Origin: (%d,%d)\n",point.x,point.y);
		
	}else 
	{
		//For one of the zero value & other non-zero
		if(point.x == 0)
		{
			if(point.y < 0)
				strcpy(point_props.axis_location, "Negative Y");
		
			if(point.y > 0)
				strcpy(point_props.axis_location, "Positive Y");
			
			point_props.quadrant = 0; // No quadrant for point on line
			
			printf("The point lies on %s axis !!! \n",point_props.axis_location);
			
		}else if(point.y == 0)
		{
			if(point.x < 0)
				strcpy(point_props.axis_location,"Negative X");
			if(point.x > 0)
				strcpy(point_props.axis_location,"Positive X");
			
			point_props.quadrant = 0;
			
			printf("The point lies on %s axis !!! \n",point_props.axis_location);
			
		}else 
		{
			//Both are non-zero. Now find exact quadrant.
			
			point_props.axis_location[0] = '\0'; //The point lying in any of the 4 quadrant can not be lying on any of the co-ordinate axes.
			
			if(point.x > 0 && point.y > 0 )
				point_props.quadrant = 1;
			else if(point.x > 0 && point.y < 0 )
				point_props.quadrant = 2;
			else if(point.x < 0 && point.y < 0 )
				point_props.quadrant = 3;
			else
				point_props.quadrant = 4;
			
			printf("The point lies on Quadrant Number %d !!! \n",point_props.quadrant);
		}
		
	}
	
		
	printf("\n\n");
	
	return (0);
}

/*

E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:0
Enter y-Coordinate:0
Coordinates: (x,y) = (0,0)
Point is the Center/Origin: (0,0)



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:0
Enter y-Coordinate:5
Coordinates: (x,y) = (0,5)
The point lies on Positive Y axis !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:-4
Enter y-Coordinate:0
Coordinates: (x,y) = (-4,0)
The point lies on Negative X axis !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:2
Enter y-Coordinate:-4
Coordinates: (x,y) = (2,-4)
The point lies on Quadrant Number 2 !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:1
Enter y-Coordinate:
0
Coordinates: (x,y) = (1,0)
The point lies on Positive X axis !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:5
Enter y-Coordinate:10
Coordinates: (x,y) = (5,10)
The point lies on Quadrant Number 1 !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:1
Enter y-Coordinate:-5
Coordinates: (x,y) = (1,-5)
The point lies on Quadrant Number 2 !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:-2
Enter y-Coordinate:-3
Coordinates: (x,y) = (-2,-3)
The point lies on Quadrant Number 3 !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:4
Enter y-Coordinate:-3
Coordinates: (x,y) = (4,-3)
The point lies on Quadrant Number 2 !!!



E:\RTR 2020\CodeRepository\rtr_2020_sunilpatil_assignments\Extra_Work\C\12-Structs\01-DifferentMethodsOfStructDeclaration\01-Method_01\03-TwoStructs>CoOrdinateSystemWithTwoStructures.exe
Enter x-Coordinate:-4
Enter y-Coordinate:5
Coordinates: (x,y) = (-4,5)
The point lies on Quadrant Number 4 !!!

*/