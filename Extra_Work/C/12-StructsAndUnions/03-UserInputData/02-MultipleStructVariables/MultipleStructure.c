#include<stdio.h>

struct Point
{
	int x;
	int y;	
};

struct Point point_A_SMP,point_B_SMP;


int main(void)
{	
	printf("Enter x:");	
	scanf("%d",&point_A_SMP.x);
	
	printf("Enter y:");	
	scanf("%d",&point_A_SMP.y);
	
	printf("Enter x:");	
	scanf("%d",&point_B_SMP.x);
	
	printf("Enter y:");	
	scanf("%d",&point_B_SMP.y);
	
	printf("\n\n");
	
	printf("Point_A: (%d,%d)\n",point_A_SMP.x,point_A_SMP.y);
	printf("Point_B: (%d,%d)\n",point_B_SMP.x,point_B_SMP.y);
		
	printf("\n\n");
	
	return (0);
}

/*

Enter x:10
Enter y:5
Enter x:4
Enter y:5


Point_A: (10,5)
Point_B: (4,5)


*/