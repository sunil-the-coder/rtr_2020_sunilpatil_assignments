#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char ch;
	
};


int main(void)
{
	struct MyData data;
	
	data.i = 10;
	data.f = 5.6f;
	data.d = 35262.53252;
	data.ch = 'A';
	
	printf("Address of i=%p\n", &data.i);
	printf("Address of f=%p\n", &data.f);
	printf("Address of d=%p\n", &data.d);
	printf("Address of ch=%p\n", &data.ch);
	
	printf("\n\n");
			
	return (0);
}

/*
Address of i=006FF948
Address of f=006FF94C
Address of d=006FF950
Address of ch=006FF958

*/