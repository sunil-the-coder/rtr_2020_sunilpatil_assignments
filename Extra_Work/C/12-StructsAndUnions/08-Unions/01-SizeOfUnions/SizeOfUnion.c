#include<stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char ch;
	
};

union MyUnion
{
	int i;
	float f;
	double d;
	char ch;
	
};


int main(void)
{
	struct MyStruct s_SMP;
	union MyUnion u_SMP;
	
	
	printf("Size of structure = %u_SMP \n", sizeof(s_SMP));
	printf("Size of union = %u_SMP \n", sizeof(u_SMP));
	
	printf("\n\n");
			
	return (0);
}

/*

Size of structure = 24
Size of union = 8

*/