#include<stdio.h>


union MyUnion
{
	int i;
	float f;
	double d;
	char ch;
	
};


int main(void)
{	
	union MyUnion u1,u2;
	
	//Mostly used for having mulitple values in single group but at at time wants to use one.
	u1.i = 10;
	u1.f = 5.6f;
	u1.d = 35262.53252;
	u1.ch = 'A';
	
	//Last assigned will overite previous values. So latest value will be used.
	printf("Value of i=%d\n", u1.i);
	printf("Value of f=%f\n", u1.f);
	printf("Value of d=%lf\n", u1.d);
	printf("Value of ch=%c\n", u1.ch);
	
	printf("\n\n");
	
	//All the address will be same.
	printf("Address of i=%p\n", &u1.i);
	printf("Address of f=%p\n", &u1.f);
	printf("Address of d=%p\n", &u1.d);
	printf("Address of ch=%p\n", &u1.ch);
	
	
	printf("\n\n");
	
	printf("For U2 members:\n\n");	
	
	u2.i = 10;
	printf("Value of i=%d\n", u2.i);
	
	u2.f = 1026.2f;
	printf("Value of f=%f\n\n", u2.f);
	
	
	printf("Address of i=%p\n", &u2.i);
	printf("Address of f=%p\n", &u2.f);
	printf("Address of d=%p\n", &u2.d);
	printf("Address of ch=%p\n", &u2.ch);
	
	
	printf("\n\n");
			
	return (0);
}

/*

Value of i=174547521
Value of f=0.000000
Value of d=35262.532520
Value of ch=A


Address of i=010FFD30
Address of f=010FFD30
Address of d=010FFD30
Address of ch=010FFD30


For U2 members:

Value of i=10
Value of f=1026.199951

Address of i=010FFD28
Address of f=010FFD28
Address of d=010FFD28
Address of ch=010FFD28

*/