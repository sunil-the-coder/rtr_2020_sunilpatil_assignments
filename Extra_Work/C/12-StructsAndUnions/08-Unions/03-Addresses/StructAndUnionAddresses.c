#include<stdio.h>


struct MyData
{
	int i;
	float f;
	double d;
	char ch;
	
};
union MyUnion
{
	int i;
	float f;
	double d;
	char ch;
	
};


int main(void)
{	

	struct MyData data_SMP;
	union MyUnion u1_SMP;
	
	data_SMP.i = 10;
	data_SMP.f = 5.6f;
	data_SMP.d = 35262.53252;
	data_SMP.ch = 'A';
	
	printf("Addresses of Structures:\n\n");
	
	printf("i=%p\n", &data_SMP.i);
	printf("f=%p\n", &data_SMP.f);
	printf("d=%p\n", &data_SMP.d);
	printf("ch=%p\n", &data_SMP.ch);
	
	
	printf("\nValues of Structures:\n\n");
	printf("i=%d\n", data_SMP.i);
	printf("f=%f\n", data_SMP.f);
	printf("d=%lf\n", data_SMP.d);
	printf("ch=%c\n", data_SMP.ch);
	
	
	//Mostly used for having mulitple values in single group but at at time wants to use one.
	u1_SMP.i = 10;
	u1_SMP.f = 5.6f;
	u1_SMP.d = 35262.53252;
	u1_SMP.ch = 'A';
	
	printf("\nAddresses of Unions:\n\n");
	
	//All the address will be same.
	printf("i=%p\n", &u1_SMP.i);
	printf("f=%p\n", &u1_SMP.f);
	printf("d=%p\n", &u1_SMP.d);
	printf("ch=%p\n", &u1_SMP.ch);
	
	printf("\nValues of Unions:\n\n");
	
	//Last assigned will overite previous values. So latest value will be used.
	printf("i=%d\n", u1_SMP.i);
	printf("f=%f\n", u1_SMP.f);
	printf("d=%lf\n", u1_SMP.d);
	printf("ch=%c\n", u1_SMP.ch);
	
	printf("\n\n");
			
	return (0);
}

/*

Addresses of Structures:

i=00E9F7EC
f=00E9F7F0
d=00E9F7F4
ch=00E9F7FC

Values of Structures:

i=10
f=5.600000
d=35262.532520
ch=A

Addresses of Unions:

i=00E9F7E4
f=00E9F7E4
d=00E9F7E4
ch=00E9F7E4

Values of Unions:

i=174547521
f=0.000000
d=35262.532520
ch=A

*/