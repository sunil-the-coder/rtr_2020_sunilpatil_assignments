#include<stdio.h>


//Maintain the padhe for each number.

struct MyNumber
{
	int num_SMP;
	int num_table_SMP[10];
};

struct NumberTable
{	
	struct MyNumber a; 
	struct MyNumber b;
	
};

int main(void)
{
	struct NumberTable table;
	int i_SMP;

	table.a.num_SMP = 2;		// 2 cha padha
	for(i_SMP = 0; i_SMP < 10; i_SMP++ )
	{
		table.a.num_table_SMP[i_SMP] = table.a.num_SMP * (i_SMP + 1);
	}
	
	printf("Table of %d:\n\n", table.a.num_SMP);	
	for(i_SMP = 0; i_SMP < 10; i_SMP++ )
	{
		printf("%d * %d = %d\n",table.a.num_SMP, (i_SMP+1), table.a.num_table_SMP[i_SMP]);
	}
	
	printf("\n\n");
	
	table.b.num_SMP = 3;		 //3 cha padha
	for(i_SMP = 0; i_SMP < 10; i_SMP++ )
	{
		table.b.num_table_SMP[i_SMP] = table.b.num_SMP * (i_SMP + 1);
	}
	
	printf("Table of %d:\n\n", table.b.num_SMP);	
	for(i_SMP = 0; i_SMP < 10; i_SMP++ )
	{
		printf("%d * %d = %d\n",table.b.num_SMP, (i_SMP+1), table.b.num_table_SMP[i_SMP]);
	}
	

	printf("\n\n");
			
	
	return (0);
}

/*

Table of 2:

2 * 1 = 2
2 * 2 = 4
2 * 3 = 6
2 * 4 = 8
2 * 5 = 10
2 * 6 = 12
2 * 7 = 14
2 * 8 = 16
2 * 9 = 18
2 * 10 = 20


Table of 3:

3 * 1 = 3
3 * 2 = 6
3 * 3 = 9
3 * 4 = 12
3 * 5 = 15
3 * 6 = 18
3 * 7 = 21
3 * 8 = 24
3 * 9 = 27
3 * 10 = 30


*/