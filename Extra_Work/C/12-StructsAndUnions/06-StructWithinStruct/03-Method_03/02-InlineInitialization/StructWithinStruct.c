#include<stdio.h>



int main(void)
{
	int length, breadth, area;
	
	struct Rectangle
	{
		
		struct Point
		{
			int x;
			int y;
			
		} point_01, point_02;
		
		
	} rect = { {1, 15}, {15, 25} };

	
	//Initialize 4 points on rectangle
	
	rect.point_01.x = 1;
	rect.point_01.y = 15;
	
	rect.point_02.x = 15;
	rect.point_02.y = 25;
	
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
		length = length * -1;
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;
	
	
	printf("Length: %d\n",length);
	printf("Breadth: %d\n",breadth);
	printf("Area: %d\n",area);
	
	printf("\n\n");
			
	
	return (0);
}

/*

Length: 10
Breadth: 14
Area: 140


*/