#include<stdio.h>



int main(void)
{
	int length_SMP, breadth_SMP, area_SMP;
	
	struct Point
	{
			int x_SMP;
			int y_SMP;
			
	};
	
		
	struct Rectangle
	{	
		struct Point point_01, point_02;

	};
	
	struct Rectangle rect = { {1, 15}, {15, 25} };

	
	//Initialize 4 points on rectangle
	
	rect.point_01.x_SMP = 1;
	rect.point_01.y_SMP = 15;
	
	rect.point_02.x_SMP = 15;
	rect.point_02.y_SMP = 25;
	
	length_SMP = rect.point_02.y_SMP - rect.point_01.y_SMP;
	if(length_SMP < 0)
		length_SMP = length_SMP * -1;
	
	breadth_SMP = rect.point_02.x_SMP - rect.point_01.x_SMP;
	if(breadth_SMP < 0)
		breadth_SMP = breadth_SMP * -1;

	area_SMP = length_SMP * breadth_SMP;
	
	
	printf("Length: %d\n",length_SMP);
	printf("Breadth: %d\n",breadth_SMP);
	printf("Area: %d\n",area_SMP);
	
	printf("\n\n");
			
	
	return (0);
}

/*

Length: 10
Breadth: 14
Area: 140


*/