#include<stdio.h>

#define MAX_CHARACTERS 50
#define MARITAL_STATUS 10
#define NUMBER_OF_EMPLOYEE 2

struct Employee
{
		char name[MAX_CHARACTERS];
		int age;
		float salary;
		char gender;
		char marital_status[MARITAL_STATUS];
		
};

int main(void)
{
	
	//Custom gets() function
	void MyGetString(char[], int);
	
	struct Employee employees[NUMBER_OF_EMPLOYEE];
	int i;
	
	for(i = 0; i < NUMBER_OF_EMPLOYEE; i++) {
	
		printf("\n\n");
		printf("Enter name:");
		//scanf("%s",&employees[i].name);
		
		MyGetString(employees[i].name,MAX_CHARACTERS);
		
		printf("\n\n");
		printf("Enter Marital Status(Married/UnMarried):");
		//scanf("%s",&employees[i].marital_status);
		MyGetString(employees[i].marital_status,MAX_CHARACTERS);
		
		printf("\n\n");
		
		printf("Enter age:");
		scanf("%d",&employees[i].age);
		
		printf("\n\n");
		printf("Enter salary:");
		scanf("%f",&employees[i].salary);
		
		printf("\n\n");
		printf("Enter gender:");
		employees[i].gender = getch(); //Necessary step ( scanf() just moved cursor to next line without accepting gender
		printf("%c",employees[i].gender); 
		
		
	}
	
	for(i = 0; i < NUMBER_OF_EMPLOYEE; i++) {
		printf("%s\n", employees[i].name);
		printf("%s\n", employees[i].marital_status);
		printf("%d years\n", employees[i].age);
		printf("Rs.%f\n", employees[i].salary);
		printf("%c\n", employees[i].gender);
		
		printf("\n\n");
	}
			
	printf("\n\n");
	
	
	return (0);
}

//I liked it. 
void MyGetString(char buffer[], int str_size)
{
	int i;
	char ch = '\0';
	
	//read one by one char until user enters to next page(\r) 
	//and till max length no of characters.
	
	i = 0;
	do
	{
		ch = getch();
		buffer[i] = ch;
		printf("%c",ch);
		i++;
		
	}while((ch != '\r') && (i < str_size));
	
	if(i == str_size) {
		buffer[i - 1] = '\0';	
	}else {
		buffer[i] = '\0';
	}
		
}

/*

married
27
50000.000000
M


Ganesh
Unmarree
26
60000.000000
M

*/