#include<stdio.h>

#define MAX_CHARACTERS 20
#define MARITAL_STATUS 10

struct Employee
{
		char name[MAX_CHARACTERS];
		int age;
		float salary;
		char gender;
		char marital_status[MARITAL_STATUS];
		
};

int main(void)
{
	struct Employee employees[2];
	int i;
	
	strcpy(employees[0].name,"Sunil Patil");
	employees[0].age = 28;
	employees[0].salary = 50000;
	employees[0].gender = 'M';
	strcpy(employees[0].marital_status,"Married");
	
	
	strcpy(employees[1].name,"Smita Patil");
	employees[1].age = 25;
	employees[1].salary = 40000;
	employees[1].gender = 'F';
	strcpy(employees[1].marital_status,"Married");
		
	
	for(i = 0; i < 2; i++) {
		printf("%s\n", employees[i].name);
		printf("%s\n", employees[i].marital_status);
		printf("%d\n", employees[i].age);
		printf("%f\n", employees[i].salary);
		printf("%c\n", employees[i].gender);
		
		printf("\n\n");
	}
			
	printf("\n\n");
	
	
	return (0);
}

/*

Sunil Patil
Married
28
50000.000000
M


Smita Patil
Married
25
40000.000000
F

*/