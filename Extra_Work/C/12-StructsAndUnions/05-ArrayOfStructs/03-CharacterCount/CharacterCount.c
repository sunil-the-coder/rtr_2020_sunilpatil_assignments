#include<stdio.h>
#include<string.h>

#define MAX_CHARACTERS 50

struct CharacterCount
{
	char ch;
	int ch_count;
	
}char_count[] = {{'A',0},{'B',0},{'C',0}};

//struct arrar inline initliazation

#define SIZE_OF_ARRAY_OF_STRUCT sizeof(char_count)
#define SIZE_OF_ONE_STRUCTURE_FROM_THE_ARRAY_OF_STRUCT sizeof(char_count[0])
#define NO_ELEMENTS_IN_ARRAY (SIZE_OF_ARRAY_OF_STRUCT / SIZE_OF_ONE_STRUCTURE_FROM_THE_ARRAY_OF_STRUCT)


int main(void)
{
	char str[MAX_CHARACTERS];
	printf("Enter string:");
	
	gets_s(str, MAX_CHARACTERS);
	
	int i,j;
	int strlength = strlen(str);
	
	for(i = 0; i < strlength; i++) 
	{
		//Iterate and capture one by one char then find in array of structure and increment its matching count.

		for(j = 0; j < NO_ELEMENTS_IN_ARRAY; j++)
		{
				//Convert first to upper case.
				str[i] = toupper(str[i]); //to match in given values.
				
				if(str[i] == char_count[j].ch) {
					char_count[j].ch_count++;
				}
		}
		
	}
	
	for(i = 0; i < NO_ELEMENTS_IN_ARRAY; i++) 
	{
		printf("%c = %d\n",char_count[i].ch, char_count[i].ch_count);
	}
	
	printf("\n\n");
			
	
	return (0);
}

/*

Enter string:ABACCBAEBAAB
A = 5
B = 4
C = 2


*/