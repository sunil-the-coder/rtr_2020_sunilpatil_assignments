#include<stdio.h>

#define ISIZE 5
#define NUM_STRINGS 2
#define MAX_CHARACTERS 10

struct MyData
{
		int numbers[ISIZE];
		char names[NUM_STRINGS][MAX_CHARACTERS];
		
};

int main(void)
{
	struct MyData mydata;
	int i, j;
	
	for(i = 0; i < ISIZE; i++)
		mydata.numbers[i] = i + 1;
		
	strcpy(mydata.names[0], "Welcome");
	strcpy(mydata.names[1], "RTR 2020-21");
	
	for(i = 0; i < ISIZE; i++)
		printf("%d ",mydata.numbers[i]);
		
	printf("\n\n");	
	
	printf("%s\n",mydata.names[0]);
	printf("%s\n",mydata.names[1]);
			
	printf("\n\n");
	
	
	return (0);
}

/*

1 2 3 4 5

Welcome
RTR 2020-21

*/