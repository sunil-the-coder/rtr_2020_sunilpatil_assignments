#include<stdio.h>

struct Student
{
	int iAge_SMP;
	float f;
	double dHeight_SMP;
	char cGender_SMP;
}stud = {10,5.4f,5.6,'M'};


int main(void)
{
	printf("\n\n");
	
	printf("Age:%d\n",stud.iAge_SMP);
	printf("f:%f\n",stud.f);
	printf("Height:%lf\n",stud.dHeight_SMP);
	printf("Gender:%c\n",stud.cGender_SMP);
	
	printf("\n\n");
	
	
	return (0);
}

/*
Age:10
f:5.400000
Height:5.600000
Gender:M

*/