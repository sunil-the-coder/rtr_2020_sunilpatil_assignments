#include<stdio.h>

struct Student
{
		int iAge_SMP;
		float f;
		double dHeight_SMP;
		char cGender_SMP;
		
};

int main(void)
{
	struct Student stud_one = {45, 3.7f, 1.23453, 'A'};
	
	struct Student stud_two = {'P', 5.7f, 12.635, 68}; // 'P' ASCII will be stored & 68 character will be stored. ('D')
	
	struct Student stud_three = {45, 'A'}; // 'A ascii 75 converts into 75.000 other default values.
	
	//Except age all other default values.
	struct Student stud_four = {56};

	printf("\n\n");	
	
	printf("Age:%d\n",stud_one.iAge_SMP);
	printf("f:%f\n",stud_one.f);
	printf("Height:%lf\n",stud_one.dHeight_SMP);
	printf("Gender:%c\n",stud_one.cGender_SMP);
	
	printf("\n\n");	
	
	printf("Age:%d\n",stud_two.iAge_SMP);
	printf("f:%f\n",stud_two.f);
	printf("Height:%lf\n",stud_two.dHeight_SMP);
	printf("Gender:%c\n",stud_two.cGender_SMP);
	
	printf("\n\n");	
	
	printf("Age:%d\n",stud_three.iAge_SMP);
	printf("f:%f\n",stud_three.f);
	printf("Height:%lf\n",stud_three.dHeight_SMP);
	printf("Gender:%c\n",stud_three.cGender_SMP);
	
	printf("\n\n");	
	
	printf("Age:%d\n",stud_four.iAge_SMP);
	printf("f:%f\n",stud_four.f);
	printf("Height:%lf\n",stud_four.dHeight_SMP);
	printf("Gender:%c\n",stud_four.cGender_SMP);
	
	printf("\n\n");
	
	
	return (0);
}

/*

Age:45
f:3.700000
Height:1.234530
Gender:A


Age:80
f:5.700000
Height:12.635000
Gender:D


Age:45
f:65.000000
Height:0.000000
Gender:


Age:56
f:0.000000
Height:0.000000
Gender:

*/