#include<stdio.h>

struct Point
{
	int x;
	int y;
};


int main(void)
{	
	struct Point AddPoints(struct Point, struct Point);
	
	struct Point p1 = {10,5};
	struct Point p2 = {40,34};
	struct Point p3;
	
	p3 = AddPoints(p1, p2);
	
	printf("\n\n");
	
	printf("p1.x(%d) + p2.x(%d) = p3.x(%d)\n\n", p1.x, p2.x, p3.x);
	printf("p1.y(%d) + p2.y(%d) = p3.y(%d)\n\n", p1.y, p2.y, p3.y);
	
	printf("\n\n");
			
	return (0);
}

struct Point AddPoints(struct Point p1, struct Point p2)
{
	
	struct Point p3;
	
	p3.x = p1.x + p2.x;
	p3.y = p1.y + p2.y;
	
	return (p3);

}
 
/*

p1.x(10) + p2.x(40) = p3.x(50)

p1.y(5) + p2.y(34) = p3.y(39)

*/