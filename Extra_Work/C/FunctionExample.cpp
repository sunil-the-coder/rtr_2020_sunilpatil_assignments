#include <stdio.h>


//call by value 
int addition(int *a, int *b) //formal parameter
{
	int ans = 0;	
	
	ans = *a + *b;
	
	//printf("Ans=%d\n",ans);
	return ans;
}

//call by pointer / reference 
void changeValue(int *r) 
{
	*r = 100;
}

//function frame => information about current function
int main(void) 
{	
	int p = 10;
	int q = 20;
	
	int result;
				
	result = addition(&p,&q); //actual parameters
	
	printf("Result:%d\n",result);
	
	changeValue(&p);
	
	printf("p=%d\n",p);
	
	return (0);
}