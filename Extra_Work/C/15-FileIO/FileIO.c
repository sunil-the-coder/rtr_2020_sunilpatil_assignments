#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *pFile = NULL;
	
	//secured IO
	if(fopen_s(&pFile, "SMPLog.txt","w") != 0)
	{
		printf("Can't open desired file. \n");
		exit(0);
	}
	
	//Write something.
	fprintf(pFile, "India is My Country.\n");
	
	fclose(pFile);
	
	pFile = NULL;
	
	return (0);
}