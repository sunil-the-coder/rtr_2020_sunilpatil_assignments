var canvas = null;
var gl = null; //context

var canvasOriginalWidth;
var canvasOriginalHeight;

var bFullscreen = false;
var pressedDigit = 0;

const webGLMacros = {  //all inside variable will be constant automatically
	SMP_ATTRIBUTE_POSITION : 0,
	SMP_ATTRIBUTE_COLOR : 1,
	SMP_ATTRIBUTE_NORMAL : 2,
	SMP_ATTRIBUTE_TEXTURE : 3
}; 

var vertextShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoCube;
var vboCubePosition;
var vboCubeTexture;

var mvpMatrixUniform;

var perspectiveProjectionMatrix;

var cubeAngle = 0.1;

var smileyTexture = 0;
var textureSamplerUniform;


var requestAnimationFrame;
requestAnimationFrame = window.requestAnimationFrame ||    //chrome
							window.webkitRequestAnimationFrame || //apple
							window.mozRequestAnimationFrame || //firefox
							window.oRequestAnimationFrame ||  //opera
							window.msRequestAnimationFrame; //microsoft

function main() {
	//1. Get canvas from dom.
	canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve original width and height of canvas 
	canvasOriginalWidth = canvas.width;
	canvasOriginalHeight = canvas.height;
	
	
	//event listeners
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); //warm up resize ( window/linux/android -> internally resize makes call to draw() )
	
	draw(); //warm up repaint call
		
}


function toggleFullscreen(){
	//platform agnostic & browsr
	
	var fullscreenElement = document.fullscreenElement ||  //chrome/opera
							document.webkitFullscreenElement ||   //apple safari 
							document.mozFullScreenEnabled ||      // mozilla firefox
							document.msFullscreenElement ||      //internet explorer
							null ;
							
	//if not fullscreen then set full screen
	if(fullscreenElement == null) { 
	
		if(canvas.requestFullscreen){ //does function pointer has any value ?
			canvas.requestFullscreen(); //call the function directly now.
		}else if(canvas.webkitRequestFullscreen){
			canvas.webkitRequestFullscreen();
		}else if(canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}else if(canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
		
	}else {		
	
		//small screen.
		if(document.exitFullscreen){  //chrome/opera
			document.exitFullscreen();
		}else if(document.webkitExitFullscreen) {  //apple
			document.webkitExitFullscreen();
		}else if(document.mozCancelFullscreen){  //firefox
			document.mozCancelFullscreen();
		}else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}	
}

function keyDown(event) {
	
	switch(event.keyCode) {
		case 27: //escape
			uninitialize();
			window.close(); //close application tab.
			break;
		case 49:
		 pressedDigit = 1;
		 break;
		case 50:
		 pressedDigit = 2;
		 break;
		case 51:
		 pressedDigit = 3;
		 break;
		case 52:
		 pressedDigit = 4;
		 break;		 
		case 70: 
				toggleFullscreen();				
				break;
		default: 
			//console.log(event.keyCode);
			break;
	}	
}

function mouseDown(){
		
}

function init() {
	
	//consistent initialization of obtaining the glContext similar to windows/linux/openGL ES
	
	//1. Get drawing context from canvas.
	gl = canvas.getContext("webgl2"); //Proper opengl chi context de
	if(!gl){
		console.log("WebGL2 loading context failed.");
	}else {
		console.log("WebGL2 loading context success.");
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
               "#version 300 es"+
                "\n"+
                "in vec4 vPosition;"+
				"in vec2 vTextCord;"+
                "uniform mat4 uMvpMatrix;"+
				"out vec2 out_textCord;"+
                "void main(void)"+
                "{"+
                "gl_Position = uMvpMatrix * vPosition;"+
				"out_textCord = vTextCord;"+
                "}";            
	
	//1. vertex shader
	vertextShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertextShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertextShaderObject);

	if(gl.getShaderParameter(vertextShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(vertextShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	//2. fragment shader
	fragmentShaderSourceCode = 
                "#version 300 es"+
                "\n"+
                "precision highp float;"+
				"in vec2 out_textCord;"+
				"uniform highp sampler2D u_textureSampler;"+
                "out vec4 fragColor;"+
                "void main(void)"+
                "{"+
                "fragColor = texture(u_textureSampler, out_textCord);"+
                "}";
             
    
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);  
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject =  gl.createProgram();
	gl.attachShader(shaderProgramObject, vertextShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.SMP_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.SMP_ATTRIBUTE_TEXTURE, "vTextCord");
	
	gl.linkProgram(shaderProgramObject);
	
	if(gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {	 //new changes
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
    mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uMvpMatrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_textureSampler");
	
	  var cubeVertices = new Float32Array([			 			
			1.0, 1.0, 1.0, 
			-1.0, 1.0, 1.0, 
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,			
			1.0, 1.0, -1.0,
			1.0, 1.0, 1.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,			
			-1.0, 1.0, -1.0,
			1.0, 1.0, -1.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,			
			-1.0, 1.0, 1.0,
			-1.0, 1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0,			
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0,
			-1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,			
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0
		]);
            

	var cubeTextCords = new Float32Array([
					1.0, 0.0,
					0.0, 0.0,
					0.0, 1.0,
					1.0, 1.0 
		]);
	
     vaoCube = gl.createVertexArray(); //new change	 
     gl.bindVertexArray(vaoCube); 

	 vboCubePosition = gl.createBuffer(); //new change
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboCubePosition);    
	 gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_POSITION);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	 
	 vboCubeTexture = gl.createBuffer(); 
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboCubeTexture);    
	 gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_TEXTURE);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	 gl.bindVertexArray(null); 

	//Depth Changes
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	
	
	//load texture from image
	smileyTexture = loadGLTexture("Smiley.png");
			
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();

}
function loadGLTexture(imgSrc) {
	
	var webGLTexture = gl.createTexture();
	webGLTexture.image = new Image();
	webGLTexture.image.src = imgSrc; 
	webGLTexture.image.onload = function() {
		gl.bindTexture(gl.TEXTURE_2D, webGLTexture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, webGLTexture.image);
		gl.bindTexture(gl.TEXTURE_2D, null);		
	}	
	
	return (webGLTexture);
}



function resize() {
	
	if(bFullscreen == true) {
		canvas.width = window.innerWidth; // only client area without caption and border
		canvas.height = window.innerHeight;
	}else {
		canvas.width = canvasOriginalWidth;
		canvas.height = canvasOriginalHeight;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix,
			45.0,
			(parseFloat(canvas.width) / parseFloat(canvas.height)),
			0.1,100.0);            
     
	
}


function draw() {
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();	
	var  modelViewProjectionMatrix = mat4.create();
	var  translationMatrix = mat4.create();
	
	mat4.translate(translationMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);	
	mat4.multiply(modelViewMatrix, modelViewMatrix, translationMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);	
	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix); 

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, smileyTexture);
	gl.uniform1i(textureSamplerUniform, 0);
	
	gl.bindVertexArray(vaoCube); 
	
		var cubeTextCords = [];
		
		if(pressedDigit == 1) {			
			cubeTextCords = new Float32Array([
					1.0, 0.0,
					0.0, 0.0,
					0.0, 1.0,
					1.0, 1.0 
			]);
			
		}else if(pressedDigit == 2) {			
			cubeTextCords = new Float32Array([
					0.5, 0.0,
					0.0, 0.0,
					0.0, 0.5,
					0.5, 0.5 
			]);
			
		}else if(pressedDigit == 3) {			
			cubeTextCords = new Float32Array([
					2.0, 0.0,
					0.0, 0.0,
					0.0, 2.0,
					2.0, 2.0 
			]);
			
		}else if(pressedDigit == 4) {	
			cubeTextCords = new Float32Array([
					0.5, 0.5,
					0.5, 0.5,
					0.5, 0.5,
					0.5, 0.5
			]);
		}
		
		gl.bindBuffer(gl.ARRAY_BUFFER, vboCubeTexture);    
		gl.bufferData(gl.ARRAY_BUFFER, cubeTextCords, gl.DYNAMIC_DRAW);
		gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_TEXTURE, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_TEXTURE);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
	 
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();
	
	requestAnimationFrame(draw, canvas);						
	
}

function update() {
	cubeAngle = cubeAngle + 0.8;
	if (cubeAngle >= 360.0)
		cubeAngle = 0.1;
}

function degToRad(degrees) {
	return (degrees * Math.PI / 180.0);
}

function uninitialize() {
	
	if(vaoCube) {
		gl.deleteVertexArray(vaoCube);
		vaoCube = null;
	}
	
	if(vboCubePosition) {
		gl.deleteBuffer(vboCubePosition);
		vboCubePosition = null;
	}
	
	if(vboCubeTexture) {
		gl.deleteBuffer(vboCubeTexture);
		vboCubeTexture = null;
	}
	
	if(shaderProgramObject) {
		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertextShaderObject) {
			gl.detachShader(shaderProgramObject, vertextShaderObject);
			gl.deleteShader(vertextShaderObject);
			vertextShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
	
	gl.deleteTexture(1, smileyTexture);
	
}






