function main() {
	//1. Get canvas from dom.
	var canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve width and height of canvas for your information
	console.log("Canvas width = "+canvas.width + " and height = "+canvas.height);

	//3. Get drawing context from canvas.
	var context = canvas.getContext("2d"); //simple context.
	if(!context){
		console.log("Obtaining context failed.");
	}else {
		console.log("Obtaining context success.");
	}

	//4. Paint window background by black color.
	context.fillStyle = "BLACK";
	context.fillRect(0, 0, canvas.width, canvas.height); // relative to canvas x,y values.

	//5. center future coming text
	context.textAlign = "center"; //horizontal center.
	context.textBaseline = "middle"; //vertical center.
	context.font = "48px sans-serif";
	
	//6. color for text
	context.fillStyle = "green"
	
	var str = "Hello World";
	context.fillText(str, canvas.width/2, canvas.height/2);

}