var canvas = null;
var gl = null; //context

var canvasOriginalWidth;
var canvasOriginalHeight;

var bFullscreen = false;


const webGLMacros = {  //all inside variable will be constant automatically
	SMP_ATTRIBUTE_POSITION : 0,
	SMP_ATTRIBUTE_COLOR : 1,
	SMP_ATTRIBUTE_NORMAL : 2,
	SMP_ATTRIBUTE_TEXTURE0 : 3
}; 

var vertextShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoPyramid;
var vboPyramidPosition;
var vboPyramidColor;


var vaoCube;
var vboCubePosition;
var vboCubeColor;

var mvpMatrixUniform;

var perspectiveProjectionMatrix;

var pyramidAngle = 0.1;
var cubeAngle = 0.1;

var requestAnimationFrame;
requestAnimationFrame = window.requestAnimationFrame ||    //chrome
							window.webkitRequestAnimationFrame || //apple
							window.mozRequestAnimationFrame || //firefox
							window.oRequestAnimationFrame ||  //opera
							window.msRequestAnimationFrame; //microsoft

function main() {
	//1. Get canvas from dom.
	canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve original width and height of canvas 
	canvasOriginalWidth = canvas.width;
	canvasOriginalHeight = canvas.height;
	
	
	//event listeners
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); //warm up resize ( window/linux/android -> internally resize makes call to draw() )
	
	draw(); //warm up repaint call
		
}


function toggleFullscreen(){
	//platform agnostic & browsr
	
	var fullscreenElement = document.fullscreenElement ||  //chrome/opera
							document.webkitFullscreenElement ||   //apple safari 
							document.mozFullScreenEnabled ||      // mozilla firefox
							document.msFullscreenElement ||      //internet explorer
							null ;
							
	//if not fullscreen then set full screen
	if(fullscreenElement == null) { 
	
		if(canvas.requestFullscreen){ //does function pointer has any value ?
			canvas.requestFullscreen(); //call the function directly now.
		}else if(canvas.webkitRequestFullscreen){
			canvas.webkitRequestFullscreen();
		}else if(canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}else if(canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
		
	}else {		
	
		//small screen.
		if(document.exitFullscreen){  //chrome/opera
			document.exitFullscreen();
		}else if(document.webkitExitFullscreen) {  //apple
			document.webkitExitFullscreen();
		}else if(document.mozCancelFullscreen){  //firefox
			document.mozCancelFullscreen();
		}else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}	
}

function keyDown(event) {
	
	switch(event.keyCode) {
		case 27: //escape
			uninitialize();
			window.close(); //close application tab.
			break;
		case 70: 
				toggleFullscreen();				
				break;
	}	
}

function mouseDown(){
		
}

function init() {
	
	//consistent initialization of obtaining the glContext similar to windows/linux/openGL ES
	
	//1. Get drawing context from canvas.
	gl = canvas.getContext("webgl2"); //Proper opengl chi context de
	if(!gl){
		console.log("WebGL2 loading context failed.");
	}else {
		console.log("WebGL2 loading context success.");
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
                "#version 300 es"+
                "\n"+
                "in vec4 vPosition;"+
				"in vec4 vColor;"+
                "uniform mat4 uMvpMatrix;"+
				"out vec4 out_color;"+
                "void main(void)"+
                "{"+
                "gl_Position = uMvpMatrix * vPosition;"+
				"out_color = vColor;"+
                "}";            
	
	//1. vertex shader
	vertextShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertextShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertextShaderObject);

	if(gl.getShaderParameter(vertextShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(vertextShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	//2. fragment shader
	fragmentShaderSourceCode = 
                 "#version 300 es"+
                "\n"+
                "precision highp float;"+
				"in vec4 out_color;" +
                "out vec4 fragColor;"+
                "void main(void)"+
                "{"+
                "fragColor = out_color;"+
                "}";
    
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);  
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject =  gl.createProgram();
	gl.attachShader(shaderProgramObject, vertextShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.SMP_ATTRIBUTE_POSITION, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.SMP_ATTRIBUTE_COLOR, "vColor");
		
	gl.linkProgram(shaderProgramObject);
	
	if(gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {	 //new changes
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
    mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uMvpMatrix");
	
	  var pyramidVertices = new Float32Array([			 
			0.0, 1.0, 0.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,			
			
			0.0, 1.0, 0.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			
			0.0, 1.0, 0.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,		
			
			0.0, 1.0, 0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
		]);
		 
		var pyramidColors = new Float32Array([
			1.0, 0.0, 0.0, 
			0.0, 1.0, 0.0, 
			0.0, 0.0, 1.0, 

			1.0, 0.0, 0.0,
			0.0, 0.0, 1.0,
			0.0, 1.0, 0.0,

			1.0, 0.0, 0.0,
			0.0, 0.0, 1.0,
			0.0, 1.0, 0.0,

			1.0, 0.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 0.0, 1.0
		]);
		
       var cubeVertices = new Float32Array([			 			
			1.0, 1.0, 1.0, 
			-1.0, 1.0, 1.0, 
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,		
			
			1.0, 1.0, -1.0,
			1.0, 1.0, 1.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,	
			
			-1.0, 1.0, -1.0,
			1.0, 1.0, -1.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,	
			
			-1.0, 1.0, 1.0,
			-1.0, 1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0,	
			
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0,
			-1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,		
			
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0
		]); 

	 var cubeColors = new Float32Array([
			1.0, 0.0, 0.0,
			1.0, 0.0, 0.0,
			1.0, 0.0, 0.0,
			1.0, 0.0, 0.0,

			0.0, 1.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 1.0, 0.0,
			0.0, 1.0, 0.0,

			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,

			1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,

			1.0, 0.0, 1.0,
			1.0, 0.0, 1.0,
			1.0, 0.0, 1.0,
			1.0, 0.0, 1.0,
			
			1.0, 1.0, 0.0,
			1.0, 1.0, 0.0,
			1.0, 1.0, 0.0,
			1.0, 1.0, 0.0
		]);
				

	//1. VAO for pyramid
     vaoPyramid = gl.createVertexArray(); //new change	 
     gl.bindVertexArray(vaoPyramid); 

	//1. vbo for position
	 vboPyramidPosition = gl.createBuffer(); //new change
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboPyramidPosition);    
	 gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_POSITION);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	  //2. vbo for color
	 vboPyramidColor = gl.createBuffer(); 
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboPyramidColor); 
	 gl.bufferData(gl.ARRAY_BUFFER, pyramidColors, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_COLOR);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	 //2. VAO for Cube
	 vaoCube = gl.createVertexArray(); //new change	 
     gl.bindVertexArray(vaoCube); 

	//1. vbo for position
	 vboCubePosition = gl.createBuffer(); //new change
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboCubePosition);    
	 gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_POSITION);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	  //2. vbo for color
	 vboCubeColor = gl.createBuffer(); 
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboCubeColor); 
	 gl.bufferData(gl.ARRAY_BUFFER, cubeColors, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_COLOR);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 
	 
	 gl.bindVertexArray(null); 

	//Depth Changes
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	//gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
			
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();

}

function resize() {
	
	if(bFullscreen == true) {
		canvas.width = window.innerWidth; // only client area without caption and border
		canvas.height = window.innerHeight;
	}else {
		canvas.width = canvasOriginalWidth;
		canvas.height = canvasOriginalHeight;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix,
			45.0,
			(parseFloat(canvas.width) / parseFloat(canvas.height)),
			0.1,100.0);            
     
	
}


function draw() {
	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	//1. Draw Pyramid
	var modelViewMatrix = mat4.create();	
	var  modelViewProjectionMatrix = mat4.create();
	var  translationMatrix = mat4.create();
	var  rotationMatrix = mat4.create();
	
	mat4.translate(translationMatrix, modelViewMatrix, [-1.5, 0.0, -5.0]);	
	mat4.rotateY(rotationMatrix, rotationMatrix, degToRad(pyramidAngle));
	
	mat4.multiply(modelViewMatrix, translationMatrix, rotationMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);	
	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix); 

	gl.bindVertexArray(vaoPyramid); 
		gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	
	
	
	//2. Draw Cube
	modelViewMatrix = mat4.create();	
	modelViewProjectionMatrix = mat4.create();
	translationMatrix = mat4.create();
	rotationMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	
	mat4.translate(translationMatrix, modelViewMatrix, [1.5, 0.0, -5.0]);	
	mat4.rotateX(rotationMatrix, rotationMatrix, degToRad(cubeAngle));
	mat4.scale(scaleMatrix, scaleMatrix, [0.75, 0.75, 0.75]);
	
	/*var scaleMatrix = new Float32Array([
            0.75,   0.0,  0.0,  0.0,
            0.0,  0.75,   0.0,  0.0,
            0.0,  0.0,  0.75,   0.0,
            0.0,  0.0,  0.0,  1.0  
         ]);
		*/ 
	
	mat4.multiply(modelViewMatrix, translationMatrix, rotationMatrix);
	mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);	
	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix); 

	gl.bindVertexArray(vaoCube); 
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
		gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);

	update();
	
	requestAnimationFrame(draw, canvas);						
	
}

function update() {
	pyramidAngle = pyramidAngle + 0.8;
	if (pyramidAngle >= 360.0)
		pyramidAngle = 0.1;
	
	cubeAngle = cubeAngle + 0.8;
	if (cubeAngle >= 360.0)
		cubeAngle = 0.1;
}

function degToRad(degrees) {
	return (degrees * Math.PI / 180.0);
}

function uninitialize() {
		
	if(vaoCube) {
		gl.deleteVertexArray(vaoCube);
		vaoCube = null;
	}
	
	if(vaoPyramid) {
		gl.deleteVertexArray(vaoPyramid);
		vaoPyramid = null;
	}
	
	if(vboCubeColor) {
		gl.deleteBuffer(vboCubeColor);
		vboCubeColor = null;
	}
	
	if(vboCubePosition) {
		gl.deleteBuffer(vboCubePosition);
		vboCubePosition = null;
	}
	

	if(vboPyramidPosition) {
		gl.deleteBuffer(vboPyramidPosition);
		vboPyramidPosition = null;
	}
	

	if(vboPyramidColor) {
		gl.deleteBuffer(vboPyramidColor);
		vboPyramidColor = null;
	}
	
	if(shaderProgramObject) {
		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertextShaderObject) {
			gl.detachShader(shaderProgramObject, vertextShaderObject);
			gl.deleteShader(vertextShaderObject);
			vertextShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
	
}






