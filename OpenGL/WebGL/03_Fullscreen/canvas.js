var canvas = null;
var context = null;

function main() {
	//1. Get canvas from dom.
	canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve width and height of canvas for your information
	console.log("Canvas width = "+canvas.width + " and height = "+canvas.height);

	//3. Get drawing context from canvas.
	context = canvas.getContext("2d"); //simple context.
	if(!context){
		console.log("Obtaining context failed.");
	}else {
		console.log("Obtaining context success.");
	}

	//4. Paint window background by black color.
	context.fillStyle = "BLACK";
	context.fillRect(0, 0, canvas.width, canvas.height); // relative to canvas x,y values.
	
	//5. Draw text
	drawText("Hello World");
	
	//events
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
}



function toggleFullscreen(){
	//platform agnostic & browsr
	
	var fullscreenElement = document.fullscreenElement ||  //chrome/opera
							document.webkitFullscreenElement ||   //apple safari 
							document.mozFullScreenEnabled ||      // mozilla firefox
							document.msFullscreenElement ||      //internet explorer
							null ;
							
	//if not fullscreen then set full screen
	if(fullscreenElement == null) { 
	
		if(canvas.requestFullscreen){ //does function pointer has any value ?
			canvas.requestFullscreen(); //call the function directly now.
		}else if(canvas.webkitRequestFullscreen){
			canvas.webkitRequestFullscreen();
		}else if(canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}else if(canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		
	}else {		
		//small screen.
		if(document.exitFullscreen){  //chrome/opera
			document.exitFullscreen();
		}else if(document.webkitExitFullscreen) {  //apple
			document.webkitExitFullscreen();
		}else if(document.mozCancelFullscreen){  //firefox
			document.mozCancelFullscreen();
		}else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}
	
}

//Custom repaint implementation as browser don't have repaint functionality 
function drawText(text) {
	
	context.textAlign = "center"; //horizontal center.
	context.textBaseline = "middle"; //vertical center.
	context.font = "48px sans-serif";	
	context.fillStyle = "green"
	context.fillText(text, canvas.width/2, canvas.height/2);
	
}

function keyDown(event) {
	
	switch(event.keyCode) {
		case 70: 
				toggleFullscreen();
				drawText("Hello World");
				break;
	}	
}

function mouseDown(){
		
}