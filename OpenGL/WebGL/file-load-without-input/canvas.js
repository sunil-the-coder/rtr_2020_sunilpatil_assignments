var canvas = null;
var gl = null; //context

var canvasOriginalWidth;
var canvasOriginalHeight;

var bFullscreen = false;


const webGLMacros = {  //all inside variable will be constant automatically
	SMP_ATTRIBUTE_POSITION : 0,
	SMP_ATTRIBUTE_COLOR : 1,
	SMP_ATTRIBUTE_NORMAL : 2,
	SMP_ATTRIBUTE_TEXTURE : 3
}; 

var vertextShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vboPosition;
var mvpMatrixUniform;

var perspectiveProjectionMatrix;


var requestAnimationFrame;

requestAnimationFrame = window.requestAnimationFrame ||    //chrome
							window.webkitRequestAnimationFrame || //apple
							window.mozRequestAnimationFrame || //firefox
							window.oRequestAnimationFrame ||  //opera
							window.msRequestAnimationFrame; //microsoft

function main() {
	//1. Get canvas from dom.
	canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve original width and height of canvas 
	canvasOriginalWidth = canvas.width;
	canvasOriginalHeight = canvas.height;
	
	
	//event listeners
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
	window.addEventListener("resize", resize, false);
	
	init();
	
	resize(); //warm up resize ( window/linux/android -> internally resize makes call to draw() )
	
	draw(); //warm up repaint call
		
}


function toggleFullscreen(){
	//platform agnostic & browsr
	
	var fullscreenElement = document.fullscreenElement ||  //chrome/opera
							document.webkitFullscreenElement ||   //apple safari 
							document.mozFullScreenEnabled ||      // mozilla firefox
							document.msFullscreenElement ||      //internet explorer
							null ;
							
	//if not fullscreen then set full screen
	if(fullscreenElement == null) { 
	
		if(canvas.requestFullscreen){ //does function pointer has any value ?
			canvas.requestFullscreen(); //call the function directly now.
		}else if(canvas.webkitRequestFullscreen){
			canvas.webkitRequestFullscreen();
		}else if(canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}else if(canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
		
	}else {		
	
		//small screen.
		if(document.exitFullscreen){  //chrome/opera
			document.exitFullscreen();
		}else if(document.webkitExitFullscreen) {  //apple
			document.webkitExitFullscreen();
		}else if(document.mozCancelFullscreen){  //firefox
			document.mozCancelFullscreen();
		}else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}	
}

function keyDown(event) {
	
	switch(event.keyCode) {
		case 27: //escape
			uninitialize();
			window.close(); //close application tab.
			break;
		case 70: 
				toggleFullscreen();				
				break;
	}	
}

function mouseDown(){
		
}

function init() {
	
	//consistent initialization of obtaining the glContext similar to windows/linux/openGL ES
	
	//1. Get drawing context from canvas.
	gl = canvas.getContext("webgl2"); //Proper opengl chi context de
	if(!gl){
		console.log("WebGL2 loading context failed.");
	}else {
		console.log("WebGL2 loading context success.");
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceCode = 
                "#version 300 es"+
                "\n"+
                "in vec4 vPosition;"+
                "uniform mat4 uMvpMatrix;"+
                "void main(void)"+
                "{"+
                "gl_Position = uMvpMatrix * vPosition;"+
                "}";            
	
	//1. vertex shader
	vertextShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertextShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertextShaderObject);

	if(gl.getShaderParameter(vertextShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(vertextShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	//2. fragment shader
	fragmentShaderSourceCode = 
                "#version 300 es"+
                "\n"+
                "precision highp float;"+
                "out vec4 fragColor;"+
                "void main(void)"+
                "{"+
                "fragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
                "}";
    
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);  
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {	
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject =  gl.createProgram();
	gl.attachShader(shaderProgramObject, vertextShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, webGLMacros.SMP_ATTRIBUTE_POSITION, "vPosition");
		
	gl.linkProgram(shaderProgramObject);
	
	if(gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {	 //new changes
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0) {
			alert(error);
			uninitialize();
		}
	}
	
    mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uMvpMatrix");
	
	  var triangleVertices = new Float32Array([
			  0.0, 1.0, 0.0,
			  -1.0, -1.0,0.0,
			  1.0, -1.0, 0.0
		]);
            

     vao = gl.createVertexArray(); //new change	 
     gl.bindVertexArray(vao); 

	 vboPosition = gl.createBuffer(); //new change
	 gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition); 
   
	 gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	 gl.vertexAttribPointer(webGLMacros.SMP_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	 gl.enableVertexAttribArray(webGLMacros.SMP_ATTRIBUTE_POSITION);
	 gl.bindBuffer(gl.ARRAY_BUFFER, null);
	 gl.bindVertexArray(null); 

			
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();

}

function resize() {
	
	if(bFullscreen == true) {
		canvas.width = window.innerWidth; // only client area without caption and border
		canvas.height = window.innerHeight;
	}else {
		canvas.width = canvasOriginalWidth;
		canvas.height = canvasOriginalHeight;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix,
			45.0,
			(parseFloat(canvas.width) / parseFloat(canvas.height)),
			0.1,100.0);            
     
	
}

function draw() {
	
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();	
	var  modelViewProjectionMatrix = mat4.create();
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix); 

	gl.bindVertexArray(vao); 
		gl.drawArrays(gl.TRIANGLES, 0, 3);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	requestAnimationFrame(draw, canvas);						
	
}

function uninitialize() {
	
	if(vao) {
		gl.deleteVertexArray(vao);
		vao = null;
	}
	
	if(vboPosition) {
		gl.deleteBuffer(vboPosition);
		vboPosition = null;
	}
	
	if(shaderProgramObject) {
		if(fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertextShaderObject) {
			gl.detachShader(shaderProgramObject, vertextShaderObject);
			gl.deleteShader(vertextShaderObject);
			vertextShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
	
}






