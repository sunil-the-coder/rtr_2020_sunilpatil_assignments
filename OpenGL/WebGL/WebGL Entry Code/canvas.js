var canvas = null;
var gl = null; //context

function main() {
	//1. Get canvas from dom.
	canvas = document.getElementById("SMP");
	if(!canvas) {
		console.log("Obtaining Canvas failed.");
	}else {
		console.log("Obtaining Canvas success.");
	}

	//2. Retrieve width and height of canvas for your information
	console.log("Canvas width = "+canvas.width + " and height = "+canvas.height);

	//3. Get drawing context from canvas.
	gl = canvas.getContext("2d"); //simple context.
	if(!gl){
		console.log("Obtaining context failed.");
	}else {
		console.log("Obtaining context success.");
	}


	//events
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	
	
}



function toggleFullscreen(){
	//platform agnostic & browsr
	
	var fullscreenElement = document.fullscreenElement ||  //chrome/opera
							document.webkitFullscreenElement ||   //apple safari 
							document.mozFullScreenEnabled ||      // mozilla firefox
							document.msFullscreenElement ||      //internet explorer
							null ;
							
	//if not fullscreen then set full screen
	if(fullscreenElement == null) { 
	
		if(canvas.requestFullscreen){ //does function pointer has any value ?
			canvas.requestFullscreen(); //call the function directly now.
		}else if(canvas.webkitRequestFullscreen){
			canvas.webkitRequestFullscreen();
		}else if(canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}else if(canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		
	}else {		
		//small screen.
		if(document.exitFullscreen){  //chrome/opera
			document.exitFullscreen();
		}else if(document.webkitExitFullscreen) {  //apple
			document.webkitExitFullscreen();
		}else if(document.mozCancelFullscreen){  //firefox
			document.mozCancelFullscreen();
		}else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}	
}

function keyDown(event) {
	
	switch(event.keyCode) {
		case 70: 
				toggleFullscreen();				
				break;
	}	
}

function mouseDown(){
		
}