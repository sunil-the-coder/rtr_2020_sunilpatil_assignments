#include<iostream>
#include<stdio.h> 
#include<stdlib.h> //exit(1)
#include<memory.h> //memset
//For GLSL extentions. Must be  included before gl.h and glu.h
#include<GL/glew.h>
#include<GL/gl.h> // For OpenGL API
#include<GL/glx.h> //Similar to WGL in windows ( Bridging API ) - For GLX API

#include<X11/Xlib.h> //compulsary for xwindows ( like windows.h in windows)
#include<X11/Xutil.h> //XVisualInfo
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //KeySym
#include "vmath.h"

using namespace std;
using namespace vmath;

//Global variables
bool bFullscreen = false;
Display *gpDisplay = NULL; // 77 member structure.
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
Status status;
GLXContext gGlxContext; // hghrc in windows

int giWindowWidth = 800;
int giWindowHeight = 600;


//fbprofile config changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int *);

glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;

GLXFBConfig gGLXFBConfig;

enum {
	SMP_ATTRIBUTE_POSITION,
	SMP_ATTRIBUTE_COLOR,
	SMP_ATTRIBUTE_NORMAL,
	SMP_ATTRIBUTE_TEXTURE,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint mvpMatrixUniform;
GLuint vao_triangle;
GLuint vao_square;
GLuint VBO_TRIAGLE_POSITION;
GLuint VBO_TRIANGLE_COLOR;
GLuint VBO_SQUARE_POSITION;
GLuint VBO_SQUARE_COLOR;

mat4 perspectiveProjectionMatrix;  // typedef of float[16]

enum SHADER_ACTION { COMPILE, LINK };
enum SHADER_NAME { VERTEX, FRAGMENT, SHADER_PROGRAM };


//You can use CLA to pass network server ip address config.
int main(void)
{

	void initialize(void);
	void resize(int, int);
	void display(void);

	//Function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void Uninitialize();

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;


	//Code
	CreateWindow();

	
	initialize();

	//Message Loop
	XEvent event; // == MSG in window ( XEvent is union which has 31 data structures)
	KeySym keysym; // key symbol ( == VK_ESCAPE in windows )

	while(bDone == false)
	{
		while(XPending(gpDisplay)) // Windows -> PeekMessage()  - Check in networked queue if any pending msgs
		{
		
		XNextEvent(gpDisplay, &event); // == GetMessage() in windows

		switch(event.type) //iMsg of WndProc
		{
			case MapNotify: // XMapWindow() -> Will trigger this event informing window is created like WM_CREATE in windows
				break;
			case KeyPress: // == WM_KEYDOWN 
				//Get actual key symbol from key code ( In windows wParam madhe windows system tumhala VK key pathavato )
				//Xkb -> XWindow keyboard
				//3rd 0-> keysymbol group ( diff language group ) - default group
				//4th 0-> Direct key press OR using key with Shift key
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
					case XK_Escape: // XServer cha key symbol
						bDone = true;
						break;
					case XK_F:
					case XK_f:
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
					default:
						break;
				}

				/*
				//You can use WM_CHAR like behavior with XLoopString API.
				XLooupString(gpDisplay, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
				}
				*/
				break;
			case ButtonPress: // == WM_LBUTTON_DOWN, WM_RBUTTON_DONW
				switch(event.xbutton.button)
				{
					case 1: //left mouse button
						break;
					case 2: //middle mouse button
						break;
					case 3: //right mouse button
						break;
					case 4: //mouse scroll up
						break;
					case 5: //mouse scroll down
						break;
					default:
						break;
				}
				break;
			case MotionNotify: //If window was moved.
				break;
			case ConfigureNotify: // == WM_SIZE
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33: //Atom representation is ( WM_DELETE_WINDOW ) - Numeric format used on network - fast.
				//Uninitialize();
				//exit(0);
				bDone = true;
				break;
			default:
				break;
		}

		}

		//Call to update here 
		display();
	}

	Uninitialize();

	return (0);
}

void CreateWindow(void)
{
	//Function Declarations
	void Uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs; //WndClass in Windows
	int defaultScreen;
	int styleMask;
	
	//fbconfig changes
	GLXFBConfig *pGLXFBConfig = NULL;  
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int numFbConfig = 0;
	
	//total 44 memberes
	static int frameBufferAttributes[]=
				{GLX_X_RENDERABLE,True,
				GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
				GLX_RENDER_TYPE,GLX_RGBA_BIT,
				GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
				GLX_RED_SIZE,8,
				GLX_GREEN_SIZE,8,
				GLX_BLUE_SIZE,8,
				GLX_ALPHA_SIZE,8,
				GLX_DEPTH_SIZE,24,
				GLX_STENCIL_SIZE,8,
				GLX_DOUBLEBUFFER,True,				
				None};
				

	//None -> 0 ( Prefer to use Macro rather than value for future modifications )
	//
	// The array which is big but i want to provide only subset of values, always set the last value to 0 => Good Programming Practice

	//Code
	//1. Establish connection to XServer on network
	gpDisplay = XOpenDisplay(NULL); // NULL -> Default behavior - $DISPLAY cha output ( host:display.screen )
	if(gpDisplay == NULL) // Abortive behavior 
	{
		printf("ERROR: Unable to open X Display. \n Exiting Now...\n");
		Uninitialize();
		exit(1); // Errorneous exit status 
	}
	
	//2. Retrieve default screen of graphics card connected display
	//DefaultScreen macro can be used which internally calls XDefaultScreen.
	defaultScreen = XDefaultScreen(gpDisplay);


	//4. Retrieve the handle to display context like windows ( hdc context )
	//4.a -> Allocate memory for visual info
	gpXVisualInfo = (XVisualInfo *) malloc(sizeof(XVisualInfo)); // Main 10 members of this structure, 
	//struct XVisualInfo has struct Visual => This is actual hdc which has 8 members.
	if(gpXVisualInfo == NULL)
	{
		printf("Error: Memory allocation failed for Visual Info. \n Existing Now.. \n");
		Uninitialize();
		exit(1);
	}

	//New function
	//gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	//fbconfig changes 
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, defaultScreen, frameBufferAttributes, &numFbConfig);   //return multiple fbconfig
	printf("Found number of fbconfig = %d and fbconfig=%p\n", numFbConfig, pGLXFBConfig);
	
	
	int bestFBConfig = -1, worstFBConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	
	for(int i = 0; i < numFbConfig; i++) {
	
		//1. Retrieve visual info from each fbconfig
		//2. If successful then only retrieve its samples.
			
			
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
			
		if(pTempXVisualInfo != NULL) {
			int sampleBuffers, samples;
					
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);				
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);
			printf("For pTempXVisualInfo->(%d) Found No of Sample Buffers: %d and Samples: %d \n",i, sampleBuffers, samples);
			
			if(bestFBConfig < 0 || sampleBuffers && samples > bestNumberOfSamples){
				bestFBConfig = i;
				bestNumberOfSamples = samples;
			}
			
			if(worstFBConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples){
				worstFBConfig = i;
				worstNumberOfSamples = samples;
			}						
		}
		
		free(pTempXVisualInfo);		
	}
	
	printf("Best fbConfig is: pTempXVisualInfo->(%d) \n",bestFBConfig);
	bestGLXFBConfig = pGLXFBConfig[bestFBConfig];
	gGLXFBConfig = bestGLXFBConfig;
	free(pGLXFBConfig);
	
	//Retrieve best visual info from best GLXFBConfig
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, gGLXFBConfig);
	

	//4.b -> Retrieve actual visualInfo by matching colormap 
	//TrueColor -> matching pixelformat with visual, 
	// It should fill up the gpX ( Windows -> ChoosePixelFormat)
	//status = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpxVisualInfo);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR: Unable to Get A Visual. \n Exiting Now.. \n");
		Uninitialize();
		exit(1);
	}

	//5. Fill up window attributes like wndclass in Windows system.
	winAttribs.border_pixel = 0; //default color border. ( pixel mhanje ithe color sangayacha - I'm not giving so take whatever you can provide )
	winAttribs.background_pixmap = 0; // No background image. ( pixmap -> pixel war map karnari image )
	//retrieve matching(TrueColor) colormap from set of colormap
	winAttribs.colormap = XCreateColormap(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), //give me colormap as per my root window's visual screen ( TrueColor wali )
			gpXVisualInfo->visual, //pass hdc parameter
			AllocNone); // I am assigning colormap to winAttributes so don't allocate new memory.

	gColormap = winAttribs.colormap;

	//Set window background color as black. ( GetStockObject in windows )
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	//Which message to be sent by XServer to Xclient. ( == GetMessage() last 2 parameters )
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	//CW - CreateWindow
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//6. Create actual window in memory with required window attributes
	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, //x
			0, //y
			giWindowWidth,
			giWindowHeight,
			0, //border width
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual, //hdc
			styleMask,
			&winAttribs);

	if(!gWindow)
	{
		printf("ERROR: Failed to create main window. \n Exiting Now... \n");
		Uninitialize();
		exit(1);
	}


	XStoreName(gpDisplay, gWindow, "SMP: Two 2D Shapes");

	//Unique behavior for click on actual window close button on different window manager.
	//Next 2 lines are not necessary to run but they are important. ( close button click on window - not need for xwindows but its need of window manager)
	// Close button position (left or right it depends on windows manager. So use WM (Window Manager) orders of XWindows)
	//3rd Param -> If same items needs to be prepared in case its already in queue. True -> Prepare new item in queue & remove old one.
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True); //Atom => Unique string - Immutable ( CLOSE -> 33)
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1); //1 -> Total orders ( In case multiple orders Atom[])

	//7. Show actual window from memory
	XMapWindow(gpDisplay, gWindow);
	
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//Code
	//Create new order(Aadnya) for WM to retrieve current state
	//_NET_WM_STATE -> Across the networked window current state 
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1; //data is union, l for long type array

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	//== SendMessage() / PostMessage()
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, //Propagate to child as well ? False -> Dont propagate. Use only for me
		StructureNotifyMask, // SubStructureNotify -> client area, StructureNotifyMask -> For entire window
		&xev); 
}

void initialize(void)
{
	//function declarations
	
	void resize(int, int);
	void Uninitialize(void);
	void checkCompileErrorIfAny(GLuint, SHADER_NAME, SHADER_ACTION);

	//get address of function of graphics library shared object at runtime
	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte *) "glXCreateContextAttribsARB");

	const int attribs[] = {GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB, // openGL version - shader madhala core profile
							None}; 
							
	
	gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs); //PP
	if(!gGlxContext) {
		printf("Not found core profile 4.5. So take whatever max system has.\n");
		//If not found then take whatever highest it has by asking minimum from programmer.
		const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
							GLX_CONTEXT_MINOR_VERSION_ARB, 0, // openGL version - shader madhala core profile
							None}; 
							
		gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs); //PP					
							
	} else {
	
		printf("Found core profile 4.5.\n");
	}
	
	//1. Create Context ( GL_TRUE -> OpenGL boolean type )
	//NULL -> no sharable display - Create new context with single display ( You could have pass another display context to create your own )
	//GL_TRUE => h/w or s/w rendering => TRUE ( H/w Rendering - Original graphic card rendering not novue/vesa renering)

	//gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE); // Similar to wglCreateContext // FFP 
	
	Bool isDirectContext = glXIsDirect(gpDisplay, gGlxContext); // check if hardware rendering 
	if(isDirectContext == True) {
		printf("The Rendering context is direct hardware rendering context.\n");
	}else {
		printf("The Rendering context is s/w rendering context.\n");
	}

	//Make 
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
	
	//Load openGL Extensions using glew
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK) {
		printf("glewInit() is failed.\n");
		Uninitialize();
	}


// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	//2. Provide source code to shader
	const GLchar *vertexShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 vPosition;" \
				"in vec4 vColor;" \
				"uniform mat4 uMvpMatrix;" \
				"out vec4 out_color;" \
				"void main(void)" \
				"{" \
				"gl_Position = uMvpMatrix * vPosition;" \
				"out_color = vColor;" \
				"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	printf("Compiling Vertex shader...\n");

	//3. compile shader
	glCompileShader(gVertexShaderObject);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject, VERTEX, COMPILE);

	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//2. Provide source code to shader.
	const GLchar *fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 out_color;" \
				"out vec4 fragColor;" \
				"void main(void)" \
				"{" \
				"fragColor = out_color;" \
				"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	printf("Compiling fragment shader...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject);
	checkCompileErrorIfAny(gFragmentShaderObject, FRAGMENT, COMPILE);

	
	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	printf("Linking shaders with shader programme ...\n");

	//Pre-link binding of shader program object with vertex shader position attribute for 
	//taking input 

	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_COLOR, "out_color");

	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject, SHADER_PROGRAM, LINK);


	//Get MVP uniform location from driver to be used in display to push the final metrices
	mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject, "uMvpMatrix");

	
	// Define all your vertices, color, shader attributes, vbo, vao initilization
	// in sequential array 
	const GLfloat triangleVertices[] = {
		0.0f, 1.0f, 0.0f, //apex
		-1.0f, -1.0f, 0.0f, //left bottom
		1.0f, -1.0f, 0.0f //right bottom
	};

	const GLfloat triangleColors[] = {
		1.0f, 0.0f, 0.0f, 
		0.0f, 1.0f, 0.0f, 
		0.0f, 0.0f, 1.0f 
	};

	const GLfloat squareVertices[] = {
		1.0f, 1.0f, 0.0f, 
		-1.0f, 1.0f, 0.0f, 
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f 
	};

	const GLfloat squareColors[] = {
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f
	};
	
	
	//1. VAO for triangle
	glGenVertexArrays(1, &vao_triangle); //Started new caset
	glBindVertexArray(vao_triangle); // Record button start 
	
	glGenBuffers(1, &VBO_TRIAGLE_POSITION); 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_TRIAGLE_POSITION); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VBO_TRIANGLE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_TRIANGLE_COLOR);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //Record stop button.


	//2. VAO for square
	glGenVertexArrays(1, &vao_square); //Started new caset
	glBindVertexArray(vao_square); // Record button start 

	glGenBuffers(1, &VBO_SQUARE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_SQUARE_POSITION);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VBO_SQUARE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_SQUARE_COLOR);
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareColors), squareColors, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //Record stop button.


	//Depth Functions
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Set the ortho project matrix as identity matrix.
	perspectiveProjectionMatrix = mat4::identity();
	
	resize(giWindowWidth, giWindowHeight);

}

void checkCompileErrorIfAny(GLuint shader, SHADER_NAME shaderName, SHADER_ACTION action) {

	void Uninitialize(void);
	
	
	GLint iShaderStatus;
	GLint iInfoLogLength;
	char *szInfoLog = NULL;

	if(action == COMPILE)
		glGetShaderiv(shader, GL_COMPILE_STATUS, &iShaderStatus);
	else if(action == LINK)
		glGetProgramiv(shader, GL_LINK_STATUS, &iShaderStatus);

	if(iShaderStatus == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL) {
				GLsizei actualWritten;
				glGetShaderInfoLog(shader, iInfoLogLength, &actualWritten, szInfoLog);

				if(shaderName == VERTEX) {
					printf("Vertex Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == FRAGMENT) {
					printf("Fragment Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == SHADER_PROGRAM) {
					printf("Shader Program Link Log: %s\n", szInfoLog);
				}
			
				free(szInfoLog);
				Uninitialize();
			}
		}
	}
}


void resize(int width, int height)
{
	if(height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle );

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Start using openGL program object.
	glUseProgram(gShaderProgramObject); //Starter of renderer in programmable pipeline

	// Start using attached vertex, fragment shaders and it will ask for your data(vbo, attribPointer etc..) - We have already recorded everything VAO.

	//OpenGL Drawing.

	//1. Draw Triangle
	mat4 modelViewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	mat4 modelViewProjectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	mat4 translationMatrix = vmath::translate(-1.5f, 0.0f, -5.0f);
	modelViewMatrix = translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // Order is important
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix); 

	glBindVertexArray(vao_triangle); 
		glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);

	//Draw sphere
	modelViewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	modelViewProjectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	translationMatrix = vmath::translate(1.5f, 0.0f, -5.0f);
	modelViewMatrix = translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // Order is important
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_square);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);


	//Stop using Open program object.
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);

}

void Uninitialize(void)
{
	GLXContext currentGlContext; //In network, check its same monitor

	currentGlContext = glXGetCurrentContext();

	if(currentGlContext == gGlxContext) {
		glXMakeCurrent(gpDisplay, 0, 0); // Coming out of openGl. No need gWindow and glxContext.
	}

	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
		gGlxContext = NULL;
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	

	//Destroy VAO
	if(vao_triangle) {
		glDeleteVertexArrays(1, &vao_triangle);
		vao_triangle = 0;
	}
	if (vao_square) {
		glDeleteVertexArrays(1, &vao_square);
		vao_square = 0;
	}

	//Destroy vbo
	if(VBO_TRIAGLE_POSITION) {
		glDeleteBuffers(1, &VBO_TRIAGLE_POSITION);
		VBO_TRIAGLE_POSITION = 0;
	}

	if (VBO_TRIANGLE_COLOR) {
		glDeleteBuffers(1, &VBO_TRIANGLE_COLOR);
		VBO_TRIANGLE_COLOR = 0;
	}
	//Destroy vbo
	if (VBO_SQUARE_POSITION) {
		glDeleteBuffers(1, &VBO_SQUARE_POSITION);
		VBO_SQUARE_POSITION = 0;
	}

	if (VBO_SQUARE_COLOR) {
		glDeleteBuffers(1, &VBO_SQUARE_COLOR);
		VBO_SQUARE_COLOR = 0;
	}

	//Safe release of shaders and shader program.
	if(gShaderProgramObject) {

		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		printf("Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			printf("Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		//unlink the shader program.
		glUseProgram(0);
	}


	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


/* 

Make sure first glew is configured correctly in .bashrc file.

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64

$ g++ -o xwindow XWindows.cpp -lX11 -lGL -lGLEW

$ ./xwindow

*/
