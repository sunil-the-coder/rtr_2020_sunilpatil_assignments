#include<iostream>
#include<stdio.h> 
#include<stdlib.h> //exit(1)
#include<memory.h> //memset
#include<GL/gl.h> // For OpenGL API
#include<GL/glx.h> //Similar to WGL in windows ( Bridging API ) - For GLX API
#include<GL/glu.h> // 1
#define _USE_MATH_DEFINES
#include <math.h>

#include<X11/Xlib.h> //compulsary for xwindows ( like windows.h in windows)
#include<X11/Xutil.h> //XVisualInfo
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //KeySym

using namespace std;

//Global variables
bool bFullscreen = false;
Display *gpDisplay = NULL; // 77 member structure.
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
Status status;
GLXContext gGlxContext; // hghrc in windows

int giWindowWidth = 800;
int giWindowHeight = 600;

//Light
bool gbLight = false;
GLfloat lightAmbient[] = { 0.1f, 0.1f, 0.1f, 1.0f }; // new ambient light ( black) 
GLfloat lightDifuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; //white light
GLfloat lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f}; //light coming from +ve z axis ( stage chya baherun)
GLfloat lightSpecular[] = {0.0f, 1.0f, 0.0f, 1.0f};

GLfloat materialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDifuse[] = {1.0f, 0.0f, 0.0f, 1.0f}; // actual object color ( jambla )
GLfloat materialSpecular[] = {0.7f, 0.7f, 0.7f, 1.0f}; //  reflected bounce back
GLfloat materialShininess = 128.0f; 

GLUquadric *quadric = NULL;

//You can use CLA to pass network server ip address config.
int main(void)
{

	void initialize(void);
	void resize(int, int);
	void display(void);

	//Function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void Uninitialize();

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;


	//Code
	CreateWindow();

	
	initialize();

	//Message Loop
	XEvent event; // == MSG in window ( XEvent is union which has 31 data structures)
	KeySym keysym; // key symbol ( == VK_ESCAPE in windows )

	while(bDone == false)
	{
		while(XPending(gpDisplay)) // Windows -> PeekMessage()  - Check in networked queue if any pending msgs
		{
		
		XNextEvent(gpDisplay, &event); // == GetMessage() in windows

		switch(event.type) //iMsg of WndProc
		{
			case MapNotify: // XMapWindow() -> Will trigger this event informing window is created like WM_CREATE in windows
				break;
			case KeyPress: // == WM_KEYDOWN 
				//Get actual key symbol from key code ( In windows wParam madhe windows system tumhala VK key pathavato )
				//Xkb -> XWindow keyboard
				//3rd 0-> keysymbol group ( diff language group ) - default group
				//4th 0-> Direct key press OR using key with Shift key
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
					case XK_Escape: // XServer cha key symbol
						bDone = true;
						break;
					case XK_F:
					case XK_f:
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;	
					case XK_L:
					case XK_l:
						printf(" L Pressed:\n");
						if(gbLight == false) {
							glEnable(GL_LIGHTING);
							gbLight = true;
						} else {							
							glDisable(GL_LIGHTING);
							gbLight = false;
						}
						break;
					default:
						break;
				}

				/*
				//You can use WM_CHAR like behavior with XLoopString API.
				XLooupString(gpDisplay, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
				}
				*/
				break;
			case ButtonPress: // == WM_LBUTTON_DOWN, WM_RBUTTON_DONW
				switch(event.xbutton.button)
				{
					case 1: //left mouse button
						break;
					case 2: //middle mouse button
						break;
					case 3: //right mouse button
						break;
					case 4: //mouse scroll up
						break;
					case 5: //mouse scroll down
						break;
					default:
						break;
				}
				break;
			case MotionNotify: //If window was moved.
				break;
			case ConfigureNotify: // == WM_SIZE
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33: //Atom representation is ( WM_DELETE_WINDOW ) - Numeric format used on network - fast.
				//Uninitialize();
				//exit(0);
				bDone = true;
				break;
			default:
				break;
		}

		}

		//Call to update here 
		display();
	}

	Uninitialize();

	return (0);
}

void CreateWindow(void)
{
	//Function Declarations
	void Uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs; //WndClass in Windows
	int defaultScreen;
	int styleMask;
	//total 44 members			
	static int frameBufferAttributes[] =  
			{GLX_DOUBLEBUFFER,True,
			GLX_RGBA,  //type of pixel needed
			GLX_RED_SIZE, 8, 
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24, // V4L(video for linux) recommends bitsize of depth to be kep around 24 or below 
			None}; // pixelformat attributes in windows // None

	//None -> 0 ( Prefer to use Macro rather than value for future modifications )
	//
	// The array which is big but i want to provide only subset of values, always set the last value to 0 => Good Programming Practice

	//Code
	//1. Establish connection to XServer on network
	gpDisplay = XOpenDisplay(NULL); // NULL -> Default behavior - $DISPLAY cha output ( host:display.screen )
	if(gpDisplay == NULL) // Abortive behavior 
	{
		printf("ERROR: Unable to open X Display. \n Exiting Now...\n");
		Uninitialize();
		exit(1); // Errorneous exit status 
	}
	
	//2. Retrieve default screen of graphics card connected display
	//DefaultScreen macro can be used which internally calls XDefaultScreen.
	defaultScreen = XDefaultScreen(gpDisplay);


	//4. Retrieve the handle to display context like windows ( hdc context )
	//4.a -> Allocate memory for visual info
	gpXVisualInfo = (XVisualInfo *) malloc(sizeof(XVisualInfo)); // Main 10 members of this structure, 
	//struct XVisualInfo has struct Visual => This is actual hdc which has 8 members.
	if(gpXVisualInfo == NULL)
	{
		printf("Error: Memory allocation failed for Visual Info. \n Existing Now.. \n");
		Uninitialize();
		exit(1);
	}

	//New function
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	//4.b -> Retrieve actual visualInfo by matching colormap 
	//TrueColor -> matching pixelformat with visual, 
	// It should fill up the gpX ( Windows -> ChoosePixelFormat)
	//status = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpxVisualInfo);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR: Unable to Get A Visual. \n Exiting Now.. \n");
		Uninitialize();
		exit(1);
	}

	//5. Fill up window attributes like wndclass in Windows system.
	winAttribs.border_pixel = 0; //default color border. ( pixel mhanje ithe color sangayacha - I'm not giving so take whatever you can provide )
	winAttribs.background_pixmap = 0; // No background image. ( pixmap -> pixel war map karnari image )
	//retrieve matching(TrueColor) colormap from set of colormap
	winAttribs.colormap = XCreateColormap(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), //give me colormap as per my root window's visual screen ( TrueColor wali )
			gpXVisualInfo->visual, //pass hdc parameter
			AllocNone); // I am assigning colormap to winAttributes so don't allocate new memory.

	gColormap = winAttribs.colormap;

	//Set window background color as black. ( GetStockObject in windows )
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	//Which message to be sent by XServer to Xclient. ( == GetMessage() last 2 parameters )
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	//CW - CreateWindow
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//6. Create actual window in memory with required window attributes
	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, //x
			0, //y
			giWindowWidth,
			giWindowHeight,
			0, //border width
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual, //hdc
			styleMask,
			&winAttribs);

	if(!gWindow)
	{
		printf("ERROR: Failed to create main window. \n Exiting Now... \n");
		Uninitialize();
		exit(1);
	}


	XStoreName(gpDisplay, gWindow, "SMP: XWindows Sphere Light with Material");

	//Unique behavior for click on actual window close button on different window manager.
	//Next 2 lines are not necessary to run but they are important. ( close button click on window - not need for xwindows but its need of window manager)
	// Close button position (left or right it depends on windows manager. So use WM (Window Manager) orders of XWindows)
	//3rd Param -> If same items needs to be prepared in case its already in queue. True -> Prepare new item in queue & remove old one.
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True); //Atom => Unique string - Immutable ( CLOSE -> 33)
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1); //1 -> Total orders ( In case multiple orders Atom[])

	//7. Show actual window from memory
	XMapWindow(gpDisplay, gWindow);
	
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//Code
	//Create new order(Aadnya) for WM to retrieve current state
	//_NET_WM_STATE -> Across the networked window current state 
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1; //data is union, l for long type array

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	//== SendMessage() / PostMessage()
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, //Propagate to child as well ? False -> Dont propagate. Use only for me
		StructureNotifyMask, // SubStructureNotify -> client area, StructureNotifyMask -> For entire window
		&xev); 
}

void initialize(void)
{
	//function declarations
	
	void resize(int, int);


	//1. Create Context ( GL_TRUE -> OpenGL boolean type )
	//NULL -> no sharable display - Create new context with single display ( You could have pass another display context to create your own )
	//GL_TRUE => h/w or s/w rendering => TRUE ( H/w Rendering - Original graphic card rendering not novue/vesa renering)
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE); // Similar to wglCreateContext

	//Make 
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	//Depth Function
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//Light code
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDifuse);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPosition);
	glEnable(GL_LIGHT1);
	
	//material properties
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	resize(giWindowWidth, giWindowHeight);

}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
		
	glViewport(0, 0, (GLsizei) width, (GLsizei) height);

	// >>> 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

void display(void)
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); //modelview duality = when model can't, view can OR  when view can't, model can.
	glLoadIdentity();

	//pushing modelling matrix by -3 back in z-axis so that its viewable to you. If given +3 it won't dispaly anything.
	glTranslatef(0.0f, 0.0f, -0.55f); // if commented then screen gets complete black. // move model matrix by -3 in zOrder.

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // internally it has glBegin() and glEnd()

	//Normals are calculated using gluSphere internally - No need to specifiy expicitely.
	glXSwapBuffers(gpDisplay, gWindow);

}




void Uninitialize(void)
{
	GLXContext currentGlContext; //In network, check its same monitor

	currentGlContext = glXGetCurrentContext();

	if(currentGlContext == gGlxContext) {
		glXMakeCurrent(gpDisplay, 0, 0); // Coming out of openGl. No need gWindow and glxContext.
	}

	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
		gGlxContext = NULL;
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}



