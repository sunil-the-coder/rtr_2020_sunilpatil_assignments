#include<iostream>
#include<stdio.h> 
#include<stdlib.h> //exit(1)
#include<memory.h> //memset
#include<GL/gl.h> // For OpenGL API
#include<GL/glx.h> //Similar to WGL in windows ( Bridging API ) - For GLX API
#include<GL/glu.h> // 1
#define _USE_MATH_DEFINES
#include <math.h>

#include<X11/Xlib.h> //compulsary for xwindows ( like windows.h in windows)
#include<X11/Xutil.h> //XVisualInfo
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //KeySym

using namespace std;

//Global variables
bool bFullscreen = false;
Display *gpDisplay = NULL; // 77 member structure.
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
Status status;
GLXContext gGlxContext; // hghrc in windows

int giWindowWidth = 800;
int giWindowHeight = 600;

//Light
bool gbLight = false;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // no ambient
GLfloat lightDifuse[] = { 1.0f, 0.0f, 0.0f, 1.0f }; //red light
GLfloat lightPosition[] = { 0.0f, 3.0f, 3.0f, 0.0f }; //will animate later - Positional light


GLfloat lightModelAmbient[] = { 0.2, 0.2, 0.2, 1.0 };
GLfloat lightModelLocalViewer[] = { 0.0f };

GLfloat xRotationAngle = 0.0f;
GLfloat yRotationAngle = 0.0f;
GLfloat zRotationAngle = 0.0f;


GLUquadric* quadric[24];
GLint keyPressed = 0;

//You can use CLA to pass network server ip address config.
int main(void)
{

	void initialize(void);
	void resize(int, int);
	void display(void);

	//Function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void Uninitialize();

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;


	//Code
	CreateWindow();

	
	initialize();

	//Message Loop
	XEvent event; // == MSG in window ( XEvent is union which has 31 data structures)
	KeySym keysym; // key symbol ( == VK_ESCAPE in windows )

	while(bDone == false)
	{
		while(XPending(gpDisplay)) // Windows -> PeekMessage()  - Check in networked queue if any pending msgs
		{
		
		XNextEvent(gpDisplay, &event); // == GetMessage() in windows

		switch(event.type) //iMsg of WndProc
		{
			case MapNotify: // XMapWindow() -> Will trigger this event informing window is created like WM_CREATE in windows
				break;
			case KeyPress: // == WM_KEYDOWN 
				//Get actual key symbol from key code ( In windows wParam madhe windows system tumhala VK key pathavato )
				//Xkb -> XWindow keyboard
				//3rd 0-> keysymbol group ( diff language group ) - default group
				//4th 0-> Direct key press OR using key with Shift key
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
					case XK_Escape: // XServer cha key symbol
						bDone = true;
						break;
					case XK_F:
					case XK_f:
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;	
					case XK_L:
					case XK_l:
						printf(" L Pressed:\n");
						if(gbLight == false) {
							glEnable(GL_LIGHTING);
							gbLight = true;
						} else {							
							glDisable(GL_LIGHTING);
							gbLight = false;
						}
						break;
					case XK_X:
					case XK_x:
						keyPressed = 1;
						xRotationAngle = 0.0f;
						break;
					case XK_Y:
					case XK_y:
						keyPressed = 2;
						yRotationAngle = 0.0f;
						break;
					case XK_Z:
					case XK_z:
						keyPressed = 3;
						zRotationAngle = 0.0f;
						break;	
					default:
						break;
				}

				/*
				//You can use WM_CHAR like behavior with XLoopString API.
				XLooupString(gpDisplay, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
				}
				*/
				break;
			case ButtonPress: // == WM_LBUTTON_DOWN, WM_RBUTTON_DONW
				switch(event.xbutton.button)
				{
					case 1: //left mouse button
						break;
					case 2: //middle mouse button
						break;
					case 3: //right mouse button
						break;
					case 4: //mouse scroll up
						break;
					case 5: //mouse scroll down
						break;
					default:
						break;
				}
				break;
			case MotionNotify: //If window was moved.
				break;
			case ConfigureNotify: // == WM_SIZE
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33: //Atom representation is ( WM_DELETE_WINDOW ) - Numeric format used on network - fast.
				//Uninitialize();
				//exit(0);
				bDone = true;
				break;
			default:
				break;
		}

		}

		//Call to update here 
		display();
	}

	Uninitialize();

	return (0);
}

void CreateWindow(void)
{
	//Function Declarations
	void Uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs; //WndClass in Windows
	int defaultScreen;
	int styleMask;
	//total 44 members			
	static int frameBufferAttributes[] =  
			{GLX_DOUBLEBUFFER,True,
			GLX_RGBA,  //type of pixel needed
			GLX_RED_SIZE, 8, 
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24, // V4L(video for linux) recommends bitsize of depth to be kep around 24 or below 
			None}; // pixelformat attributes in windows // None

	//None -> 0 ( Prefer to use Macro rather than value for future modifications )
	//
	// The array which is big but i want to provide only subset of values, always set the last value to 0 => Good Programming Practice

	//Code
	//1. Establish connection to XServer on network
	gpDisplay = XOpenDisplay(NULL); // NULL -> Default behavior - $DISPLAY cha output ( host:display.screen )
	if(gpDisplay == NULL) // Abortive behavior 
	{
		printf("ERROR: Unable to open X Display. \n Exiting Now...\n");
		Uninitialize();
		exit(1); // Errorneous exit status 
	}
	
	//2. Retrieve default screen of graphics card connected display
	//DefaultScreen macro can be used which internally calls XDefaultScreen.
	defaultScreen = XDefaultScreen(gpDisplay);


	//4. Retrieve the handle to display context like windows ( hdc context )
	//4.a -> Allocate memory for visual info
	gpXVisualInfo = (XVisualInfo *) malloc(sizeof(XVisualInfo)); // Main 10 members of this structure, 
	//struct XVisualInfo has struct Visual => This is actual hdc which has 8 members.
	if(gpXVisualInfo == NULL)
	{
		printf("Error: Memory allocation failed for Visual Info. \n Existing Now.. \n");
		Uninitialize();
		exit(1);
	}

	//New function
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	//4.b -> Retrieve actual visualInfo by matching colormap 
	//TrueColor -> matching pixelformat with visual, 
	// It should fill up the gpX ( Windows -> ChoosePixelFormat)
	//status = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpxVisualInfo);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR: Unable to Get A Visual. \n Exiting Now.. \n");
		Uninitialize();
		exit(1);
	}

	//5. Fill up window attributes like wndclass in Windows system.
	winAttribs.border_pixel = 0; //default color border. ( pixel mhanje ithe color sangayacha - I'm not giving so take whatever you can provide )
	winAttribs.background_pixmap = 0; // No background image. ( pixmap -> pixel war map karnari image )
	//retrieve matching(TrueColor) colormap from set of colormap
	winAttribs.colormap = XCreateColormap(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), //give me colormap as per my root window's visual screen ( TrueColor wali )
			gpXVisualInfo->visual, //pass hdc parameter
			AllocNone); // I am assigning colormap to winAttributes so don't allocate new memory.

	gColormap = winAttribs.colormap;

	//Set window background color as black. ( GetStockObject in windows )
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	//Which message to be sent by XServer to Xclient. ( == GetMessage() last 2 parameters )
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	//CW - CreateWindow
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//6. Create actual window in memory with required window attributes
	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, //x
			0, //y
			giWindowWidth,
			giWindowHeight,
			0, //border width
			gpXVisualInfo->depth,
			InputOutput,
			gpXVisualInfo->visual, //hdc
			styleMask,
			&winAttribs);

	if(!gWindow)
	{
		printf("ERROR: Failed to create main window. \n Exiting Now... \n");
		Uninitialize();
		exit(1);
	}


	XStoreName(gpDisplay, gWindow, "SMP: XWindows 24 Spheres with Positional Light");

	//Unique behavior for click on actual window close button on different window manager.
	//Next 2 lines are not necessary to run but they are important. ( close button click on window - not need for xwindows but its need of window manager)
	// Close button position (left or right it depends on windows manager. So use WM (Window Manager) orders of XWindows)
	//3rd Param -> If same items needs to be prepared in case its already in queue. True -> Prepare new item in queue & remove old one.
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True); //Atom => Unique string - Immutable ( CLOSE -> 33)
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1); //1 -> Total orders ( In case multiple orders Atom[])

	//7. Show actual window from memory
	XMapWindow(gpDisplay, gWindow);
	
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//Code
	//Create new order(Aadnya) for WM to retrieve current state
	//_NET_WM_STATE -> Across the networked window current state 
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1; //data is union, l for long type array

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	//== SendMessage() / PostMessage()
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, //Propagate to child as well ? False -> Dont propagate. Use only for me
		StructureNotifyMask, // SubStructureNotify -> client area, StructureNotifyMask -> For entire window
		&xev); 
}

void initialize(void)
{
	//function declarations
	
	void resize(int, int);


	//1. Create Context ( GL_TRUE -> OpenGL boolean type )
	//NULL -> no sharable display - Create new context with single display ( You could have pass another display context to create your own )
	//GL_TRUE => h/w or s/w rendering => TRUE ( H/w Rendering - Original graphic card rendering not novue/vesa renering)
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE); // Similar to wglCreateContext

	//Make 
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	//Depth Function
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	//Initialization of lights
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDifuse);
	glEnable(GL_LIGHT0);

	//Initialize the 24 quadric.
	for (int i = 0; i < 24; i++)
		quadric[i] = gluNewQuadric();


	resize(giWindowWidth, giWindowHeight);

}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
		
	glViewport(0, 0, (GLsizei) width, (GLsizei) height);

	// >>> 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

void display(void)
{

	void Draw24Sphere(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -20.0f);

	if (keyPressed == 1) {

		glRotatef(xRotationAngle, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = xRotationAngle;

	}
	else if (keyPressed == 2) {

		glRotatef(yRotationAngle, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = yRotationAngle;

	}
	else if (keyPressed == 3) {

		glRotatef(zRotationAngle, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = zRotationAngle;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	//glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);

	Draw24Sphere();
	
	glXSwapBuffers(gpDisplay, gWindow);

}
void Draw24Sphere()
{

	GLfloat materialAmbient[4];
	GLfloat materialDifuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//glViewport(0, currentHeight/4, 200/6, currentHeight/4);
	
	//1. 1st sphere on 1st column, emerald
	materialAmbient[0] = 0.0215;
	materialAmbient[1] = 0.1745;
	materialAmbient[2] = 0.0215;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.07568;
	materialDifuse[1] = 0.61424;
	materialDifuse[2] = 0.07568;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.633;
	materialSpecular[1] = 0.727811;
	materialSpecular[2] = 0.633;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-10.0f, 6.0f, 0.0f);
	gluSphere(quadric[0], 1.2f, 30, 30);
	glPopMatrix();


	//glViewport(currentWidth/5, currentHeight/4, currentWidth/5, currentHeight/3);
	//2. 2nd sphere on 1st column, jade
	materialAmbient[0] = 0.135;
	materialAmbient[1] = 0.2225;
	materialAmbient[2] = 0.1575;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.54;
	materialDifuse[1] = 0.89;
	materialDifuse[2] = 0.63;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.316228;
	materialSpecular[1] = 0.316228;
	materialSpecular[2] = 0.316228;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-6.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//3. 3rd sphere on 1st column, obsidian
	materialAmbient[0] = 0.05375;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.06625;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.18725;
	materialDifuse[1] = 0.17;
	materialDifuse[2] = 0.22525;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.33741;
	materialSpecular[1] = 0.328634;
	materialSpecular[2] = 0.346435;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.3 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-2.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//4. 4th sphere on 1st column, obsidian
	materialAmbient[0] = 0.25;
	materialAmbient[1] = 0.20725;
	materialAmbient[2] = 0.20725;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 1.0f;
	materialDifuse[1] = 0.829f;
	materialDifuse[2] = 0.829f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.088 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(2.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//5. 5th sphere on 1st column, obsidian
	materialAmbient[0] = 0.1745;
	materialAmbient[1] = 0.01175;
	materialAmbient[2] = 0.01175;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.61242f;
	materialDifuse[1] = 0.04146f;
	materialDifuse[2] = 0.04146f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(6.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//6. 6th sphere on 1st column, obsidian
	materialAmbient[0] = 0.1;
	materialAmbient[1] = 0.18725;
	materialAmbient[2] = 0.1745;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.396f;
	materialDifuse[1] = 0.74151;
	materialDifuse[2] = 0.69102f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.297254;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.6306678f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(10.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//7. 7th sphere on 2nd row, turquoise
	 materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.780392f;
	materialDifuse[1] = 0.568627;;
	materialDifuse[2] = 0.113725f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.992147f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.21794872 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//8. 8th sphere on 2nd row, bronze
	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.714f;
	materialDifuse[1] = 0.4284f;;
	materialDifuse[2] = 0.18144f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.2 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//9. 9th sphere on 2nd row, chrome
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.4f;;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//10. 10th sphere on 2nd row, chrome
	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.7038f;
	materialDifuse[1] = 0.27048f;;
	materialDifuse[2] = 0.0828f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.256777f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//11. 11th sphere on 2nd row, gold
	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.75164f;
	materialDifuse[1] = 0.60648f;;
	materialDifuse[2] = 0.22648f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//12. 12th sphere on 2nd row, silver
	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.50754f;
	materialDifuse[1] = 0.50754f;;
	materialDifuse[2] = 0.50754f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	
	//13. 13th sphere on 3rd row, black
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.01f;
	materialDifuse[1] = 0.01f;
	materialDifuse[2] = 0.01f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.50f;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//14. 14th sphere on 3rd row, cyan
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.0f;
	materialDifuse[1] = 0.50980392f;
	materialDifuse[2] = 0.50980392f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//15. 15th sphere on 3rd row, green
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.1f;
	materialDifuse[1] = 0.35f;
	materialDifuse[2] = 0.1f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//16. 16th sphere on 3rd row, red
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.0f;
	materialDifuse[2] = 0.0f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();


	//17. 17th sphere on 3rd row, white
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.55f;
	materialDifuse[1] = 0.55f;
	materialDifuse[2] = 0.55f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//18. 18th sphere on 3rd row, yello plastic
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.0f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	
	//19. 19th sphere on 4th row, black
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.01f;
	materialDifuse[1] = 0.01f;
	materialDifuse[2] = 0.01f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.4;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//20. 20th sphere on 4th row, cyan
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.5f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//21. 21st sphere on 4th row, green
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//22. 22nd sphere on 4th row, red
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.4f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();


	//23. 23rd sphere on 4th row, white
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.5f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//24. 24th sphere on 4th row, rubber
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix(); 
}


void Uninitialize(void)
{
	GLXContext currentGlContext; //In network, check its same monitor

	currentGlContext = glXGetCurrentContext();

	if(currentGlContext == gGlxContext) {
		glXMakeCurrent(gpDisplay, 0, 0); // Coming out of openGl. No need gWindow and glxContext.
	}

	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
		gGlxContext = NULL;
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	for (int i = 0; i < 24; i++) {
		if (quadric[i]) {
			gluDeleteQuadric(quadric[i]);
			quadric[i] = NULL;
		}
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}



