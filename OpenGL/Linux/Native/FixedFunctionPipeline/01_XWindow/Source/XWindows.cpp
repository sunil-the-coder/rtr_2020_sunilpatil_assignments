#include<iostream>
#include<stdio.h> 
#include<stdlib.h> //exit(1)
#include<memory.h> //memset

#include<X11/Xlib.h> //compulsary for xwindows ( like windows.h in windows)
#include<X11/Xutil.h> //XVisualInfo
#include<X11/XKBlib.h> //XkbKeycodeToKeysym()
#include<X11/keysym.h> //KeySym

using namespace std;

//Global variables
bool bFullscreen = false;
Display *gpDisplay = NULL; // 77 member structure.
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
Status status;

//You can use CLA to pass network server ip address config.
int main(void)
{
	//Function declarations
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void Uninitialize();

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	
	//Code
	CreateWindow();

	//Message Loop
	XEvent event; // == MSG in window ( XEvent is union which has 31 data structures)
	KeySym keysym; // key symbol ( == VK_ESCAPE in windows )

	while(1)
	{
		//It does getMessage() and translateMessage()
		XNextEvent(gpDisplay, &event); // == GetMessage() in windows

		switch(event.type) //iMsg of WndProc
		{
			case MapNotify: // XMapWindow() -> Will trigger this event informing window is created like WM_CREATE in windows
				break;
			case KeyPress: // == WM_KEYDOWN 
				//Get actual key symbol from key code ( In windows wParam madhe windows system tumhala VK key pathavato )
				//Xkb -> XWindow keyboard
				//3rd 0-> keysymbol group ( diff language group ) - default group ( take all symbols )
				//4th 0-> Direct key press(0) OR using key with Shift key
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
					case XK_Escape: // XServer cha key symbol
						Uninitialize();
						exit(0);
						break;
					case XK_F:
					case XK_f:
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
					default:
						break;
				}

				/*
				//You can use WM_CHAR like behavior with XLoopString API.
				XLooupString(gpDisplay, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
				}
				*/
				break;
			case ButtonPress: // == WM_LBUTTON_DOWN, WM_RBUTTON_DONW
				switch(event.xbutton.button)
				{
					case 1: //left mouse button
						break;
					case 2: //middle mouse button
						break;
					case 3: //right mouse button
						break;
					case 4: //mouse scroll up
						break;
					case 5: //mouse scroll down
						break;
					default:
						break;
				}
				break;
			case MotionNotify: //If window was moved.
				break;
			case ConfigureNotify: // == WM_SIZE
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33: //Atom representation is ( WM_DELETE_WINDOW ) - Numeric format used on network - fast.
				Uninitialize();
				exit(0);
				break;
			default:
				break;
		}		
	}

	Uninitialize();

	return (0);
}

void CreateWindow(void)
{
	//Function Declarations
	void Uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs; //WndClass in Windows
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	//Code
	//1. Establish connection to XServer on network
	gpDisplay = XOpenDisplay(NULL); // NULL -> Default behavior - $DISPLAY cha output ( host:display.screen )
	if(gpDisplay == NULL) // Abortive behavior 
	{
		printf("ERROR: Unable to open X Display. \n Exiting Now...\n");
		Uninitialize();
		exit(1); // Errorneous exit status 
	}
	
	//2. Retrive default screen of graphics card connected display
	//DefaultScreen macro can be used which internally calls XDefaultScreen.
	defaultScreen = XDefaultScreen(gpDisplay);

	//X -> Its function, wihtout x -> its macro

	//3. Retrive default depth for current screen ( pfd )
	defaultDepth = DefaultDepth(gpDisplay, defaultScreen); //Retrieve color depth of screen (black + white, 2^16, 2^32)

	//4. Retrieve the handle to display context like windows ( hdc context )
	//4.a -> Allocate memory for visual info
	gpXVisualInfo = (XVisualInfo *) malloc(sizeof(XVisualInfo)); // Main 10 members of this structure, 
	//struct XVisualInfo has struct Visual => This is actual hdc which has 8 members.
	if(gpXVisualInfo == NULL)
	{
		printf("Error: Memory allocation failed for Visual Info. \n Existing Now.. \n");
		Uninitialize();
		exit(1);
	}

	//4.b -> Retrieve actual visualInfo by matching colormap 
	//TrueColor -> matching pixelformat with visual, 
	// It should fill up the gpX ( Windows -> ChoosePixelFormat)
	status = XMatchVisualInfo(gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);
	if(status == 0)
	{
		printf("ERROR: Unable to Get A Visual. \n Exiting Now.. \n");
		Uninitialize();
		exit(1);
	}

	//5. Fill up window attributes like wndclass in Windows system.
	winAttribs.border_pixel = 0; //default color border. ( pixel mhanje ithe color sangayacha - I'm not giving so take whatever you can provide )
	winAttribs.background_pixmap = 0; // No background image. ( pixmap -> pixel war map karnari image )
	//retrieve matching(TrueColor) colormap from set of colormap
	winAttribs.colormap = XCreateColormap(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen), //give me colormap as per my root window's visual screen ( TrueColor wali )
			gpXVisualInfo->visual, //pass hdc parameter
			AllocNone); // I am assigning colormap to winAttributes so don't allocate new memory.

	gColormap = winAttribs.colormap;

	//Set window background color as black. ( GetStockObject in windows )
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	//Which message to be sent by XServer to Xclient. ( == GetMessage() last 2 parameters )
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	//CW - CreateWindow
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//6. Create actual window in memory with required window attributes
	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, //x
			0, //y
			giWindowWidth,
			giWindowHeight,
			0, //border width - default
			gpXVisualInfo->depth,
			InputOutput, 
			gpXVisualInfo->visual, //hdc
			styleMask,
			&winAttribs);

	if(!gWindow)
	{
		printf("ERROR: Failed to create main window. \n Exiting Now... \n");
		Uninitialize();
		exit(1);
	}


	XStoreName(gpDisplay, gWindow, "SMP: My First XWindows Window");

	//Unique behavior for click on actual window close button on different window manager.
	//Next 2 lines are not necessary to run but they are important. ( close button click on window - not need for xwindows but its need of window manager)
	// Close button position (left or right it depends on windows manager. So use WM (Window Manager) orders of XWindows)
	//3rd Param -> If same items needs to be prepared in case its already in queue. True -> Prepare new item in queue & remove old one.
	//Window close karayachi garj hi window manager chi aahe not XWindows chi. Ani kahi WM che API aahet te specific WM barobar boltat.
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True); //Atom => Unique string - Immutable ( CLOSE -> 33)
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1); //1 -> Total orders ( In case multiple orders Atom[])

	//7. Show actual window from memory
	XMapWindow(gpDisplay, gWindow);
	
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//Code
	//Create new order(Aadnya) for WM to retrieve current state
	//_NET_WM_STATE -> Across the networked window current state 
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1; //data is union, l for long type array

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	//== SendMessage() / PostMessage()
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, //Propagate to child as well ? False -> Dont propagate. Use only for me
		StructureNotifyMask, // SubStructureNotify -> client area, StructureNotifyMask -> For entire window
		&xev); 
}

void Uninitialize(void)
{
	if( )
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}



