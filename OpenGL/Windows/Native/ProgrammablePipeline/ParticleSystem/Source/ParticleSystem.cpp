#define STB_IMAGE_IMPLEMENTATION
#include<windows.h>
#include<GL/glew.h>
#include<gl/GL.h>

#include<stdio.h>
#include"vmath.h"
#include "stb_image.h"
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

//global variables
HDC ghdc = NULL;
HWND ghwnd = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_VELOCITY,
	AMC_ATTRIBUTE_START_TIME,
	AMC_ATTRIBUTE_COLOR,
};
GLuint vao; //vertex array object
GLuint vboStartTime, vboVelocities, vboColor, vboPosition; //vertex buffer object
GLuint mUniform, vUniform, pUniform, LDUniform, KDUniform, LAUniform, KAUniform, KSUniform, LSUniform, LightPositionUniform, MaterialShininessUniform, LKeyIsPressedUniform;

GLuint TimeUniform, BackgroundUniform,tex_starUniform;
mat4 perspectiveProjectionMatrix;

//Particle system
const int g_numRainVertices = 150000;
static GLint arrayWidth, arrayHeight;
static GLfloat *verts = NULL;
static GLfloat *colors = NULL;
static GLfloat *velocities = NULL;
static GLfloat *startTimes = NULL;
static GLfloat *rainDropTexture = NULL;
float g_heightRange = 2.0f;
float g_radiusMin = 0.0f;
float g_radiusRange = 4.0f;



float lightAmbient[4] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffused[4] = { 1.0f,1.0f,1.0f,1.0f };
float lightSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };

float materialAmbient[4] = { 0.0f,0.0f,0.0f,1.0f };
float materialDiffused[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialShininess = 250.0f;

//random numbers; replace with a better quality generator for better particle distribution
//----------------------------------------------------------------------------------------

float random1()
{
	return (float((double)rand() / ((double)(RAND_MAX)+(double)(1))));
}

GLuint textureID0, textureID1, textureID2, textureID3, textureID4, textureID5, textureID6,textureID7 ;


BOOL LoadTexture(GLuint *texture, char* fileName)
{
	int width, height, bitDepth;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *texData = stbi_load(fileName, &width, &height, &bitDepth, 4);
	if (!texData)
	{
		printf("Failed to find: %s\n", "star.png");
		return FALSE;
	}

	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, texData);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_image_free(texData);

	return TRUE;
}
void createPoints()
{
	//float random();
	GLfloat *vptr,  *velptr, *stptr;
	GLfloat *rptr;


	if (verts != NULL)
		free(verts);

	verts = (GLfloat *)malloc(g_numRainVertices * 3 * sizeof(GLfloat));
	//colors = (GLfloat *)malloc(g_numRainVertices * 3 * sizeof(GLfloat));
	velocities = (GLfloat *)malloc(g_numRainVertices * 3 * sizeof(GLfloat));
	startTimes = (GLfloat *)malloc(g_numRainVertices * sizeof(GLfloat));
	rainDropTexture = (GLfloat *)malloc(g_numRainVertices * sizeof(GLfloat));

	vptr = verts;
	//cptr = colors;
	velptr = velocities;
	stptr = startTimes;
	rptr = rainDropTexture;

	for (int i = 0; i<g_numRainVertices; i++)
	{
		float SeedX;
		float SeedZ;
		bool pointIsInside = false;
		while (!pointIsInside)
		{
			SeedX = random1() - 0.5f;
			SeedZ = random1() - 0.5f;
			if (sqrt(SeedX*SeedX + SeedZ*SeedZ) <= 0.5f)
				pointIsInside = true;
		}
		//save these random locations for reinitializing rain particles that have fallen out of bounds
		SeedX *= g_radiusRange;
		SeedZ *= g_radiusRange;
		float SeedY = random1()*g_heightRange;

		*vptr = SeedX;
		*(vptr + 1) = 2.5f;
		*(vptr + 2) = SeedZ;
		vptr += 3;

		/**cptr = ((float)rand() / RAND_MAX)*0.5f + 0.5f;
		*(cptr + 1) = ((float)rand() / RAND_MAX)*0.5f + 0.5f;
		*(cptr + 2) = ((float)rand() / RAND_MAX)*0.5f + 0.5f;
		cptr += 3;*/

		*velptr = 40.0f*(random1() / 20.0f);;
		*(velptr + 1) = 40.0f*(random1() / 20.0f);
		*(velptr + 2) = 40.0f*(random1() / 10.0f);
		velptr += 3;
			
		*stptr = ((float)rand() / RAND_MAX)*10.0f;
		stptr++;

		//get an integer between 1 and 8 inclusive to decide which of the 8 types of rain textures the particle will use
		*rptr = (float)int(floor(random1() * 8 + 1));
		rptr++;
	}
}

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmddLine, int iCmdShow)
{
	//functions Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet;

	//code
	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created and opened Successfully\n");
	}

	//initialization of WNDCLASSSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


	//register the above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -5)
	{
		fprintf(gpFile, "glewInit failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization succcceded\n");
	}
	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//call update here
				update();
			}
			//call display here
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void resize(int, int);
	void display(void);
	void unInitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		unInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev{ sizeof(WINDOWPLACEMENT) };
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,
			GWL_STYLE);
		if (dwStyle&WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev)
				&&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle&~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;

	}
	else
	{
		SetWindowLong(ghwnd,
			GWL_STYLE,
			dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,
			&wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void)
{
	BOOL LoadTexture(GLuint *texture, char* fileName);
	void resize(int, int);
	void unInitialize(void);
	//variable Declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));

	//code
	//Initialize pfd
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (!wglMakeCurrent(ghdc, ghrc))
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		return(-5);
	}

	//VertexShader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Shader source code
	GLchar *vertexShaderSourceCode =
		"#version 420 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in float vColor;" \
		"in vec3 vVelocity;" \
		"in float StartTime;" \
		"uniform float Time;" \
		"uniform vec4 Background;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"out float Color;" \
		"out vec3 tNormVertexShader;" \
		"out vec4 eye_coordinatesVertexShader;" \

		"void main(void)" \
		"{" \
		"vec4 vert;" \
		"float t=Time-StartTime;" \
		"if(t>0)" \
		"{" \
		"	vert=vPosition+vec4(vVelocity*t,0.0);" \
		"	vert.y-=4.9*t*t;" \
		"Color=vColor;" \
		"}" \
		"else" \
		"{" \
		"	vert=vPosition;" \
		"	Color=vColor;" \
		"}" \
		"gl_PointSize = 30.0;															\n" \
		"eye_coordinatesVertexShader=u_v_matrix*u_m_matrix*vPosition;" \
		"tNormVertexShader=mat3(u_v_matrix*u_m_matrix)*vec3(1.0,0.0,0.0);" \

		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vert;" \
		"}";

	/* \*/
	//Specify the above source code to the shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile the vertex shader source code
	glCompileShader(gVertexShaderObject);

	//Error Checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, szInfoLog);
				free(szInfoLog);
				unInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}


	//FragmentShader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Shader source code
	GLchar *fragmentShaderSourceCode =
		"#version 420 core" \
		"\n" \
		"in float Color;" \
		"out vec4 FragColor;" \
		"uniform sampler2D tex_star0;" \
		"uniform sampler2D tex_star1;" \
		"uniform sampler2D tex_star2;" \
		"uniform sampler2D tex_star3;" \
		"uniform sampler2D tex_star4;" \
		"uniform sampler2D tex_star5;" \
		"uniform sampler2D tex_star6;" \
		"uniform sampler2D tex_star7;" \
		"in vec3 tNormVertexShader;" \
		"in vec4 eye_coordinatesVertexShader;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ks;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ka;" \
		"uniform vec4 u_Light_Position;" \
		"uniform float u_MaterialShininess;" \
		"void main(void)" \
		"{" \
		"vec3 tNorm=normalize(tNormVertexShader);"
		"vec3 lightDirection=normalize(vec3(u_Light_Position-eye_coordinatesVertexShader));" \
		"float tndotld=max(dot(lightDirection,tNorm),0.0);" \
		"vec3 ReflectionVector=reflect(-lightDirection,tNorm);" \
		"vec3 viewerVector=normalize(vec3(-eye_coordinatesVertexShader.xyz));" \
		"vec3 ambient=u_La*u_Ka;" \
		"vec3 diffused=u_Ld*u_Kd*tndotld;" \
		"vec3 specular=u_Ls*u_Ks*pow(max(dot(ReflectionVector,viewerVector),0.0),u_MaterialShininess);" \
		"vec3 phong_ads_light = ambient+diffused+specular;" \

		"if(Color >= 0.0 && Color<=1.0)" \
		"	FragColor= texture(tex_star0, gl_PointCoord);" \
		"else if(Color > 1.0 && Color<=2.0)" \
		"	FragColor= texture(tex_star1, gl_PointCoord);" \
		"else if(Color > 2.0 && Color<=3.0)" \
		"	FragColor= texture(tex_star2, gl_PointCoord);" \
		"else if(Color > 3.0 && Color<=4.0)" \
		"	FragColor= texture(tex_star3, gl_PointCoord);" \
		"else if(Color > 4.0 && Color<=5.0)" \
		"	FragColor= texture(tex_star4, gl_PointCoord);" \
		"else if(Color > 5.0 && Color<=6.0)" \
		"	FragColor= texture(tex_star5, gl_PointCoord);" \
		"else if(Color > 6.0 && Color<=7.0)" \
		"	FragColor= texture(tex_star6, gl_PointCoord);" \
		"else if(Color > 7.0 && Color<=8.0)" \
		"	FragColor= texture(tex_star7, gl_PointCoord);" \
		"else" \
		"	FragColor= texture(tex_star7, gl_PointCoord);" \
		"FragColor=FragColor*vec4(phong_ads_light,1.0);"
		"}";

	//Specify the above source code to the shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile the vertex shader source code
	glCompileShader(gFragmentShaderObject);

	//Error Checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, szInfoLog);
				free(szInfoLog);
				unInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create a shader program Object
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach Fragment Shader to Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding Vertex Attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_VELOCITY, "vVelocity");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_START_TIME, "StartTime");

	//Link the program 
	glLinkProgram(gShaderProgramObject);

	//error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, szInfoLog);
				free(szInfoLog);
				unInitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}
	
	//post linking
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	TimeUniform = glGetUniformLocation(gShaderProgramObject, "Time");
	BackgroundUniform = glGetUniformLocation(gShaderProgramObject, "Background");
	tex_starUniform = glGetUniformLocation(gShaderProgramObject, "tex_star0");
	LDUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	KDUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	LAUniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	KAUniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	LSUniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	KSUniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	MaterialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_MaterialShininess");
	LightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_Light_Position");
	LKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyIsPressed");

	//arrayWidth = 64;
	//arrayHeight = 64;*PARTICLE_HEIGHT
	createPoints();

	//create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//Position
	glGenBuffers(1, &vboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER,
		3 * g_numRainVertices * sizeof(float) ,
		verts,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	////Velocities
	glGenBuffers(1, &vboVelocities);
	glBindBuffer(GL_ARRAY_BUFFER, vboVelocities);
	glBufferData(GL_ARRAY_BUFFER,
		3 * g_numRainVertices * sizeof(GLfloat),
		velocities,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VELOCITY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboStartTime);
	glBindBuffer(GL_ARRAY_BUFFER, vboStartTime);
	glBufferData(GL_ARRAY_BUFFER,
		g_numRainVertices * sizeof(GLfloat),
		startTimes,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_START_TIME);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Color
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		g_numRainVertices * sizeof(GLfloat),
		rainDropTexture,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	char *ch = "rain0.png";
	glActiveTexture(GL_TEXTURE0);
	LoadTexture(&textureID0, ch);

	char *ch1 = "rain1.png";
	glActiveTexture(GL_TEXTURE1);
	LoadTexture(&textureID1, ch1);

	char *ch2 = "rain2.png";
	glActiveTexture(GL_TEXTURE2);
	LoadTexture(&textureID2, ch2);
	
	char *ch3 = "rain3.png";
	glActiveTexture(GL_TEXTURE3);
	LoadTexture(&textureID3, ch3);
	
	char *ch4 = "rain4.png";
	glActiveTexture(GL_TEXTURE4);
	LoadTexture(&textureID4, ch4);

	char *ch5 = "rain5.png";
	glActiveTexture(GL_TEXTURE5);
	LoadTexture(&textureID5, ch5);

	char *ch6 = "rain6.png";
	glActiveTexture(GL_TEXTURE6);
	LoadTexture(&textureID6, ch6);

	char *ch7 = "rain7.png";
	glActiveTexture(GL_TEXTURE7);
	LoadTexture(&textureID7, ch7);

	//glPointSize(2.0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = mat4::identity(); 
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void unInitialize(void)
{
	glDeleteTextures(1, &textureID0);
	glDeleteTextures(1, &textureID1);
	glDeleteTextures(1, &textureID2);
	glDeleteTextures(1, &textureID3);
	glDeleteTextures(1, &textureID4);
	glDeleteTextures(1, &textureID5);
	glDeleteTextures(1, &textureID6);
	glDeleteTextures(1, &textureID7);

	if (verts != NULL)
	{
		free(verts);
		verts = NULL;
	}
	if (verts != NULL)
	{
		free(colors);
		colors = NULL;
	}
	if (velocities != NULL)
	{
		free(velocities);
		velocities = NULL;
	}
	if (startTimes != NULL)
	{
		free(startTimes);
		startTimes=NULL;
	}
	if (rainDropTexture != NULL)
	{
		free(rainDropTexture);
		rainDropTexture = NULL;
	}
	if (vboStartTime)
	{
		glDeleteBuffers(1, &vboStartTime);
		vboStartTime = 0;
	}
	if (vboVelocities)
	{
		glDeleteBuffers(1, &vboVelocities);
		vboVelocities = 0;
	}
	if (vboColor)
	{
		glDeleteBuffers(1, &vboColor);
		vboColor = 0;
	}
	if (vboPosition)
	{
		glDeleteBuffers(1, &vboPosition);
		vboPosition = 0;
	}
	if (vao)
	{
		glDeleteBuffers(1, &vao);
		vao = 0;
	}

	GLsizei shaderCount;
	GLsizei shaderNumber;
	if (gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint)* shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (bFullScreen == true)
	{

		SetWindowLong(ghwnd,
			GWL_STYLE,
			dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,
			&wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
	}
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}

}

void resize(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f, (float)width / (float)height, 0.1f, 100.0f);
}

float particleTime = 0.0f;
void display(void)
{
	glEnable(GL_POINT_SPRITE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	//Declaration of matrices

	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	translationMatrix = translate(0.0f, 0.0f, -3.0f);
	modelMatrix = modelMatrix*translationMatrix;
	//rotationMatrix = rotate(rotateSphere, rotateSphere, rotateSphere);

	//modelMatrix = modelMatrix*rotationMatrix;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID0);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star0"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureID1);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star1"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textureID2);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star2"), 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, textureID3);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star3"), 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, textureID4);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star4"), 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, textureID5);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star5"), 5);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, textureID6);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star6"), 6);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, textureID7);
	glUniform1i(glGetUniformLocation(gShaderProgramObject, "tex_star7"), 7);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	//send necessary matrices to shaders

	glUniformMatrix4fv(mUniform,
		1,
		GL_FALSE,
		modelMatrix);
	glUniformMatrix4fv(vUniform,
		1,
		GL_FALSE,
		viewMatrix);
	glUniformMatrix4fv(pUniform,
		1,
		GL_FALSE,
		perspectiveProjectionMatrix);

	glUniform4fv(LightPositionUniform, 1, (GLfloat*)lightPosition);
	glUniform3fv(LAUniform, 1, (GLfloat*)lightAmbient);
	glUniform3fv(KAUniform, 1, (GLfloat*)materialAmbient);
	glUniform3fv(LDUniform, 1, (GLfloat*)lightDiffused);
	glUniform3fv(KDUniform, 1, (GLfloat*)materialDiffused);
	glUniform3fv(LSUniform, 1, (GLfloat*)lightSpecular);
	glUniform3fv(KSUniform, 1, (GLfloat*)materialSpecular);
	glUniform1f(MaterialShininessUniform, materialShininess);

	if (particleTime > 5.0f)
		particleTime = 0.0f;
	particleTime = particleTime + 0.001f;
	glUniform1f(TimeUniform,
		particleTime);
	glUniform4f(BackgroundUniform,
		0.0f,1.0f,0.0f,0.0f);
	//Bind with textures
	glEnable(GL_PROGRAM_POINT_SIZE);
	//Bind with vao
	glBindVertexArray(vao);

	//Bind with textures

	//Draw necessary scene
	glDrawArrays(GL_POINTS, 0, g_numRainVertices);

	//Unbind vao
	glBindVertexArray(0);
	glUseProgram(0);
	SwapBuffers(ghdc);
}

void update(void)
{
}
