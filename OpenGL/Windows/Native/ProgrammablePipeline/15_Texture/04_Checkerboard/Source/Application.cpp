#include<windows.h>
#include<stdio.h>

//For GLSL extentions. Must be  included before gl.h and glu.h
#include<GL\glew.h>

#include<gl\gl.h>
#include "vmath.h"
#include "Application.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

using namespace vmath;

enum {
	SMP_ATTRIBUTE_POSITION=0,
	SMP_ATTRIBUTE_COLOR,
	SMP_ATTRIBUTE_NORMAL,
	SMP_ATTRIBUTE_TEXTURE,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
};


//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint mvpMatrixUniform;
GLuint vao_cube;
GLuint VBO_CUBE_POSITION;
GLuint VBO_CUBE_TEXTURE;

mat4 perspectiveProjectionMatrix;  // typedef of float[16]

//texture
GLuint textureName = 0;
GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

GLuint textureSamplerUniform;

enum SHADER_ACTION { COMPILE = 0, LINK };
enum SHADER_NAME { VERTEX = 0, FRAGMENT, SHADER_PROGRAM };

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:PP:Checkerboard");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP:PP Checkerboard"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;		
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void checkCompileErrorIfAny(GLuint, SHADER_NAME, SHADER_ACTION);
	void loadGLTexture();


	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "Loading glew...\n");
	//Load openGL Extensions using glew before using any OpenGL function.
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK) {
		fprintf(gpFile, "glewInit() is failed.\n");
		DestroyWindow(ghwnd);	
	}

	fprintf(gpFile, "Loaded glew.\n");


	//OpenGL related log
	fprintf(gpFile, "OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for(int i = 0; i < numExtensions; i++) {		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	fprintf(gpFile, "\n-------------------------------------------------- \n");

	// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	const GLchar *vertexShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 vPosition;" \
				"in vec2 vTextCord;" \
				"uniform mat4 uMvpMatrix;" \
				"out vec2 out_textCord;" \
				"void main(void)" \
				"{" \
				"gl_Position = uMvpMatrix * vPosition;" \
				"out_textCord = vTextCord;" \
				"}";

	fprintf(gpFile, "Providing shader source...\n");
	glShaderSource(gVertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling Vertex shader...\n");
	
	//3. compile shader
	glCompileShader(gVertexShaderObject);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject, VERTEX, COMPILE);

	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//2. Provide source code to shader.
	const GLchar *fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec2 out_textCord;" \
				"uniform sampler2D u_textureSampler;" \
				"out vec4 fragColor;" \
				"void main(void)" \
				"{" \
				"fragColor = texture(u_textureSampler, out_textCord);" \
				"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling fragment shader...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject);
	checkCompileErrorIfAny(gFragmentShaderObject, FRAGMENT, COMPILE);

	
	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	fprintf(gpFile, "Linking shaders with shader programme ...\n");

	//Pre-link binding of shader program object with vertex shader position attribute for 
	//taking input 
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_TEXTURE, "vTextCord");

	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject, SHADER_PROGRAM, LINK);
	fflush(gpFile);

	//Get MVP uniform location from driver to be used in display to push the final metrices
	mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject, "uMvpMatrix");
	textureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_textureSampler");

	

	const GLfloat cubeTextCoords[] = {
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f 
		};


	//1. VAO for cube
	glGenVertexArrays(1, &vao_cube); //Started new caset
	glBindVertexArray(vao_cube); // Record button start 

	glGenBuffers(1, &VBO_CUBE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_CUBE_POSITION);
	glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VBO_CUBE_TEXTURE);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_CUBE_TEXTURE);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTextCoords), cubeTextCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_TEXTURE);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //Record stop button.

	//In display, you can now use above caset to play similar steps.
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	//Loading textures for 2 objects ( pyramid, cube )
	loadGLTexture();

	//Enable the textures
	glEnable(GL_TEXTURE_2D);

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Set the ortho project matrix as identity matrix.
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void checkCompileErrorIfAny(GLuint shader, SHADER_NAME shaderName, SHADER_ACTION action) {

	GLint iShaderStatus;
	GLint iInfoLogLength;
	char *szInfoLog = NULL;

	if(action == COMPILE)
		glGetShaderiv(shader, GL_COMPILE_STATUS, &iShaderStatus);
	else if(action == LINK)
		glGetProgramiv(shader, GL_LINK_STATUS, &iShaderStatus);

	if(iShaderStatus == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL) {
				GLsizei actualWritten;
				glGetShaderInfoLog(shader, iInfoLogLength, &actualWritten, szInfoLog);

				if(shaderName == VERTEX) {
					fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == FRAGMENT) {
					fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == SHADER_PROGRAM) {
					fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				}
			
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}

void loadGLTexture()
{
	void makeCheckImage(void);

	makeCheckImage();

	glGenTextures(1, &textureName);
	glBindTexture(GL_TEXTURE_2D, textureName);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkImage);
	glGenerateMipmap(GL_TEXTURE_2D);

	//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

}

void makeCheckImage()
{
	int i, j , c;

	for(i = 0; i < CHECK_IMAGE_HEIGHT; i++){
		for(j = 0; j < CHECK_IMAGE_WIDTH; j++) {
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			checkImage[i][j][0] = (GLubyte) c ;
			checkImage[i][j][1] = (GLubyte) c;
			checkImage[i][j][2] = (GLubyte) c;
			checkImage[i][j][3] = (GLubyte) 255;
		}
	}
}


void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle );

}

void Display()
{

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Start using openGL program object.
	glUseProgram(gShaderProgramObject); //Starter of renderer in programmable pipeline

	
	//Draw cube
	mat4 modelViewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	mat4 modelViewProjectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	mat4 translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);

	modelViewMatrix = translationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // Order is important
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
	//Pass texture sampler
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureName);
	glUniform1i(textureSamplerUniform, 0); 	

	glBindVertexArray(vao_cube);

		const GLfloat frontCubeVertices[] = {
			-2.0f, -1.0f, 0.0f, 
			-2.0f, 1.0f, 0.0f, 
			0.0f, 1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
		};


		glBindBuffer(GL_ARRAY_BUFFER, VBO_CUBE_POSITION);
		glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), frontCubeVertices, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

		const GLfloat angularCubeVertices[] = {
			1.0f, -1.0f, 0.0f, 
			1.0f, 1.0f, 0.0f, 
			2.41421f,1.0f,-1.41421f,
			2.41421f,-1.0f,-1.41421f,
		};

		glBindBuffer(GL_ARRAY_BUFFER, VBO_CUBE_POSITION);
		glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(GLfloat), angularCubeVertices, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


	glBindVertexArray(0);


	//Stop using Open program object.
	glUseProgram(0);
	
	SwapBuffers(ghdc);
}


void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	
	if (vao_cube) {
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	
	//Destroy vbo
	if (VBO_CUBE_POSITION) {
		glDeleteBuffers(1, &VBO_CUBE_POSITION);
		VBO_CUBE_POSITION = 0;
	}

	if (VBO_CUBE_TEXTURE) {
		glDeleteBuffers(1, &VBO_CUBE_TEXTURE);
		VBO_CUBE_TEXTURE = 0;
	}

	//Safe release of shaders and shader program.
	if(gShaderProgramObject) {

		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		fprintf(gpFile, "Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			fprintf(gpFile, "Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		//unlink the shader program.
		glUseProgram(0);
	}

	
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	glDeleteTextures(1, &textureName);

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}


/* 

*** Compile rc file - MUST STEP

$ cl /c /EHsc /I C:\glew-2.1.0\include Application.cpp
$ rc Application.res
$ link Application.obj Application.res user32.lib gdi32.lib /LIBPATH:C:\glew-2.1.0\lib\Release\Win32

*/