#include<windows.h>
#include<stdio.h>

//For GLSL extentions. Must be  included before gl.h and glu.h
#include<GL\glew.h>

#include<gl\gl.h>
#include "vmath.h"
#include "Application.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

using namespace vmath;



//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbLight = false;

enum {
	SMP_ATTRIBUTE_POSITION,
	//SMP_ATTRIBUTE_COLOR,
	SMP_ATTRIBUTE_NORMAL,
	SMP_ATTRIBUTE_TEXTURE,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
};


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_pyramid;
GLuint VBO_PYRAMID_POSITION;
GLuint VBO_PYRAMID_NORMAL;

//Uniform declarations
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint perspectiveProjectionUniform;

GLuint laUniform[2]; //light Ambient uniform
GLuint ldUniform[2]; //light Diffuse uniform
GLuint lsUniform[2]; //light Specular uniform
GLuint lightPositionUniform[2]; //material Diffuse

GLuint kaUniform; //material Ambient
GLuint kdUniform; //material Diffuse
GLuint ksUniform; //material Specular
GLuint materialShininessUniform; 

GLuint lKeyPressedUniform;

struct Lights {
	vec3 ambientLight;
	vec3 diffuseLight;
	vec3 specularLight;
	vec4 lightDirection;
};

struct Lights lights[2]; // init later in initialize()

GLfloat materialAmbient[] = {0.0f, 1.0f, 0.0f};
GLfloat materialDifuse[] = {1.0f, 0.0f, 0.0f}; // actual object color ( white )
GLfloat materialSpecular[] = {0.0f, 0.0f, 1.0f}; //  reflected bounce back
GLfloat materialShininess = 128.0f; 


mat4 perspectiveProjectionMatrix;  // typedef of float[16]

enum SHADER_ACTION { COMPILE = 0, LINK };
enum SHADER_NAME { VERTEX = 0, FRAGMENT, SHADER_PROGRAM };

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:PP: Two steady lights on moving pyramid");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP:PP Two steady lights on moving pyramid"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case 'l':
			case 'L': 
				if(gbLight == false) {
					glEnable(GL_LIGHTING);
					gbLight = true;
				} else {
					glDisable(GL_LIGHTING);
					gbLight = false;
				}
			break;		
		}
		break;
	//case WM_CHAR:
			
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void checkCompileErrorIfAny(GLuint, SHADER_NAME, SHADER_ACTION);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "Loading glew...\n");
	//Load openGL Extensions using glew before using any OpenGL function.
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK) {
		fprintf(gpFile, "glewInit() is failed.\n");
		DestroyWindow(ghwnd);	
	}

	fprintf(gpFile, "Loaded glew.\n");


	//OpenGL related log
	fprintf(gpFile, "OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for(int i = 0; i < numExtensions; i++) {		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	fprintf(gpFile, "\n-------------------------------------------------- \n");

	// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	const GLchar *vertexShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 vPosition;" \
				"in vec3 vNormal;" \
				"uniform mat4 u_modelMatrix;" \
				"uniform mat4 u_viewMatrix;" \
				"uniform mat4 u_projectionMatrix;" \
				"uniform int u_lKeyPressed;" \
				"uniform vec3 u_la[2];" \
				"uniform vec3 u_ld[2];" \
				"uniform vec3 u_ls[2];" \
				"uniform vec4 u_lightPosition[2];" \
				"uniform vec3 u_ka;" \
				"uniform vec3 u_kd;" \
				"uniform vec3 u_ks;" \
				"uniform float u_materialShininess;" \
				"out vec3 phongAdsLight;" \
				"void main(void)" \
				"{" \
				"vec3 lightDirection[2];" \
				"vec3 reflectionVector[2];" \
				"vec3 ambient[2];" \
				"vec3 diffuse[2];" \
				"vec3 specular[2];" \
				"if(u_lKeyPressed == 1) {" \
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
				"vec3 transformedNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
				"vec3 viewVector = normalize(-eyeCoordinates.xyz);" \
				"for(int i = 0; i < 2;i++) {" \
				"lightDirection[i] = normalize(vec3(u_lightPosition[i] - eyeCoordinates));" \
				"reflectionVector[i] = reflect(-lightDirection[i], transformedNormal);" \
				"ambient[i] = u_la[i] * u_ka;" \
				"diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormal), 0.0);" \
				"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewVector), 0.0), u_materialShininess);" \
				"phongAdsLight = phongAdsLight + ambient[i] + diffuse[i] + specular[i];" \
				"}" \
				"}else {" \
				"phongAdsLight = vec3(1.0, 1.0, 1.0);" \
				"}" \
				"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \
				"}";

	fprintf(gpFile, "Providing shader source...\n");
	glShaderSource(gVertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling Vertex shader...\n");
	
	//3. compile shader
	glCompileShader(gVertexShaderObject);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject, VERTEX, COMPILE);

	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//"fragColor = vec4(phongAdsLight, 1.0);" \
	//2. Provide source code to shader.
	const GLchar *fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec3 phongAdsLight;" \
				"out vec4 fragColor;" \
				"void main(void)" \
				"{" \
				"fragColor = vec4(phongAdsLight, 1.0);" \
				"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling fragment shader...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject);
	checkCompileErrorIfAny(gFragmentShaderObject, FRAGMENT, COMPILE);

	
	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	fprintf(gpFile, "Linking shaders with shader programme ...\n");

	//Pre-link binding of shader program object with vertex shader position attribute for 
	//taking input 
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_NORMAL, "vNormal");

	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject, SHADER_PROGRAM, LINK);
	fflush(gpFile);

	//Get MVP uniform location from driver to be used in display to push the final metrices
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	perspectiveProjectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	laUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_la[0]");
	ldUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_ld[0]");
	lsUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_ls[0]");
	lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[0]");
	laUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_la[1]");
	ldUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_ld[1]");
	lsUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_ls[1]");
	lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[1]");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

	// Define all your vertices, color, shader attributes, vbo, vao initilization
	// in sequential array 
	// const GLfloat pyramidVertices[] = {
	// 	//front
		 
	// 	-1.0f, -1.0f, 1.0f,0.0f, 1.0f, 0.0f,
	// 	1.0f, -1.0f, 1.0f,
	// 	//right
	// 	0.0f, 1.0f, 0.0f,
	// 	1.0f, -1.0f, 1.0f,
	// 	1.0f, -1.0f, -1.0f,
	// 	//back
	// 	0.0f, 1.0f, 0.0f,
	// 	1.0f, -1.0f, -1.0f,
	// 	-1.0f, -1.0f, -1.0f,
	// 	//left
	// 	0.0f, 1.0f, 0.0f,
	// 	-1.0f, -1.0f, -1.0f,
	// 	-1.0f, -1.0f, 1.0f
	// };

	
	const GLfloat pyramidVertices[] = {
		// Front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// Right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// Back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	const GLfloat pyramidNormals[] = {
		// Front face
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		// Right face
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		// Back face
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		// Left face
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f
	};
	
	//Intialize the lights
	lights[0].ambientLight = vec3(0.0f, 0.0f, 0.0f);
	lights[0].diffuseLight = vec3(1.0f, 0.0f, 0.0f); //red light
	lights[0].specularLight = vec3(1.0f, 0.0f, 0.0f);
	lights[0].lightDirection = vec4(2.0f, 0.0f, 0.0f, 1.0f);

	lights[1].ambientLight = vec3(0.0f, 0.0f, 0.0f);
	lights[1].diffuseLight = vec3(0.0f, 0.0f, 1.0f); //blue light
	lights[1].specularLight = vec3(0.0f, 0.0f, 1.0f);
	lights[1].lightDirection = vec4(-2.0f, 0.0f, 0.0f,1.0f);


	//1. VAO for pyramid
	glGenVertexArrays(1, &vao_pyramid); //Started new caset
	glBindVertexArray(vao_pyramid); // Record button start 
	
	glGenBuffers(1, &VBO_PYRAMID_POSITION); 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_PYRAMID_POSITION); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &VBO_PYRAMID_NORMAL); 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_PYRAMID_NORMAL); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); //Record stop button.
	
	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Set the ortho project matrix as identity matrix.
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void checkCompileErrorIfAny(GLuint shader, SHADER_NAME shaderName, SHADER_ACTION action) {

	GLint iShaderStatus;
	GLint iInfoLogLength;
	char *szInfoLog = NULL;

	if(action == COMPILE)
		glGetShaderiv(shader, GL_COMPILE_STATUS, &iShaderStatus);
	else if(action == LINK)
		glGetProgramiv(shader, GL_LINK_STATUS, &iShaderStatus);

	if(iShaderStatus == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL) {
				GLsizei actualWritten;
				glGetShaderInfoLog(shader, iInfoLogLength, &actualWritten, szInfoLog);

				if(shaderName == VERTEX) {
					fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == FRAGMENT) {
					fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == SHADER_PROGRAM) {
					fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				}
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}


void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle );

}

float pyramidAngle = 0.1f;

void Display()
{
	
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Start using openGL program object.
	glUseProgram(gShaderProgramObject); //Starter of renderer in programmable pipeline
	
	if(gbLight == true) {
		glUniform1i(lKeyPressedUniform, 1);

		for(int i = 0; i < 2; i++) {
			glUniform3fv(laUniform[i], 1, lights[i].ambientLight);
			glUniform3fv(ldUniform[i], 1, lights[i].diffuseLight);
			glUniform3fv(lsUniform[i], 1, lights[i].specularLight);
			glUniform4fv(lightPositionUniform[i], 1, lights[i].lightDirection);	
		}
		
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDifuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);

		fprintf(gpFile, "All uniform data is passed.\n");

	}else {
		glUniform1i(lKeyPressedUniform, 0);
		// for(int i = 0; i < 2; i++) {
		// 	glUniform4fv(laUniform[i], 1, lights[i].ambientLight);
		// 	glUniform4fv(ldUniform[i], 1, lights[i].diffuseLight);
		// 	glUniform4fv(lsUniform[i], 1, lights[i].specularLight);
		// 	glUniform4fv(lightPositionUniform[i], 1, lights[i].lightDirection);	
		// }
		
		// glUniform4f(kaUniform, materialAmbient[0],materialAmbient[1],materialAmbient[2],materialAmbient[3]);
		// glUniform4fv(kdUniform, 1, materialDifuse);
		// glUniform4fv(ksUniform, 1, materialSpecular);
		// glUniform1f(materialShininessUniform, materialShininess);
	}

	// Start using attached vertex, fragment shaders and it will ask for your data(vbo, attribPointer etc..) - We have already recorded everything VAO.

	//OpenGL Drawing.

	mat4 modelMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	mat4 viewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	//mat4 projectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	mat4 translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	mat4 rotationMatrix = vmath::rotate(pyramidAngle, 0.0f, 1.0f, 0.0f);

	modelMatrix = translationMatrix * rotationMatrix;

	// Ithe shader madhala 'uMvpMatrix' prepare hoto with modelViewProjectionMatrix.
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);  // 4 - 4*4 matrix ( mat4 - its array) ( Premultiplied matrix.)
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(perspectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glBindVertexArray(vao_pyramid); 
		glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	//Stop using Open program object.
	glUseProgram(0);

	pyramidAngle = pyramidAngle + 0.4f;
	if (pyramidAngle >= 360.0f)
		pyramidAngle = 0.1f;

	SwapBuffers(ghdc);
}


void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	//Destroy VAO
	if(vao_pyramid) {
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}
	
	//Destroy vbo
	if(VBO_PYRAMID_POSITION) {
		glDeleteBuffers(1, &VBO_PYRAMID_POSITION);
		VBO_PYRAMID_POSITION = 0;
	}


	//Safe release of shaders and shader program.
	if(gShaderProgramObject) {

		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		fprintf(gpFile, "Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			fprintf(gpFile, "Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		//unlink the shader program.
		glUseProgram(0);
	}

	
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}


/* 

$ cl /c /EHsc /I C:\glew-2.1.0\include Application.cpp

$ link Application.obj Application.res user32.lib gdi32.lib /LIBPATH:C:\glew-2.1.0\lib\Release\Win32

*/