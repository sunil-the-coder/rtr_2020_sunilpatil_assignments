#include<windows.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include <stdio.h>
#include "GeometryPrimitives.h"

#define _USE_MATH_DEFINES
#include <math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define X 0.525731112119133606
#define Z 0.850650808352039932

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLfloat gLightAmbient[]={0.1f,0.1f,0.1f,1.0f};
GLfloat gLightDiffuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat gLightSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat gLightPositions[]={100.0f,100.0f,100.0f,1.0f};

GLfloat diffuseMaterial[4] = {0.5, 0.5, 0.5, 1.0};
GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};

boolean gbLight =  false;

GLUquadric *quadric = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:DynamicColorMaterial");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Dynamic Color Material"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case 'a':
		case 'A':
			diffuseMaterial [0] += 0.1;
			if (diffuseMaterial [0] > 1.0)
				diffuseMaterial [0] = 0.0;
			glColor4fv(diffuseMaterial);
			break;
		case 's':
		case 'S':
			diffuseMaterial [1] += 0.1;
			if (diffuseMaterial [1] > 1.0)
				diffuseMaterial [1] = 0.0;
			glColor4fv(diffuseMaterial);
			break;
		case 'l':	
		case 'L':		
			if(gbLight==false)
			{
				
				glEnable(GL_LIGHTING);
				gbLight=true;
				
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbLight=false;
				
			}
			break;
		default:
			break;
		}
		break;
	
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	//Depth Function
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// glLightfv(GL_LIGHT1,GL_AMBIENT,gLightAmbient);
	// glLightfv(GL_LIGHT1,GL_DIFFUSE,gLightDiffuse);
	// glLightfv(GL_LIGHT1,GL_POSITION,gLightPositions);
	// glLightfv(GL_LIGHT1,GL_SPECULAR,gLightSpecular);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


	glShadeModel(GL_SMOOTH);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseMaterial);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, 25.0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	
	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}



void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calls internally to glFrustrm(-w,w,-H,H,zNear, zFar)
	//param -> fovy( field of view y - (Dolyanche Disnara Angle) - measured in angle ( 45 degree)), 
	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

/*void Display()
{

	static GLint vertices[] = {25, 25,
								100, 325,
								175, 25,
								175, 325,
								250,25,
								325, 325};

	static GLfloat colors[] = {1.0f, 0.2f, 0.2f,
								0.2f, 0.2f, 1.0f,
								0.8f, 1.0f, 0.2f,
								0.75f, 0.75f, 0.75f,
								0.35f, 0.35f, 0.35f,
								0.5f, 0.5f, 0.5f};

	static GLfloat interwined[] = {1.0f, 0.2f, 1.0f, 100.0f, 100.0f, 0.0f,
								1.0f, 0.2f, 0.2f, 0.0f, 200.0f/1000.0f, 0.0f,
								1.0f, 1.0f, 0.2f, 100.0f/1000.0f, 300.0f/1000.0f, 0.0f,
								0.2f, 1.0f, 0.2f, 200.0f/1000.0f, 300.0f/1000.0f, 0.0f,
								0.2f, 1.0f, 1.0f, 300.0f/1000.0f, 200.0f/1000.0f, 0.0f,
								0.2f, 0.2f, 1.0f, 200.0f/1000.0f, 100.0f/1000.0f, 0.0f
								};

	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //modelview duality = when model can't, view can OR  when view can't, model can.
	glLoadIdentity();

	//pushing modelling matrix by -3 back in z-axis so that its viewable to you. If given +3 it won't dispaly anything.
	glTranslatef(0.0f, 0.0f, -3.0f); // if commented then screen gets complete black. // move model matrix by -3 in zOrder.
	glScalef(0.5f, 0.5f, 0.5f);

#if 0
	//step-1 - enable
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
#endif
	//step-2 data ( method-1 )
	//glColorPointer(3, GL_FLOAT, 0, colors);
	//glVertexPointer(2, GL_INT, 0, vertices);

#if 0 // step-2 ( method - 2)
	glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), &interwined[0]);
	glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), &interwined[3]);
#endif

	glInterleavedArrays(GL_C3F_V3F, 0, interwined);

	//step - draw
	glBegin(GL_TRIANGLES);
		glArrayElement(2);
		glArrayElement(3);
		glArrayElement(5);
	glEnd();


	SwapBuffers(ghdc);
}

*/

// void Display()
// {

// 	static GLint vertices[] = {25, 25,
// 								100, 325,
// 								175, 25,
// 								175, 325,
// 								250,25,
// 								325, 325};

// 	static GLfloat colors[] = {1.0f, 0.2f, 0.2f,
// 								0.2f, 0.2f, 1.0f,
// 								0.8f, 1.0f, 0.2f,
// 								0.75f, 0.75f, 0.75f,
// 								0.35f, 0.35f, 0.35f,
// 								0.5f, 0.5f, 0.5f};

// 	static GLfloat interwined[] = {1.0f, 0.2f, 1.0f, 100.0f, 100.0f, 0.0f,
// 								1.0f, 0.2f, 0.2f, 0.0f, 200.0f/1000.0f, 0.0f,
// 								1.0f, 1.0f, 0.2f, 100.0f/1000.0f, 300.0f/1000.0f, 0.0f,
// 								0.2f, 1.0f, 0.2f, 200.0f/1000.0f, 300.0f/1000.0f, 0.0f,
// 								0.2f, 1.0f, 1.0f, 300.0f/1000.0f, 200.0f/1000.0f, 0.0f,
// 								0.2f, 0.2f, 1.0f, 200.0f/1000.0f, 100.0f/1000.0f, 0.0f
// 								};


// 	//place all vertices for all faces of quads
// 	static GLubyte allIndices[] = {};


// 	static GLubyte frontIndices[] = {4,5,6,7};
// 	static GLubyte rightIndices[] = {1,2,6,5};
// 	static GLubyte bottomIndices[] = {0,1,5,4};
// 	static GLubyte backIndices[] = {0,3,2,1};
// 	static GLubyte leftIndices[] = {0,4,7,3};
// 	static GLubyte topIndices[] = {2,3,7,6};


// 	//code
// 	glClear(GL_COLOR_BUFFER_BIT);
// 	glMatrixMode(GL_MODELVIEW); 
// 	glLoadIdentity();

// 	//pushing modelling matrix by -3 back in z-axis so that its viewable to you. If given +3 it won't dispaly anything.
// 	glTranslatef(0.0f, 0.0f, -3.0f); // if commented then screen gets complete black. // move model matrix by -3 in zOrder.
// 	glScalef(0.5f, 0.5f, 0.5f);

// 	//Interwined
// 	glInterleavedArrays(GL_C3F_V3F, 0, interwined);

// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, frontIndices);
// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, rightIndices);
// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, bottomIndices);
// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, backIndices);
// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, leftIndices);
// 	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_BYTE, topIndices);

// 	glBegin(GL_TRIANGLES);
// 		glArrayElement(2);
// 		glArrayElement(3);
// 		glArrayElement(5);
// 	glEnd();

// 	SwapBuffers(ghdc);
// }


// void Display()
// {
// 	void normcrossprod(float v1[3], float v2[3], float out[3]);

// 	static GLfloat angle = 0.0f;
// 	static GLfloat color = 0.0f;

// 	//2:3 Building Icosahedron
// 	static GLfloat vdata[12][3] = {
// 		{-X, 0.0, Z}, {X, 0.0, Z}, {-X, 0.0, -Z}, {X, 0.0, -Z},
// {0.0, Z, X}, {0.0, Z, -X}, {0.0, -Z, X}, {0.0, -Z, -X},
// {Z, X, 0.0}, {-Z, X, 0.0}, {Z, -X, 0.0}, {-Z, -X, 0.0}
// 	};

// 	static GLuint topIndices[20][3] = {
// 		{1,4,0}, {4,9,0}, {4,5,9}, {8,5,4}, {1,8,4},
// {1,10,8}, {10,3,8}, {8,3,5}, {3,2,5}, {3,7,2},
// {3,10,7}, {10,6,7}, {6,11,7}, {6,0,11}, {6,1,0},
// {10,1,6}, {11,0,9}, {2,11,9}, {5,2,9}, {11,2,7}
// 	}; 

// 	GLfloat dl[3], d2[3], norm [3];
	
// 	//code
// 	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
// 	glMatrixMode(GL_MODELVIEW); 
// 	glLoadIdentity();

// 	//pushing modelling matrix by -3 back in z-axis so that its viewable to you. If given +3 it won't dispaly anything.
// 	glTranslatef(0.0f, 0.0f, -5.0f); // if commented then screen gets complete black. // move model matrix by -3 in zOrder.
// 	//glRotatef(angle, 1.0f, 0.0f, 0.0f);
// 	glRotatef(angle, 0.0f, 1.0f, 0.0f);
// 	//glRotatef(angle, 0.0f, 0.0f, 1.0f);
	
// 	int i, j;

// 	glBegin(GL_TRIANGLES);
// 		for(i = 0; i < 20; i++){

// 		// for(i = 0; i < 20; i++){
// 		// 	//glColor3f(color, color, color);
			
// 		// 	for (j = 0; j < 3; j++) { 
// 		// 		dl[j] = vdata[topIndices[i] [0]] [j] - vdata[topIndices[i][1]][j];
// 		// 		d2[j] = vdata[topIndices[i] [1]] [j] - vdata[topIndices[i][2]][j];
// 		// 	}

// 		// 	normcrossprod (dl , d2 , norm);
// 		// 	glNormal3fv (norm);

// 		// 	// if(color >= 1.0f)
// 		// 	// 	color = 0.0f;
// 		// 	// else
// 		// 	// 	color += 0.05f;
// 		// 	glVertex3fv(&vdata[ topIndices[i][0] ] [0]);
// 		// 	glVertex3fv(&vdata[ topIndices[i][1] ] [0]);
// 		// 	glVertex3fv(&vdata[ topIndices[i][2] ] [0]);
// 		// }

// 	glEnd();

// 	// if(color >= 1.0f)
// 	// 		color = 0.0f;
// 	// else
// 	// 		color += 0.05f;

// 	if(angle >= 360.0f)
// 		angle = 0.0f;
// 	else
// 		angle += 0.1f;	


// 	SwapBuffers(ghdc);
// }

void Display()
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	//pushing modelling matrix by -3 back in z-axis so that its viewable to you. If given +3 it won't dispaly anything.
	glTranslatef(0.0f, 0.0f, -3.0f); // if commented then screen gets complete black. // move model matrix by -3 in zOrder.
	//glRotatef(angle, 1.0f, 0.0f, 0.0f);
//	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	//glRotatef(angle, 0.0f, 0.0f, 1.0f);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30); // internally it has glBegin() and glEnd()

	SwapBuffers(ghdc);
}

void normalize(float v[3]) {

	GLfloat d = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	if(d == 0.0) {
		fprintf(gpFile, "Zero length vector.\n");
		return;
	}

	v[0] /= d;
	v[1] /= d;
	v[2] /= d;

}

//formula to find cross product - 
//https://www.mathsisfun.com/algebra/vectors-cross-product.html
//https://calcworkshop.com/vectors-and-the-geometry-of-space/cross-product-in-3d/
void normcrossprod(float v1[3], float v2[3], float out[3])
{
	void normalize(float v[3]);

	out[0] = v1[1] * v2[2] - v1[2] * v2[1];
	out[1] = v1[2] * v2[0] - v1[0] * v2[2];
	out[2] = v1[0] * v2[1] - v1[1] * v2[0];

	normalize(out);
}


void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}
