#include <windows.h>
#include "MyMath.h"

#pragma comment(lib,"MyMath.lib")

//Callback function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:WindowApp");


	//code
	//initialization of wndClass.
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //actual window icon
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName; 
	wndclass.lpszMenuName = NULL; //no custom menu
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION); //taskbar icon

	//register wndclass with os.
	RegisterClassEx(&wndclass);

	//Create in memory window
	hwnd = CreateWindow(szAppName,
		TEXT("SMP: MyWindow 2"),
		WS_OVERLAPPEDWINDOW, //window style
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL, //Deafult desktop as parent
		NULL, // No custom menu
		hInstance,
		NULL); 

	//Enable window to visible on desktop.
	ShowWindow(hwnd, iCmdShow); //SW_SHOWMAXIMIZED, 
	UpdateWindow(hwnd); // draw the window with my given brush

	//Starts the Heart - Message Loop.

	while (GetMessage(&msg, NULL, 0, 0)) //NULL -> All window message including child windows - All types of messages
	{
		TranslateMessage(&msg); //Perform low level processing ( h/w to s/w messages etc.)
		DispatchMessage(&msg);  // Post message to WndProc by extracting details from msg struct.
	}

	return ((int)msg.wParam); // Mostly use the values passed from PostQuitMessage(0)

}

//Define callback WndProc

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	int i = 5;
	int j;
	wchar_t str[255];

	//Handle events based on your customization logic.
	switch (iMsg)
	{
	case WM_CREATE:
		j = MakeSquare(i);
		wsprintf(str, TEXT("Square of %d is %d"), i, j);
		MessageBox(hwnd, str, TEXT("Square Result:"), MB_OK);
		break;
	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("WM_LBUTTONBOWN received with values."), TEXT("Message"), MB_OK);
		break;
	case WM_RBUTTONDOWN:
		MessageBox(hwnd, TEXT("Right Button click recieved."), TEXT("Message"), MB_OK);
		break;
	case WM_KEYDOWN:
		MessageBox(hwnd, TEXT("Received keydown event."), TEXT("Message"), MB_OK);
		break;
	case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam)); //Handle events by OS

}