#include <windows.h>

//Callback function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:DLLExplicit");


	//code
	//initialization of wndClass.
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //actual window icon
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName; 
	wndclass.lpszMenuName = NULL; //no custom menu
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION); //taskbar icon

	//register wndclass with os.
	RegisterClassEx(&wndclass);

	//Create in memory window
	hwnd = CreateWindow(szAppName,
		TEXT("SMP: DLL Explicit"),
		WS_OVERLAPPEDWINDOW, //window style
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL, //Deafult desktop as parent
		NULL, // No custom menu
		hInstance,
		NULL); 

	//Enable window to visible on desktop.
	ShowWindow(hwnd, iCmdShow); //SW_SHOWMAXIMIZED, 
	UpdateWindow(hwnd); // draw the window with my given brush

	//Starts the Heart - Message Loop.

	while (GetMessage(&msg, NULL, 0, 0)) //NULL -> All window message including child windows - All types of messages
	{
		TranslateMessage(&msg); //Perform low level processing ( h/w to s/w messages etc.)
		DispatchMessage(&msg);  // Post message to WndProc by extracting details from msg struct.
	}

	return ((int)msg.wParam); // Mostly use the values passed from PostQuitMessage(0)

}

//Define callback WndProc

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HMODULE hDll = NULL;
	typedef int (*MakeSquareFunctionPtr)(int);
	MakeSquareFunctionPtr makeSquareFunctionPtr = NULL;

	int i = 5;
	int j;
	TCHAR str[255];

	switch (iMsg)
	{
	case WM_CREATE:

		hDll = LoadLibrary(TEXT("MyMath.dll")); //Starting virtual address where dll is loaded into the memory will be returned.
		if (hDll == NULL)
		{
			MessageBox(hwnd, TEXT("Dll load failed."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}

		//Return starting address from VAS for given function.
		makeSquareFunctionPtr = (MakeSquareFunctionPtr) GetProcAddress(hDll, "MakeSquare");

		if (makeSquareFunctionPtr == NULL) {
			FreeLibrary(hDll);
			MessageBox(hwnd, TEXT("GetProcAddress Failed."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}

		j = makeSquareFunctionPtr(10);

		wsprintf(str, TEXT("Square of %d is %d"), i, j);

		MessageBox(hwnd, str, TEXT("Message"), MB_OK);

		FreeLibrary(hDll);
		break;
	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam)); //Handle events by OS

}