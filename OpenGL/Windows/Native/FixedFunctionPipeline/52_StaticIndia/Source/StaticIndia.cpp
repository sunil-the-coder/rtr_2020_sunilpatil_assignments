#include<windows.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include <stdio.h>
#include "StaticIndia.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:StaticIndia");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Static India"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}

void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	//ViewPort Tranformation
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calls internally to glFrustrm(-w,w,-H,H,zNear, zFar)
	//param -> fovy( field of view y - (Dolyanche Disnara Angle) - measured in angle ( 45 degree)), 
	//Projection Tranformation
	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

void Display()
{
	void DisplayI(float);
	void DisplayN(void);
	void DisplayD(void);
	void DisplayA(void);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); 
	DisplayI(-1.4f);
	DisplayN();
	DisplayD();
	DisplayI(0.7f);
	DisplayA();

	SwapBuffers(ghdc);
}


void DisplayI(float xTranslation)
{
	glLoadIdentity();

	glTranslatef(xTranslation, 0.0f, -4.5f);

	//Upper -
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);

		glVertex3f(0.2f, 0.5f, 0.0f);		
		glVertex3f(-0.2f, 0.5f, 0.0f);
		glVertex3f(-0.2f, 0.45f, 0.0f);
		glVertex3f(0.2f, 0.45f, 0.0f);

	glEnd();

	//Lower -
	glBegin(GL_QUADS);

		glColor3f(0.3f, 0.5f, 0.2f);

		glVertex3f(0.2f, -0.5f, 0.0f);		
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glVertex3f(-0.2f, -0.45f, 0.0f);
		glVertex3f(0.2f, -0.45f, 0.0f);

	glEnd();

	//Middle |
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.03f, 0.45f, 0.0f);		
		glVertex3f(-0.03f, 0.45f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(-0.03f, -0.45f, 0.0f);
		glVertex3f(0.03f, -0.45f, 0.0f);

	glEnd();

}

void DisplayN(void)
{
	glLoadIdentity();

	glTranslatef(-0.7f, 0.0f, -4.5f);

	//Start |
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);		
		glVertex3f(-0.35f, 0.5f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(-0.35f, -0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	//End |
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.5f, 0.0f);		
		glVertex3f(0.25f, 0.5f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(0.25f, -0.5f, 0.0f);
		glVertex3f(0.3f, -0.5f, 0.0f);

	glEnd();

	//Middle '\'
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);		
		glVertex3f(-0.35f, 0.5f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(0.25f, -0.5f, 0.0f);
		glVertex3f(0.3f, -0.5f, 0.0f);

	glEnd();


}

void DisplayD(void)
{
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -4.5f);

	//Upper -
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);

		glVertex3f(0.3f, 0.5f, 0.0f);		
		glVertex3f(-0.25f, 0.5f, 0.0f);
		glVertex3f(-0.25f, 0.45f, 0.0f);
		glVertex3f(0.3f, 0.45f, 0.0f);

	glEnd();

	//Lower -
	glBegin(GL_QUADS);

		glColor3f(0.3f, 0.5f, 0.2f);

		glVertex3f(0.3f, -0.5f, 0.0f);		
		glVertex3f(-0.25f, -0.5f, 0.0f);
		glVertex3f(-0.25f, -0.45f, 0.0f);
		glVertex3f(0.3f, -0.45f, 0.0f);

	glEnd();

	//Start |
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(-0.15f, 0.45f, 0.0f);		
		glVertex3f(-0.2f, 0.45f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(-0.2f, -0.45f, 0.0f);
		glVertex3f(-0.15f, -0.45f, 0.0f);

	glEnd();


	//End |
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.45f, 0.0f);		
		glVertex3f(0.25f, 0.45f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(0.25f, -0.45f, 0.0f);
		glVertex3f(0.3f, -0.45f, 0.0f);

	glEnd();

}

void DisplayA(void)
{
	glLoadIdentity();

	glTranslatef(1.4f, 0.0f, -4.5f);

	//Left /
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.35f, 0.0f);		
		glVertex3f(0.0f, 0.5f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(-0.35f, -0.5f, 0.0f);
		glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	//Right '\'
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);		
		glVertex3f(0.0f, 0.35f, 0.0f);
		
		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(0.3f, -0.5f, 0.0f);
		glVertex3f(0.35f, -0.5f, 0.0f);

	glEnd();

	//Middle -
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.12f, 0.03f, 0.0f);		
		glVertex3f(-0.12f, 0.03f, 0.0f);

		glColor3f(0.3f, 0.5f, 0.2f);			
		glVertex3f(-0.15f, -0.03f, 0.0f);
		glVertex3f(0.15f, -0.03f, 0.0f);

	glEnd();


}
