//header files
#include <windows.h>

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
DWORD dwStyle;
//Why sizeof() here rather than just {} => Better to give complete byte size to be allocated. 
//You don't have to set using memset or 0.
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; //At the time of initialization its best.
bool gbFullscreen = false;
HWND ghwnd;


//winmain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:WindowFullScreen");
	RECT rect;
	UINT x, y;
	

	//Initialization of wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	x = 0;
	y = 0;

	//Register wndclass with OS.
	RegisterClassEx(&wndclass);


	//Calculate the positions of  for centered window.
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	//Create in memory window
	hwnd = CreateWindow(szAppName,
		TEXT("SMP:FullScreenWindow"),
		WS_OVERLAPPEDWINDOW,
		x,
		y,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	//To use in other functions other than WndProc and WinMain.
	ghwnd = hwnd;

	//Display window from prepared in memory window
	ShowWindow(hwnd, iCmdShow);

	//Paint the window
	UpdateWindow(hwnd);

	//Start the Message Loop ( heart )
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg); //Low level processing of msg
		DispatchMessage(&msg); //Pass on message parameters to WndProc.
	}

	return ((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declaration
	void ToggleFullscreen(void);

	//local variable declaration.
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rect;
	TCHAR str[] = TEXT("FullSCreen Capable Window");


	//code
	switch (iMsg) 
	{
	
	case WM_KEYDOWN:
		switch (wParam)
		{
			case 0x46: //'f'
			case 0X66: //'F'
				ToggleFullscreen();
				break;
			default: 
				break;
		}
		break;
	case WM_PAINT:
		GetClientRect(hwnd, &rect);
		hdc = BeginPaint(hwnd, &ps);
			SetBkColor(hdc, RGB(0, 0, 0));
			SetTextColor(hdc, RGB(0,255,0));
			DrawText(hdc, str, -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	//varible declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };

	LPDEVMODE

	//code
	//normal window to fullscreen window
	if (gbFullscreen == false)
	{
		//1. Get the current window style ( WS_OVERLAPPEDWINDOW) -> Take it at runtime.
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//2. check if ws_overlappedwindow style present or not in dwStyle.
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//3. Store current window coordinates and also full monitor coordinates
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITOR_DEFAULTTOPRIMARY), &mi))
			{
				//4. Take out overlappend window from dwStyle.
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//5. Set the new window position using monitor info.
				SetWindowPos(ghwnd,HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top, 
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);

				//Framechanged -> Send the WM_NCCALCSIZE message ( Calculate non client area size calculation )
			}
		}

		ShowCursor(false);
		gbFullscreen = true;
	}
	else  //fullscreen window to normal window
	{
		//1. Set window style back to overlapped window + thick frame + max + min caps.
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		//2. Set the window placement.
		SetWindowPlacement(ghwnd, &wpPrev);

		//3. Set the flags back to normal.
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullscreen = false;
		
	}
}
