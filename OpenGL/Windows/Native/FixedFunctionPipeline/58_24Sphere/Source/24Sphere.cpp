#include<windows.h>
#include<GL/GL.h>
#include<GL/GLU.h>
#include <stdio.h>
#define MYICON 101

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

bool gbLight = false;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // no ambient
GLfloat lightDifuse[] = { 1.0f, 0.0f, 0.0f, 1.0f }; //red light
GLfloat lightPosition[] = { 0.0f, 3.0f, 3.0f, 0.0f }; //will animate later - Positional light


GLfloat lightModelAmbient[] = { 0.2, 0.2, 0.2, 1.0 };
GLfloat lightModelLocalViewer[] = { 0.0f };

GLfloat xRotationAngle = 0.0f;
GLfloat yRotationAngle = 0.0f;
GLfloat zRotationAngle = 0.0f;


GLUquadric* quadric[24];
GLint keyPressed = 0;

GLsizei currentWidth = (GLsizei)WIN_WIDTH;
GLsizei currentHeight = (GLsizei)WIN_HEIGHT;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:24Sphere");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: 24Sphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if (gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case 'l':
		case 'L':
			if (gbLight == false)
			{

				glEnable(GL_LIGHTING);
				gbLight = true;

			}
			else
			{
				glDisable(GL_LIGHTING);
				gbLight = false;

			}
			break;
		case 'x':
		case 'X':
			keyPressed = 1;
			xRotationAngle = 0.0f;
			break;
		case 'y':
		case 'Y':
			keyPressed = 2;
			yRotationAngle = 0.0f;
			break;
		case 'z':
		case 'Z':
			keyPressed = 3;
			zRotationAngle = 0.0f;
			break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}

}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void Update();

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);


	//Depth Function
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	//Initialization of lights
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDifuse);
	glEnable(GL_LIGHT0);

	//Initialize the 24 quadric.
	for (int i = 0; i < 24; i++)
		quadric[i] = gluNewQuadric();

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}



void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	currentWidth = (GLsizei)width;
	currentHeight = (GLsizei)height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calls internally to glFrustrm(-w,w,-H,H,zNear, zFar)
	//param -> fovy( field of view y - (Dolyanche Disnara Angle) - measured in angle ( 45 degree)), 
	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle )

// //Remove barrel distortion using gluOrtho
// if(width <= height) {
// 	glOrtho(-0.0f,
// 			15.5f,
// 			0.0f,
// 			15.5f * ((GLfloat)height / (GLfloat)width),
// 			-10.0f,
// 			10.0f);
// }else {
// 	glOrtho(-0.0f,
// 			15.5f * ((GLfloat)width / (GLfloat)height),
// 			0.0f,
// 			15.5f,
// 			-10.0f,
// 			10.0f);
// }
}

void Display()
{
	void Draw24Sphere(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -10.0f);

	if (keyPressed == 1) {

		glRotatef(xRotationAngle, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = xRotationAngle;

	}
	else if (keyPressed == 2) {

		glRotatef(yRotationAngle, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = yRotationAngle;

	}
	else if (keyPressed == 3) {

		glRotatef(zRotationAngle, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = zRotationAngle;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	//glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);

	Draw24Sphere();

	SwapBuffers(ghdc);
}

void Draw24Sphere()
{

	GLfloat materialAmbient[4];
	GLfloat materialDifuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glViewport(0, currentHeight/4, 200/6, currentHeight/4);
	//1. 1st sphere on 1st column, emerald
	materialAmbient[0] = 0.0215;
	materialAmbient[1] = 0.1745;
	materialAmbient[2] = 0.0215;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.07568;
	materialDifuse[1] = 0.61424;
	materialDifuse[2] = 0.07568;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.633;
	materialSpecular[1] = 0.727811;
	materialSpecular[2] = 0.633;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-10.0f, 6.0f, 0.0f);
	gluSphere(quadric[0], 1.2f, 30, 30);
	glPopMatrix();


	glViewport(currentWidth/5, currentHeight/4, currentWidth/5, currentHeight/3);
	//2. 2nd sphere on 1st column, jade
	materialAmbient[0] = 0.135;
	materialAmbient[1] = 0.2225;
	materialAmbient[2] = 0.1575;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.54;
	materialDifuse[1] = 0.89;
	materialDifuse[2] = 0.63;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.316228;
	materialSpecular[1] = 0.316228;
	materialSpecular[2] = 0.316228;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-6.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//3. 3rd sphere on 1st column, obsidian
	materialAmbient[0] = 0.05375;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.06625;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.18725;
	materialDifuse[1] = 0.17;
	materialDifuse[2] = 0.22525;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.33741;
	materialSpecular[1] = 0.328634;
	materialSpecular[2] = 0.346435;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.3 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(-2.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//4. 4th sphere on 1st column, obsidian
	materialAmbient[0] = 0.25;
	materialAmbient[1] = 0.20725;
	materialAmbient[2] = 0.20725;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 1.0f;
	materialDifuse[1] = 0.829f;
	materialDifuse[2] = 0.829f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.088 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(2.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//5. 5th sphere on 1st column, obsidian
	materialAmbient[0] = 0.1745;
	materialAmbient[1] = 0.01175;
	materialAmbient[2] = 0.01175;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.61242f;
	materialDifuse[1] = 0.04146f;
	materialDifuse[2] = 0.04146f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(6.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//6. 6th sphere on 1st column, obsidian
	materialAmbient[0] = 0.1;
	materialAmbient[1] = 0.18725;
	materialAmbient[2] = 0.1745;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.396f;
	materialDifuse[1] = 0.74151;
	materialDifuse[2] = 0.69102f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.297254;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.6306678f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
	glTranslatef(10.0f, 6.0f, 0.0f);
	gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//7. 7th sphere on 2nd row, turquoise
	 materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.780392f;
	materialDifuse[1] = 0.568627;;
	materialDifuse[2] = 0.113725f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.992147f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.21794872 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//8. 8th sphere on 2nd row, bronze
	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.714f;
	materialDifuse[1] = 0.4284f;;
	materialDifuse[2] = 0.18144f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.2 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//9. 9th sphere on 2nd row, chrome
	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.4f;;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//10. 10th sphere on 2nd row, chrome
	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.7038f;
	materialDifuse[1] = 0.27048f;;
	materialDifuse[2] = 0.0828f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.256777f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//11. 11th sphere on 2nd row, gold
	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.75164f;
	materialDifuse[1] = 0.60648f;;
	materialDifuse[2] = 0.22648f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//12. 12th sphere on 2nd row, silver
	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.50754f;
	materialDifuse[1] = 0.50754f;;
	materialDifuse[2] = 0.50754f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, 2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	
	//13. 13th sphere on 3rd row, black
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.01f;
	materialDifuse[1] = 0.01f;
	materialDifuse[2] = 0.01f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.50f;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//14. 14th sphere on 3rd row, cyan
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.0f;
	materialDifuse[1] = 0.50980392f;
	materialDifuse[2] = 0.50980392f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//15. 15th sphere on 3rd row, green
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.1f;
	materialDifuse[1] = 0.35f;
	materialDifuse[2] = 0.1f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//16. 16th sphere on 3rd row, red
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.0f;
	materialDifuse[2] = 0.0f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();


	//17. 17th sphere on 3rd row, white
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.55f;
	materialDifuse[1] = 0.55f;
	materialDifuse[2] = 0.55f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//18. 18th sphere on 3rd row, yello plastic
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.0f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, -2.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	
	//19. 19th sphere on 4th row, black
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.01f;
	materialDifuse[1] = 0.01f;
	materialDifuse[2] = 0.01f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.4;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-10.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//20. 20th sphere on 4th row, cyan
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.5f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-6.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//21. 21st sphere on 4th row, green
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.4f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(-2.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//22. 22nd sphere on 4th row, red
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.4f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(2.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();


	//23. 23rd sphere on 4th row, white
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.5f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(6.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix();

	//24. 24th sphere on 4th row, rubber
	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	materialDifuse[0] = 0.5f;
	materialDifuse[1] = 0.5f;
	materialDifuse[2] = 0.4f;
	materialDifuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDifuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glPushMatrix();
		glTranslatef(10.0f, -6.0f, 0.0f);
		gluSphere(quadric[1], 1.2f, 30, 30);
	glPopMatrix(); 
}

void Update()
{
	if (keyPressed == 1) {
		xRotationAngle = xRotationAngle + 1.0f;
	}

	if (keyPressed == 2) {
		yRotationAngle = yRotationAngle + 1.0f;
	}

	if (keyPressed == 3) {
		zRotationAngle = zRotationAngle + 1.0f;
	}

}


void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	for (int i = 0; i < 24; i++) {
		if (quadric[i]) {
			gluDeleteQuadric(quadric[i]);
			quadric[i] = NULL;
		}
	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}