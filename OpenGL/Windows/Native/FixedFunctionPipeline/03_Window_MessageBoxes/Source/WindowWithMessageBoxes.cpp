#include <windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:MyWindowWithMB");

	//Initialize the wndclass.
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register our new class with OS.
	RegisterClassEx(&wndclass);

	//Create your in-memory window passing total 11 parameters.

	hwnd = CreateWindow(szAppName,
		TEXT("MyWindow with MessageBoxes"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);

	UpdateWindow(hwnd);


	//Start the heart of an application. Message Loop.
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);

}

//Define callback function.

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	switch (iMsg)
	{
	case WM_CREATE:
		//MessageBox(hwnd, TEXT("Message Received."), TEXT("Message"), MB_OK);
		//MessageBox(hwnd, TEXT("Message Received."), TEXT("Message"), MB_OKCANCEL);
		//MessageBox(hwnd, TEXT("Would you like to proceed further ?"), TEXT("Message"), MB_YESNOCANCEL);
		//MessageBox(hwnd, TEXT("Message not delivered."), TEXT("Message"), MB_RETRYCANCEL);
		//MessageBox(hwnd, TEXT("Job Done."), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
		//MessageBox(hwnd, TEXT("Job Failed."), TEXT("Message"), MB_OK | MB_ICONERROR);
		MessageBox(hwnd, TEXT("System is running slow."), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}


