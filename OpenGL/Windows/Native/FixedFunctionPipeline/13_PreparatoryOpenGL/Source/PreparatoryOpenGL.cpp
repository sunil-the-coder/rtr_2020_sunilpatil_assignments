#include <windows.h>
#include <stdio.h>
#include "prepwindow.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global function declaratations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
FILE* gpFile;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd;
bool gbFullScreen = false;
bool gbActiveWindow = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations
	void Initialize(void);


	//local variable declarations.
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:OpenGLPreparatoryWindow");
	HWND hwnd;
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file creation failed. Can't proceed."), TEXT("Log Message"), MB_OK | MB_ICONERROR);
		exit(0);
	}

	//Initialize the your customized window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	//Register window with OS.
	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	//Create in memory window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: OpenGL Preparatory Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Start the Game Loop - First Game Loop ( _/\_ )
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Here you should call UpdateWindow for OpenGL Rendering

				//Here you should call Display for OpenGL Rendering

				void Display(void);

				Display();
			}
		}
	}

	return ((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize();

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			fprintf(gpFile, "Toggling using %c \n",char(wParam));
			ToggleFullScreen();
			break;
		}
		break;
	
	case WM_DESTROY:
		fprintf(gpFile, "Window is closing \n");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//code
	//Normal mode to fullscreen mode.
	if (gbFullScreen == false)
	{
		//1. Get the current style.
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		//2. Check if it has OVERLAPPEDWINDOWS
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			//3. Store current settings of the window as  well as retrive full monitor settings.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//4. Remove the overlapped window settings like caption bar, min, max etcc.
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//5. set the new positions for window with the help of monitorinfo.
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, 
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		//Fullscreen to normal mode.

		//1. Add WS_VERLAPPED window style back to window.

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		
		//2. Set placement back using wpPrev instance of WindowPlacement.
		SetWindowPlacement(ghwnd, &wpPrev);

		//3. Set the window position ( SWP_NOMOVE = Don't change x & y. SWP_NOSIZE = Don't change width and height )
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, 
			(SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED));

		//4. Enable cursor again back
		ShowCursor(true);

		gbFullScreen = false;
	}
}

void Initialize(void)
{
	//function declarations
	void ReSize(int, int);

	//varible declaration

	//code

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up code
}

void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;
}

void Display(void)
{
	//code
}


void UnInitialize(void)
{
	//code
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}
}
