#define STB_IMAGE_IMPLEMENTATION

#include<windows.h>
#include<GL/glew.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include <stdio.h>
#include "Example.h"
#include "stb_image.h"


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

UINT pressedDigit = 0;


//texture
GLuint cubemap_texture = 0;



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:TexturedSmiley");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Textured Smiley"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		//New changes
		case VK_NUMPAD1:
		case 0x31:
				pressedDigit = 1;
				glEnable(GL_TEXTURE_2D);
				break;
		case VK_NUMPAD2:
		case 0x32:
				pressedDigit = 2;
				glEnable(GL_TEXTURE_2D);
			break;
		case VK_NUMPAD3:
		case 0x33:
				pressedDigit = 3;
				glEnable(GL_TEXTURE_2D);
			break;
		case VK_NUMPAD4:
		case 0x34:
				pressedDigit = 4;
				glEnable(GL_TEXTURE_2D);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		 default:
		 	glDisable(GL_TEXTURE_2D);
		 	break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void UnInitialize(void);

	//texture
	bool loadGLTexture(GLuint *, TCHAR[]);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	//Loading textures for smiley image
	//loadGLTexture(&cubemap_texture, MAKEINTRESOURCE(SMILEY_BITMAP));

	//Enable the textures
	//glEnable(GL_TEXTURE_2D);

	const char* images[]= {"right.png", "left.png", "top.png", "bottom.png", "back.png", "front.png"};	

	GLenum targetPoints[6] = {	GL_TEXTURE_CUBE_MAP_POSITIVE_X,
								GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
								GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
								GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
								GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
								GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
							};

	glGenTextures(1, &cubemap_texture); 
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);

	for( int i = 0; i < 6; i++)
	{
		//Equivalent to LoadImage
		int width, height, bitDepth;
		GLenum eFormat;
		unsigned char *texData = stbi_load(images[i], &width, &height, &bitDepth, 0);
		if(texData != NULL) {
			glTexImage2D(targetPoints[i], 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texData);
			stbi_image_free(texData);
			texData = NULL;
		}else {
			fprintf(gpFile, "texture can't be loaded...");
			fflush(gpFile);
			UnInitialize();
		}
			
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}


bool loadGLTexture(GLuint *texture, TCHAR resourceID[])
{
	//variable declarations
	bool bResult = false;

	//os image loading code - native to OS
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//code
	//GetModuleHandle() - return current process's handle if passed with NULL ( hInstance)
	// width * height ( 0, 0  -> It means this img is bitmap and not icon or cursor.)
	//LR_CREATEDIBSECTION -> image is resource ( LR - Load as Resource). 
	//DIB Section -> Device Indepedent Bitmap.
	//3rd param - image type.
	hBitmap = (HBITMAP) LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if(hBitmap)
	{
		bResult = true;
		
		//REutrn information about current image.
		GetObject(hBitmap, sizeof(BITMAP), &bmp);


		//Now with this information load textures- OpenGL Texture Code

		//image data by difference of 4 rows - ( R, G, B, A). - How openGL going to create image in its memory ?
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);


		//Create object of texture - OpenGL speaking to driver.
		glGenTextures(1, texture); // representative

		glBindTexture(GL_TEXTURE_2D, *texture); //bind to specific reprsentation

		//how the data to be bahaved - Setting texture Mapping
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);


		//Push actual data to Graphich Memory with the help of graphcics driver.
		//3 - RGB ( How you want ? internal format)
		//GL_BGR_EXT - Image data format. - Original format.
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		//gluBuild2DMipmaps() = glTexTmage2D() + glGenerateMipmaps() 

		DeleteObject(hBitmap); // OS function
	}


	return bResult;
}




void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calls internally to glFrustrm(-w,w,-H,H,zNear, zFar)
	//param -> fovy( field of view y - (Dolyanche Disnara Angle) - measured in angle ( 45 degree)), 
	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

void Display()
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f); 
	
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	glEnable(GL_TEXTURE_GEN_R);

	//Cube.
	glBegin(GL_QUADS);

		//FRONT 
		//glColor3f(1.0f, 0.0f, 0.0f); // glTexCoord2f();
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);

		//RIGHT
		//glColor3f(0.0f, 1.0f, 0.0f);
		
		glTexCoord2f(1.0f, 0.0f);		
		glVertex3f(1.0f, 1.0f, -1.0f);

		glTexCoord2f(1.0f, 1.0f);		
		glVertex3f(1.0f, 1.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f);		
		glVertex3f(1.0f, -1.0f, 1.0f);

		glTexCoord2f(0.0f, 0.0f);		
		glVertex3f(1.0f, -1.0f, -1.0f);

		//BACK
		//glColor3f(0.0f, 0.0f, 1.0f);
		
		glTexCoord2f(1.0f, 0.0f);				
		glVertex3f(-1.0f, 1.0f, -1.0f);

		glTexCoord2f(1.0f, 1.0f);				
		glVertex3f(1.0f, 1.0f, -1.0f);

		glTexCoord2f(0.0f, 1.0f);				
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(0.0f, 0.0f);				
		glVertex3f(-1.0f, -1.0f, -1.0f);

		//LEFT
		//glColor3f(1.0f, 1.0f, 1.0f);

		glTexCoord2f(0.0f, 0.0f);				
		glVertex3f(-1.0f, 1.0f, 1.0f);

		glTexCoord2f(1.0f, 0.0f);				
		glVertex3f(-1.0f, 1.0f, -1.0f);

		glTexCoord2f(1.0f, 1.0f);				
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(0.0f, 1.0f);				
		glVertex3f(-1.0f, -1.0f, 1.0f);

		//TOP
		//glColor3f(1.0f, 0.0f, 1.0f);

		glTexCoord2f(0.0f, 1.0f);				
		glVertex3f(1.0f, 1.0f, -1.0f);

		glTexCoord2f(0.0f, 0.0f);				
		glVertex3f(-1.0f, 1.0f, -1.0f);

		glTexCoord2f(1.0f, 0.0f);				
		glVertex3f(-1.0f, 1.0f, 1.0f);

		glTexCoord2f(1.0f, 1.0f);				
		glVertex3f(1.0f, 1.0f, 1.0f);

		//BOTTOM
		//glColor3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(1.0f, 1.0f);				
		glVertex3f(1.0f, -1.0f, -1.0f);

		glTexCoord2f(0.0f, 1.0f);				
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glTexCoord2f(0.0f, 0.0f);				
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glTexCoord2f(1.0f, 0.0f);				
		glVertex3f(1.0f, -1.0f, 1.0f);


	glEnd();



	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_GEN_R);
	glDisable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_TEXTURE_2D);

	SwapBuffers(ghdc);
}



void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	glDeleteTextures(1, &cubemap_texture);


	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}

