#include<windows.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include <stdio.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include "DeathlyHallowsAnimation.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:DeathlyHallowsAnimation");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Deathly Hallows Animation"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}

void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	//ViewPort Tranformation
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calls internally to glFrustrm(-w,w,-H,H,zNear, zFar)
	//param -> fovy( field of view y - (Dolyanche Disnara Angle) - measured in angle ( 45 degree)), 
	//Projection Tranformation
	gluPerspective(45.0f, //fovy ( Angle creation ) for top and bottom
				  ((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
				   0.1f, // camera near to eye
				   100.0f); // rear ( viewing angle )

}

void Display()
{
	//function declaration
	void DrawHollowTriangle(void);
	void DrawCircle(void);
	void DrawVerticalLine(void);

	static GLfloat lineYAxis = 2.0f;
	static GLfloat tXAxis = -1.5f;
	static GLfloat tYAxis = -1.5f;
	static GLfloat cXAxis = 1.5f;
	static GLfloat cYAxis = -1.5f;
	static GLfloat rotateAngle = 0.0f;

	int lineAmount = 1000; 
	GLfloat radius = 0.55f; //mutate based on how circle touches triangle 
	GLfloat twicePi = 2 * M_PI; 
	GLfloat angle;


	glClear(GL_COLOR_BUFFER_BIT);

	//1. Set model matrix to center and draw center line
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	glTranslatef(0.0f, lineYAxis, -2.3f);

	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(0.0f, 0.9f,0.0f);
		glVertex3f(0.0f, -0.9f,0.0f);

	glEnd();


	//2. Reset model matrix to center & draw traingle
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	glTranslatef(tXAxis, tYAxis, -2.3f);
	//glRotatef(rotateAngle, 0.0f, 0.0f, 1.0f); - Test with rotation

	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(0.0f, 0.9f, 0.0f);
		glVertex3f(-0.9f, -0.9f, 0.0f);

		glVertex3f(-0.9f, -0.9f, 0.0f);
		glVertex3f(0.9f, -0.9f, 0.0f);

		glVertex3f(0.9f, -0.9f, 0.0f);
		glVertex3f(0.0f, 0.9f, 0.0f);

	glEnd();


	//3. Reset model matrix to center & draw circle
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity();

	glTranslatef(cXAxis, cYAxis, -2.3f);
	
	glColor3f(1.0f, 1.0f, 0.0f);

	glBegin(GL_LINE_LOOP);

	    for(int j = 0; j <= lineAmount; j++) 
	    { 
	    	angle = j *  (twicePi / lineAmount);
	        glVertex3f(0.0f + radius * cos(angle), -0.34f + radius * sin(angle), 0.0f);
	    }

    glEnd();


	if(!(lineYAxis <= 0.000f)) 
		lineYAxis = lineYAxis - 0.0005f;

	if(!((tXAxis >= 0.000f) || (tYAxis >= 0.000f)))
	{
		tXAxis = tXAxis + 0.0005f;
		tYAxis = tYAxis + 0.0005f;
	}

	if(!((cXAxis <= 0.000f) || (cYAxis >= 0.000f)))
	{
		cXAxis = cXAxis - 0.0005f;
		cYAxis = cYAxis + 0.0005f;
	}

	// if(!(rotateAngle >= 360.0f)) {
	// 	rotateAngle = rotateAngle + 0.2f;
	// }

	SwapBuffers(ghdc);
}

void DrawVerticalLine(void)
{
	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(0.0f, 0.9f,0.0f);
		glVertex3f(0.0f, -0.9f,0.0f);

	glEnd();	
}

void DrawHollowTriangle(void)
{
	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(0.0f, 0.9f, 0.0f);
		glVertex3f(-0.9f, -0.9f, 0.0f);

		glVertex3f(-0.9f, -0.9f, 0.0f);
		glVertex3f(0.9f, -0.9f, 0.0f);

		glVertex3f(0.9f, -0.9f, 0.0f);
		glVertex3f(0.0f, 0.9f, 0.0f);


	glEnd();
}



void DrawCircle(void)
{
	//Center of circle - ( 0.0f, -0.34f)
	int lineAmount = 1000; 

	GLfloat radius = 0.55f; //mutate based on how circle touches triangle 
	GLfloat twicePi = 2 * M_PI; 
	GLfloat angle;

	glColor3f(1.0f, 1.0f, 0.0f);

	glBegin(GL_LINE_LOOP);

	    for(int j = 0; j <= lineAmount; j++) 
	    { 
	    	angle = j *  (twicePi / lineAmount);
	        glVertex3f(0.0f + radius * cos(angle), -0.34f + radius * sin(angle), 0.0f);
	    }

    glEnd();
	
}
