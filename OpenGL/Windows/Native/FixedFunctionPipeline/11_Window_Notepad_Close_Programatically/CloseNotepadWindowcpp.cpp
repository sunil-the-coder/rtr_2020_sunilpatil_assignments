#include <windows.h>
#include <stdio.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);

FILE* gpFile = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declaration
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:CloseNotepad");

	//Try to create log file here.
	if (fopen_s(&gpFile, "SMPLog.log", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed."), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


	//Register with windows
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Close Notepad Window"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Start the heart.

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg); //low level processing.
		DispatchMessage(&msg); // call to wndproc with required msg details.
	}

	fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	switch (iMsg)
	{
	case WM_CREATE:		
		//Try to fetch all windows running on desktop.
		EnumWindows(EnumWindowsProc, 0);
		break;

	case WM_DESTROY:
		//fprintf(gpFile, "Jai Hind.\n");
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	char str[256];
	unsigned int number = 0;
	TCHAR szClassName[256];
	
	//int iLength = GetWindowText(hwnd, szClassName, -1);
	int iLength = GetClassName(hwnd, szClassName, 256);

	//convert from wchar_t to char* for writing into file. 
	wcstombs_s(&number, str, iLength+1, szClassName, iLength+1);
	fprintf(gpFile, str);
	fprintf(gpFile, "\n");

	if (wcscmp(szClassName, TEXT("Notepad")) == 0)
	{
		fprintf(gpFile, "#### Sending WM_CLOSE Message to Notepad ####\n");
		PostMessage(hwnd, WM_QUIT, 0, 0);
	}

	return TRUE;
}