#include <windows.h>
#include <stdio.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

UINT screenWidth = 0;
UINT screenHeight = 0;
FILE* gpFile = NULL;
UINT x = 0;
UINT y = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SMP:CenteredWindow");
	RECT rect;
	

	if (fopen_s(&gpFile, "SMPLog.log", "w") != 0) {
		MessageBox(NULL, TEXT("Can't create file."), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}

	//initialization of wndclass object.
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register with OS.
	RegisterClassEx(&wndclass);

	//Calculate the x,y co-ordindate so that window will appear in center.

	//screenWidth = GetSystemMetrics(SM_CXSCREEN);
	//screenHeight = GetSystemMetrics(SM_CYSCREEN);

	SystemParametersInfo(SPI_GETWORKAREA,0, &rect, 0 );

	screenWidth = rect.right;
	screenHeight = rect.bottom;

	//Find the middle of both width & height.
	x = screenWidth / 2;
	y = screenHeight / 2;

	x = x - 400;
	y = y - 300;

	//Consider window width & height is 800*600

	hwnd = CreateWindow(szAppName,
		TEXT("SMP: Centered Window"),
		WS_OVERLAPPEDWINDOW,
		x,
		y,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Start message loop.

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Width:%d and Height:%d\n", screenWidth, screenHeight);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "x:%d and y:%d\n", x, y);
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}
