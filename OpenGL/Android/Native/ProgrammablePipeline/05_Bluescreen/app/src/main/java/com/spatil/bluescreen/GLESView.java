package com.spatil.bluescreen;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // For OpenGL ES 3.2
import javax.microedition.khronos.opengles.GL10; // for OpenGL 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;


import android.content.Context;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {

    private GestureDetector gestureDetector;
    private final Context context;

    public GLESView(Context drawingContext) {
        super(drawingContext);

        context = drawingContext;

        setEGLContextClientVersion(3); // openGL 3.2 support

        setRenderer(this); // all rendering code will be part of current object

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // WM_PAINT or Invalidate Rect

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //initialize() all here.

        //OpenGL-ES version check
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("SMP:OpenGL Version: "+version);

        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("SMP:GLSL Version:"+glslVersion);        

        initialize(gl);

    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        //resize() all here.

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {

        //draw() / display()
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //not important but still useful in android based event application
        int eventAction = event.getAction();

        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return (true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY) {
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public void onShowPress(MotionEvent e) {
                
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { 
       // uninitialize();
        System.exit(0);

        return(true);        
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return (true);
    }


    private void initialize(GL10 gl) {

        //draw background frame color
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    private void resize(int width, int height) {
        //Geometry changes on viewport
        GLES32.glViewport(0, 0, width, height);
    }

    public void draw() {

        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
    }


}