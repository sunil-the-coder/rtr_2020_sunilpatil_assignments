package com.spatil.ortho;

public interface Constants {
	int SMP_ATTRIBUTE_POSITION = 0;
	int SMP_ATTRIBUTE_COLOR = 1;
	int SMP_ATTRIBUTE_NORMAL = 2;
	int SMP_ATTRIBUTE_TEXTURE = 3;
}