package com.spatil.perspective;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // For OpenGL ES 3.2
import javax.microedition.khronos.opengles.GL10; // for OpenGL 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.Matrix; //matrix math

import android.content.Context;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import static com.spatil.perspective.Constants.*;

enum ShaderAction { 
    COMPILE, 
    LINK 
}

enum ShaderName { 
    VERTEX, 
    FRAGMENT, 
    SHADER_PROGRAM 
}

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {

    private GestureDetector gestureDetector;
    private final Context context;

    private int vertextShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;
    private int mvpMatrixUniform;

    private int[] vao = new int[1];
    private int[] vboPosition = new int[1];

    private float[] perspectiveProjectionMatrix = new float[16];

    public GLESView(Context drawingContext) {
        super(drawingContext);

        context = drawingContext;

        setEGLContextClientVersion(3); // openGL 3.2 support

        setRenderer(this); // all rendering code will be part of current object

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // WM_PAINT or Invalidate Rect

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //initialize() all here.

        //OpenGL-ES version check
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("SMP:OpenGL-ES Version: "+version);

        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("SMP:GLSL Version: "+glslVersion);        

        initialize(gl);

    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        //resize() all here.

        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {

        //draw() / display()
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //not important but still useful in android based event application
        int eventAction = event.getAction();

        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return (true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY) {
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public void onShowPress(MotionEvent e) {
                
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { 
       // uninitialize();
        System.exit(0);

        return(true);        
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return (true);
    }


    private void initialize(GL10 gl) {

        //1. Create vertex shader object.
        vertextShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        //2. Provide source code to shader
        final String vertexShaderSourceCode = String.format
            (
                "#version 320 es"+
                "\n"+
                "in vec4 vPosition;"+
                "uniform mat4 uMvpMatrix;"+
                "void main(void)"+
                "{"+
                "gl_Position = uMvpMatrix * vPosition;"+
                "}"
            );

        GLES32.glShaderSource(vertextShaderObject, vertexShaderSourceCode);
         
        System.out.println("SMP:Compiling Vertex shader...\n");     

         //3. compile shader
         GLES32.glCompileShader(vertextShaderObject);

        //Check for compilation errors if any.
        checkCompileErrorIfAny(vertextShaderObject, ShaderName.VERTEX, ShaderAction.COMPILE);

         //1. Create fragment shader object.
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        //2. provide source to shader
        final String fragmentShaderSourceCode = String.format
            (
                "#version 320 es"+
                "\n"+
                "precision highp float;"+
                "out vec4 fragColor;"+
                "void main(void)"+
                "{"+
                "fragColor = vec4(1.0, 1.0, 1.0, 1.0);"+
                "}"
            );

        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);  

        System.out.println("SMP:Compiling Fragment shader...\n");    

        //3. compile shader
         GLES32.glCompileShader(fragmentShaderObject);

        //Check for compilation errors if any.
        checkCompileErrorIfAny(fragmentShaderObject, ShaderName.FRAGMENT, ShaderAction.COMPILE);
 

        // SHADER LINKER PROGRAMME //
        //1. Create the shader programme
        shaderProgramObject =  GLES32.glCreateProgram();

        //2. Attach all shaders to shader program for linking
        GLES32.glAttachShader(shaderProgramObject, vertextShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        System.out.println("SMP:Linking shaders with shader programme ...\n");

        //Pre-link binding of shader program object with vertex shader position attribute for 
        //taking input 
        GLES32.glBindAttribLocation(shaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");

        //3. Link all shaders to create final executable
        GLES32.glLinkProgram(shaderProgramObject);

        //Linker error checking
        checkCompileErrorIfAny(shaderProgramObject, ShaderName.SHADER_PROGRAM, ShaderAction.LINK);

        //Get MVP uniform location from driver to be used in display to push the final metrices
        mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "uMvpMatrix");

        // Define all your vertices, color, shader attributes, vbo, vao initilization
        // in sequential array 
        final float triangleVertices[] = new float[] {
             0.0f, 1.0f, 0.0f, //apex
            -1.0f, -1.0f, 0.0f, //left bottom
            1.0f, -1.0f, 0.0f //right bottom
        };

        GLES32.glGenVertexArrays(1, vao, 0); 
        GLES32.glBindVertexArray(vao[0]); 

        GLES32.glGenBuffers(1, vboPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]); 

        //Allocate floatBuffer for native cpu not for jvm.
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(triangleVertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);
        GLES32.glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3,  GLES32.GL_FLOAT,  false, 0, 0);
        GLES32.glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindVertexArray(0); //Record stop button.


        //Depth function
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        GLES32.glEnable(GLES32.GL_CULL_FACE);

        System.out.println("SMP:Setting window backgroud color.\n");

        //draw background frame color
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

       Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

     void checkCompileErrorIfAny(int shader, ShaderName shaderName, ShaderAction action) {

        int[] iShaderStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        if(action == ShaderAction.COMPILE)
            GLES32.glGetShaderiv(shader, GLES32.GL_COMPILE_STATUS, iShaderStatus, 0);
        else if(action == ShaderAction.LINK)
            GLES32.glGetProgramiv(shader, GLES32.GL_LINK_STATUS, iShaderStatus, 0);

        if(iShaderStatus[0] == GLES32.GL_FALSE) {
            GLES32.glGetShaderiv(shader, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0) {                        
                szInfoLog = GLES32.glGetShaderInfoLog(shader);

                if(shaderName == ShaderName.VERTEX) {
                    System.out.println("SMP:Vertex Shader Compilation Log:"+ szInfoLog +"\n");
                }else if(shaderName == ShaderName.FRAGMENT) {
                   System.out.println("SMP:Fragment Shader Compilation Log:"+ szInfoLog +"\n");
                }else if(shaderName == ShaderName.SHADER_PROGRAM) {
                    System.out.println("SMP:Shader Program Link Log:"+ szInfoLog +"\n");
                }        
                uninitialize();
                System.exit(0);
            }
        }
    }

    private void resize(int width, int height) {

        if(height == 0)
            height = 1;

        //Geometry changes on viewport
        GLES32.glViewport(0, 0, width, height);

       Matrix.perspectiveM(
        perspectiveProjectionMatrix,
        0,45.0f,
        ((float)width / (float)height),
        0.1f,
        100.0f)    
        
     }

    public void draw() {

        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] translationMatrix = new float[16];

        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -3.0f);

        modelViewMatrix = translationMatrix;
        
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        
        GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix, 0); 

        GLES32.glBindVertexArray(vao[0]); 
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        
        requestRender();
    }

    void uninitialize() {

        if(vao[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }

        if(vboPosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboPosition, 0);
            vboPosition[0] = 0;
        }

        if(shaderProgramObject != 0) {

            if(vertextShaderObject != 0) {
                //detach vertex shader
                GLES32.glDetachShader(shaderProgramObject, vertextShaderObject);
                GLES32.glDeleteShader(vertextShaderObject);
                vertextShaderObject = 0;
            }

            if(fragmentShaderObject != 0) {
                //detach vertex shader
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        
        }

       

    }
}

/*
 $ gradlew.bat clean
 $ gradlew.bat build
 $ adb -d install -r app\build\outputs\apk\debug\app-debug.apk

  To watch logs
    
    $

  $ 
*/