package com.spatil.mob_events;

import androidx.appcompat.widget.AppCompatTextView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;

import android.graphics.Color;
import android.view.Gravity;

public class MyTextView extends AppCompatTextView implements OnGestureListener, OnDoubleTapListener {

    private GestureDetector gestureDetector;

    public MyTextView(Context context) {
        super(context);

        setText("Hello World !!!");
        setTextSize(64);
        setTextColor(Color.rgb(0, 255, 0));
        setGravity(Gravity.CENTER);
        setBackgroundColor(Color.rgb(0,0,0));

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //not important but still useful in android based event application
        int eventAction = event.getAction();

        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return (true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        
        setText("Double Tap");
       
        return (true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        
        return (true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        
        setText("Single Tap");
        
        return (true);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        
        return (true);
    }

    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2, float velocityX, float velocityY) {
        
        return (true);
    }

    @Override
    public void onLongPress(MotionEvent e) {
        
        setText("Long Press");
    }

    @Override
    public void onShowPress(MotionEvent e) {
                
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        setText("Scrolling");
        return(true);
        // System.exit(0);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return (true);
    }


}