nvcc -c -o signwave.obj signwave.cu

cl /c /EHsc /I "C:\glew-2.1.0\include" /I "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\include" Application.cpp

link Application.obj Application.res signwave.obj user32.lib gdi32.lib /LIBPATH:"C:\glew-2.1.0\lib\Release\x64"  /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\lib\x64"
