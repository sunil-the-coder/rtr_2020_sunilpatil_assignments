__kernel void signwave_kernel(__global float4* pos, unsigned int mesh_width, unsigned int mesh_height, float animationTime)
{
	unsigned int x = get_global_id(0);
	unsigned int y = get_global_id(1);
	
	float u = x / (float) mesh_width;
	float v = y / (float) mesh_height;
	u = (u * 2.0f) - 1.0f;
	v = (v * 2.0f) - 1.0f;
	
	float freq = 4.0f;	
	float w = sin(u * freq + animationTime) *  cos(v * freq + animationTime) * 0.5f;
	
	pos[y * mesh_width + x] = (float4)(u, w, v, 1.0f);
	
}
