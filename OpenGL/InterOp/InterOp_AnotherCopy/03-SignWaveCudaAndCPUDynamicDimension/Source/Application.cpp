#include<windows.h>
#include<stdio.h>
#include<GL\glew.h>
#include<gl\gl.h>

#include<cuda_gl_interop.h>
#include<cuda_runtime.h>
#include "signwave.cu.h"



//For GLSL extentions. Must be  included before gl.h and glu.h

#include "vmath.h"
#include "Application.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "cudart.lib")

using namespace vmath;

enum {
	SMP_ATTRIBUTE_POSITION,
	SMP_ATTRIBUTE_COLOR,
	SMP_ATTRIBUTE_NORMAL,
	SMP_ATTRIBUTE_TEXTURE,
};


//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint mvpMatrixUniform;
GLuint colorUniform;
GLuint vao;
GLuint VBO_POSITION;
GLuint VBO_COLOR;

UINT pressedButton = 0;


mat4 perspectiveProjectionMatrix;  // typedef of float[16]

enum SHADER_ACTION { COMPILE = 0, LINK };
enum SHADER_NAME { VERTEX = 0, FRAGMENT, SHADER_PROGRAM };


//------ Sign Wave ---
unsigned int mesh_width = 128;
unsigned int mesh_height = 128;

//float pos[mesh_width][mesh_height][4];
float animationTime = 0.0f;

float *pos;

//---- cuda changes
cudaError_t result;
struct cudaGraphicsResource *cuda_graphics_resource = NULL;
GLuint VBO_GPU;
bool bOnGpu = false;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:Sign Wave");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Sign Wave"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);
	
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	void UpdateArray();

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case 0x43:	// C
		case 0x63:	// c
			bOnGpu = false;
			break;		
		case 0x48:	// H
		case 0x68:	// H
			bOnGpu = true;
			break;	
		case 'i':	// i
		case 'I':	// I
			mesh_width *= 2;
			mesh_height *= 2;
			fprintf(gpFile, "Increase: width:%d, height:%d\n", mesh_width, mesh_height);
			UpdateArray();
			break;			
		case 'd':	// d
		case 'D':	// D
			mesh_width /= 2;
			mesh_height /= 2;
			if(mesh_width < 128) {
				mesh_width = 128;	
				mesh_height = 128;					
			}			
			fprintf(gpFile, "Decrease: width:%d, height:%d\n", mesh_width, mesh_height);
			UpdateArray();
			break;			
		case 'r':	
		case 'R':
			pressedButton = 1;		
			fprintf(gpFile, "RED\n");
			break;	
		case 'g':	
		case 'G':
			pressedButton = 2;			
			fprintf(gpFile, "GREEN\n");
			break;	
		case 'b':	
		case 'B':
			pressedButton = 3;		
			fprintf(gpFile, "BLUE\n");			
			break;	
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void checkCompileErrorIfAny(GLuint, SHADER_NAME, SHADER_ACTION);
	void UpdateArray();
	
	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;


	//code
	ghdc = GetDC(ghwnd);
	
	
	//cuda set gpu device
	int devCount = 0;
	cudaError_t ret_cuda;
	ret_cuda = cudaGetDeviceCount(&devCount);
	if(ret_cuda != cudaSuccess) {
		fprintf(gpFile, "cudaDeviceCount() failed due to %s. Exiting now..\n",
				cudaGetErrorString(ret_cuda));
		DestroyWindow(ghwnd);	
		
	}else if(devCount == 0)
	{
		fprintf(gpFile, "Device does not support cuda. Exiting Now..\n",
				cudaGetErrorString(ret_cuda));
		DestroyWindow(ghwnd);		
	}else {
		
		cudaSetDevice(0);
	}
	

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "Loading glew...\n");
	//Load openGL Extensions using glew before using any OpenGL function.
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK) {
		fprintf(gpFile, "glewInit() is failed.\n");
		DestroyWindow(ghwnd);	
	}

	fprintf(gpFile, "Loaded glew.\n");


	//OpenGL related log
	fprintf(gpFile, "OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for(int i = 0; i < numExtensions; i++) {		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	fprintf(gpFile, "\n-------------------------------------------------- \n");

	// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	const GLchar *vertexShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 vPosition;" \
				"uniform mat4 uMvpMatrix;" \
				"void main(void)" \
				"{" \
				"gl_Position = uMvpMatrix * vPosition;" \
				"}";

	fprintf(gpFile, "Providing shader source...\n");
	glShaderSource(gVertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling Vertex shader...\n");
	
	//3. compile shader
	glCompileShader(gVertexShaderObject);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject, VERTEX, COMPILE);

	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//2. Provide source code to shader.
	const GLchar *fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"out vec4 fragColor;" \
				"uniform vec4 u_color;" \
				"void main(void)" \
				"{" \
				"fragColor = u_color;" \
				"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling fragment shader...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject);
	checkCompileErrorIfAny(gFragmentShaderObject, FRAGMENT, COMPILE);

	
	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	fprintf(gpFile, "Linking shaders with shader programme ...\n");

	//Pre-link binding of shader program object with vertex shader position attribute for 
	//taking input 

	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");

	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject, SHADER_PROGRAM, LINK);
	fflush(gpFile);


	//Get MVP uniform location from driver to be used in display to push the final metrices
	mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject, "uMvpMatrix");
	colorUniform = glGetUniformLocation(gShaderProgramObject, "u_color");

	// Define all your vertices, color, shader attributes, vbo, vao initilization
	// in sequential array 

	
	//Initialize default positions.
	UpdateArray();

	//Passing your actual vertices etc. to openGL pipeline with buffers.
	glGenVertexArrays(1, &vao); //Started new caset - ( Navin rule )
	glBindVertexArray(vao); // Record button start - Pudhache sagle stmt ya sathich


	// ---------------------------------------------RECORD THIS STEPS for REUSE IN DISPLAY ------------------------------------------------------------------------------------//
	
	glGenBuffers(1, &VBO_POSITION); // return the names symbol represent the internal buffer created by openGL in VRAM
	glBindBuffer(GL_ARRAY_BUFFER, VBO_POSITION); 
	glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	//glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	//glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	/*glGenBuffers(1, &VBO_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_COLOR);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);*/
	
	
	//CUDA VBO binding
	glGenBuffers(1, &VBO_GPU); 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_GPU); 
	glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	
	//OpenGL Cuda Buffer Registration ( InterOp )
	result = cudaGraphicsGLRegisterBuffer(&cuda_graphics_resource, VBO_GPU, 
											cudaGraphicsMapFlagsWriteDiscard);
	if(result != cudaSuccess) {
		fprintf(gpFile, "cudaGraphicsGLRegisterBuffer() failed due to %s. Exiting now..\n",
				cudaGetErrorString(result));
		DestroyWindow(ghwnd);	
	}
	
	
	glBindVertexArray(0); //Record stop button.

	
	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Set the ortho project matrix as identity matrix.
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void UpdateArray() {
	
	if(pos != NULL) {
		free(pos);
		pos = NULL;
	}
	
	pos = (float *)malloc(sizeof(float) * (mesh_width * mesh_height * 4));
	
	for(int i = 0; i < mesh_width; i++)
	{
		for(int j = 0; j < mesh_height; j++)
		{
			for(int k = 0; k < 4; k++)
			{
				//index_SMP = (i_SMP * NUM_COLUMNS_SMP * DEPTH_SMP) + (j_SMP * DEPTH_SMP) + k_SMP;
				int index = (i * mesh_height * 4) + (j * 4) + k;
				pos[index] = 0.0f;
			}
		}
	}
	
	
	
//	if(VBO_GPU && bOnGpu == true)
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO_GPU); 
		glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * 4 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		result = cudaGraphicsGLRegisterBuffer(&cuda_graphics_resource, VBO_GPU, cudaGraphicsMapFlagsWriteDiscard);		
	}
	
}

void checkCompileErrorIfAny(GLuint shader, SHADER_NAME shaderName, SHADER_ACTION action) {

	GLint iShaderStatus;
	GLint iInfoLogLength;
	char *szInfoLog = NULL;

	if(action == COMPILE)
		glGetShaderiv(shader, GL_COMPILE_STATUS, &iShaderStatus);
	else if(action == LINK)
		glGetProgramiv(shader, GL_LINK_STATUS, &iShaderStatus);

	if(iShaderStatus == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL) {
				GLsizei actualWritten;
				glGetShaderInfoLog(shader, iInfoLogLength, &actualWritten, szInfoLog);

				if(shaderName == VERTEX) {
					fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == FRAGMENT) {
					fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == SHADER_PROGRAM) {
					fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				}
			
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}


void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle );

}

void Display()
{
	//code
	
	//function declaration
	void launchCpuKernel(const unsigned int mesh_width, const unsigned int mesh_height, float animationTime);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Start using openGL program object.
	glUseProgram(gShaderProgramObject); //Starter of renderer in programmable pipeline
	
	if(pressedButton == 1) {		
		glUniform4f(colorUniform, 1.0f, 0.0f, 0.0f, 1.0f);		
	}else if(pressedButton == 2) {
		glUniform4f(colorUniform, 0.0f, 1.0f, 0.0f, 1.0f);
	}else if(pressedButton == 3) {
		glUniform4f(colorUniform, 0.0f, 0.0f, 1.0f, 1.0f);
	}else {
		glUniform4f(colorUniform, 1.0f, 0.5f, 0.0f, 1.0f);
	}
	
	mat4 modelViewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	mat4 modelViewProjectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	mat4 translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewMatrix = translationMatrix;


	//Mutiply the modelview and orthographic matrix to get the modelviewprojection matrix.
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // Order is important

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);  // 4 - 4*4 matrix ( mat4 - its array) ( Premultiplied matrix.)

	
	//Bind VAO
	glBindVertexArray(vao); // glBegin()  in FFP ( Caset start )
	
	if(bOnGpu == true) {
		//1. Map resource ( false -> no stream for synchronization )
		result = cudaGraphicsMapResources(1, &cuda_graphics_resource, false);
		if(result != cudaSuccess) {
			fprintf(gpFile, "cudaGraphicsMapResources() failed due to %s. Exiting now..\n",
					cudaGetErrorString(result));
			DestroyWindow(ghwnd);
		}
		
		//2. Get the pointer	
		float4 *p_pos = NULL;
		size_t numBytes;
		
		result = cudaGraphicsResourceGetMappedPointer((void**)&p_pos, &numBytes, cuda_graphics_resource);
		if(result != cudaSuccess) {
			fprintf(gpFile, "cudaGraphicsResourceGetMappedPointer() failed due to %s. Exiting now..\n",
					cudaGetErrorString(result));
			DestroyWindow(ghwnd);
		}
		
		//3. Launch cuda kernel
		launchCudaKernel(p_pos, mesh_width, mesh_height, animationTime);
		
		//4. Get cuda processed p_pos which is mapped to gpuVbo representing cudaGraphicsResource
		cudaGraphicsUnmapResources(1, &cuda_graphics_resource, false);
		
		//5. You have all position now in gpuVbo
		glBindBuffer(GL_ARRAY_BUFFER, VBO_GPU); 
		
		//Non need of buffer data as data is already in vbo_gpu.
		//glBufferData(GL_ARRAY_BUFFER, ARRAY_SIZE * 4, p_pos, GL_DYNAMIC_DRAW);
		
	}else {
		
		launchCpuKernel(mesh_width, mesh_height, animationTime);
		glBindBuffer(GL_ARRAY_BUFFER, VBO_POSITION); 
		glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * 4 * sizeof(float), pos, GL_DYNAMIC_DRAW);
	}
		
	
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0); //

	// Draw now either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_POINTS, 0, mesh_width * mesh_height); 

	//Unbind vao
	glBindVertexArray(0);  //Caset stop

	animationTime += 0.01f;

	//Stop using Open program object.
	glUseProgram(0);
	
	SwapBuffers(ghdc);
}


void launchCpuKernel(const unsigned int mesh_width,const unsigned int mesh_height, float animationTime)
{
	for(int i = 0; i < mesh_width; i++)
	{
		for(int j = 0; j < mesh_height; j++)
		{
			for(int k = 0; k < 4; k++)
			{
				float u = i / (float) mesh_width;
				float v = j / (float) mesh_height;
				u = (u * 2.0f) - 1.0f;
				v = (v * 2.0f) - 1.0f;
				
				float freq = 4.0f;
				
				float w = sinf(u * freq + animationTime) *  cosf(v * freq + animationTime) * 0.5f;
				
				int index = (i * mesh_height * 4) + (j * 4) + k;
				
				if(k == 0)
					pos[index] = u;
				if(k == 1)
					pos[index] = w;
				if(k == 2)
					pos[index] = v;
				if(k == 3)
					pos[index] = 1.0f;
			}
		}
	}
}

void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	//Destroy VAO
	if(vao) {
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if(cuda_graphics_resource) {
		cudaGraphicsUnregisterResource(cuda_graphics_resource);
		cuda_graphics_resource = NULL;
	}
	
	//Destroy vbo
	if(VBO_POSITION) {
		glDeleteBuffers(1, &VBO_POSITION);
		VBO_POSITION = 0;
	}

	if(VBO_GPU) {
		glDeleteBuffers(1, &VBO_GPU);
		VBO_GPU = 0;
	}
	
	if(pos != NULL) {
		free(pos);
		pos = NULL;
	}
	
	//Safe release of shaders and shader program.
	if(gShaderProgramObject) {

		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		fprintf(gpFile, "Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			fprintf(gpFile, "Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		//unlink the shader program.
		glUseProgram(0);
	}

	
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}


/* 

// Please run on Visual Studio x64  native command prompt with admin access.

$ nvcc -c -o signwave.obj signwave.cu

$ cl /c /EHsc /I "C:\glew-2.1.0\include" /I "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\include" Application.cpp

$ link Application.obj Application.res signwave.obj user32.lib gdi32.lib /LIBPATH:"C:\glew-2.1.0\lib\Release\x64"  /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.0\lib\x64"

*/