__global__ void signwave_kernel(float4* pos, unsigned int mesh_width, unsigned int mesh_height, float animationTime)
{
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
	
	float u = x / (float) mesh_width;
	float v = y / (float) mesh_height;
	u = (u * 2.0f) - 1.0f;
	v = (v * 2.0f) - 1.0f;
	
	float freq = 4.0f;	
	float w = sinf(u * freq + animationTime) *  cosf(v * freq + animationTime) * 0.5f;
	
	pos[y * mesh_width + x] = make_float4(u, w, v, 1.0f);
	
}


void launchCudaKernel(float4 *pos, const unsigned int mesh_width,const unsigned int mesh_height, float animationTime) {

	dim3 block = dim3(8, 8, 1);
	dim3 grid = dim3((mesh_width / block.x), (mesh_height / block.y), 1);
	
	signwave_kernel<<<grid,block>>>(pos, mesh_width, mesh_height, animationTime);
}