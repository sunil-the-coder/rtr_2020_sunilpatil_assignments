#include<windows.h>
#include<stdio.h>

//For GLSL extentions. Must be  included before gl.h and glu.h
#include<GL\glew.h>

#include<gl\gl.h>
#include "vmath.h"
#include "Application.h"
#include "Sphere.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")

using namespace vmath;

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

bool gbLight = false;

enum {
	SMP_ATTRIBUTE_POSITION = 0,
	SMP_ATTRIBUTE_COLOR,
	SMP_ATTRIBUTE_NORMAL,
	SMP_ATTRIBUTE_TEXTURE,
};


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVertexShaderObject_skybox;
GLuint gFragmentShaderObject_skybox;
GLuint gShaderProgramObject_skybox;

GLuint vao;
GLuint VBO_POSITION;
GLuint VBO_NORMAL;
GLuint VBO_ELEMENT;

GLuint vao_skybox;

//Uniform declarations
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint viewMatrixUniform_skybox;
GLuint perspectiveProjectionUniform;

GLuint laUniform; //light Ambient uniform
GLuint ldUniform; //light Diffuse uniform
GLuint lsUniform; //light Specular uniform
GLuint lightPositionUniform; //material Diffuse

GLuint kaUniform; //material Ambient
GLuint kdUniform; //material Diffuse
GLuint ksUniform; //material Specular
GLuint materialShininessUniform; 

GLuint lKeyPressedUniform;

mat4 perspectiveProjectionMatrix;  // typedef of float[16]

// GLfloat lightAmbient[] = { 0.1f, 0.1f, 0.1f, 1.0f }; // new ambient light ( black) 
// GLfloat lightDifuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; //white light
// GLfloat lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f}; //light coming from +ve z axis ( stage chya baherun)
// GLfloat lightSpecular[] = {0.0f, 1.0f, 0.0f, 1.0f};

// GLfloat materialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
// GLfloat materialDifuse[] = {0.5f, 0.2f, 0.7f, 1.0f}; // actual object color ( jambla )
// GLfloat materialSpecular[] = {0.7f, 0.7f, 0.7f, 1.0f}; //  reflected bounce back
// GLfloat materialShininess = 128.0f; 

GLfloat lightAmbient[] = { 0.1f, 0.1f, 0.1f, 1.0f }; // new ambient light ( black) 
GLfloat lightDifuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; //white light
GLfloat lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f}; //light coming from +ve z axis ( stage chya baherun)
GLfloat lightSpecular[] = {0.0f, 1.0f, 0.0f, 1.0f};

GLfloat materialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDifuse[] = {1.0f, 0.0f, 0.0f, 1.0f}; // actual object color ( jambla )
GLfloat materialSpecular[] = {0.7f, 0.7f, 0.7f, 1.0f}; //  reflected bounce back
GLfloat materialShininess = 128.0f; 

enum SHADER_ACTION { COMPILE = 0, LINK };
enum SHADER_NAME { VERTEX = 0, FRAGMENT, SHADER_PROGRAM };

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumElements;
GLuint gNumVertices;


 GLuint          tex_envmap;
 GLuint          envmaps[3];
 int             envmap_index;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	void Initialize(void);
	void Display(void);


	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:Pre Fragment Lighting");
	int x, y;
	RECT rect;
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "File Created Successfully.\n");
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Per Fragment Lighting"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}


	//fclose(gpFile);

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void UnInitialize(void);
	
	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "Window Created.\n");
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
			case 'l':
			case 'L': 
				if(gbLight == false) {
					glEnable(GL_LIGHTING);
					gbLight = true;
				} else {
					glDisable(GL_LIGHTING);
					gbLight = false;
				}
			break;	
			case 'E':			 
                    envmap_index = (envmap_index + 1) % 3;
                    tex_envmap = envmaps[envmap_index];	
                    break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

void Initialize(void)
{
	//function declaration
	void ReSize(int, int);
	void checkCompileErrorIfAny(GLuint, SHADER_NAME, SHADER_ACTION);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	//pfd.cDepthBits = 32; // For future in 3D

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) 
	{
		fprintf(gpFile, "ChoosePixelFormat() failed.\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() is failed.\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() is failed.\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() is failed.\n");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "Loading glew...\n");
	//Load openGL Extensions using glew before using any OpenGL function.
	GLenum glewError = glewInit();
	if(glewError != GLEW_OK) {
		fprintf(gpFile, "glewInit() is failed.\n");
		DestroyWindow(ghwnd);	
	}

	fprintf(gpFile, "Loaded glew.\n");


	//OpenGL related log
	fprintf(gpFile, "OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "OpenGL GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL enabled extensions
	GLint numExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for(int i = 0; i < numExtensions; i++) {		
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	fprintf(gpFile, "\n-------------------------------------------------- \n");

	// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	const GLchar *vertexShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec4 vPosition;" \
				"in vec3 vNormal;" \
				"uniform mat4 u_modelMatrix;" \
				"uniform mat4 u_viewMatrix;" \
				"uniform mat4 u_projectionMatrix;" \
				"uniform int u_lKeyPressed;" \
				"uniform vec4 u_lightPosition;" \
				"out vec3 transformedNormal;" \
				"out vec3 lightDirection;" \
				"out vec3 viewVector;" \
				"void main(void)" \
				"{" \
				"if(u_lKeyPressed == 1) {" \
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
				"transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal;" \
				"lightDirection = vec3(u_lightPosition - eyeCoordinates);" \
				"viewVector = -eyeCoordinates.xyz;" \
				"}" \
				"gl_Position = u_projectionMatrix * u_modelMatrix * u_viewMatrix * vPosition;" \
				"}";

	fprintf(gpFile, "Providing shader source...\n");
	glShaderSource(gVertexShaderObject, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling Vertex shader...\n");
	
	//3. compile shader
	glCompileShader(gVertexShaderObject);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject, VERTEX, COMPILE);
	
	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//2. Provide source code to shader.
	const GLchar *fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"in vec3 transformedNormal;" \
				"in vec3 lightDirection;" \
				"in vec3 viewVector;" \
				"uniform vec3 u_la;" \
				"uniform vec3 u_ld;" \
				"uniform vec3 u_ls;" \
				"uniform vec3 u_ka;" \
				"uniform vec3 u_kd;" \
				"uniform vec3 u_ks;" \
				"uniform float u_materialShininess;" \
				"uniform int u_lKeyPressed;" \
				"out vec4 fragColor;" \
				"vec3 phongAdsLight;" \
				"void main(void)" \
				"{" \
				"if(u_lKeyPressed == 1) {" \
				"vec3 ntransformedNormal = normalize(transformedNormal);" \
				"vec3 nlightDirection = normalize(lightDirection);" \
				"vec3 nviewVector = normalize(viewVector);" \
				"vec3 reflectionVector = reflect(-nlightDirection, ntransformedNormal);" \
				"vec3 ambient = u_la * u_ka;" \
				"vec3 diffuse = u_ld * u_kd * max(dot(nlightDirection, ntransformedNormal), 0.0);" \
				"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, nviewVector), 0.0), u_materialShininess);" \
				"phongAdsLight = ambient + diffuse + specular;" \
				"} else {" \
				"phongAdsLight = vec3(1.0, 1.0f, 1.0f);" \
				"}" \
				"fragColor = vec4(phongAdsLight, 1.0);" \
				"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling fragment shader...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject);
	checkCompileErrorIfAny(gFragmentShaderObject, FRAGMENT, COMPILE);
	

	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	fprintf(gpFile, "Linking shaders with shader programme ...\n");

	//Pre-link binding of shader program object with vertex shader position attribute for 
	//taking input 

	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SMP_ATTRIBUTE_NORMAL, "vNormal");

	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject, SHADER_PROGRAM, LINK);
	

	//Get MVP uniform location from driver to be used in display to push the final metrices
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	perspectiveProjectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");


	// Define all your vertices, color, shader attributes, vbo, vao initilization
	// in sequential array 

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//1. Next 2 stmt hi SOY keleli aahe.   Vertex array la name nahiye, so its imaginary aahe.
	glGenVertexArrays(1, &vao); //Started new caset - ( Navin rule )
	glBindVertexArray(vao); // Record button start - Pudhache sagle stmt ya sathich

	//vbo for position
	glGenBuffers(1, &VBO_POSITION); // return the names symbol represent the internal buffer created by openGL in VRAM
	glBindBuffer(GL_ARRAY_BUFFER, VBO_POSITION); 	
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vbo for normals
	glGenBuffers(1, &VBO_NORMAL); // return the names symbol represent the internal buffer created by openGL in VRAM
	glBindBuffer(GL_ARRAY_BUFFER, VBO_NORMAL); 	
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SMP_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SMP_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//vbo for elements
	glGenBuffers(1, &VBO_ELEMENT); // return the names symbol represent the internal buffer created by openGL in VRAM
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO_ELEMENT); 	
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//unbind vao
	glBindVertexArray(0); //Record stop button.



// -------------------------------------------------------------

	// VERTEX SHADER //
	//1. create shader
	gVertexShaderObject_skybox = glCreateShader(GL_VERTEX_SHADER);

	//2. Provide source code to shader
	 vertexShaderSourceCode = 
	 			"#version 450 core" \
				"\n" \
				"uniform mat4 u_viewMatrix;" \
				"out vec3 tc;" \
				"void main(void)" \
				"{" \
				"vec3[4] vertices = vec3[4] (" \
				"vec3(-1.0, -1.0, 1.0)," \
                "vec3( 1.0, -1.0, 1.0)," \
                "vec3(-1.0,  1.0, 1.0)," \
                "vec3( 1.0,  1.0, 1.0)" \
				");" \
				"tc = mat3(u_viewMatrix) * vertices[gl_VertexID];" \
				"gl_Position = vec3(vertices[gl_VertexID], 1.0);" \
				"}";

	fprintf(gpFile, "Providing shader source...\n");
	glShaderSource(gVertexShaderObject_skybox, 1, (const GLchar **) &vertexShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling Vertex shader for skybox...\n");
	
	//3. compile shader
	glCompileShader(gVertexShaderObject_skybox);

	//Check for compilation errors if any.
	checkCompileErrorIfAny(gVertexShaderObject_skybox, VERTEX, COMPILE);
	
	// FRAGMENT SHADER ///
	//2. Create shader
	gFragmentShaderObject_skybox = glCreateShader(GL_FRAGMENT_SHADER);

	//2. Provide source code to shader.
	fragmentShaderSourceCode = 
				"#version 450 core" \
				"\n" \
				"uniform samplerCube tex_cubemap;" \
				"in vec3 tc;" \
				"out vec4 color; " \
				"void main(void)" \
				"{" \
				"color = texture(tex_cubemap, tc);" \
				"}";

	glShaderSource(gFragmentShaderObject_skybox, 1, (const GLchar **) &fragmentShaderSourceCode, NULL);

	fprintf(gpFile, "Compiling fragment shader for skybox...\n");

	//3. Compile shader.
	glCompileShader(gFragmentShaderObject_skybox);
	checkCompileErrorIfAny(gFragmentShaderObject_skybox, FRAGMENT, COMPILE);
	

	// SHADER LINKER PROGRAMME //

	//1. Create the the shader programme
	gShaderProgramObject_skybox = glCreateProgram();

	//2. Attach all shaders to shader program for linking
	glAttachShader(gShaderProgramObject_skybox, gVertexShaderObject);
	glAttachShader(gShaderProgramObject_skybox, gFragmentShaderObject);

	fprintf(gpFile, "Linking shaders with shader programme ...\n");


	//3. Link all shaders to create final executable
	glLinkProgram(gShaderProgramObject_skybox);

	//Linker error checking
	checkCompileErrorIfAny(gShaderProgramObject_skybox, SHADER_PROGRAM, LINK);
	

	//Get MVP uniform location from driver to be used in display to push the final metrices
	viewMatrixUniform_skybox = glGetUniformLocation(gShaderProgramObject_skybox, "u_viewMatrix");


	//1. Next 2 stmt hi SOY keleli aahe.   Vertex array la name nahiye, so its imaginary aahe.
	glGenVertexArrays(1, &vao_skybox); //Started new caset - ( Navin rule )
	glBindVertexArray(vao_skybox); // Record button start - Pudhache sagle stmt ya sathich

	//unbind vao
	glBindVertexArray(0); //Record stop button.

// ------------------------------------------------------------




	//In display, you can now use above caset to play similar steps.

	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	//SetCleareColor
	fprintf(gpFile, "Setting color.\n");

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Set the ortho project matrix as identity matrix.
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
}

void checkCompileErrorIfAny(GLuint shader, SHADER_NAME shaderName, SHADER_ACTION action) {

	GLint iShaderStatus;
	GLint iInfoLogLength;
	char *szInfoLog = NULL;

	if(action == COMPILE)
		glGetShaderiv(shader, GL_COMPILE_STATUS, &iShaderStatus);
	else if(action == LINK)
		glGetProgramiv(shader, GL_LINK_STATUS, &iShaderStatus);

	if(iShaderStatus == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0) {
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL) {
				GLsizei actualWritten;
				glGetShaderInfoLog(shader, iInfoLogLength, &actualWritten, szInfoLog);

				if(shaderName == VERTEX) {
					fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == FRAGMENT) {
					fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				}else if(shaderName == SHADER_PROGRAM) {
					fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				}
				fflush(gpFile);
				free(szInfoLog);
				DestroyWindow(ghwnd);
			}
		}
	}
}


void ReSize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, //fovy ( Angle creation ) for top and bottom
		((GLfloat)width / (GLfloat)height), //aspect ratio () - ratio for left & right
		0.1f, // camera near to eye
		100.0f); // rear ( viewing angle );

}

static double currentTime = 0.0f;

void Display()
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Start using openGL program object.
	glUseProgram(gShaderProgramObject); //Starter of renderer in programmable pipeline

	if(gbLight == true) {
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3fv(laUniform, 1, lightAmbient);
		glUniform3fv(ldUniform, 1, lightDifuse);
		glUniform3fv(lsUniform, 1, lightSpecular);
		glUniform4fv(lightPositionUniform, 1, lightPosition);
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDifuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);
	}else {
		glUniform1i(lKeyPressedUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	mat4 viewMatrix = mat4::identity();  // == glLoadIdentity in display in FFP - Only need model matrix as we are using modelviewduality with translation
	//mat4 projectionMatrix = mat4::identity(); // == resize madhala glLoadIdentity() in FPP
	mat4 translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix;

	// Ithe shader madhala 'uMvpMatrix' prepare hoto with modelViewProjectionMatrix.
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);  // 4 - 4*4 matrix ( mat4 - its array) ( Premultiplied matrix.)
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(perspectiveProjectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix);


	//Bind VAO
	glBindVertexArray(vao); // glBegin()  in FFP ( Caset start )

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO_ELEMENT);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//Unbind vao
	glBindVertexArray(0);  //Caset stop



	glUseProgram(gShaderProgramObject_skybox); //Starter of renderer in programmable pipeline

	 const float t = (float)currentTime * 0.1f;
	viewMatrix = vmath::lookat(vmath::vec3(15.0f * sinf(t), 0.0f, 15.0f * cosf(t)),
                                                vmath::vec3(0.0f, 0.0f, 0.0f),
                                                vmath::vec3(0.0f, 1.0f, 0.0f));
	
	static const GLfloat gray[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    static const GLfloat ones[] = { 1.0f };

    glClearBufferfv(GL_COLOR, 0, gray);
    glClearBufferfv(GL_DEPTH, 0, ones);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex_envmap);

    glUseProgram(gVertexShaderObject_skybox);
    glBindVertexArray(vao_skybox);

    glUniformMatrix4fv(viewMatrixUniform_skybox, 1, GL_FALSE, viewMatrix);

	//Unbind vao
	glBindVertexArray(0);  //Caset stop
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	//Stop using Open program object.
	glUseProgram(0);
	
	SwapBuffers(ghdc);
}


void UnInitialize(void)
{

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	//Destroy VAO
	if(vao) {
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	//Destroy vbo
	if(VBO_POSITION) {
		glDeleteBuffers(1, &VBO_POSITION);
		VBO_POSITION = 0;
	}
	//Destroy vbo
	if(VBO_NORMAL) {
		glDeleteBuffers(1, &VBO_NORMAL);
		VBO_NORMAL = 0;
	}
	//Destroy vbo
	if(VBO_ELEMENT) {
		glDeleteBuffers(1, &VBO_ELEMENT);
		VBO_ELEMENT = 0;
	}

	//Safe release of shaders and shader program.
	if(gShaderProgramObject) {

		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		fprintf(gpFile, "Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			fprintf(gpFile, "Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		//unlink the shader program.
		glUseProgram(0);
	}

	//Safe release of shaders and shader program.
	if(gShaderProgramObject_skybox) {

		glUseProgram(gShaderProgramObject_skybox);

		GLsizei shaderCount = 0;

		glGetProgramiv(gShaderProgramObject_skybox, GL_ATTACHED_SHADERS, &shaderCount);
		fprintf(gpFile, "Total shaders attached to shader program are: %d\n", shaderCount);

		GLuint *pShader = NULL;

		pShader = (GLuint *) malloc( sizeof(GLuint) * shaderCount);
		if(pShader == NULL) {
			fprintf(gpFile, "Can't allocate memory for storing shaders.\n Exiting now...\n");			
		}

		glGetAttachedShaders(gShaderProgramObject_skybox, shaderCount, &shaderCount, pShader);

		for(GLsizei i = 0; i < shaderCount; i++) {
			glDetachShader(gShaderProgramObject_skybox, pShader[i]);
			glDeleteShader(pShader[i]);
		}

		free(pShader);
		pShader = NULL;

		//delete the shader program.
		glDeleteProgram(gShaderProgramObject_skybox);
		gShaderProgramObject_skybox = 0;

		//unlink the shader program.
		glUseProgram(0);
	}

	
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (gpFile)
	{
		fprintf(gpFile, "File closed. \n");
		free(gpFile);
		gpFile = NULL;
	}
}


/* 
Copy Sphere.dll to Windows\SysWoW64

$ cl /c /EHsc /I C:\glew-2.1.0\include Application.cpp

$ link Application.obj Application.res user32.lib gdi32.lib Sphere.lib /LIBPATH:C:\glew-2.1.0\lib\Release\Win32

*/