#include<stdio.h>
#include<d3d11.h> // GL/gl.h
#include<math.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib") // (DirectX Graphics Infrastructure - abstract all low level graphics card API. == wgl  )

int main(void)
{
	//variable declarations
	
	IDXGIFactory *pIDXGIFactory = NULL;
	IDXGIAdapter *pIDXGIAdapter = NULL; // physical device driver(graphics card) -> Adapter, logical representation -> ID311Device

	struct DXGI_ADAPTER_DESC dxgiAdapterDesc; // == PixelFormatDescriptor
	HRESULT hr;
	char str[255];

	
	//code
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void **)&pIDXGIFactory); // internally calls to CoInitialize()  and CoCreateInstance() API of COM
	if(FAILED(hr)) 
	{
		printf("CreateDXGIFactory() failed. \n");
		goto cleanup;
	}

	if(pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		printf("DXGIAdapter can not be found. \n");
		goto cleanup;
	}

	ZeroMemory((void *)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));


	//Get the pixel format descriptor from graphics driver.
	pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	//code page_ascii code page (CharMap page in windows for diff language)
	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	printf("Graphics Card Name = %s \n", str);
	printf("Graphics Card VRAM = %I64d bytes \n", (__int64) dxgiAdapterDesc.DedicatedVideoMemory);
	printf("Graphics Card VRAM in GB = %d GB \n", (int) (ceil(dxgiAdapterDesc.DedicatedVideoMemory/1024.0/1024.0/1024.0)));

	cleanup:
		if(pIDXGIAdapter) {
			pIDXGIAdapter->Release();			
			pIDXGIAdapter = NULL;
		}

		if(pIDXGIFactory) {
			pIDXGIFactory->Release();			
			pIDXGIFactory = NULL;
		}

	return (0);
}


