#include<windows.h>
#include<stdio.h>
#include<math.h>
#include<d3d11.h>


#include "Application.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
bool gbFullScreen = false;
bool gbActiveWindow = false;

HWND ghwnd;
FILE* gpFile = NULL;
char gszLogFileName[] = "Log.txt";

DWORD dwStyle;
WINDOWPLACEMENT wpPrev;

HDC ghdc = NULL;
HGLRC ghrc = NULL;


//D3D11 variables
float gClearColor[4]; //Red, Green, Blue, Alpha
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local function declarations.
	HRESULT Initialize(void);
	void Display(void);
	void UnInitialize(void);

	//local variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("SMP:Direct3D Bluescreen");
	int x, y;
	RECT rect;
	bool bDone = false;
	HRESULT hr;

	if (fopen_s(&gpFile, gszLogFileName, "w") != 0) {
		MessageBox(NULL, TEXT("Log file creation failed."), TEXT("Error: File Creation"), MB_OK | MB_ICONERROR);		
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "File Created Successfully.\n");
		fclose(gpFile);
	}

	//initialize wndclass
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	x = (rect.right / 2) - 400;
	y = (rect.bottom / 2) - 300;

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMP: Direct3D Bluescreen"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;


	//Initialize 3D
	hr = Initialize();
	if(FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() failed. Exiting Now.\n");
		fclose(gpFile);
		UnInitialize();		

	}else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Success. \n");
		fclose(gpFile);
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//Even if no message, stil continue displaying graphics.
			if(gbActiveWindow == true)
				Display();
		}
	}

	UnInitialize();

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//local function declarations
	void ToggleFullScreen(void);
	HRESULT ReSize(int, int);
	void UnInitialize(void);
	HRESULT hr;
	
	switch (iMsg)
	{
	case WM_CREATE:
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Window Created.\n");
		fclose(gpFile);
		break;
	case WM_SETFOCUS: 
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: 
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		if(gpID3D11DeviceContext) {
			hr = ReSize(LOWORD(lParam), HIWORD(lParam));	
			if(FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "ReSize() failed. Exiting Now.\n");
				fclose(gpFile);
				UnInitialize();	
			}
		}		
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//Normal to fullscreen window
	if (gbFullScreen == false) 
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			//Get the current size & full monitor size.
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				//remove the overlappend window
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				//set new window placement.

				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

		ShowCursor(true);

		gbFullScreen = false;
	}
	
}

HRESULT Initialize(void)
{
	//function declaration
	HRESULT ReSize(int, int);
	HRESULT printD3DInfo();

	//variable declarations
	HRESULT hr;
	UINT numFeatureLevels = 1; // You can ask multiple.. depends on d3dFeatureLevel_Required
	UINT numDriverTypes = 0;
	UINT createDeviceFlags = 0;

	D3D_DRIVER_TYPE d3dDriverType; // To hold actual returned driver from list of driver
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_Required = D3D_FEATURE_LEVEL_11_0; // This is what i want to have
	D3D_FEATURE_LEVEL d3dFeatureLevel_Acquired = D3D_FEATURE_LEVEL_10_0; // Lowest default possible
	
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); //Calculate dynamic from array size


	//1. Log graphics card information
	hr = printD3DInfo();

	//2.  Prepare swapchain descriptor structure. ( pixelformatdescriptor)
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1; //D3d default provide 1 buffer
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0; // MSAA ( Multi Sampling Anti Aliasing) - 0 ( Decision by OS)
	dxgiSwapChainDesc.Windowed = TRUE; // basic window screen ( FALSE - fullscreen)

	//3. Retrieve the 3 interfaces with D3D11CreateDeviceAndSwapChain

	for(UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,			// Hardware Adapter ( Choose best yourself)
			d3dDriverType,	// Selected driver type
			NULL,			// If custom software rasterizer then it's return handle
			createDeviceFlags,	// Flags
			&d3dFeatureLevel_Required, // Required feature level
			numFeatureLevels, // Number of feature levels ( size of featureLevel if array)
			D3D11_SDK_VERSION, // SDK version
			&dxgiSwapChainDesc, // Swap chain descriptor
			&gpIDXGISwapChain, // Swap chain
			&gpID3D11Device, //Device
			&d3dFeatureLevel_Acquired, // Aquired feature level
			&gpID3D11DeviceContext //Device Context
			);

		if(SUCCEEDED(hr))
			break;
	}

	if(FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain failed. Exiting Now.\n");
		fclose(gpFile);
		return (hr);

	}else {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain Successed.\n");
		fprintf_s(gpFile, "Choosen Driver Type: ");
		if(d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type.\n");

		}else if(d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type.\n");

		}else if(d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type.\n");

		}else {

			fprintf_s(gpFile, "Unknown Type.\n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level is: ");

		if(d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_11_0){
			fprintf_s(gpFile, "11.0\n");			
		}else if(d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_10_1){
			fprintf_s(gpFile, "10.1\n");			
		}else if(d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_10_0){
			fprintf_s(gpFile, "10.0\n");			
		}
		else {
			fprintf_s(gpFile, "Unknown\n");			
		}

		fclose(gpFile);
	}

	//set clear color
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	//3. Resize framebuffer content
	hr = ReSize(WIN_WIDTH, WIN_HEIGHT); //warm up
	if(FAILED(hr)) {
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ReSize failed. Exiting Now.\n");
		fclose(gpFile);
		return (hr);
	}

	return (S_OK);
}

HRESULT printD3DInfo() {

	//variable declarations
	IDXGIFactory *pIDXGIFactory = NULL;
	IDXGIAdapter *pIDXGIAdapter = NULL; // physical device driver(graphics card) -> Adapter, logical representation -> ID311Device

	struct DXGI_ADAPTER_DESC dxgiAdapterDesc; // == PixelFormatDescriptor
	HRESULT hr;
	char str[255];

	//code
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void **)&pIDXGIFactory); // internally calls to CoInitialize()  and CoCreateInstance() API of COM
	if(FAILED(hr)) 
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateDXGIFactory() failed.\n");
		fclose(gpFile);		
		goto cleanup;
	}

	if(pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "DXGIAdapter can not be found.\n");
		fclose(gpFile);
		goto cleanup;
	}

	ZeroMemory((void *)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	//Get the pixel format descriptor from graphics driver.
	pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	//code page_ascii code page (CharMap page in windows for diff language)
	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	fopen_s(&gpFile, gszLogFileName, "a+");	
	fprintf_s(gpFile, "Graphics Card Name = %s \n", str);
	fprintf_s(gpFile, "Graphics Card VRAM = %I64d bytes \n", (__int64) dxgiAdapterDesc.DedicatedVideoMemory);
	fprintf_s(gpFile, "Graphics Card VRAM in GB = %d GB \n", (int) (ceil(dxgiAdapterDesc.DedicatedVideoMemory/1024.0/1024.0/1024.0)));
	fclose(gpFile);

	cleanup:
		if(pIDXGIAdapter) {
			pIDXGIAdapter->Release();			
			pIDXGIAdapter = NULL;
		}

		if(pIDXGIFactory) {
			pIDXGIFactory->Release();			
			pIDXGIFactory = NULL;
		}

		return hr;
}

HRESULT ReSize(int width, int height)
{
	HRESULT hr = S_OK;

	//free if already size dependent resources
	if(gpID3D11RenderTargetView) {
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//Resize swap chain buffers ( UNORM - Unsinged normalized ) 0 -> best flags use
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//Create Render target view from existing framebuffer
	//1.  Dummmy texture buffer
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pID3D11Texture2D_BackBuffer);

	//2. From dumy to actual render texture
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if(FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "CreateRenderTargetView failed. Exiting Now..\n");
		fclose(gpFile);
		return (hr);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//3. set render target to pipeline ( NULL -> Depth stencil view )
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);


	//4. Set the viewport
	D3D11_VIEWPORT d3dViewPort;
	ZeroMemory((void *)&d3dViewPort, sizeof(D3D11_VIEWPORT));
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float) width;
	d3dViewPort.Height = (float) height;
	d3dViewPort.MinDepth = 0.0f;	
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	return (hr);
	
}

void Display()
{
	//code

	//clear color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);



	//swap buffer ( 1st 0- > monitor refresh rate, 2nd 0 -> Only single buffer render not multiple)
	gpIDXGISwapChain->Present(0, 0);
}


void UnInitialize(void)
{

	if(gpID3D11RenderTargetView){
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if(gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if(gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if(gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Uninitialize() success. \n");
		fprintf_s(gpFile, "File closed. \n");
		fclose(gpFile);
		free(gpFile);
		gpFile = NULL;
	}
}

/*

 $ cl /c Application.cpp
 $ rc Application.res
 $ link Application.obj Application.res user32.lib gdi32.lib
 $ Application.exe
 
*/